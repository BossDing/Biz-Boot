/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50727
Source Host           : localhost:3306
Source Database       : biz-boot

Target Server Type    : MYSQL
Target Server Version : 50727
File Encoding         : 65001

Date: 2019-12-15 22:07:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `fly_admin`
-- ----------------------------
DROP TABLE IF EXISTS `fly_admin`;
CREATE TABLE `fly_admin` (
  `id` bigint(20) NOT NULL COMMENT '用户id编号',
  `user_name` char(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '用户名',
  `email` char(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '邮箱',
  `password` char(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '加密后的密码',
  `nickname` char(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '姓名、昵称',
  `avatar` char(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '头像',
  `phone` char(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '手机号,11位',
  `currency` int(11) DEFAULT '0' COMMENT '资金，可以是积分、金币、等等站内虚拟货币',
  `freezemoney` decimal(11,2) DEFAULT '0.00' COMMENT '账户冻结余额，金钱,RMB，单位：元',
  `lastip` char(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '最后一次登陆的ip',
  `money` decimal(11,2) DEFAULT '0.00' COMMENT '账户可用余额，金钱,RMB，单位：元',
  `idcardauth` tinyint(2) DEFAULT '0' COMMENT '是否已经经过真实身份认证了（身份证、银行卡绑定等）。默认为没有认证。预留字段。1已认证；0未认证',
  `sign` char(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '个人签名',
  `sex` char(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '1男、0女、2未知',
  `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '自我描述',
  `create_time` datetime NOT NULL COMMENT '注册时间,时间戳',
  `last_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `status` tinyint(3) DEFAULT '0' COMMENT '0 可用, 1 禁用, 2 注销',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`,`user_name`,`phone`) USING BTREE,
  KEY `username` (`user_name`,`email`,`phone`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户信息表。系统登陆的用户信息都在此处';

-- ----------------------------
-- Records of fly_admin
-- ----------------------------
INSERT INTO `fly_admin` VALUES ('369555184164286491', 'admin', '', '$2a$10$zTEGRa7oUcKxJ/JUZNaPouHKBVg1gyAsFeR0Igow2lO5hGya6LiW.', '总管理', 'default.png', '17000000002', '0', '0.00', '127.0.0.1', '0.00', '0', null, '1', null, '2019-08-23 00:55:33', '2019-08-26 08:32:42', '0', '0');
INSERT INTO `fly_admin` VALUES ('369555184164286492', 'agency', '', '$2a$10$HGv0RBwVXs9BGHI6EMmtcuG64t9Slv9LRL../wPNb3Gi.rrfuzAsi', '代理', 'default.png', '17000000001', '0', '0.00', '127.0.0.1', '0.00', '0', null, '1', null, '2019-08-23 00:55:35', '2019-08-26 08:32:46', '0', '0');
INSERT INTO `fly_admin` VALUES ('369555184164286493', 'ceshi', '', '$2a$10$HGv0RBwVXs9BGHI6EMmtcuG64t9Slv9LRL../wPNb3Gi.rrfuzAsi', 'ceshi', 'default.png', '', '0', '0.00', '127.0.0.1', '0.00', '0', null, '1', null, '2019-08-23 00:55:37', '2019-08-26 08:32:48', '0', '0');

-- ----------------------------
-- Table structure for `fly_admin_menu`
-- ----------------------------
DROP TABLE IF EXISTS `fly_admin_menu`;
CREATE TABLE `fly_admin_menu` (
  `id` bigint(20) NOT NULL COMMENT '菜单ID',
  `menu_name` varchar(50) NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父菜单ID',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  `url` varchar(200) DEFAULT '#' COMMENT '请求地址',
  `target` varchar(20) DEFAULT '0' COMMENT '打开方式（0页签 1新窗口）',
  `menu_type` char(1) DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` tinyint(1) DEFAULT '0' COMMENT '菜单状态（1显示 0隐藏）',
  `perms` varchar(100) DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) DEFAULT '#' COMMENT '菜单图标',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `create_user_id` bigint(20) DEFAULT '0' COMMENT '创建者ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) DEFAULT '0' COMMENT '更新者id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='菜单权限表';

-- ----------------------------
-- Records of fly_admin_menu
-- ----------------------------
INSERT INTO `fly_admin_menu` VALUES ('398850275361959936', '系统管理', '0', '系统管理目录', '#', '0', 'M', '1', '', 'layui-icon layui-icon-set', '1', '0', '2018-03-16 11:33:00', '399966142094901248', '2019-12-05 14:03:44');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959937', '网站管理', '0', '系统监控目录', '#', '0', 'M', '1', '', 'layui-icon layui-icon-website', '2', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959938', '内容管理', '0', '系统工具目录', '#', '0', 'M', '1', '', 'layui-icon layui-icon-read', '3', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959939', '用户管理', '0', '', '#', '0', 'M', '1', '', 'layui-icon layui-icon-user', '4', '0', '2018-03-01 00:00:00', '0', '2018-03-01 00:00:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959940', '管理员管理', '0', '操作日志菜单', '#', '0', 'M', '1', '', 'layui-icon layui-icon-user', '1', '0', '2018-03-16 11:33:00', '399579765805154304', '2019-12-04 12:28:25');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959941', '用户管理', '398850275361959939', '用户管理菜单', '/admin/user/user_list.do', '0', 'C', '1', 'system:user:view', '#', '1', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959942', '会员分组', '398850275361959939', '角色管理菜单', '/admin/role/user_role_list.do', '0', 'C', '1', 'system:role:view', '#', '2', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959943', '管理员管理', '398850275361959940', '数据监控菜单', '/admin/user/admin_list.do', '0', 'C', '1', 'monitor:data:view', '#', '3', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959944', '角色管理', '398850275361959940', '服务监控菜单', '/admin/role/admin_role_list.do', '0', 'C', '1', 'monitor:server:view', '#', '3', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959945', '菜单管理', '398850275361959939', '前台用户菜单管理菜单', '/admin/menu/user_list.do', '0', 'C', '1', 'system:dept:list', '#', '1', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959946', '菜单管理', '398850275361959940', '管理员菜单管理菜单', '/admin/menu/admin_list.do', '0', 'C', '1', 'system:menu:view', '#', '3', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959947', '基本信息', '398850275361960025', '部门管理菜单', '/admin/system/website.do', '0', 'C', '1', 'system:dept:view', '#', '0', '0', '2018-03-16 11:33:00', '400089841951309824', '2019-12-05 22:15:17');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959948', '邮件服务', '398850275361959936', '岗位管理菜单', '/admin/system/mailset.do', '0', 'C', '1', 'system:post:view', '#', '5', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959950', '系统参数', '398850275361959936', '系统参数列表', '/admin/system/system_list.do', '0', 'C', '1', 'system:config:view', '#', '7', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959951', '通知公告', '398850275361959936', '通知公告菜单', '/system/notice', '0', 'C', '1', 'system:notice:view', '#', '8', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959952', '日志管理', '0', '日志管理菜单', '#', '0', 'M', '1', '', 'layui-icon layui-icon-list', '9', '0', '2018-03-16 11:33:00', '0', '2019-12-03 13:16:55');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959953', '网站列表', '398850275361959937', '在线用户菜单', '/admin/site/list.do', '0', 'C', '1', 'monitor:online:view', '#', '1', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959954', '定时任务', '398850275361959937', '定时任务菜单', '/monitor/job', '0', 'C', '1', 'monitor:job:view', '#', '2', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959955', '新闻管理', '398850275361959938', '新闻管理菜单', '/admin/news/news_list.do', '0', 'C', '1', 'nwes:list:view', '#', '1', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959956', '产品管理', '398850275361959938', '代码生成菜单', '/tool/gen', '0', 'C', '1', 'tool:gen:view', '#', '2', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959957', '招聘管理', '398850275361959938', '系统接口菜单', '/tool/swagger', '0', 'C', '1', 'tool:swagger:view', '#', '3', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959958', '登录日志', '398850275361959952', '登录日志菜单', '/monitor/logininfor', '0', 'C', '1', 'monitor:logininfor:view', '#', '2', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959959', '用户查询', '398850275361959941', '', '#', '0', 'F', '1', 'system:user:list', '#', '1', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959960', '用户新增', '398850275361959941', '', '#', '0', 'F', '1', 'system:user:add', '#', '2', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959961', '用户修改', '398850275361959941', '', '#', '0', 'F', '1', 'system:user:edit', '#', '3', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959962', '用户删除', '398850275361959941', '', '#', '0', 'F', '1', 'system:user:remove', '#', '4', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959963', '用户导出', '398850275361959941', '', '#', '0', 'F', '1', 'system:user:export', '#', '5', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959964', '用户导入', '398850275361959941', '', '#', '0', 'F', '1', 'system:user:import', '#', '6', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959965', '重置密码', '398850275361959941', '', '#', '0', 'F', '1', 'system:user:resetPwd', '#', '7', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959966', '角色查询', '398850275361959944', '', '#', '0', 'F', '1', 'system:role:list', '#', '1', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959967', '角色新增', '398850275361959944', '', '#', '0', 'F', '1', 'system:role:add', '#', '2', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959968', '角色修改', '398850275361959944', '', '#', '0', 'F', '1', 'system:role:edit', '#', '3', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959969', '角色删除', '398850275361959944', '', '#', '0', 'F', '1', 'system:role:remove', '#', '4', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959970', '角色导出', '398850275361959944', '', '#', '0', 'F', '1', 'system:role:export', '#', '5', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959971', '菜单查询', '398850275361959946', '', '#', '0', 'F', '1', 'system:menu:list', '#', '1', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959972', '菜单新增', '398850275361959946', '', '#', '0', 'F', '1', 'system:menu:add', '#', '2', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959973', '菜单修改', '398850275361959946', '', '#', '0', 'F', '1', 'system:menu:edit', '#', '3', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959974', '菜单删除', '398850275361959946', '', '#', '0', 'F', '1', 'system:menu:remove', '#', '4', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959975', '部门新增', '398850275361959962', '', '#', '0', 'F', '1', 'system:dept:add', '#', '2', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959976', '部门修改', '398850275361959962', '', '#', '0', 'F', '1', 'system:dept:edit', '#', '3', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959977', '部门删除', '398850275361959962', '', '#', '0', 'F', '1', 'system:dept:remove', '#', '4', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959978', '岗位查询', '398850275361959963', '', '#', '0', 'F', '1', 'system:post:list', '#', '1', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959979', '岗位新增', '398850275361959963', '', '#', '0', 'F', '1', 'system:post:add', '#', '2', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959980', '岗位修改', '398850275361959963', '', '#', '0', 'F', '1', 'system:post:edit', '#', '3', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959981', '岗位删除', '398850275361959963', '', '#', '0', 'F', '1', 'system:post:remove', '#', '4', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959982', '岗位导出', '398850275361959963', '', '#', '0', 'F', '1', 'system:post:export', '#', '5', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959983', '字典查询', '398850275361959964', '', '#', '0', 'F', '1', 'system:dict:list', '#', '1', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959984', '字典新增', '398850275361959964', '', '#', '0', 'F', '1', 'system:dict:add', '#', '2', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959985', '字典修改', '398850275361959964', '', '#', '0', 'F', '1', 'system:dict:edit', '#', '3', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959986', '字典删除', '398850275361959964', '', '#', '0', 'F', '1', 'system:dict:remove', '#', '4', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959987', '字典导出', '398850275361959964', '', '#', '0', 'F', '1', 'system:dict:export', '#', '5', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959988', '参数查询', '398850275361959965', '', '#', '0', 'F', '1', 'system:config:list', '#', '1', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959989', '参数新增', '398850275361959965', '', '#', '0', 'F', '1', 'system:config:add', '#', '2', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959990', '参数修改', '398850275361959965', '', '#', '0', 'F', '1', 'system:config:edit', '#', '3', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959991', '参数删除', '398850275361959965', '', '#', '0', 'F', '1', 'system:config:remove', '#', '4', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959992', '参数导出', '398850275361959965', '', '#', '0', 'F', '1', 'system:config:export', '#', '5', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959993', '公告查询', '398850275361959947', '', '#', '0', 'F', '1', 'system:notice:list', '#', '1', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959994', '公告新增', '398850275361959947', '', '#', '0', 'F', '1', 'system:notice:add', '#', '2', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959995', '公告修改', '398850275361959947', '', '#', '0', 'F', '1', 'system:notice:edit', '#', '3', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959996', '公告删除', '398850275361959947', '', '#', '0', 'F', '1', 'system:notice:remove', '#', '4', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959997', '操作查询', '398850275361959952', '', '#', '0', 'F', '1', 'monitor:operlog:list', '#', '1', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959998', '操作删除', '398850275361959952', '', '#', '0', 'F', '1', 'monitor:operlog:remove', '#', '2', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361959999', '详细信息', '398850275361959952', '', '#', '0', 'F', '1', 'monitor:operlog:detail', '#', '3', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960000', '日志导出', '398850275361959952', '', '#', '0', 'F', '1', 'monitor:operlog:export', '#', '4', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960001', '登录查询', '398850275361959952', '', '#', '0', 'F', '1', 'monitor:logininfor:list', '#', '1', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960002', '登录删除', '398850275361959952', '', '#', '0', 'F', '1', 'monitor:logininfor:remove', '#', '2', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960003', '日志导出', '398850275361959952', '', '#', '0', 'F', '1', 'monitor:logininfor:export', '#', '3', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960004', '在线查询', '398850275361959968', '', '#', '0', 'F', '1', 'monitor:online:list', '#', '1', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960005', '批量强退', '398850275361959968', '', '#', '0', 'F', '1', 'monitor:online:batchForceLogout', '#', '2', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960006', '单条强退', '398850275361959968', '', '#', '0', 'F', '1', 'monitor:online:forceLogout', '#', '3', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960007', '任务查询', '398850275361959969', '', '#', '0', 'F', '1', 'monitor:job:list', '#', '1', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960008', '任务新增', '398850275361959969', '', '#', '0', 'F', '1', 'monitor:job:add', '#', '2', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960009', '任务修改', '398850275361959969', '', '#', '0', 'F', '1', 'monitor:job:edit', '#', '3', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960010', '任务删除', '398850275361959969', '', '#', '0', 'F', '1', 'monitor:job:remove', '#', '4', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960011', '状态修改', '398850275361959969', '', '#', '0', 'F', '1', 'monitor:job:changeStatus', '#', '5', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960012', '任务详细', '398850275361959969', '', '#', '0', 'F', '1', 'monitor:job:detail', '#', '6', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960013', '任务导出', '398850275361959969', '', '#', '0', 'F', '1', 'monitor:job:export', '#', '7', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960017', '模板列表', '398850275361960024', '', '/admin/template/list.do', '0', 'C', '1', 'tool:gen:preview', '#', '4', '0', '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960019', '地区菜单', '398850275361959936', '地区菜单', '/system/districts', '0', 'C', '1', 'system:districts:view', '#', '11', '0', '2018-03-01 00:00:00', '0', '2018-03-01 00:00:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960020', '地区查询', '398850275361960019', '', '#', '0', 'F', '1', 'system:districts:list', '#', '1', '0', '2018-03-01 00:00:00', '0', '2018-03-01 00:00:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960021', '地区新增', '398850275361960019', '', '#', '0', 'F', '1', 'system:districts:add', '#', '2', '0', '2018-03-01 00:00:00', '0', '2018-03-01 00:00:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960022', '地区修改', '398850275361960019', '', '#', '0', 'F', '1', 'system:districts:edit', '#', '3', '0', '2018-03-01 00:00:00', '0', '2018-03-01 00:00:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960023', '地区删除', '398850275361960019', '', '#', '0', 'F', '1', 'system:districts:remove', '#', '4', '0', '2018-03-01 00:00:00', '0', '2018-03-01 00:00:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960024', '模板管理', '0', '', '#', '0', 'M', '1', null, 'layui-icon layui-icon-template', '4', '0', '2019-08-09 19:17:35', '0', '2019-12-03 16:34:25');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960025', '官网管理', '398850275361959936', '用户站点菜单', '/admin/system_site/index.do', '1', 'C', '1', 'website:website:view', 'layui-icon layui-icon-rate-solid', '1', '0', '2018-03-01 00:00:00', '400095186157305856', '2019-12-05 22:36:31');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960026', '用户站点查询', '398850275361960025', '', '#', '0', 'F', '1', 'website:website:list', '#', '1', '0', '2018-03-01 00:00:00', '0', '2018-03-01 00:00:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960027', '用户站点新增', '398850275361960025', '', '#', '0', 'F', '1', 'website:website:add', '#', '2', '0', '2018-03-01 00:00:00', '0', '2018-03-01 00:00:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960028', '用户站点修改', '398850275361960025', '', '#', '0', 'F', '1', 'website:website:edit', '#', '3', '0', '2018-03-01 00:00:00', '0', '2018-03-01 00:00:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960029', '用户站点删除', '398850275361960025', '', '#', '0', 'F', '1', 'website:website:remove', '#', '4', '0', '2018-03-01 00:00:00', '0', '2018-03-01 00:00:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960030', '用户管理', '398850275361960025', '用户管理菜单', '/website/appuser', '0', 'C', '1', 'website:appuser:view', '#', '1', '0', '2018-03-01 00:00:00', '0', '2019-08-11 23:55:05');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960031', '用户管理查询', '398850275361960030', '', '#', '0', 'F', '1', 'website:appuser:list', '#', '1', '0', '2018-03-01 00:00:00', '0', '2018-03-01 00:00:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960032', '用户管理新增', '398850275361960030', '', '#', '0', 'F', '1', 'website:appuser:add', '#', '2', '0', '2018-03-01 00:00:00', '0', '2018-03-01 00:00:00');
INSERT INTO `fly_admin_menu` VALUES ('398850275361960033', '用户管理修改', '398850275361960030', '', '#', '0', 'F', '1', 'website:appuser:edit', '#', '3', '0', '2018-03-01 00:00:00', '0', '2018-03-01 00:00:00');

-- ----------------------------
-- Table structure for `fly_admin_role`
-- ----------------------------
DROP TABLE IF EXISTS `fly_admin_role`;
CREATE TABLE `fly_admin_role` (
  `id` bigint(20) NOT NULL COMMENT '角色ID',
  `role_name` varchar(30) NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) NOT NULL COMMENT '角色权限字符串',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '角色状态（0停用，1正常 ）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `sort_order` int(4) NOT NULL DEFAULT '0' COMMENT '显示顺序',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色信息表';

-- ----------------------------
-- Records of fly_admin_role
-- ----------------------------
INSERT INTO `fly_admin_role` VALUES ('1', '超级管理员', 'admin', '1', 'admin', '2018-03-16 11:33:00', 'admin', '2019-11-27 21:58:30', '管理员', '1', '0');
INSERT INTO `fly_admin_role` VALUES ('2', '代理商', 'agency', '1', 'admin', '2018-03-16 11:33:00', 'admin', '2019-11-27 21:58:22', '商代理，可以开通子代理、网站', '2', '0');
INSERT INTO `fly_admin_role` VALUES ('3', '企业用户', 'company', '1', 'admin', '2019-08-12 21:23:56', '', '2019-11-27 21:58:34', '企业用户组', '3', '0');
INSERT INTO `fly_admin_role` VALUES ('397188424894775296', 'fhbcfghfgh', 'fghfghgfh', '1', '', '2019-11-27 22:06:05', '', null, 'fghfghfghfgh', '0', '0');

-- ----------------------------
-- Table structure for `fly_admin_role_menu_merge`
-- ----------------------------
DROP TABLE IF EXISTS `fly_admin_role_menu_merge`;
CREATE TABLE `fly_admin_role_menu_merge` (
  `id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of fly_admin_role_menu_merge
-- ----------------------------
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668814229504', '1', '398850275361959936');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668814229505', '1', '398850275361959937');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668814229506', '1', '398850275361959938');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668814229507', '1', '100');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668814229508', '1', '101');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668814229509', '1', '102');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668814229510', '1', '103');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668814229511', '1', '104');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668814229512', '1', '105');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668814229513', '1', '106');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668814229514', '1', '107');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668814229515', '1', '108');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668814229516', '1', '109');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668814229517', '1', '110');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668814229518', '1', '111');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423808', '1', '112');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423809', '1', '113');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423810', '1', '114');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423811', '1', '115');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423812', '1', '500');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423813', '1', '501');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423814', '1', '1000');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423815', '1', '1001');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423816', '1', '1002');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423817', '1', '1003');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423818', '1', '1004');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423819', '1', '1005');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423820', '1', '1006');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423821', '1', '1007');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423822', '1', '1008');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423823', '1', '1009');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423824', '1', '1010');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423825', '1', '1011');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423826', '1', '1012');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423827', '1', '1013');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423828', '1', '1014');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423829', '1', '1015');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423830', '1', '1016');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423831', '1', '1017');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423832', '1', '1018');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423833', '1', '1019');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423834', '1', '1020');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423835', '1', '1021');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423836', '1', '1022');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668818423837', '1', '1023');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668822618112', '1', '1024');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668822618113', '1', '1025');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668822618114', '1', '1026');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668822618115', '1', '1027');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364959668822618116', '1', '1028');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018306375680', '1', '1029');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310569984', '1', '1030');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310569985', '1', '1031');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310569986', '1', '1032');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310569987', '1', '1033');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310569988', '1', '1034');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310569989', '1', '1035');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310569990', '1', '1036');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310569991', '1', '1037');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310569992', '1', '1038');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310569993', '1', '1039');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310569994', '1', '1040');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310569995', '1', '1041');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310569996', '1', '1042');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310569997', '1', '1043');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310569998', '1', '1044');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310569999', '1', '1045');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570000', '1', '1046');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570001', '1', '1047');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570002', '1', '1048');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570003', '1', '1049');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570004', '1', '1050');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570005', '1', '1051');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570006', '1', '1052');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570007', '1', '1053');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570008', '1', '1054');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570009', '1', '1055');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570010', '1', '1056');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570011', '1', '1057');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570012', '1', '1058');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570013', '1', '1059');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570014', '1', '1060');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570015', '1', '2000');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570016', '1', '2001');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570017', '1', '2002');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570018', '1', '2003');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570019', '1', '2004');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570020', '1', '2005');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570021', '1', '2006');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570022', '1', '2007');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570023', '1', '2008');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018310570024', '1', '2009');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018314764288', '1', '2010');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018314764289', '2', '1');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018314764290', '2', '100');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018314764291', '2', '107');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018314764292', '2', '1000');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018314764293', '2', '1035');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018314764294', '2', '2010');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364961018314764295', '2', '2011');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226106544128', '2', '2012');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226106544129', '2', '2013');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226106544130', '2', '2014');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226106544131', '2', '2015');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226106544132', '2', '2016');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226106544133', '2', '2017');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226106544134', '2', '2018');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226106544135', '2', '2019');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226106544136', '2', '2020');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226106544137', '3', '2');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226106544138', '3', '3');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226106544139', '3', '109');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226106544140', '3', '113');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226106544141', '3', '1017');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226106544142', '3', '1018');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226106544143', '3', '1019');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226110738432', '3', '1020');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226110738433', '3', '1021');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226110738434', '3', '1022');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226110738435', '3', '1023');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226110738436', '3', '1024');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226110738437', '3', '2010');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226110738438', '3', '2011');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226110738439', '3', '2012');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226110738440', '3', '2013');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226110738441', '3', '2014');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226110738442', '3', '2015');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226110738443', '3', '2016');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226110738444', '3', '2017');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226110738445', '3', '2018');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226110738446', '3', '2019');
INSERT INTO `fly_admin_role_menu_merge` VALUES ('364962226110738447', '3', '2020');

-- ----------------------------
-- Table structure for `fly_admin_role_merge`
-- ----------------------------
DROP TABLE IF EXISTS `fly_admin_role_merge`;
CREATE TABLE `fly_admin_role_merge` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户的id，user.id,一个用户可以有多个角色',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色的id，role.id ，一个用户可以有多个角色',
  PRIMARY KEY (`id`),
  KEY `userid` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户拥有哪些角色';

-- ----------------------------
-- Records of fly_admin_role_merge
-- ----------------------------
INSERT INTO `fly_admin_role_merge` VALUES ('364903831198236672', '364903830757834752', '1');
INSERT INTO `fly_admin_role_merge` VALUES ('364903831219208192', '364903830757834752', '2');
INSERT INTO `fly_admin_role_merge` VALUES ('364903831240179712', '364903830757834752', '3');
INSERT INTO `fly_admin_role_merge` VALUES ('369555184160092162', '369555184164286492', '2');
INSERT INTO `fly_admin_role_merge` VALUES ('369555184160092164', '369555184164286493', '3');
INSERT INTO `fly_admin_role_merge` VALUES ('369555184160092165', '369555184164286491', '1');

-- ----------------------------
-- Table structure for `fly_agency`
-- ----------------------------
DROP TABLE IF EXISTS `fly_agency`;
CREATE TABLE `fly_agency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(38) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '人名，公司名',
  `phone` char(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '代理的联系电话',
  `userid` int(11) DEFAULT NULL COMMENT '对应user.id',
  `reg_oss_have` int(11) DEFAULT NULL COMMENT '其客户注册成功后，会员所拥有的免费OSS空间',
  `oss_price` int(11) DEFAULT '1' COMMENT 'OSS空间的售价，单位是毛， 时间是年。  如填写30，则为每10M空间3元每年',
  `address` char(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '办公地址',
  `qq` char(13) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '代理商的QQ',
  `site_size` int(11) DEFAULT '0' COMMENT '站点数量，站点余额。1个对应着一个网站/年',
  `version` int(11) DEFAULT '0' COMMENT '乐观锁',
  `parent_id` int(11) DEFAULT '0' COMMENT '推荐人id，父级代理的agency.id。若父级代理是总管理，则为0',
  `addtime` int(11) DEFAULT '0' COMMENT '开通时间',
  `expiretime` int(11) DEFAULT '0' COMMENT '到期时间。按年进行续费（站币）',
  `state` tinyint(2) DEFAULT '1' COMMENT '代理状态，1正常；2冻结',
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`,`parent_id`,`expiretime`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='代理商信息';

-- ----------------------------
-- Records of fly_agency
-- ----------------------------
INSERT INTO `fly_agency` VALUES ('51', '管雷鸣', '17000000001', '392', '1024', '120', '山东潍坊', '921153866', '99999998', '1', '0', '1512818402', '2143123200', '1');

-- ----------------------------
-- Table structure for `fly_agency_site`
-- ----------------------------
DROP TABLE IF EXISTS `fly_agency_site`;
CREATE TABLE `fly_agency_site` (
  `id` bigint(20) NOT NULL,
  `agency_id` bigint(20) NOT NULL COMMENT '代理商id',
  `site_id` bigint(20) NOT NULL COMMENT '代理商开通的网站id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of fly_agency_site
-- ----------------------------

-- ----------------------------
-- Table structure for `fly_agency_user`
-- ----------------------------
DROP TABLE IF EXISTS `fly_agency_user`;
CREATE TABLE `fly_agency_user` (
  `id` bigint(20) NOT NULL,
  `agency_id` bigint(20) NOT NULL COMMENT '代理商id',
  `invited_id` bigint(20) NOT NULL COMMENT '被邀请的用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of fly_agency_user
-- ----------------------------

-- ----------------------------
-- Table structure for `fly_area`
-- ----------------------------
DROP TABLE IF EXISTS `fly_area`;
CREATE TABLE `fly_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '行政区域父ID，例如区县的pid指向市，市的pid指向省，省的pid则是0',
  `area_name` varchar(120) NOT NULL DEFAULT '' COMMENT '行政区域名称',
  `type` tinyint(3) NOT NULL DEFAULT '0' COMMENT '行政区域类型，如如1则是省， 如果是2则是市，如果是3则是区县',
  `code` int(11) NOT NULL DEFAULT '0' COMMENT '行政区域编码',
  `sort` int(11) NOT NULL DEFAULT '50' COMMENT '排序',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `region_type` (`type`),
  KEY `agency_id` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3232 DEFAULT CHARSET=utf8mb4 COMMENT='行政区域表';

-- ----------------------------
-- Records of fly_area
-- ----------------------------
INSERT INTO `fly_area` VALUES ('1', '0', '北京市', '1', '11', '50');
INSERT INTO `fly_area` VALUES ('2', '0', '天津市', '1', '12', '50');
INSERT INTO `fly_area` VALUES ('3', '0', '河北省', '1', '13', '50');
INSERT INTO `fly_area` VALUES ('4', '0', '山西省', '1', '14', '50');
INSERT INTO `fly_area` VALUES ('5', '0', '内蒙古自治区', '1', '15', '50');
INSERT INTO `fly_area` VALUES ('6', '0', '辽宁省', '1', '21', '50');
INSERT INTO `fly_area` VALUES ('7', '0', '吉林省', '1', '22', '50');
INSERT INTO `fly_area` VALUES ('8', '0', '黑龙江省', '1', '23', '50');
INSERT INTO `fly_area` VALUES ('9', '0', '上海市', '1', '31', '50');
INSERT INTO `fly_area` VALUES ('10', '0', '江苏省', '1', '32', '50');
INSERT INTO `fly_area` VALUES ('11', '0', '浙江省', '1', '33', '50');
INSERT INTO `fly_area` VALUES ('12', '0', '安徽省', '1', '34', '50');
INSERT INTO `fly_area` VALUES ('13', '0', '福建省', '1', '35', '50');
INSERT INTO `fly_area` VALUES ('14', '0', '江西省', '1', '36', '50');
INSERT INTO `fly_area` VALUES ('15', '0', '山东省', '1', '37', '50');
INSERT INTO `fly_area` VALUES ('16', '0', '河南省', '1', '41', '50');
INSERT INTO `fly_area` VALUES ('17', '0', '湖北省', '1', '42', '50');
INSERT INTO `fly_area` VALUES ('18', '0', '湖南省', '1', '43', '50');
INSERT INTO `fly_area` VALUES ('19', '0', '广东省', '1', '44', '50');
INSERT INTO `fly_area` VALUES ('20', '0', '广西壮族自治区', '1', '45', '50');
INSERT INTO `fly_area` VALUES ('21', '0', '海南省', '1', '46', '50');
INSERT INTO `fly_area` VALUES ('22', '0', '重庆市', '1', '50', '50');
INSERT INTO `fly_area` VALUES ('23', '0', '四川省', '1', '51', '50');
INSERT INTO `fly_area` VALUES ('24', '0', '贵州省', '1', '52', '50');
INSERT INTO `fly_area` VALUES ('25', '0', '云南省', '1', '53', '50');
INSERT INTO `fly_area` VALUES ('26', '0', '西藏自治区', '1', '54', '50');
INSERT INTO `fly_area` VALUES ('27', '0', '陕西省', '1', '61', '50');
INSERT INTO `fly_area` VALUES ('28', '0', '甘肃省', '1', '62', '50');
INSERT INTO `fly_area` VALUES ('29', '0', '青海省', '1', '63', '50');
INSERT INTO `fly_area` VALUES ('30', '0', '宁夏回族自治区', '1', '64', '50');
INSERT INTO `fly_area` VALUES ('31', '0', '新疆维吾尔自治区', '1', '65', '50');
INSERT INTO `fly_area` VALUES ('32', '1', '市辖区', '2', '1101', '50');
INSERT INTO `fly_area` VALUES ('33', '2', '市辖区', '2', '1201', '50');
INSERT INTO `fly_area` VALUES ('34', '3', '石家庄市', '2', '1301', '50');
INSERT INTO `fly_area` VALUES ('35', '3', '唐山市', '2', '1302', '50');
INSERT INTO `fly_area` VALUES ('36', '3', '秦皇岛市', '2', '1303', '50');
INSERT INTO `fly_area` VALUES ('37', '3', '邯郸市', '2', '1304', '50');
INSERT INTO `fly_area` VALUES ('38', '3', '邢台市', '2', '1305', '50');
INSERT INTO `fly_area` VALUES ('39', '3', '保定市', '2', '1306', '50');
INSERT INTO `fly_area` VALUES ('40', '3', '张家口市', '2', '1307', '50');
INSERT INTO `fly_area` VALUES ('41', '3', '承德市', '2', '1308', '50');
INSERT INTO `fly_area` VALUES ('42', '3', '沧州市', '2', '1309', '50');
INSERT INTO `fly_area` VALUES ('43', '3', '廊坊市', '2', '1310', '50');
INSERT INTO `fly_area` VALUES ('44', '3', '衡水市', '2', '1311', '50');
INSERT INTO `fly_area` VALUES ('45', '3', '省直辖县级行政区划', '2', '1390', '50');
INSERT INTO `fly_area` VALUES ('46', '4', '太原市', '2', '1401', '50');
INSERT INTO `fly_area` VALUES ('47', '4', '大同市', '2', '1402', '50');
INSERT INTO `fly_area` VALUES ('48', '4', '阳泉市', '2', '1403', '50');
INSERT INTO `fly_area` VALUES ('49', '4', '长治市', '2', '1404', '50');
INSERT INTO `fly_area` VALUES ('50', '4', '晋城市', '2', '1405', '50');
INSERT INTO `fly_area` VALUES ('51', '4', '朔州市', '2', '1406', '50');
INSERT INTO `fly_area` VALUES ('52', '4', '晋中市', '2', '1407', '50');
INSERT INTO `fly_area` VALUES ('53', '4', '运城市', '2', '1408', '50');
INSERT INTO `fly_area` VALUES ('54', '4', '忻州市', '2', '1409', '50');
INSERT INTO `fly_area` VALUES ('55', '4', '临汾市', '2', '1410', '50');
INSERT INTO `fly_area` VALUES ('56', '4', '吕梁市', '2', '1411', '50');
INSERT INTO `fly_area` VALUES ('57', '5', '呼和浩特市', '2', '1501', '50');
INSERT INTO `fly_area` VALUES ('58', '5', '包头市', '2', '1502', '50');
INSERT INTO `fly_area` VALUES ('59', '5', '乌海市', '2', '1503', '50');
INSERT INTO `fly_area` VALUES ('60', '5', '赤峰市', '2', '1504', '50');
INSERT INTO `fly_area` VALUES ('61', '5', '通辽市', '2', '1505', '50');
INSERT INTO `fly_area` VALUES ('62', '5', '鄂尔多斯市', '2', '1506', '50');
INSERT INTO `fly_area` VALUES ('63', '5', '呼伦贝尔市', '2', '1507', '50');
INSERT INTO `fly_area` VALUES ('64', '5', '巴彦淖尔市', '2', '1508', '50');
INSERT INTO `fly_area` VALUES ('65', '5', '乌兰察布市', '2', '1509', '50');
INSERT INTO `fly_area` VALUES ('66', '5', '兴安盟', '2', '1522', '50');
INSERT INTO `fly_area` VALUES ('67', '5', '锡林郭勒盟', '2', '1525', '50');
INSERT INTO `fly_area` VALUES ('68', '5', '阿拉善盟', '2', '1529', '50');
INSERT INTO `fly_area` VALUES ('69', '6', '沈阳市', '2', '2101', '50');
INSERT INTO `fly_area` VALUES ('70', '6', '大连市', '2', '2102', '50');
INSERT INTO `fly_area` VALUES ('71', '6', '鞍山市', '2', '2103', '50');
INSERT INTO `fly_area` VALUES ('72', '6', '抚顺市', '2', '2104', '50');
INSERT INTO `fly_area` VALUES ('73', '6', '本溪市', '2', '2105', '50');
INSERT INTO `fly_area` VALUES ('74', '6', '丹东市', '2', '2106', '50');
INSERT INTO `fly_area` VALUES ('75', '6', '锦州市', '2', '2107', '50');
INSERT INTO `fly_area` VALUES ('76', '6', '营口市', '2', '2108', '50');
INSERT INTO `fly_area` VALUES ('77', '6', '阜新市', '2', '2109', '50');
INSERT INTO `fly_area` VALUES ('78', '6', '辽阳市', '2', '2110', '50');
INSERT INTO `fly_area` VALUES ('79', '6', '盘锦市', '2', '2111', '50');
INSERT INTO `fly_area` VALUES ('80', '6', '铁岭市', '2', '2112', '50');
INSERT INTO `fly_area` VALUES ('81', '6', '朝阳市', '2', '2113', '50');
INSERT INTO `fly_area` VALUES ('82', '6', '葫芦岛市', '2', '2114', '50');
INSERT INTO `fly_area` VALUES ('83', '7', '长春市', '2', '2201', '50');
INSERT INTO `fly_area` VALUES ('84', '7', '吉林市', '2', '2202', '50');
INSERT INTO `fly_area` VALUES ('85', '7', '四平市', '2', '2203', '50');
INSERT INTO `fly_area` VALUES ('86', '7', '辽源市', '2', '2204', '50');
INSERT INTO `fly_area` VALUES ('87', '7', '通化市', '2', '2205', '50');
INSERT INTO `fly_area` VALUES ('88', '7', '白山市', '2', '2206', '50');
INSERT INTO `fly_area` VALUES ('89', '7', '松原市', '2', '2207', '50');
INSERT INTO `fly_area` VALUES ('90', '7', '白城市', '2', '2208', '50');
INSERT INTO `fly_area` VALUES ('91', '7', '延边朝鲜族自治州', '2', '2224', '50');
INSERT INTO `fly_area` VALUES ('92', '8', '哈尔滨市', '2', '2301', '50');
INSERT INTO `fly_area` VALUES ('93', '8', '齐齐哈尔市', '2', '2302', '50');
INSERT INTO `fly_area` VALUES ('94', '8', '鸡西市', '2', '2303', '50');
INSERT INTO `fly_area` VALUES ('95', '8', '鹤岗市', '2', '2304', '50');
INSERT INTO `fly_area` VALUES ('96', '8', '双鸭山市', '2', '2305', '50');
INSERT INTO `fly_area` VALUES ('97', '8', '大庆市', '2', '2306', '50');
INSERT INTO `fly_area` VALUES ('98', '8', '伊春市', '2', '2307', '50');
INSERT INTO `fly_area` VALUES ('99', '8', '佳木斯市', '2', '2308', '50');
INSERT INTO `fly_area` VALUES ('100', '8', '七台河市', '2', '2309', '50');
INSERT INTO `fly_area` VALUES ('101', '8', '牡丹江市', '2', '2310', '50');
INSERT INTO `fly_area` VALUES ('102', '8', '黑河市', '2', '2311', '50');
INSERT INTO `fly_area` VALUES ('103', '8', '绥化市', '2', '2312', '50');
INSERT INTO `fly_area` VALUES ('104', '8', '大兴安岭地区', '2', '2327', '50');
INSERT INTO `fly_area` VALUES ('105', '9', '市辖区', '2', '3101', '50');
INSERT INTO `fly_area` VALUES ('106', '10', '南京市', '2', '3201', '50');
INSERT INTO `fly_area` VALUES ('107', '10', '无锡市', '2', '3202', '50');
INSERT INTO `fly_area` VALUES ('108', '10', '徐州市', '2', '3203', '50');
INSERT INTO `fly_area` VALUES ('109', '10', '常州市', '2', '3204', '50');
INSERT INTO `fly_area` VALUES ('110', '10', '苏州市', '2', '3205', '50');
INSERT INTO `fly_area` VALUES ('111', '10', '南通市', '2', '3206', '50');
INSERT INTO `fly_area` VALUES ('112', '10', '连云港市', '2', '3207', '50');
INSERT INTO `fly_area` VALUES ('113', '10', '淮安市', '2', '3208', '50');
INSERT INTO `fly_area` VALUES ('114', '10', '盐城市', '2', '3209', '50');
INSERT INTO `fly_area` VALUES ('115', '10', '扬州市', '2', '3210', '50');
INSERT INTO `fly_area` VALUES ('116', '10', '镇江市', '2', '3211', '50');
INSERT INTO `fly_area` VALUES ('117', '10', '泰州市', '2', '3212', '50');
INSERT INTO `fly_area` VALUES ('118', '10', '宿迁市', '2', '3213', '50');
INSERT INTO `fly_area` VALUES ('119', '11', '杭州市', '2', '3301', '50');
INSERT INTO `fly_area` VALUES ('120', '11', '宁波市', '2', '3302', '50');
INSERT INTO `fly_area` VALUES ('121', '11', '温州市', '2', '3303', '50');
INSERT INTO `fly_area` VALUES ('122', '11', '嘉兴市', '2', '3304', '50');
INSERT INTO `fly_area` VALUES ('123', '11', '湖州市', '2', '3305', '50');
INSERT INTO `fly_area` VALUES ('124', '11', '绍兴市', '2', '3306', '50');
INSERT INTO `fly_area` VALUES ('125', '11', '金华市', '2', '3307', '50');
INSERT INTO `fly_area` VALUES ('126', '11', '衢州市', '2', '3308', '50');
INSERT INTO `fly_area` VALUES ('127', '11', '舟山市', '2', '3309', '50');
INSERT INTO `fly_area` VALUES ('128', '11', '台州市', '2', '3310', '50');
INSERT INTO `fly_area` VALUES ('129', '11', '丽水市', '2', '3311', '50');
INSERT INTO `fly_area` VALUES ('130', '12', '合肥市', '2', '3401', '50');
INSERT INTO `fly_area` VALUES ('131', '12', '芜湖市', '2', '3402', '50');
INSERT INTO `fly_area` VALUES ('132', '12', '蚌埠市', '2', '3403', '50');
INSERT INTO `fly_area` VALUES ('133', '12', '淮南市', '2', '3404', '50');
INSERT INTO `fly_area` VALUES ('134', '12', '马鞍山市', '2', '3405', '50');
INSERT INTO `fly_area` VALUES ('135', '12', '淮北市', '2', '3406', '50');
INSERT INTO `fly_area` VALUES ('136', '12', '铜陵市', '2', '3407', '50');
INSERT INTO `fly_area` VALUES ('137', '12', '安庆市', '2', '3408', '50');
INSERT INTO `fly_area` VALUES ('138', '12', '黄山市', '2', '3410', '50');
INSERT INTO `fly_area` VALUES ('139', '12', '滁州市', '2', '3411', '50');
INSERT INTO `fly_area` VALUES ('140', '12', '阜阳市', '2', '3412', '50');
INSERT INTO `fly_area` VALUES ('141', '12', '宿州市', '2', '3413', '50');
INSERT INTO `fly_area` VALUES ('142', '12', '六安市', '2', '3415', '50');
INSERT INTO `fly_area` VALUES ('143', '12', '亳州市', '2', '3416', '50');
INSERT INTO `fly_area` VALUES ('144', '12', '池州市', '2', '3417', '50');
INSERT INTO `fly_area` VALUES ('145', '12', '宣城市', '2', '3418', '50');
INSERT INTO `fly_area` VALUES ('146', '13', '福州市', '2', '3501', '50');
INSERT INTO `fly_area` VALUES ('147', '13', '厦门市', '2', '3502', '50');
INSERT INTO `fly_area` VALUES ('148', '13', '莆田市', '2', '3503', '50');
INSERT INTO `fly_area` VALUES ('149', '13', '三明市', '2', '3504', '50');
INSERT INTO `fly_area` VALUES ('150', '13', '泉州市', '2', '3505', '50');
INSERT INTO `fly_area` VALUES ('151', '13', '漳州市', '2', '3506', '50');
INSERT INTO `fly_area` VALUES ('152', '13', '南平市', '2', '3507', '50');
INSERT INTO `fly_area` VALUES ('153', '13', '龙岩市', '2', '3508', '50');
INSERT INTO `fly_area` VALUES ('154', '13', '宁德市', '2', '3509', '50');
INSERT INTO `fly_area` VALUES ('155', '14', '南昌市', '2', '3601', '50');
INSERT INTO `fly_area` VALUES ('156', '14', '景德镇市', '2', '3602', '50');
INSERT INTO `fly_area` VALUES ('157', '14', '萍乡市', '2', '3603', '50');
INSERT INTO `fly_area` VALUES ('158', '14', '九江市', '2', '3604', '50');
INSERT INTO `fly_area` VALUES ('159', '14', '新余市', '2', '3605', '50');
INSERT INTO `fly_area` VALUES ('160', '14', '鹰潭市', '2', '3606', '50');
INSERT INTO `fly_area` VALUES ('161', '14', '赣州市', '2', '3607', '50');
INSERT INTO `fly_area` VALUES ('162', '14', '吉安市', '2', '3608', '50');
INSERT INTO `fly_area` VALUES ('163', '14', '宜春市', '2', '3609', '50');
INSERT INTO `fly_area` VALUES ('164', '14', '抚州市', '2', '3610', '50');
INSERT INTO `fly_area` VALUES ('165', '14', '上饶市', '2', '3611', '50');
INSERT INTO `fly_area` VALUES ('166', '15', '济南市', '2', '3701', '50');
INSERT INTO `fly_area` VALUES ('167', '15', '青岛市', '2', '3702', '50');
INSERT INTO `fly_area` VALUES ('168', '15', '淄博市', '2', '3703', '50');
INSERT INTO `fly_area` VALUES ('169', '15', '枣庄市', '2', '3704', '50');
INSERT INTO `fly_area` VALUES ('170', '15', '东营市', '2', '3705', '50');
INSERT INTO `fly_area` VALUES ('171', '15', '烟台市', '2', '3706', '50');
INSERT INTO `fly_area` VALUES ('172', '15', '潍坊市', '2', '3707', '50');
INSERT INTO `fly_area` VALUES ('173', '15', '济宁市', '2', '3708', '50');
INSERT INTO `fly_area` VALUES ('174', '15', '泰安市', '2', '3709', '50');
INSERT INTO `fly_area` VALUES ('175', '15', '威海市', '2', '3710', '50');
INSERT INTO `fly_area` VALUES ('176', '15', '日照市', '2', '3711', '50');
INSERT INTO `fly_area` VALUES ('177', '15', '莱芜市', '2', '3712', '50');
INSERT INTO `fly_area` VALUES ('178', '15', '临沂市', '2', '3713', '50');
INSERT INTO `fly_area` VALUES ('179', '15', '德州市', '2', '3714', '50');
INSERT INTO `fly_area` VALUES ('180', '15', '聊城市', '2', '3715', '50');
INSERT INTO `fly_area` VALUES ('181', '15', '滨州市', '2', '3716', '50');
INSERT INTO `fly_area` VALUES ('182', '15', '菏泽市', '2', '3717', '50');
INSERT INTO `fly_area` VALUES ('183', '16', '郑州市', '2', '4101', '50');
INSERT INTO `fly_area` VALUES ('184', '16', '开封市', '2', '4102', '50');
INSERT INTO `fly_area` VALUES ('185', '16', '洛阳市', '2', '4103', '50');
INSERT INTO `fly_area` VALUES ('186', '16', '平顶山市', '2', '4104', '50');
INSERT INTO `fly_area` VALUES ('187', '16', '安阳市', '2', '4105', '50');
INSERT INTO `fly_area` VALUES ('188', '16', '鹤壁市', '2', '4106', '50');
INSERT INTO `fly_area` VALUES ('189', '16', '新乡市', '2', '4107', '50');
INSERT INTO `fly_area` VALUES ('190', '16', '焦作市', '2', '4108', '50');
INSERT INTO `fly_area` VALUES ('191', '16', '濮阳市', '2', '4109', '50');
INSERT INTO `fly_area` VALUES ('192', '16', '许昌市', '2', '4110', '50');
INSERT INTO `fly_area` VALUES ('193', '16', '漯河市', '2', '4111', '50');
INSERT INTO `fly_area` VALUES ('194', '16', '三门峡市', '2', '4112', '50');
INSERT INTO `fly_area` VALUES ('195', '16', '南阳市', '2', '4113', '50');
INSERT INTO `fly_area` VALUES ('196', '16', '商丘市', '2', '4114', '50');
INSERT INTO `fly_area` VALUES ('197', '16', '信阳市', '2', '4115', '50');
INSERT INTO `fly_area` VALUES ('198', '16', '周口市', '2', '4116', '50');
INSERT INTO `fly_area` VALUES ('199', '16', '驻马店市', '2', '4117', '50');
INSERT INTO `fly_area` VALUES ('200', '16', '省直辖县级行政区划', '2', '4190', '50');
INSERT INTO `fly_area` VALUES ('201', '17', '武汉市', '2', '4201', '50');
INSERT INTO `fly_area` VALUES ('202', '17', '黄石市', '2', '4202', '50');
INSERT INTO `fly_area` VALUES ('203', '17', '十堰市', '2', '4203', '50');
INSERT INTO `fly_area` VALUES ('204', '17', '宜昌市', '2', '4205', '50');
INSERT INTO `fly_area` VALUES ('205', '17', '襄阳市', '2', '4206', '50');
INSERT INTO `fly_area` VALUES ('206', '17', '鄂州市', '2', '4207', '50');
INSERT INTO `fly_area` VALUES ('207', '17', '荆门市', '2', '4208', '50');
INSERT INTO `fly_area` VALUES ('208', '17', '孝感市', '2', '4209', '50');
INSERT INTO `fly_area` VALUES ('209', '17', '荆州市', '2', '4210', '50');
INSERT INTO `fly_area` VALUES ('210', '17', '黄冈市', '2', '4211', '50');
INSERT INTO `fly_area` VALUES ('211', '17', '咸宁市', '2', '4212', '50');
INSERT INTO `fly_area` VALUES ('212', '17', '随州市', '2', '4213', '50');
INSERT INTO `fly_area` VALUES ('213', '17', '恩施土家族苗族自治州', '2', '4228', '50');
INSERT INTO `fly_area` VALUES ('214', '17', '省直辖县级行政区划', '2', '4290', '50');
INSERT INTO `fly_area` VALUES ('215', '18', '长沙市', '2', '4301', '50');
INSERT INTO `fly_area` VALUES ('216', '18', '株洲市', '2', '4302', '50');
INSERT INTO `fly_area` VALUES ('217', '18', '湘潭市', '2', '4303', '50');
INSERT INTO `fly_area` VALUES ('218', '18', '衡阳市', '2', '4304', '50');
INSERT INTO `fly_area` VALUES ('219', '18', '邵阳市', '2', '4305', '50');
INSERT INTO `fly_area` VALUES ('220', '18', '岳阳市', '2', '4306', '50');
INSERT INTO `fly_area` VALUES ('221', '18', '常德市', '2', '4307', '50');
INSERT INTO `fly_area` VALUES ('222', '18', '张家界市', '2', '4308', '50');
INSERT INTO `fly_area` VALUES ('223', '18', '益阳市', '2', '4309', '50');
INSERT INTO `fly_area` VALUES ('224', '18', '郴州市', '2', '4310', '50');
INSERT INTO `fly_area` VALUES ('225', '18', '永州市', '2', '4311', '50');
INSERT INTO `fly_area` VALUES ('226', '18', '怀化市', '2', '4312', '50');
INSERT INTO `fly_area` VALUES ('227', '18', '娄底市', '2', '4313', '50');
INSERT INTO `fly_area` VALUES ('228', '18', '湘西土家族苗族自治州', '2', '4331', '50');
INSERT INTO `fly_area` VALUES ('229', '19', '广州市', '2', '4401', '50');
INSERT INTO `fly_area` VALUES ('230', '19', '韶关市', '2', '4402', '50');
INSERT INTO `fly_area` VALUES ('231', '19', '深圳市', '2', '4403', '50');
INSERT INTO `fly_area` VALUES ('232', '19', '珠海市', '2', '4404', '50');
INSERT INTO `fly_area` VALUES ('233', '19', '汕头市', '2', '4405', '50');
INSERT INTO `fly_area` VALUES ('234', '19', '佛山市', '2', '4406', '50');
INSERT INTO `fly_area` VALUES ('235', '19', '江门市', '2', '4407', '50');
INSERT INTO `fly_area` VALUES ('236', '19', '湛江市', '2', '4408', '50');
INSERT INTO `fly_area` VALUES ('237', '19', '茂名市', '2', '4409', '50');
INSERT INTO `fly_area` VALUES ('238', '19', '肇庆市', '2', '4412', '50');
INSERT INTO `fly_area` VALUES ('239', '19', '惠州市', '2', '4413', '50');
INSERT INTO `fly_area` VALUES ('240', '19', '梅州市', '2', '4414', '50');
INSERT INTO `fly_area` VALUES ('241', '19', '汕尾市', '2', '4415', '50');
INSERT INTO `fly_area` VALUES ('242', '19', '河源市', '2', '4416', '50');
INSERT INTO `fly_area` VALUES ('243', '19', '阳江市', '2', '4417', '50');
INSERT INTO `fly_area` VALUES ('244', '19', '清远市', '2', '4418', '50');
INSERT INTO `fly_area` VALUES ('245', '19', '东莞市', '2', '4419', '50');
INSERT INTO `fly_area` VALUES ('246', '19', '中山市', '2', '4420', '50');
INSERT INTO `fly_area` VALUES ('247', '19', '潮州市', '2', '4451', '50');
INSERT INTO `fly_area` VALUES ('248', '19', '揭阳市', '2', '4452', '50');
INSERT INTO `fly_area` VALUES ('249', '19', '云浮市', '2', '4453', '50');
INSERT INTO `fly_area` VALUES ('250', '20', '南宁市', '2', '4501', '50');
INSERT INTO `fly_area` VALUES ('251', '20', '柳州市', '2', '4502', '50');
INSERT INTO `fly_area` VALUES ('252', '20', '桂林市', '2', '4503', '50');
INSERT INTO `fly_area` VALUES ('253', '20', '梧州市', '2', '4504', '50');
INSERT INTO `fly_area` VALUES ('254', '20', '北海市', '2', '4505', '50');
INSERT INTO `fly_area` VALUES ('255', '20', '防城港市', '2', '4506', '50');
INSERT INTO `fly_area` VALUES ('256', '20', '钦州市', '2', '4507', '50');
INSERT INTO `fly_area` VALUES ('257', '20', '贵港市', '2', '4508', '50');
INSERT INTO `fly_area` VALUES ('258', '20', '玉林市', '2', '4509', '50');
INSERT INTO `fly_area` VALUES ('259', '20', '百色市', '2', '4510', '50');
INSERT INTO `fly_area` VALUES ('260', '20', '贺州市', '2', '4511', '50');
INSERT INTO `fly_area` VALUES ('261', '20', '河池市', '2', '4512', '50');
INSERT INTO `fly_area` VALUES ('262', '20', '来宾市', '2', '4513', '50');
INSERT INTO `fly_area` VALUES ('263', '20', '崇左市', '2', '4514', '50');
INSERT INTO `fly_area` VALUES ('264', '21', '海口市', '2', '4601', '50');
INSERT INTO `fly_area` VALUES ('265', '21', '三亚市', '2', '4602', '50');
INSERT INTO `fly_area` VALUES ('266', '21', '三沙市', '2', '4603', '50');
INSERT INTO `fly_area` VALUES ('267', '21', '儋州市', '2', '4604', '50');
INSERT INTO `fly_area` VALUES ('268', '21', '省直辖县级行政区划', '2', '4690', '50');
INSERT INTO `fly_area` VALUES ('269', '22', '市辖区', '2', '5001', '50');
INSERT INTO `fly_area` VALUES ('270', '22', '县', '2', '5002', '50');
INSERT INTO `fly_area` VALUES ('271', '23', '成都市', '2', '5101', '50');
INSERT INTO `fly_area` VALUES ('272', '23', '自贡市', '2', '5103', '50');
INSERT INTO `fly_area` VALUES ('273', '23', '攀枝花市', '2', '5104', '50');
INSERT INTO `fly_area` VALUES ('274', '23', '泸州市', '2', '5105', '50');
INSERT INTO `fly_area` VALUES ('275', '23', '德阳市', '2', '5106', '50');
INSERT INTO `fly_area` VALUES ('276', '23', '绵阳市', '2', '5107', '50');
INSERT INTO `fly_area` VALUES ('277', '23', '广元市', '2', '5108', '50');
INSERT INTO `fly_area` VALUES ('278', '23', '遂宁市', '2', '5109', '50');
INSERT INTO `fly_area` VALUES ('279', '23', '内江市', '2', '5110', '50');
INSERT INTO `fly_area` VALUES ('280', '23', '乐山市', '2', '5111', '50');
INSERT INTO `fly_area` VALUES ('281', '23', '南充市', '2', '5113', '50');
INSERT INTO `fly_area` VALUES ('282', '23', '眉山市', '2', '5114', '50');
INSERT INTO `fly_area` VALUES ('283', '23', '宜宾市', '2', '5115', '50');
INSERT INTO `fly_area` VALUES ('284', '23', '广安市', '2', '5116', '50');
INSERT INTO `fly_area` VALUES ('285', '23', '达州市', '2', '5117', '50');
INSERT INTO `fly_area` VALUES ('286', '23', '雅安市', '2', '5118', '50');
INSERT INTO `fly_area` VALUES ('287', '23', '巴中市', '2', '5119', '50');
INSERT INTO `fly_area` VALUES ('288', '23', '资阳市', '2', '5120', '50');
INSERT INTO `fly_area` VALUES ('289', '23', '阿坝藏族羌族自治州', '2', '5132', '50');
INSERT INTO `fly_area` VALUES ('290', '23', '甘孜藏族自治州', '2', '5133', '50');
INSERT INTO `fly_area` VALUES ('291', '23', '凉山彝族自治州', '2', '5134', '50');
INSERT INTO `fly_area` VALUES ('292', '24', '贵阳市', '2', '5201', '50');
INSERT INTO `fly_area` VALUES ('293', '24', '六盘水市', '2', '5202', '50');
INSERT INTO `fly_area` VALUES ('294', '24', '遵义市', '2', '5203', '50');
INSERT INTO `fly_area` VALUES ('295', '24', '安顺市', '2', '5204', '50');
INSERT INTO `fly_area` VALUES ('296', '24', '毕节市', '2', '5205', '50');
INSERT INTO `fly_area` VALUES ('297', '24', '铜仁市', '2', '5206', '50');
INSERT INTO `fly_area` VALUES ('298', '24', '黔西南布依族苗族自治州', '2', '5223', '50');
INSERT INTO `fly_area` VALUES ('299', '24', '黔东南苗族侗族自治州', '2', '5226', '50');
INSERT INTO `fly_area` VALUES ('300', '24', '黔南布依族苗族自治州', '2', '5227', '50');
INSERT INTO `fly_area` VALUES ('301', '25', '昆明市', '2', '5301', '50');
INSERT INTO `fly_area` VALUES ('302', '25', '曲靖市', '2', '5303', '50');
INSERT INTO `fly_area` VALUES ('303', '25', '玉溪市', '2', '5304', '50');
INSERT INTO `fly_area` VALUES ('304', '25', '保山市', '2', '5305', '50');
INSERT INTO `fly_area` VALUES ('305', '25', '昭通市', '2', '5306', '50');
INSERT INTO `fly_area` VALUES ('306', '25', '丽江市', '2', '5307', '50');
INSERT INTO `fly_area` VALUES ('307', '25', '普洱市', '2', '5308', '50');
INSERT INTO `fly_area` VALUES ('308', '25', '临沧市', '2', '5309', '50');
INSERT INTO `fly_area` VALUES ('309', '25', '楚雄彝族自治州', '2', '5323', '50');
INSERT INTO `fly_area` VALUES ('310', '25', '红河哈尼族彝族自治州', '2', '5325', '50');
INSERT INTO `fly_area` VALUES ('311', '25', '文山壮族苗族自治州', '2', '5326', '50');
INSERT INTO `fly_area` VALUES ('312', '25', '西双版纳傣族自治州', '2', '5328', '50');
INSERT INTO `fly_area` VALUES ('313', '25', '大理白族自治州', '2', '5329', '50');
INSERT INTO `fly_area` VALUES ('314', '25', '德宏傣族景颇族自治州', '2', '5331', '50');
INSERT INTO `fly_area` VALUES ('315', '25', '怒江傈僳族自治州', '2', '5333', '50');
INSERT INTO `fly_area` VALUES ('316', '25', '迪庆藏族自治州', '2', '5334', '50');
INSERT INTO `fly_area` VALUES ('317', '26', '拉萨市', '2', '5401', '50');
INSERT INTO `fly_area` VALUES ('318', '26', '日喀则市', '2', '5402', '50');
INSERT INTO `fly_area` VALUES ('319', '26', '昌都市', '2', '5403', '50');
INSERT INTO `fly_area` VALUES ('320', '26', '林芝市', '2', '5404', '50');
INSERT INTO `fly_area` VALUES ('321', '26', '山南市', '2', '5405', '50');
INSERT INTO `fly_area` VALUES ('322', '26', '那曲地区', '2', '5424', '50');
INSERT INTO `fly_area` VALUES ('323', '26', '阿里地区', '2', '5425', '50');
INSERT INTO `fly_area` VALUES ('324', '27', '西安市', '2', '6101', '50');
INSERT INTO `fly_area` VALUES ('325', '27', '铜川市', '2', '6102', '50');
INSERT INTO `fly_area` VALUES ('326', '27', '宝鸡市', '2', '6103', '50');
INSERT INTO `fly_area` VALUES ('327', '27', '咸阳市', '2', '6104', '50');
INSERT INTO `fly_area` VALUES ('328', '27', '渭南市', '2', '6105', '50');
INSERT INTO `fly_area` VALUES ('329', '27', '延安市', '2', '6106', '50');
INSERT INTO `fly_area` VALUES ('330', '27', '汉中市', '2', '6107', '50');
INSERT INTO `fly_area` VALUES ('331', '27', '榆林市', '2', '6108', '50');
INSERT INTO `fly_area` VALUES ('332', '27', '安康市', '2', '6109', '50');
INSERT INTO `fly_area` VALUES ('333', '27', '商洛市', '2', '6110', '50');
INSERT INTO `fly_area` VALUES ('334', '28', '兰州市', '2', '6201', '50');
INSERT INTO `fly_area` VALUES ('335', '28', '嘉峪关市', '2', '6202', '50');
INSERT INTO `fly_area` VALUES ('336', '28', '金昌市', '2', '6203', '50');
INSERT INTO `fly_area` VALUES ('337', '28', '白银市', '2', '6204', '50');
INSERT INTO `fly_area` VALUES ('338', '28', '天水市', '2', '6205', '50');
INSERT INTO `fly_area` VALUES ('339', '28', '武威市', '2', '6206', '50');
INSERT INTO `fly_area` VALUES ('340', '28', '张掖市', '2', '6207', '50');
INSERT INTO `fly_area` VALUES ('341', '28', '平凉市', '2', '6208', '50');
INSERT INTO `fly_area` VALUES ('342', '28', '酒泉市', '2', '6209', '50');
INSERT INTO `fly_area` VALUES ('343', '28', '庆阳市', '2', '6210', '50');
INSERT INTO `fly_area` VALUES ('344', '28', '定西市', '2', '6211', '50');
INSERT INTO `fly_area` VALUES ('345', '28', '陇南市', '2', '6212', '50');
INSERT INTO `fly_area` VALUES ('346', '28', '临夏回族自治州', '2', '6229', '50');
INSERT INTO `fly_area` VALUES ('347', '28', '甘南藏族自治州', '2', '6230', '50');
INSERT INTO `fly_area` VALUES ('348', '29', '西宁市', '2', '6301', '50');
INSERT INTO `fly_area` VALUES ('349', '29', '海东市', '2', '6302', '50');
INSERT INTO `fly_area` VALUES ('350', '29', '海北藏族自治州', '2', '6322', '50');
INSERT INTO `fly_area` VALUES ('351', '29', '黄南藏族自治州', '2', '6323', '50');
INSERT INTO `fly_area` VALUES ('352', '29', '海南藏族自治州', '2', '6325', '50');
INSERT INTO `fly_area` VALUES ('353', '29', '果洛藏族自治州', '2', '6326', '50');
INSERT INTO `fly_area` VALUES ('354', '29', '玉树藏族自治州', '2', '6327', '50');
INSERT INTO `fly_area` VALUES ('355', '29', '海西蒙古族藏族自治州', '2', '6328', '50');
INSERT INTO `fly_area` VALUES ('356', '30', '银川市', '2', '6401', '50');
INSERT INTO `fly_area` VALUES ('357', '30', '石嘴山市', '2', '6402', '50');
INSERT INTO `fly_area` VALUES ('358', '30', '吴忠市', '2', '6403', '50');
INSERT INTO `fly_area` VALUES ('359', '30', '固原市', '2', '6404', '50');
INSERT INTO `fly_area` VALUES ('360', '30', '中卫市', '2', '6405', '50');
INSERT INTO `fly_area` VALUES ('361', '31', '乌鲁木齐市', '2', '6501', '50');
INSERT INTO `fly_area` VALUES ('362', '31', '克拉玛依市', '2', '6502', '50');
INSERT INTO `fly_area` VALUES ('363', '31', '吐鲁番市', '2', '6504', '50');
INSERT INTO `fly_area` VALUES ('364', '31', '哈密市', '2', '6505', '50');
INSERT INTO `fly_area` VALUES ('365', '31', '昌吉回族自治州', '2', '6523', '50');
INSERT INTO `fly_area` VALUES ('366', '31', '博尔塔拉蒙古自治州', '2', '6527', '50');
INSERT INTO `fly_area` VALUES ('367', '31', '巴音郭楞蒙古自治州', '2', '6528', '50');
INSERT INTO `fly_area` VALUES ('368', '31', '阿克苏地区', '2', '6529', '50');
INSERT INTO `fly_area` VALUES ('369', '31', '克孜勒苏柯尔克孜自治州', '2', '6530', '50');
INSERT INTO `fly_area` VALUES ('370', '31', '喀什地区', '2', '6531', '50');
INSERT INTO `fly_area` VALUES ('371', '31', '和田地区', '2', '6532', '50');
INSERT INTO `fly_area` VALUES ('372', '31', '伊犁哈萨克自治州', '2', '6540', '50');
INSERT INTO `fly_area` VALUES ('373', '31', '塔城地区', '2', '6542', '50');
INSERT INTO `fly_area` VALUES ('374', '31', '阿勒泰地区', '2', '6543', '50');
INSERT INTO `fly_area` VALUES ('375', '31', '自治区直辖县级行政区划', '2', '6590', '50');
INSERT INTO `fly_area` VALUES ('376', '32', '东城区', '3', '110101', '50');
INSERT INTO `fly_area` VALUES ('377', '32', '西城区', '3', '110102', '50');
INSERT INTO `fly_area` VALUES ('378', '32', '朝阳区', '3', '110105', '50');
INSERT INTO `fly_area` VALUES ('379', '32', '丰台区', '3', '110106', '50');
INSERT INTO `fly_area` VALUES ('380', '32', '石景山区', '3', '110107', '50');
INSERT INTO `fly_area` VALUES ('381', '32', '海淀区', '3', '110108', '50');
INSERT INTO `fly_area` VALUES ('382', '32', '门头沟区', '3', '110109', '50');
INSERT INTO `fly_area` VALUES ('383', '32', '房山区', '3', '110111', '50');
INSERT INTO `fly_area` VALUES ('384', '32', '通州区', '3', '110112', '50');
INSERT INTO `fly_area` VALUES ('385', '32', '顺义区', '3', '110113', '50');
INSERT INTO `fly_area` VALUES ('386', '32', '昌平区', '3', '110114', '50');
INSERT INTO `fly_area` VALUES ('387', '32', '大兴区', '3', '110115', '50');
INSERT INTO `fly_area` VALUES ('388', '32', '怀柔区', '3', '110116', '50');
INSERT INTO `fly_area` VALUES ('389', '32', '平谷区', '3', '110117', '50');
INSERT INTO `fly_area` VALUES ('390', '32', '密云区', '3', '110118', '50');
INSERT INTO `fly_area` VALUES ('391', '32', '延庆区', '3', '110119', '50');
INSERT INTO `fly_area` VALUES ('392', '33', '和平区', '3', '120101', '50');
INSERT INTO `fly_area` VALUES ('393', '33', '河东区', '3', '120102', '50');
INSERT INTO `fly_area` VALUES ('394', '33', '河西区', '3', '120103', '50');
INSERT INTO `fly_area` VALUES ('395', '33', '南开区', '3', '120104', '50');
INSERT INTO `fly_area` VALUES ('396', '33', '河北区', '3', '120105', '50');
INSERT INTO `fly_area` VALUES ('397', '33', '红桥区', '3', '120106', '50');
INSERT INTO `fly_area` VALUES ('398', '33', '东丽区', '3', '120110', '50');
INSERT INTO `fly_area` VALUES ('399', '33', '西青区', '3', '120111', '50');
INSERT INTO `fly_area` VALUES ('400', '33', '津南区', '3', '120112', '50');
INSERT INTO `fly_area` VALUES ('401', '33', '北辰区', '3', '120113', '50');
INSERT INTO `fly_area` VALUES ('402', '33', '武清区', '3', '120114', '50');
INSERT INTO `fly_area` VALUES ('403', '33', '宝坻区', '3', '120115', '50');
INSERT INTO `fly_area` VALUES ('404', '33', '滨海新区', '3', '120116', '50');
INSERT INTO `fly_area` VALUES ('405', '33', '宁河区', '3', '120117', '50');
INSERT INTO `fly_area` VALUES ('406', '33', '静海区', '3', '120118', '50');
INSERT INTO `fly_area` VALUES ('407', '33', '蓟州区', '3', '120119', '50');
INSERT INTO `fly_area` VALUES ('408', '34', '长安区', '3', '130102', '50');
INSERT INTO `fly_area` VALUES ('409', '34', '桥西区', '3', '130104', '50');
INSERT INTO `fly_area` VALUES ('410', '34', '新华区', '3', '130105', '50');
INSERT INTO `fly_area` VALUES ('411', '34', '井陉矿区', '3', '130107', '50');
INSERT INTO `fly_area` VALUES ('412', '34', '裕华区', '3', '130108', '50');
INSERT INTO `fly_area` VALUES ('413', '34', '藁城区', '3', '130109', '50');
INSERT INTO `fly_area` VALUES ('414', '34', '鹿泉区', '3', '130110', '50');
INSERT INTO `fly_area` VALUES ('415', '34', '栾城区', '3', '130111', '50');
INSERT INTO `fly_area` VALUES ('416', '34', '井陉县', '3', '130121', '50');
INSERT INTO `fly_area` VALUES ('417', '34', '正定县', '3', '130123', '50');
INSERT INTO `fly_area` VALUES ('418', '34', '行唐县', '3', '130125', '50');
INSERT INTO `fly_area` VALUES ('419', '34', '灵寿县', '3', '130126', '50');
INSERT INTO `fly_area` VALUES ('420', '34', '高邑县', '3', '130127', '50');
INSERT INTO `fly_area` VALUES ('421', '34', '深泽县', '3', '130128', '50');
INSERT INTO `fly_area` VALUES ('422', '34', '赞皇县', '3', '130129', '50');
INSERT INTO `fly_area` VALUES ('423', '34', '无极县', '3', '130130', '50');
INSERT INTO `fly_area` VALUES ('424', '34', '平山县', '3', '130131', '50');
INSERT INTO `fly_area` VALUES ('425', '34', '元氏县', '3', '130132', '50');
INSERT INTO `fly_area` VALUES ('426', '34', '赵县', '3', '130133', '50');
INSERT INTO `fly_area` VALUES ('427', '34', '晋州市', '3', '130183', '50');
INSERT INTO `fly_area` VALUES ('428', '34', '新乐市', '3', '130184', '50');
INSERT INTO `fly_area` VALUES ('429', '35', '路南区', '3', '130202', '50');
INSERT INTO `fly_area` VALUES ('430', '35', '路北区', '3', '130203', '50');
INSERT INTO `fly_area` VALUES ('431', '35', '古冶区', '3', '130204', '50');
INSERT INTO `fly_area` VALUES ('432', '35', '开平区', '3', '130205', '50');
INSERT INTO `fly_area` VALUES ('433', '35', '丰南区', '3', '130207', '50');
INSERT INTO `fly_area` VALUES ('434', '35', '丰润区', '3', '130208', '50');
INSERT INTO `fly_area` VALUES ('435', '35', '曹妃甸区', '3', '130209', '50');
INSERT INTO `fly_area` VALUES ('436', '35', '滦县', '3', '130223', '50');
INSERT INTO `fly_area` VALUES ('437', '35', '滦南县', '3', '130224', '50');
INSERT INTO `fly_area` VALUES ('438', '35', '乐亭县', '3', '130225', '50');
INSERT INTO `fly_area` VALUES ('439', '35', '迁西县', '3', '130227', '50');
INSERT INTO `fly_area` VALUES ('440', '35', '玉田县', '3', '130229', '50');
INSERT INTO `fly_area` VALUES ('441', '35', '遵化市', '3', '130281', '50');
INSERT INTO `fly_area` VALUES ('442', '35', '迁安市', '3', '130283', '50');
INSERT INTO `fly_area` VALUES ('443', '36', '海港区', '3', '130302', '50');
INSERT INTO `fly_area` VALUES ('444', '36', '山海关区', '3', '130303', '50');
INSERT INTO `fly_area` VALUES ('445', '36', '北戴河区', '3', '130304', '50');
INSERT INTO `fly_area` VALUES ('446', '36', '抚宁区', '3', '130306', '50');
INSERT INTO `fly_area` VALUES ('447', '36', '青龙满族自治县', '3', '130321', '50');
INSERT INTO `fly_area` VALUES ('448', '36', '昌黎县', '3', '130322', '50');
INSERT INTO `fly_area` VALUES ('449', '36', '卢龙县', '3', '130324', '50');
INSERT INTO `fly_area` VALUES ('450', '37', '邯山区', '3', '130402', '50');
INSERT INTO `fly_area` VALUES ('451', '37', '丛台区', '3', '130403', '50');
INSERT INTO `fly_area` VALUES ('452', '37', '复兴区', '3', '130404', '50');
INSERT INTO `fly_area` VALUES ('453', '37', '峰峰矿区', '3', '130406', '50');
INSERT INTO `fly_area` VALUES ('454', '37', '邯郸县', '3', '130421', '50');
INSERT INTO `fly_area` VALUES ('455', '37', '临漳县', '3', '130423', '50');
INSERT INTO `fly_area` VALUES ('456', '37', '成安县', '3', '130424', '50');
INSERT INTO `fly_area` VALUES ('457', '37', '大名县', '3', '130425', '50');
INSERT INTO `fly_area` VALUES ('458', '37', '涉县', '3', '130426', '50');
INSERT INTO `fly_area` VALUES ('459', '37', '磁县', '3', '130427', '50');
INSERT INTO `fly_area` VALUES ('460', '37', '肥乡县', '3', '130428', '50');
INSERT INTO `fly_area` VALUES ('461', '37', '永年县', '3', '130429', '50');
INSERT INTO `fly_area` VALUES ('462', '37', '邱县', '3', '130430', '50');
INSERT INTO `fly_area` VALUES ('463', '37', '鸡泽县', '3', '130431', '50');
INSERT INTO `fly_area` VALUES ('464', '37', '广平县', '3', '130432', '50');
INSERT INTO `fly_area` VALUES ('465', '37', '馆陶县', '3', '130433', '50');
INSERT INTO `fly_area` VALUES ('466', '37', '魏县', '3', '130434', '50');
INSERT INTO `fly_area` VALUES ('467', '37', '曲周县', '3', '130435', '50');
INSERT INTO `fly_area` VALUES ('468', '37', '武安市', '3', '130481', '50');
INSERT INTO `fly_area` VALUES ('469', '38', '桥东区', '3', '130502', '50');
INSERT INTO `fly_area` VALUES ('470', '38', '桥西区', '3', '130503', '50');
INSERT INTO `fly_area` VALUES ('471', '38', '邢台县', '3', '130521', '50');
INSERT INTO `fly_area` VALUES ('472', '38', '临城县', '3', '130522', '50');
INSERT INTO `fly_area` VALUES ('473', '38', '内丘县', '3', '130523', '50');
INSERT INTO `fly_area` VALUES ('474', '38', '柏乡县', '3', '130524', '50');
INSERT INTO `fly_area` VALUES ('475', '38', '隆尧县', '3', '130525', '50');
INSERT INTO `fly_area` VALUES ('476', '38', '任县', '3', '130526', '50');
INSERT INTO `fly_area` VALUES ('477', '38', '南和县', '3', '130527', '50');
INSERT INTO `fly_area` VALUES ('478', '38', '宁晋县', '3', '130528', '50');
INSERT INTO `fly_area` VALUES ('479', '38', '巨鹿县', '3', '130529', '50');
INSERT INTO `fly_area` VALUES ('480', '38', '新河县', '3', '130530', '50');
INSERT INTO `fly_area` VALUES ('481', '38', '广宗县', '3', '130531', '50');
INSERT INTO `fly_area` VALUES ('482', '38', '平乡县', '3', '130532', '50');
INSERT INTO `fly_area` VALUES ('483', '38', '威县', '3', '130533', '50');
INSERT INTO `fly_area` VALUES ('484', '38', '清河县', '3', '130534', '50');
INSERT INTO `fly_area` VALUES ('485', '38', '临西县', '3', '130535', '50');
INSERT INTO `fly_area` VALUES ('486', '38', '南宫市', '3', '130581', '50');
INSERT INTO `fly_area` VALUES ('487', '38', '沙河市', '3', '130582', '50');
INSERT INTO `fly_area` VALUES ('488', '39', '竞秀区', '3', '130602', '50');
INSERT INTO `fly_area` VALUES ('489', '39', '莲池区', '3', '130606', '50');
INSERT INTO `fly_area` VALUES ('490', '39', '满城区', '3', '130607', '50');
INSERT INTO `fly_area` VALUES ('491', '39', '清苑区', '3', '130608', '50');
INSERT INTO `fly_area` VALUES ('492', '39', '徐水区', '3', '130609', '50');
INSERT INTO `fly_area` VALUES ('493', '39', '涞水县', '3', '130623', '50');
INSERT INTO `fly_area` VALUES ('494', '39', '阜平县', '3', '130624', '50');
INSERT INTO `fly_area` VALUES ('495', '39', '定兴县', '3', '130626', '50');
INSERT INTO `fly_area` VALUES ('496', '39', '唐县', '3', '130627', '50');
INSERT INTO `fly_area` VALUES ('497', '39', '高阳县', '3', '130628', '50');
INSERT INTO `fly_area` VALUES ('498', '39', '容城县', '3', '130629', '50');
INSERT INTO `fly_area` VALUES ('499', '39', '涞源县', '3', '130630', '50');
INSERT INTO `fly_area` VALUES ('500', '39', '望都县', '3', '130631', '50');
INSERT INTO `fly_area` VALUES ('501', '39', '安新县', '3', '130632', '50');
INSERT INTO `fly_area` VALUES ('502', '39', '易县', '3', '130633', '50');
INSERT INTO `fly_area` VALUES ('503', '39', '曲阳县', '3', '130634', '50');
INSERT INTO `fly_area` VALUES ('504', '39', '蠡县', '3', '130635', '50');
INSERT INTO `fly_area` VALUES ('505', '39', '顺平县', '3', '130636', '50');
INSERT INTO `fly_area` VALUES ('506', '39', '博野县', '3', '130637', '50');
INSERT INTO `fly_area` VALUES ('507', '39', '雄县', '3', '130638', '50');
INSERT INTO `fly_area` VALUES ('508', '39', '涿州市', '3', '130681', '50');
INSERT INTO `fly_area` VALUES ('509', '39', '安国市', '3', '130683', '50');
INSERT INTO `fly_area` VALUES ('510', '39', '高碑店市', '3', '130684', '50');
INSERT INTO `fly_area` VALUES ('511', '40', '桥东区', '3', '130702', '50');
INSERT INTO `fly_area` VALUES ('512', '40', '桥西区', '3', '130703', '50');
INSERT INTO `fly_area` VALUES ('513', '40', '宣化区', '3', '130705', '50');
INSERT INTO `fly_area` VALUES ('514', '40', '下花园区', '3', '130706', '50');
INSERT INTO `fly_area` VALUES ('515', '40', '万全区', '3', '130708', '50');
INSERT INTO `fly_area` VALUES ('516', '40', '崇礼区', '3', '130709', '50');
INSERT INTO `fly_area` VALUES ('517', '40', '张北县', '3', '130722', '50');
INSERT INTO `fly_area` VALUES ('518', '40', '康保县', '3', '130723', '50');
INSERT INTO `fly_area` VALUES ('519', '40', '沽源县', '3', '130724', '50');
INSERT INTO `fly_area` VALUES ('520', '40', '尚义县', '3', '130725', '50');
INSERT INTO `fly_area` VALUES ('521', '40', '蔚县', '3', '130726', '50');
INSERT INTO `fly_area` VALUES ('522', '40', '阳原县', '3', '130727', '50');
INSERT INTO `fly_area` VALUES ('523', '40', '怀安县', '3', '130728', '50');
INSERT INTO `fly_area` VALUES ('524', '40', '怀来县', '3', '130730', '50');
INSERT INTO `fly_area` VALUES ('525', '40', '涿鹿县', '3', '130731', '50');
INSERT INTO `fly_area` VALUES ('526', '40', '赤城县', '3', '130732', '50');
INSERT INTO `fly_area` VALUES ('527', '41', '双桥区', '3', '130802', '50');
INSERT INTO `fly_area` VALUES ('528', '41', '双滦区', '3', '130803', '50');
INSERT INTO `fly_area` VALUES ('529', '41', '鹰手营子矿区', '3', '130804', '50');
INSERT INTO `fly_area` VALUES ('530', '41', '承德县', '3', '130821', '50');
INSERT INTO `fly_area` VALUES ('531', '41', '兴隆县', '3', '130822', '50');
INSERT INTO `fly_area` VALUES ('532', '41', '平泉县', '3', '130823', '50');
INSERT INTO `fly_area` VALUES ('533', '41', '滦平县', '3', '130824', '50');
INSERT INTO `fly_area` VALUES ('534', '41', '隆化县', '3', '130825', '50');
INSERT INTO `fly_area` VALUES ('535', '41', '丰宁满族自治县', '3', '130826', '50');
INSERT INTO `fly_area` VALUES ('536', '41', '宽城满族自治县', '3', '130827', '50');
INSERT INTO `fly_area` VALUES ('537', '41', '围场满族蒙古族自治县', '3', '130828', '50');
INSERT INTO `fly_area` VALUES ('538', '42', '新华区', '3', '130902', '50');
INSERT INTO `fly_area` VALUES ('539', '42', '运河区', '3', '130903', '50');
INSERT INTO `fly_area` VALUES ('540', '42', '沧县', '3', '130921', '50');
INSERT INTO `fly_area` VALUES ('541', '42', '青县', '3', '130922', '50');
INSERT INTO `fly_area` VALUES ('542', '42', '东光县', '3', '130923', '50');
INSERT INTO `fly_area` VALUES ('543', '42', '海兴县', '3', '130924', '50');
INSERT INTO `fly_area` VALUES ('544', '42', '盐山县', '3', '130925', '50');
INSERT INTO `fly_area` VALUES ('545', '42', '肃宁县', '3', '130926', '50');
INSERT INTO `fly_area` VALUES ('546', '42', '南皮县', '3', '130927', '50');
INSERT INTO `fly_area` VALUES ('547', '42', '吴桥县', '3', '130928', '50');
INSERT INTO `fly_area` VALUES ('548', '42', '献县', '3', '130929', '50');
INSERT INTO `fly_area` VALUES ('549', '42', '孟村回族自治县', '3', '130930', '50');
INSERT INTO `fly_area` VALUES ('550', '42', '泊头市', '3', '130981', '50');
INSERT INTO `fly_area` VALUES ('551', '42', '任丘市', '3', '130982', '50');
INSERT INTO `fly_area` VALUES ('552', '42', '黄骅市', '3', '130983', '50');
INSERT INTO `fly_area` VALUES ('553', '42', '河间市', '3', '130984', '50');
INSERT INTO `fly_area` VALUES ('554', '43', '安次区', '3', '131002', '50');
INSERT INTO `fly_area` VALUES ('555', '43', '广阳区', '3', '131003', '50');
INSERT INTO `fly_area` VALUES ('556', '43', '固安县', '3', '131022', '50');
INSERT INTO `fly_area` VALUES ('557', '43', '永清县', '3', '131023', '50');
INSERT INTO `fly_area` VALUES ('558', '43', '香河县', '3', '131024', '50');
INSERT INTO `fly_area` VALUES ('559', '43', '大城县', '3', '131025', '50');
INSERT INTO `fly_area` VALUES ('560', '43', '文安县', '3', '131026', '50');
INSERT INTO `fly_area` VALUES ('561', '43', '大厂回族自治县', '3', '131028', '50');
INSERT INTO `fly_area` VALUES ('562', '43', '霸州市', '3', '131081', '50');
INSERT INTO `fly_area` VALUES ('563', '43', '三河市', '3', '131082', '50');
INSERT INTO `fly_area` VALUES ('564', '44', '桃城区', '3', '131102', '50');
INSERT INTO `fly_area` VALUES ('565', '44', '冀州区', '3', '131103', '50');
INSERT INTO `fly_area` VALUES ('566', '44', '枣强县', '3', '131121', '50');
INSERT INTO `fly_area` VALUES ('567', '44', '武邑县', '3', '131122', '50');
INSERT INTO `fly_area` VALUES ('568', '44', '武强县', '3', '131123', '50');
INSERT INTO `fly_area` VALUES ('569', '44', '饶阳县', '3', '131124', '50');
INSERT INTO `fly_area` VALUES ('570', '44', '安平县', '3', '131125', '50');
INSERT INTO `fly_area` VALUES ('571', '44', '故城县', '3', '131126', '50');
INSERT INTO `fly_area` VALUES ('572', '44', '景县', '3', '131127', '50');
INSERT INTO `fly_area` VALUES ('573', '44', '阜城县', '3', '131128', '50');
INSERT INTO `fly_area` VALUES ('574', '44', '深州市', '3', '131182', '50');
INSERT INTO `fly_area` VALUES ('575', '45', '定州市', '3', '139001', '50');
INSERT INTO `fly_area` VALUES ('576', '45', '辛集市', '3', '139002', '50');
INSERT INTO `fly_area` VALUES ('577', '46', '小店区', '3', '140105', '50');
INSERT INTO `fly_area` VALUES ('578', '46', '迎泽区', '3', '140106', '50');
INSERT INTO `fly_area` VALUES ('579', '46', '杏花岭区', '3', '140107', '50');
INSERT INTO `fly_area` VALUES ('580', '46', '尖草坪区', '3', '140108', '50');
INSERT INTO `fly_area` VALUES ('581', '46', '万柏林区', '3', '140109', '50');
INSERT INTO `fly_area` VALUES ('582', '46', '晋源区', '3', '140110', '50');
INSERT INTO `fly_area` VALUES ('583', '46', '清徐县', '3', '140121', '50');
INSERT INTO `fly_area` VALUES ('584', '46', '阳曲县', '3', '140122', '50');
INSERT INTO `fly_area` VALUES ('585', '46', '娄烦县', '3', '140123', '50');
INSERT INTO `fly_area` VALUES ('586', '46', '古交市', '3', '140181', '50');
INSERT INTO `fly_area` VALUES ('587', '47', '城区', '3', '140202', '50');
INSERT INTO `fly_area` VALUES ('588', '47', '矿区', '3', '140203', '50');
INSERT INTO `fly_area` VALUES ('589', '47', '南郊区', '3', '140211', '50');
INSERT INTO `fly_area` VALUES ('590', '47', '新荣区', '3', '140212', '50');
INSERT INTO `fly_area` VALUES ('591', '47', '阳高县', '3', '140221', '50');
INSERT INTO `fly_area` VALUES ('592', '47', '天镇县', '3', '140222', '50');
INSERT INTO `fly_area` VALUES ('593', '47', '广灵县', '3', '140223', '50');
INSERT INTO `fly_area` VALUES ('594', '47', '灵丘县', '3', '140224', '50');
INSERT INTO `fly_area` VALUES ('595', '47', '浑源县', '3', '140225', '50');
INSERT INTO `fly_area` VALUES ('596', '47', '左云县', '3', '140226', '50');
INSERT INTO `fly_area` VALUES ('597', '47', '大同县', '3', '140227', '50');
INSERT INTO `fly_area` VALUES ('598', '48', '城区', '3', '140302', '50');
INSERT INTO `fly_area` VALUES ('599', '48', '矿区', '3', '140303', '50');
INSERT INTO `fly_area` VALUES ('600', '48', '郊区', '3', '140311', '50');
INSERT INTO `fly_area` VALUES ('601', '48', '平定县', '3', '140321', '50');
INSERT INTO `fly_area` VALUES ('602', '48', '盂县', '3', '140322', '50');
INSERT INTO `fly_area` VALUES ('603', '49', '城区', '3', '140402', '50');
INSERT INTO `fly_area` VALUES ('604', '49', '郊区', '3', '140411', '50');
INSERT INTO `fly_area` VALUES ('605', '49', '长治县', '3', '140421', '50');
INSERT INTO `fly_area` VALUES ('606', '49', '襄垣县', '3', '140423', '50');
INSERT INTO `fly_area` VALUES ('607', '49', '屯留县', '3', '140424', '50');
INSERT INTO `fly_area` VALUES ('608', '49', '平顺县', '3', '140425', '50');
INSERT INTO `fly_area` VALUES ('609', '49', '黎城县', '3', '140426', '50');
INSERT INTO `fly_area` VALUES ('610', '49', '壶关县', '3', '140427', '50');
INSERT INTO `fly_area` VALUES ('611', '49', '长子县', '3', '140428', '50');
INSERT INTO `fly_area` VALUES ('612', '49', '武乡县', '3', '140429', '50');
INSERT INTO `fly_area` VALUES ('613', '49', '沁县', '3', '140430', '50');
INSERT INTO `fly_area` VALUES ('614', '49', '沁源县', '3', '140431', '50');
INSERT INTO `fly_area` VALUES ('615', '49', '潞城市', '3', '140481', '50');
INSERT INTO `fly_area` VALUES ('616', '50', '城区', '3', '140502', '50');
INSERT INTO `fly_area` VALUES ('617', '50', '沁水县', '3', '140521', '50');
INSERT INTO `fly_area` VALUES ('618', '50', '阳城县', '3', '140522', '50');
INSERT INTO `fly_area` VALUES ('619', '50', '陵川县', '3', '140524', '50');
INSERT INTO `fly_area` VALUES ('620', '50', '泽州县', '3', '140525', '50');
INSERT INTO `fly_area` VALUES ('621', '50', '高平市', '3', '140581', '50');
INSERT INTO `fly_area` VALUES ('622', '51', '朔城区', '3', '140602', '50');
INSERT INTO `fly_area` VALUES ('623', '51', '平鲁区', '3', '140603', '50');
INSERT INTO `fly_area` VALUES ('624', '51', '山阴县', '3', '140621', '50');
INSERT INTO `fly_area` VALUES ('625', '51', '应县', '3', '140622', '50');
INSERT INTO `fly_area` VALUES ('626', '51', '右玉县', '3', '140623', '50');
INSERT INTO `fly_area` VALUES ('627', '51', '怀仁县', '3', '140624', '50');
INSERT INTO `fly_area` VALUES ('628', '52', '榆次区', '3', '140702', '50');
INSERT INTO `fly_area` VALUES ('629', '52', '榆社县', '3', '140721', '50');
INSERT INTO `fly_area` VALUES ('630', '52', '左权县', '3', '140722', '50');
INSERT INTO `fly_area` VALUES ('631', '52', '和顺县', '3', '140723', '50');
INSERT INTO `fly_area` VALUES ('632', '52', '昔阳县', '3', '140724', '50');
INSERT INTO `fly_area` VALUES ('633', '52', '寿阳县', '3', '140725', '50');
INSERT INTO `fly_area` VALUES ('634', '52', '太谷县', '3', '140726', '50');
INSERT INTO `fly_area` VALUES ('635', '52', '祁县', '3', '140727', '50');
INSERT INTO `fly_area` VALUES ('636', '52', '平遥县', '3', '140728', '50');
INSERT INTO `fly_area` VALUES ('637', '52', '灵石县', '3', '140729', '50');
INSERT INTO `fly_area` VALUES ('638', '52', '介休市', '3', '140781', '50');
INSERT INTO `fly_area` VALUES ('639', '53', '盐湖区', '3', '140802', '50');
INSERT INTO `fly_area` VALUES ('640', '53', '临猗县', '3', '140821', '50');
INSERT INTO `fly_area` VALUES ('641', '53', '万荣县', '3', '140822', '50');
INSERT INTO `fly_area` VALUES ('642', '53', '闻喜县', '3', '140823', '50');
INSERT INTO `fly_area` VALUES ('643', '53', '稷山县', '3', '140824', '50');
INSERT INTO `fly_area` VALUES ('644', '53', '新绛县', '3', '140825', '50');
INSERT INTO `fly_area` VALUES ('645', '53', '绛县', '3', '140826', '50');
INSERT INTO `fly_area` VALUES ('646', '53', '垣曲县', '3', '140827', '50');
INSERT INTO `fly_area` VALUES ('647', '53', '夏县', '3', '140828', '50');
INSERT INTO `fly_area` VALUES ('648', '53', '平陆县', '3', '140829', '50');
INSERT INTO `fly_area` VALUES ('649', '53', '芮城县', '3', '140830', '50');
INSERT INTO `fly_area` VALUES ('650', '53', '永济市', '3', '140881', '50');
INSERT INTO `fly_area` VALUES ('651', '53', '河津市', '3', '140882', '50');
INSERT INTO `fly_area` VALUES ('652', '54', '忻府区', '3', '140902', '50');
INSERT INTO `fly_area` VALUES ('653', '54', '定襄县', '3', '140921', '50');
INSERT INTO `fly_area` VALUES ('654', '54', '五台县', '3', '140922', '50');
INSERT INTO `fly_area` VALUES ('655', '54', '代县', '3', '140923', '50');
INSERT INTO `fly_area` VALUES ('656', '54', '繁峙县', '3', '140924', '50');
INSERT INTO `fly_area` VALUES ('657', '54', '宁武县', '3', '140925', '50');
INSERT INTO `fly_area` VALUES ('658', '54', '静乐县', '3', '140926', '50');
INSERT INTO `fly_area` VALUES ('659', '54', '神池县', '3', '140927', '50');
INSERT INTO `fly_area` VALUES ('660', '54', '五寨县', '3', '140928', '50');
INSERT INTO `fly_area` VALUES ('661', '54', '岢岚县', '3', '140929', '50');
INSERT INTO `fly_area` VALUES ('662', '54', '河曲县', '3', '140930', '50');
INSERT INTO `fly_area` VALUES ('663', '54', '保德县', '3', '140931', '50');
INSERT INTO `fly_area` VALUES ('664', '54', '偏关县', '3', '140932', '50');
INSERT INTO `fly_area` VALUES ('665', '54', '原平市', '3', '140981', '50');
INSERT INTO `fly_area` VALUES ('666', '55', '尧都区', '3', '141002', '50');
INSERT INTO `fly_area` VALUES ('667', '55', '曲沃县', '3', '141021', '50');
INSERT INTO `fly_area` VALUES ('668', '55', '翼城县', '3', '141022', '50');
INSERT INTO `fly_area` VALUES ('669', '55', '襄汾县', '3', '141023', '50');
INSERT INTO `fly_area` VALUES ('670', '55', '洪洞县', '3', '141024', '50');
INSERT INTO `fly_area` VALUES ('671', '55', '古县', '3', '141025', '50');
INSERT INTO `fly_area` VALUES ('672', '55', '安泽县', '3', '141026', '50');
INSERT INTO `fly_area` VALUES ('673', '55', '浮山县', '3', '141027', '50');
INSERT INTO `fly_area` VALUES ('674', '55', '吉县', '3', '141028', '50');
INSERT INTO `fly_area` VALUES ('675', '55', '乡宁县', '3', '141029', '50');
INSERT INTO `fly_area` VALUES ('676', '55', '大宁县', '3', '141030', '50');
INSERT INTO `fly_area` VALUES ('677', '55', '隰县', '3', '141031', '50');
INSERT INTO `fly_area` VALUES ('678', '55', '永和县', '3', '141032', '50');
INSERT INTO `fly_area` VALUES ('679', '55', '蒲县', '3', '141033', '50');
INSERT INTO `fly_area` VALUES ('680', '55', '汾西县', '3', '141034', '50');
INSERT INTO `fly_area` VALUES ('681', '55', '侯马市', '3', '141081', '50');
INSERT INTO `fly_area` VALUES ('682', '55', '霍州市', '3', '141082', '50');
INSERT INTO `fly_area` VALUES ('683', '56', '离石区', '3', '141102', '50');
INSERT INTO `fly_area` VALUES ('684', '56', '文水县', '3', '141121', '50');
INSERT INTO `fly_area` VALUES ('685', '56', '交城县', '3', '141122', '50');
INSERT INTO `fly_area` VALUES ('686', '56', '兴县', '3', '141123', '50');
INSERT INTO `fly_area` VALUES ('687', '56', '临县', '3', '141124', '50');
INSERT INTO `fly_area` VALUES ('688', '56', '柳林县', '3', '141125', '50');
INSERT INTO `fly_area` VALUES ('689', '56', '石楼县', '3', '141126', '50');
INSERT INTO `fly_area` VALUES ('690', '56', '岚县', '3', '141127', '50');
INSERT INTO `fly_area` VALUES ('691', '56', '方山县', '3', '141128', '50');
INSERT INTO `fly_area` VALUES ('692', '56', '中阳县', '3', '141129', '50');
INSERT INTO `fly_area` VALUES ('693', '56', '交口县', '3', '141130', '50');
INSERT INTO `fly_area` VALUES ('694', '56', '孝义市', '3', '141181', '50');
INSERT INTO `fly_area` VALUES ('695', '56', '汾阳市', '3', '141182', '50');
INSERT INTO `fly_area` VALUES ('696', '57', '新城区', '3', '150102', '50');
INSERT INTO `fly_area` VALUES ('697', '57', '回民区', '3', '150103', '50');
INSERT INTO `fly_area` VALUES ('698', '57', '玉泉区', '3', '150104', '50');
INSERT INTO `fly_area` VALUES ('699', '57', '赛罕区', '3', '150105', '50');
INSERT INTO `fly_area` VALUES ('700', '57', '土默特左旗', '3', '150121', '50');
INSERT INTO `fly_area` VALUES ('701', '57', '托克托县', '3', '150122', '50');
INSERT INTO `fly_area` VALUES ('702', '57', '和林格尔县', '3', '150123', '50');
INSERT INTO `fly_area` VALUES ('703', '57', '清水河县', '3', '150124', '50');
INSERT INTO `fly_area` VALUES ('704', '57', '武川县', '3', '150125', '50');
INSERT INTO `fly_area` VALUES ('705', '58', '东河区', '3', '150202', '50');
INSERT INTO `fly_area` VALUES ('706', '58', '昆都仑区', '3', '150203', '50');
INSERT INTO `fly_area` VALUES ('707', '58', '青山区', '3', '150204', '50');
INSERT INTO `fly_area` VALUES ('708', '58', '石拐区', '3', '150205', '50');
INSERT INTO `fly_area` VALUES ('709', '58', '白云鄂博矿区', '3', '150206', '50');
INSERT INTO `fly_area` VALUES ('710', '58', '九原区', '3', '150207', '50');
INSERT INTO `fly_area` VALUES ('711', '58', '土默特右旗', '3', '150221', '50');
INSERT INTO `fly_area` VALUES ('712', '58', '固阳县', '3', '150222', '50');
INSERT INTO `fly_area` VALUES ('713', '58', '达尔罕茂明安联合旗', '3', '150223', '50');
INSERT INTO `fly_area` VALUES ('714', '59', '海勃湾区', '3', '150302', '50');
INSERT INTO `fly_area` VALUES ('715', '59', '海南区', '3', '150303', '50');
INSERT INTO `fly_area` VALUES ('716', '59', '乌达区', '3', '150304', '50');
INSERT INTO `fly_area` VALUES ('717', '60', '红山区', '3', '150402', '50');
INSERT INTO `fly_area` VALUES ('718', '60', '元宝山区', '3', '150403', '50');
INSERT INTO `fly_area` VALUES ('719', '60', '松山区', '3', '150404', '50');
INSERT INTO `fly_area` VALUES ('720', '60', '阿鲁科尔沁旗', '3', '150421', '50');
INSERT INTO `fly_area` VALUES ('721', '60', '巴林左旗', '3', '150422', '50');
INSERT INTO `fly_area` VALUES ('722', '60', '巴林右旗', '3', '150423', '50');
INSERT INTO `fly_area` VALUES ('723', '60', '林西县', '3', '150424', '50');
INSERT INTO `fly_area` VALUES ('724', '60', '克什克腾旗', '3', '150425', '50');
INSERT INTO `fly_area` VALUES ('725', '60', '翁牛特旗', '3', '150426', '50');
INSERT INTO `fly_area` VALUES ('726', '60', '喀喇沁旗', '3', '150428', '50');
INSERT INTO `fly_area` VALUES ('727', '60', '宁城县', '3', '150429', '50');
INSERT INTO `fly_area` VALUES ('728', '60', '敖汉旗', '3', '150430', '50');
INSERT INTO `fly_area` VALUES ('729', '61', '科尔沁区', '3', '150502', '50');
INSERT INTO `fly_area` VALUES ('730', '61', '科尔沁左翼中旗', '3', '150521', '50');
INSERT INTO `fly_area` VALUES ('731', '61', '科尔沁左翼后旗', '3', '150522', '50');
INSERT INTO `fly_area` VALUES ('732', '61', '开鲁县', '3', '150523', '50');
INSERT INTO `fly_area` VALUES ('733', '61', '库伦旗', '3', '150524', '50');
INSERT INTO `fly_area` VALUES ('734', '61', '奈曼旗', '3', '150525', '50');
INSERT INTO `fly_area` VALUES ('735', '61', '扎鲁特旗', '3', '150526', '50');
INSERT INTO `fly_area` VALUES ('736', '61', '霍林郭勒市', '3', '150581', '50');
INSERT INTO `fly_area` VALUES ('737', '62', '东胜区', '3', '150602', '50');
INSERT INTO `fly_area` VALUES ('738', '62', '康巴什区', '3', '150603', '50');
INSERT INTO `fly_area` VALUES ('739', '62', '达拉特旗', '3', '150621', '50');
INSERT INTO `fly_area` VALUES ('740', '62', '准格尔旗', '3', '150622', '50');
INSERT INTO `fly_area` VALUES ('741', '62', '鄂托克前旗', '3', '150623', '50');
INSERT INTO `fly_area` VALUES ('742', '62', '鄂托克旗', '3', '150624', '50');
INSERT INTO `fly_area` VALUES ('743', '62', '杭锦旗', '3', '150625', '50');
INSERT INTO `fly_area` VALUES ('744', '62', '乌审旗', '3', '150626', '50');
INSERT INTO `fly_area` VALUES ('745', '62', '伊金霍洛旗', '3', '150627', '50');
INSERT INTO `fly_area` VALUES ('746', '63', '海拉尔区', '3', '150702', '50');
INSERT INTO `fly_area` VALUES ('747', '63', '扎赉诺尔区', '3', '150703', '50');
INSERT INTO `fly_area` VALUES ('748', '63', '阿荣旗', '3', '150721', '50');
INSERT INTO `fly_area` VALUES ('749', '63', '莫力达瓦达斡尔族自治旗', '3', '150722', '50');
INSERT INTO `fly_area` VALUES ('750', '63', '鄂伦春自治旗', '3', '150723', '50');
INSERT INTO `fly_area` VALUES ('751', '63', '鄂温克族自治旗', '3', '150724', '50');
INSERT INTO `fly_area` VALUES ('752', '63', '陈巴尔虎旗', '3', '150725', '50');
INSERT INTO `fly_area` VALUES ('753', '63', '新巴尔虎左旗', '3', '150726', '50');
INSERT INTO `fly_area` VALUES ('754', '63', '新巴尔虎右旗', '3', '150727', '50');
INSERT INTO `fly_area` VALUES ('755', '63', '满洲里市', '3', '150781', '50');
INSERT INTO `fly_area` VALUES ('756', '63', '牙克石市', '3', '150782', '50');
INSERT INTO `fly_area` VALUES ('757', '63', '扎兰屯市', '3', '150783', '50');
INSERT INTO `fly_area` VALUES ('758', '63', '额尔古纳市', '3', '150784', '50');
INSERT INTO `fly_area` VALUES ('759', '63', '根河市', '3', '150785', '50');
INSERT INTO `fly_area` VALUES ('760', '64', '临河区', '3', '150802', '50');
INSERT INTO `fly_area` VALUES ('761', '64', '五原县', '3', '150821', '50');
INSERT INTO `fly_area` VALUES ('762', '64', '磴口县', '3', '150822', '50');
INSERT INTO `fly_area` VALUES ('763', '64', '乌拉特前旗', '3', '150823', '50');
INSERT INTO `fly_area` VALUES ('764', '64', '乌拉特中旗', '3', '150824', '50');
INSERT INTO `fly_area` VALUES ('765', '64', '乌拉特后旗', '3', '150825', '50');
INSERT INTO `fly_area` VALUES ('766', '64', '杭锦后旗', '3', '150826', '50');
INSERT INTO `fly_area` VALUES ('767', '65', '集宁区', '3', '150902', '50');
INSERT INTO `fly_area` VALUES ('768', '65', '卓资县', '3', '150921', '50');
INSERT INTO `fly_area` VALUES ('769', '65', '化德县', '3', '150922', '50');
INSERT INTO `fly_area` VALUES ('770', '65', '商都县', '3', '150923', '50');
INSERT INTO `fly_area` VALUES ('771', '65', '兴和县', '3', '150924', '50');
INSERT INTO `fly_area` VALUES ('772', '65', '凉城县', '3', '150925', '50');
INSERT INTO `fly_area` VALUES ('773', '65', '察哈尔右翼前旗', '3', '150926', '50');
INSERT INTO `fly_area` VALUES ('774', '65', '察哈尔右翼中旗', '3', '150927', '50');
INSERT INTO `fly_area` VALUES ('775', '65', '察哈尔右翼后旗', '3', '150928', '50');
INSERT INTO `fly_area` VALUES ('776', '65', '四子王旗', '3', '150929', '50');
INSERT INTO `fly_area` VALUES ('777', '65', '丰镇市', '3', '150981', '50');
INSERT INTO `fly_area` VALUES ('778', '66', '乌兰浩特市', '3', '152201', '50');
INSERT INTO `fly_area` VALUES ('779', '66', '阿尔山市', '3', '152202', '50');
INSERT INTO `fly_area` VALUES ('780', '66', '科尔沁右翼前旗', '3', '152221', '50');
INSERT INTO `fly_area` VALUES ('781', '66', '科尔沁右翼中旗', '3', '152222', '50');
INSERT INTO `fly_area` VALUES ('782', '66', '扎赉特旗', '3', '152223', '50');
INSERT INTO `fly_area` VALUES ('783', '66', '突泉县', '3', '152224', '50');
INSERT INTO `fly_area` VALUES ('784', '67', '二连浩特市', '3', '152501', '50');
INSERT INTO `fly_area` VALUES ('785', '67', '锡林浩特市', '3', '152502', '50');
INSERT INTO `fly_area` VALUES ('786', '67', '阿巴嘎旗', '3', '152522', '50');
INSERT INTO `fly_area` VALUES ('787', '67', '苏尼特左旗', '3', '152523', '50');
INSERT INTO `fly_area` VALUES ('788', '67', '苏尼特右旗', '3', '152524', '50');
INSERT INTO `fly_area` VALUES ('789', '67', '东乌珠穆沁旗', '3', '152525', '50');
INSERT INTO `fly_area` VALUES ('790', '67', '西乌珠穆沁旗', '3', '152526', '50');
INSERT INTO `fly_area` VALUES ('791', '67', '太仆寺旗', '3', '152527', '50');
INSERT INTO `fly_area` VALUES ('792', '67', '镶黄旗', '3', '152528', '50');
INSERT INTO `fly_area` VALUES ('793', '67', '正镶白旗', '3', '152529', '50');
INSERT INTO `fly_area` VALUES ('794', '67', '正蓝旗', '3', '152530', '50');
INSERT INTO `fly_area` VALUES ('795', '67', '多伦县', '3', '152531', '50');
INSERT INTO `fly_area` VALUES ('796', '68', '阿拉善左旗', '3', '152921', '50');
INSERT INTO `fly_area` VALUES ('797', '68', '阿拉善右旗', '3', '152922', '50');
INSERT INTO `fly_area` VALUES ('798', '68', '额济纳旗', '3', '152923', '50');
INSERT INTO `fly_area` VALUES ('799', '69', '和平区', '3', '210102', '50');
INSERT INTO `fly_area` VALUES ('800', '69', '沈河区', '3', '210103', '50');
INSERT INTO `fly_area` VALUES ('801', '69', '大东区', '3', '210104', '50');
INSERT INTO `fly_area` VALUES ('802', '69', '皇姑区', '3', '210105', '50');
INSERT INTO `fly_area` VALUES ('803', '69', '铁西区', '3', '210106', '50');
INSERT INTO `fly_area` VALUES ('804', '69', '苏家屯区', '3', '210111', '50');
INSERT INTO `fly_area` VALUES ('805', '69', '浑南区', '3', '210112', '50');
INSERT INTO `fly_area` VALUES ('806', '69', '沈北新区', '3', '210113', '50');
INSERT INTO `fly_area` VALUES ('807', '69', '于洪区', '3', '210114', '50');
INSERT INTO `fly_area` VALUES ('808', '69', '辽中区', '3', '210115', '50');
INSERT INTO `fly_area` VALUES ('809', '69', '康平县', '3', '210123', '50');
INSERT INTO `fly_area` VALUES ('810', '69', '法库县', '3', '210124', '50');
INSERT INTO `fly_area` VALUES ('811', '69', '新民市', '3', '210181', '50');
INSERT INTO `fly_area` VALUES ('812', '70', '中山区', '3', '210202', '50');
INSERT INTO `fly_area` VALUES ('813', '70', '西岗区', '3', '210203', '50');
INSERT INTO `fly_area` VALUES ('814', '70', '沙河口区', '3', '210204', '50');
INSERT INTO `fly_area` VALUES ('815', '70', '甘井子区', '3', '210211', '50');
INSERT INTO `fly_area` VALUES ('816', '70', '旅顺口区', '3', '210212', '50');
INSERT INTO `fly_area` VALUES ('817', '70', '金州区', '3', '210213', '50');
INSERT INTO `fly_area` VALUES ('818', '70', '普兰店区', '3', '210214', '50');
INSERT INTO `fly_area` VALUES ('819', '70', '长海县', '3', '210224', '50');
INSERT INTO `fly_area` VALUES ('820', '70', '瓦房店市', '3', '210281', '50');
INSERT INTO `fly_area` VALUES ('821', '70', '庄河市', '3', '210283', '50');
INSERT INTO `fly_area` VALUES ('822', '71', '铁东区', '3', '210302', '50');
INSERT INTO `fly_area` VALUES ('823', '71', '铁西区', '3', '210303', '50');
INSERT INTO `fly_area` VALUES ('824', '71', '立山区', '3', '210304', '50');
INSERT INTO `fly_area` VALUES ('825', '71', '千山区', '3', '210311', '50');
INSERT INTO `fly_area` VALUES ('826', '71', '台安县', '3', '210321', '50');
INSERT INTO `fly_area` VALUES ('827', '71', '岫岩满族自治县', '3', '210323', '50');
INSERT INTO `fly_area` VALUES ('828', '71', '海城市', '3', '210381', '50');
INSERT INTO `fly_area` VALUES ('829', '72', '新抚区', '3', '210402', '50');
INSERT INTO `fly_area` VALUES ('830', '72', '东洲区', '3', '210403', '50');
INSERT INTO `fly_area` VALUES ('831', '72', '望花区', '3', '210404', '50');
INSERT INTO `fly_area` VALUES ('832', '72', '顺城区', '3', '210411', '50');
INSERT INTO `fly_area` VALUES ('833', '72', '抚顺县', '3', '210421', '50');
INSERT INTO `fly_area` VALUES ('834', '72', '新宾满族自治县', '3', '210422', '50');
INSERT INTO `fly_area` VALUES ('835', '72', '清原满族自治县', '3', '210423', '50');
INSERT INTO `fly_area` VALUES ('836', '73', '平山区', '3', '210502', '50');
INSERT INTO `fly_area` VALUES ('837', '73', '溪湖区', '3', '210503', '50');
INSERT INTO `fly_area` VALUES ('838', '73', '明山区', '3', '210504', '50');
INSERT INTO `fly_area` VALUES ('839', '73', '南芬区', '3', '210505', '50');
INSERT INTO `fly_area` VALUES ('840', '73', '本溪满族自治县', '3', '210521', '50');
INSERT INTO `fly_area` VALUES ('841', '73', '桓仁满族自治县', '3', '210522', '50');
INSERT INTO `fly_area` VALUES ('842', '74', '元宝区', '3', '210602', '50');
INSERT INTO `fly_area` VALUES ('843', '74', '振兴区', '3', '210603', '50');
INSERT INTO `fly_area` VALUES ('844', '74', '振安区', '3', '210604', '50');
INSERT INTO `fly_area` VALUES ('845', '74', '宽甸满族自治县', '3', '210624', '50');
INSERT INTO `fly_area` VALUES ('846', '74', '东港市', '3', '210681', '50');
INSERT INTO `fly_area` VALUES ('847', '74', '凤城市', '3', '210682', '50');
INSERT INTO `fly_area` VALUES ('848', '75', '古塔区', '3', '210702', '50');
INSERT INTO `fly_area` VALUES ('849', '75', '凌河区', '3', '210703', '50');
INSERT INTO `fly_area` VALUES ('850', '75', '太和区', '3', '210711', '50');
INSERT INTO `fly_area` VALUES ('851', '75', '黑山县', '3', '210726', '50');
INSERT INTO `fly_area` VALUES ('852', '75', '义县', '3', '210727', '50');
INSERT INTO `fly_area` VALUES ('853', '75', '凌海市', '3', '210781', '50');
INSERT INTO `fly_area` VALUES ('854', '75', '北镇市', '3', '210782', '50');
INSERT INTO `fly_area` VALUES ('855', '76', '站前区', '3', '210802', '50');
INSERT INTO `fly_area` VALUES ('856', '76', '西市区', '3', '210803', '50');
INSERT INTO `fly_area` VALUES ('857', '76', '鲅鱼圈区', '3', '210804', '50');
INSERT INTO `fly_area` VALUES ('858', '76', '老边区', '3', '210811', '50');
INSERT INTO `fly_area` VALUES ('859', '76', '盖州市', '3', '210881', '50');
INSERT INTO `fly_area` VALUES ('860', '76', '大石桥市', '3', '210882', '50');
INSERT INTO `fly_area` VALUES ('861', '77', '海州区', '3', '210902', '50');
INSERT INTO `fly_area` VALUES ('862', '77', '新邱区', '3', '210903', '50');
INSERT INTO `fly_area` VALUES ('863', '77', '太平区', '3', '210904', '50');
INSERT INTO `fly_area` VALUES ('864', '77', '清河门区', '3', '210905', '50');
INSERT INTO `fly_area` VALUES ('865', '77', '细河区', '3', '210911', '50');
INSERT INTO `fly_area` VALUES ('866', '77', '阜新蒙古族自治县', '3', '210921', '50');
INSERT INTO `fly_area` VALUES ('867', '77', '彰武县', '3', '210922', '50');
INSERT INTO `fly_area` VALUES ('868', '78', '白塔区', '3', '211002', '50');
INSERT INTO `fly_area` VALUES ('869', '78', '文圣区', '3', '211003', '50');
INSERT INTO `fly_area` VALUES ('870', '78', '宏伟区', '3', '211004', '50');
INSERT INTO `fly_area` VALUES ('871', '78', '弓长岭区', '3', '211005', '50');
INSERT INTO `fly_area` VALUES ('872', '78', '太子河区', '3', '211011', '50');
INSERT INTO `fly_area` VALUES ('873', '78', '辽阳县', '3', '211021', '50');
INSERT INTO `fly_area` VALUES ('874', '78', '灯塔市', '3', '211081', '50');
INSERT INTO `fly_area` VALUES ('875', '79', '双台子区', '3', '211102', '50');
INSERT INTO `fly_area` VALUES ('876', '79', '兴隆台区', '3', '211103', '50');
INSERT INTO `fly_area` VALUES ('877', '79', '大洼区', '3', '211104', '50');
INSERT INTO `fly_area` VALUES ('878', '79', '盘山县', '3', '211122', '50');
INSERT INTO `fly_area` VALUES ('879', '80', '银州区', '3', '211202', '50');
INSERT INTO `fly_area` VALUES ('880', '80', '清河区', '3', '211204', '50');
INSERT INTO `fly_area` VALUES ('881', '80', '铁岭县', '3', '211221', '50');
INSERT INTO `fly_area` VALUES ('882', '80', '西丰县', '3', '211223', '50');
INSERT INTO `fly_area` VALUES ('883', '80', '昌图县', '3', '211224', '50');
INSERT INTO `fly_area` VALUES ('884', '80', '调兵山市', '3', '211281', '50');
INSERT INTO `fly_area` VALUES ('885', '80', '开原市', '3', '211282', '50');
INSERT INTO `fly_area` VALUES ('886', '81', '双塔区', '3', '211302', '50');
INSERT INTO `fly_area` VALUES ('887', '81', '龙城区', '3', '211303', '50');
INSERT INTO `fly_area` VALUES ('888', '81', '朝阳县', '3', '211321', '50');
INSERT INTO `fly_area` VALUES ('889', '81', '建平县', '3', '211322', '50');
INSERT INTO `fly_area` VALUES ('890', '81', '喀喇沁左翼蒙古族自治县', '3', '211324', '50');
INSERT INTO `fly_area` VALUES ('891', '81', '北票市', '3', '211381', '50');
INSERT INTO `fly_area` VALUES ('892', '81', '凌源市', '3', '211382', '50');
INSERT INTO `fly_area` VALUES ('893', '82', '连山区', '3', '211402', '50');
INSERT INTO `fly_area` VALUES ('894', '82', '龙港区', '3', '211403', '50');
INSERT INTO `fly_area` VALUES ('895', '82', '南票区', '3', '211404', '50');
INSERT INTO `fly_area` VALUES ('896', '82', '绥中县', '3', '211421', '50');
INSERT INTO `fly_area` VALUES ('897', '82', '建昌县', '3', '211422', '50');
INSERT INTO `fly_area` VALUES ('898', '82', '兴城市', '3', '211481', '50');
INSERT INTO `fly_area` VALUES ('899', '83', '南关区', '3', '220102', '50');
INSERT INTO `fly_area` VALUES ('900', '83', '宽城区', '3', '220103', '50');
INSERT INTO `fly_area` VALUES ('901', '83', '朝阳区', '3', '220104', '50');
INSERT INTO `fly_area` VALUES ('902', '83', '二道区', '3', '220105', '50');
INSERT INTO `fly_area` VALUES ('903', '83', '绿园区', '3', '220106', '50');
INSERT INTO `fly_area` VALUES ('904', '83', '双阳区', '3', '220112', '50');
INSERT INTO `fly_area` VALUES ('905', '83', '九台区', '3', '220113', '50');
INSERT INTO `fly_area` VALUES ('906', '83', '农安县', '3', '220122', '50');
INSERT INTO `fly_area` VALUES ('907', '83', '榆树市', '3', '220182', '50');
INSERT INTO `fly_area` VALUES ('908', '83', '德惠市', '3', '220183', '50');
INSERT INTO `fly_area` VALUES ('909', '84', '昌邑区', '3', '220202', '50');
INSERT INTO `fly_area` VALUES ('910', '84', '龙潭区', '3', '220203', '50');
INSERT INTO `fly_area` VALUES ('911', '84', '船营区', '3', '220204', '50');
INSERT INTO `fly_area` VALUES ('912', '84', '丰满区', '3', '220211', '50');
INSERT INTO `fly_area` VALUES ('913', '84', '永吉县', '3', '220221', '50');
INSERT INTO `fly_area` VALUES ('914', '84', '蛟河市', '3', '220281', '50');
INSERT INTO `fly_area` VALUES ('915', '84', '桦甸市', '3', '220282', '50');
INSERT INTO `fly_area` VALUES ('916', '84', '舒兰市', '3', '220283', '50');
INSERT INTO `fly_area` VALUES ('917', '84', '磐石市', '3', '220284', '50');
INSERT INTO `fly_area` VALUES ('918', '85', '铁西区', '3', '220302', '50');
INSERT INTO `fly_area` VALUES ('919', '85', '铁东区', '3', '220303', '50');
INSERT INTO `fly_area` VALUES ('920', '85', '梨树县', '3', '220322', '50');
INSERT INTO `fly_area` VALUES ('921', '85', '伊通满族自治县', '3', '220323', '50');
INSERT INTO `fly_area` VALUES ('922', '85', '公主岭市', '3', '220381', '50');
INSERT INTO `fly_area` VALUES ('923', '85', '双辽市', '3', '220382', '50');
INSERT INTO `fly_area` VALUES ('924', '86', '龙山区', '3', '220402', '50');
INSERT INTO `fly_area` VALUES ('925', '86', '西安区', '3', '220403', '50');
INSERT INTO `fly_area` VALUES ('926', '86', '东丰县', '3', '220421', '50');
INSERT INTO `fly_area` VALUES ('927', '86', '东辽县', '3', '220422', '50');
INSERT INTO `fly_area` VALUES ('928', '87', '东昌区', '3', '220502', '50');
INSERT INTO `fly_area` VALUES ('929', '87', '二道江区', '3', '220503', '50');
INSERT INTO `fly_area` VALUES ('930', '87', '通化县', '3', '220521', '50');
INSERT INTO `fly_area` VALUES ('931', '87', '辉南县', '3', '220523', '50');
INSERT INTO `fly_area` VALUES ('932', '87', '柳河县', '3', '220524', '50');
INSERT INTO `fly_area` VALUES ('933', '87', '梅河口市', '3', '220581', '50');
INSERT INTO `fly_area` VALUES ('934', '87', '集安市', '3', '220582', '50');
INSERT INTO `fly_area` VALUES ('935', '88', '浑江区', '3', '220602', '50');
INSERT INTO `fly_area` VALUES ('936', '88', '江源区', '3', '220605', '50');
INSERT INTO `fly_area` VALUES ('937', '88', '抚松县', '3', '220621', '50');
INSERT INTO `fly_area` VALUES ('938', '88', '靖宇县', '3', '220622', '50');
INSERT INTO `fly_area` VALUES ('939', '88', '长白朝鲜族自治县', '3', '220623', '50');
INSERT INTO `fly_area` VALUES ('940', '88', '临江市', '3', '220681', '50');
INSERT INTO `fly_area` VALUES ('941', '89', '宁江区', '3', '220702', '50');
INSERT INTO `fly_area` VALUES ('942', '89', '前郭尔罗斯蒙古族自治县', '3', '220721', '50');
INSERT INTO `fly_area` VALUES ('943', '89', '长岭县', '3', '220722', '50');
INSERT INTO `fly_area` VALUES ('944', '89', '乾安县', '3', '220723', '50');
INSERT INTO `fly_area` VALUES ('945', '89', '扶余市', '3', '220781', '50');
INSERT INTO `fly_area` VALUES ('946', '90', '洮北区', '3', '220802', '50');
INSERT INTO `fly_area` VALUES ('947', '90', '镇赉县', '3', '220821', '50');
INSERT INTO `fly_area` VALUES ('948', '90', '通榆县', '3', '220822', '50');
INSERT INTO `fly_area` VALUES ('949', '90', '洮南市', '3', '220881', '50');
INSERT INTO `fly_area` VALUES ('950', '90', '大安市', '3', '220882', '50');
INSERT INTO `fly_area` VALUES ('951', '91', '延吉市', '3', '222401', '50');
INSERT INTO `fly_area` VALUES ('952', '91', '图们市', '3', '222402', '50');
INSERT INTO `fly_area` VALUES ('953', '91', '敦化市', '3', '222403', '50');
INSERT INTO `fly_area` VALUES ('954', '91', '珲春市', '3', '222404', '50');
INSERT INTO `fly_area` VALUES ('955', '91', '龙井市', '3', '222405', '50');
INSERT INTO `fly_area` VALUES ('956', '91', '和龙市', '3', '222406', '50');
INSERT INTO `fly_area` VALUES ('957', '91', '汪清县', '3', '222424', '50');
INSERT INTO `fly_area` VALUES ('958', '91', '安图县', '3', '222426', '50');
INSERT INTO `fly_area` VALUES ('959', '92', '道里区', '3', '230102', '50');
INSERT INTO `fly_area` VALUES ('960', '92', '南岗区', '3', '230103', '50');
INSERT INTO `fly_area` VALUES ('961', '92', '道外区', '3', '230104', '50');
INSERT INTO `fly_area` VALUES ('962', '92', '平房区', '3', '230108', '50');
INSERT INTO `fly_area` VALUES ('963', '92', '松北区', '3', '230109', '50');
INSERT INTO `fly_area` VALUES ('964', '92', '香坊区', '3', '230110', '50');
INSERT INTO `fly_area` VALUES ('965', '92', '呼兰区', '3', '230111', '50');
INSERT INTO `fly_area` VALUES ('966', '92', '阿城区', '3', '230112', '50');
INSERT INTO `fly_area` VALUES ('967', '92', '双城区', '3', '230113', '50');
INSERT INTO `fly_area` VALUES ('968', '92', '依兰县', '3', '230123', '50');
INSERT INTO `fly_area` VALUES ('969', '92', '方正县', '3', '230124', '50');
INSERT INTO `fly_area` VALUES ('970', '92', '宾县', '3', '230125', '50');
INSERT INTO `fly_area` VALUES ('971', '92', '巴彦县', '3', '230126', '50');
INSERT INTO `fly_area` VALUES ('972', '92', '木兰县', '3', '230127', '50');
INSERT INTO `fly_area` VALUES ('973', '92', '通河县', '3', '230128', '50');
INSERT INTO `fly_area` VALUES ('974', '92', '延寿县', '3', '230129', '50');
INSERT INTO `fly_area` VALUES ('975', '92', '尚志市', '3', '230183', '50');
INSERT INTO `fly_area` VALUES ('976', '92', '五常市', '3', '230184', '50');
INSERT INTO `fly_area` VALUES ('977', '93', '龙沙区', '3', '230202', '50');
INSERT INTO `fly_area` VALUES ('978', '93', '建华区', '3', '230203', '50');
INSERT INTO `fly_area` VALUES ('979', '93', '铁锋区', '3', '230204', '50');
INSERT INTO `fly_area` VALUES ('980', '93', '昂昂溪区', '3', '230205', '50');
INSERT INTO `fly_area` VALUES ('981', '93', '富拉尔基区', '3', '230206', '50');
INSERT INTO `fly_area` VALUES ('982', '93', '碾子山区', '3', '230207', '50');
INSERT INTO `fly_area` VALUES ('983', '93', '梅里斯达斡尔族区', '3', '230208', '50');
INSERT INTO `fly_area` VALUES ('984', '93', '龙江县', '3', '230221', '50');
INSERT INTO `fly_area` VALUES ('985', '93', '依安县', '3', '230223', '50');
INSERT INTO `fly_area` VALUES ('986', '93', '泰来县', '3', '230224', '50');
INSERT INTO `fly_area` VALUES ('987', '93', '甘南县', '3', '230225', '50');
INSERT INTO `fly_area` VALUES ('988', '93', '富裕县', '3', '230227', '50');
INSERT INTO `fly_area` VALUES ('989', '93', '克山县', '3', '230229', '50');
INSERT INTO `fly_area` VALUES ('990', '93', '克东县', '3', '230230', '50');
INSERT INTO `fly_area` VALUES ('991', '93', '拜泉县', '3', '230231', '50');
INSERT INTO `fly_area` VALUES ('992', '93', '讷河市', '3', '230281', '50');
INSERT INTO `fly_area` VALUES ('993', '94', '鸡冠区', '3', '230302', '50');
INSERT INTO `fly_area` VALUES ('994', '94', '恒山区', '3', '230303', '50');
INSERT INTO `fly_area` VALUES ('995', '94', '滴道区', '3', '230304', '50');
INSERT INTO `fly_area` VALUES ('996', '94', '梨树区', '3', '230305', '50');
INSERT INTO `fly_area` VALUES ('997', '94', '城子河区', '3', '230306', '50');
INSERT INTO `fly_area` VALUES ('998', '94', '麻山区', '3', '230307', '50');
INSERT INTO `fly_area` VALUES ('999', '94', '鸡东县', '3', '230321', '50');
INSERT INTO `fly_area` VALUES ('1000', '94', '虎林市', '3', '230381', '50');
INSERT INTO `fly_area` VALUES ('1001', '94', '密山市', '3', '230382', '50');
INSERT INTO `fly_area` VALUES ('1002', '95', '向阳区', '3', '230402', '50');
INSERT INTO `fly_area` VALUES ('1003', '95', '工农区', '3', '230403', '50');
INSERT INTO `fly_area` VALUES ('1004', '95', '南山区', '3', '230404', '50');
INSERT INTO `fly_area` VALUES ('1005', '95', '兴安区', '3', '230405', '50');
INSERT INTO `fly_area` VALUES ('1006', '95', '东山区', '3', '230406', '50');
INSERT INTO `fly_area` VALUES ('1007', '95', '兴山区', '3', '230407', '50');
INSERT INTO `fly_area` VALUES ('1008', '95', '萝北县', '3', '230421', '50');
INSERT INTO `fly_area` VALUES ('1009', '95', '绥滨县', '3', '230422', '50');
INSERT INTO `fly_area` VALUES ('1010', '96', '尖山区', '3', '230502', '50');
INSERT INTO `fly_area` VALUES ('1011', '96', '岭东区', '3', '230503', '50');
INSERT INTO `fly_area` VALUES ('1012', '96', '四方台区', '3', '230505', '50');
INSERT INTO `fly_area` VALUES ('1013', '96', '宝山区', '3', '230506', '50');
INSERT INTO `fly_area` VALUES ('1014', '96', '集贤县', '3', '230521', '50');
INSERT INTO `fly_area` VALUES ('1015', '96', '友谊县', '3', '230522', '50');
INSERT INTO `fly_area` VALUES ('1016', '96', '宝清县', '3', '230523', '50');
INSERT INTO `fly_area` VALUES ('1017', '96', '饶河县', '3', '230524', '50');
INSERT INTO `fly_area` VALUES ('1018', '97', '萨尔图区', '3', '230602', '50');
INSERT INTO `fly_area` VALUES ('1019', '97', '龙凤区', '3', '230603', '50');
INSERT INTO `fly_area` VALUES ('1020', '97', '让胡路区', '3', '230604', '50');
INSERT INTO `fly_area` VALUES ('1021', '97', '红岗区', '3', '230605', '50');
INSERT INTO `fly_area` VALUES ('1022', '97', '大同区', '3', '230606', '50');
INSERT INTO `fly_area` VALUES ('1023', '97', '肇州县', '3', '230621', '50');
INSERT INTO `fly_area` VALUES ('1024', '97', '肇源县', '3', '230622', '50');
INSERT INTO `fly_area` VALUES ('1025', '97', '林甸县', '3', '230623', '50');
INSERT INTO `fly_area` VALUES ('1026', '97', '杜尔伯特蒙古族自治县', '3', '230624', '50');
INSERT INTO `fly_area` VALUES ('1027', '98', '伊春区', '3', '230702', '50');
INSERT INTO `fly_area` VALUES ('1028', '98', '南岔区', '3', '230703', '50');
INSERT INTO `fly_area` VALUES ('1029', '98', '友好区', '3', '230704', '50');
INSERT INTO `fly_area` VALUES ('1030', '98', '西林区', '3', '230705', '50');
INSERT INTO `fly_area` VALUES ('1031', '98', '翠峦区', '3', '230706', '50');
INSERT INTO `fly_area` VALUES ('1032', '98', '新青区', '3', '230707', '50');
INSERT INTO `fly_area` VALUES ('1033', '98', '美溪区', '3', '230708', '50');
INSERT INTO `fly_area` VALUES ('1034', '98', '金山屯区', '3', '230709', '50');
INSERT INTO `fly_area` VALUES ('1035', '98', '五营区', '3', '230710', '50');
INSERT INTO `fly_area` VALUES ('1036', '98', '乌马河区', '3', '230711', '50');
INSERT INTO `fly_area` VALUES ('1037', '98', '汤旺河区', '3', '230712', '50');
INSERT INTO `fly_area` VALUES ('1038', '98', '带岭区', '3', '230713', '50');
INSERT INTO `fly_area` VALUES ('1039', '98', '乌伊岭区', '3', '230714', '50');
INSERT INTO `fly_area` VALUES ('1040', '98', '红星区', '3', '230715', '50');
INSERT INTO `fly_area` VALUES ('1041', '98', '上甘岭区', '3', '230716', '50');
INSERT INTO `fly_area` VALUES ('1042', '98', '嘉荫县', '3', '230722', '50');
INSERT INTO `fly_area` VALUES ('1043', '98', '铁力市', '3', '230781', '50');
INSERT INTO `fly_area` VALUES ('1044', '99', '向阳区', '3', '230803', '50');
INSERT INTO `fly_area` VALUES ('1045', '99', '前进区', '3', '230804', '50');
INSERT INTO `fly_area` VALUES ('1046', '99', '东风区', '3', '230805', '50');
INSERT INTO `fly_area` VALUES ('1047', '99', '郊区', '3', '230811', '50');
INSERT INTO `fly_area` VALUES ('1048', '99', '桦南县', '3', '230822', '50');
INSERT INTO `fly_area` VALUES ('1049', '99', '桦川县', '3', '230826', '50');
INSERT INTO `fly_area` VALUES ('1050', '99', '汤原县', '3', '230828', '50');
INSERT INTO `fly_area` VALUES ('1051', '99', '同江市', '3', '230881', '50');
INSERT INTO `fly_area` VALUES ('1052', '99', '富锦市', '3', '230882', '50');
INSERT INTO `fly_area` VALUES ('1053', '99', '抚远市', '3', '230883', '50');
INSERT INTO `fly_area` VALUES ('1054', '100', '新兴区', '3', '230902', '50');
INSERT INTO `fly_area` VALUES ('1055', '100', '桃山区', '3', '230903', '50');
INSERT INTO `fly_area` VALUES ('1056', '100', '茄子河区', '3', '230904', '50');
INSERT INTO `fly_area` VALUES ('1057', '100', '勃利县', '3', '230921', '50');
INSERT INTO `fly_area` VALUES ('1058', '101', '东安区', '3', '231002', '50');
INSERT INTO `fly_area` VALUES ('1059', '101', '阳明区', '3', '231003', '50');
INSERT INTO `fly_area` VALUES ('1060', '101', '爱民区', '3', '231004', '50');
INSERT INTO `fly_area` VALUES ('1061', '101', '西安区', '3', '231005', '50');
INSERT INTO `fly_area` VALUES ('1062', '101', '林口县', '3', '231025', '50');
INSERT INTO `fly_area` VALUES ('1063', '101', '绥芬河市', '3', '231081', '50');
INSERT INTO `fly_area` VALUES ('1064', '101', '海林市', '3', '231083', '50');
INSERT INTO `fly_area` VALUES ('1065', '101', '宁安市', '3', '231084', '50');
INSERT INTO `fly_area` VALUES ('1066', '101', '穆棱市', '3', '231085', '50');
INSERT INTO `fly_area` VALUES ('1067', '101', '东宁市', '3', '231086', '50');
INSERT INTO `fly_area` VALUES ('1068', '102', '爱辉区', '3', '231102', '50');
INSERT INTO `fly_area` VALUES ('1069', '102', '嫩江县', '3', '231121', '50');
INSERT INTO `fly_area` VALUES ('1070', '102', '逊克县', '3', '231123', '50');
INSERT INTO `fly_area` VALUES ('1071', '102', '孙吴县', '3', '231124', '50');
INSERT INTO `fly_area` VALUES ('1072', '102', '北安市', '3', '231181', '50');
INSERT INTO `fly_area` VALUES ('1073', '102', '五大连池市', '3', '231182', '50');
INSERT INTO `fly_area` VALUES ('1074', '103', '北林区', '3', '231202', '50');
INSERT INTO `fly_area` VALUES ('1075', '103', '望奎县', '3', '231221', '50');
INSERT INTO `fly_area` VALUES ('1076', '103', '兰西县', '3', '231222', '50');
INSERT INTO `fly_area` VALUES ('1077', '103', '青冈县', '3', '231223', '50');
INSERT INTO `fly_area` VALUES ('1078', '103', '庆安县', '3', '231224', '50');
INSERT INTO `fly_area` VALUES ('1079', '103', '明水县', '3', '231225', '50');
INSERT INTO `fly_area` VALUES ('1080', '103', '绥棱县', '3', '231226', '50');
INSERT INTO `fly_area` VALUES ('1081', '103', '安达市', '3', '231281', '50');
INSERT INTO `fly_area` VALUES ('1082', '103', '肇东市', '3', '231282', '50');
INSERT INTO `fly_area` VALUES ('1083', '103', '海伦市', '3', '231283', '50');
INSERT INTO `fly_area` VALUES ('1084', '104', '呼玛县', '3', '232721', '50');
INSERT INTO `fly_area` VALUES ('1085', '104', '塔河县', '3', '232722', '50');
INSERT INTO `fly_area` VALUES ('1086', '104', '漠河县', '3', '232723', '50');
INSERT INTO `fly_area` VALUES ('1087', '105', '黄浦区', '3', '310101', '50');
INSERT INTO `fly_area` VALUES ('1088', '105', '徐汇区', '3', '310104', '50');
INSERT INTO `fly_area` VALUES ('1089', '105', '长宁区', '3', '310105', '50');
INSERT INTO `fly_area` VALUES ('1090', '105', '静安区', '3', '310106', '50');
INSERT INTO `fly_area` VALUES ('1091', '105', '普陀区', '3', '310107', '50');
INSERT INTO `fly_area` VALUES ('1092', '105', '虹口区', '3', '310109', '50');
INSERT INTO `fly_area` VALUES ('1093', '105', '杨浦区', '3', '310110', '50');
INSERT INTO `fly_area` VALUES ('1094', '105', '闵行区', '3', '310112', '50');
INSERT INTO `fly_area` VALUES ('1095', '105', '宝山区', '3', '310113', '50');
INSERT INTO `fly_area` VALUES ('1096', '105', '嘉定区', '3', '310114', '50');
INSERT INTO `fly_area` VALUES ('1097', '105', '浦东新区', '3', '310115', '50');
INSERT INTO `fly_area` VALUES ('1098', '105', '金山区', '3', '310116', '50');
INSERT INTO `fly_area` VALUES ('1099', '105', '松江区', '3', '310117', '50');
INSERT INTO `fly_area` VALUES ('1100', '105', '青浦区', '3', '310118', '50');
INSERT INTO `fly_area` VALUES ('1101', '105', '奉贤区', '3', '310120', '50');
INSERT INTO `fly_area` VALUES ('1102', '105', '崇明区', '3', '310151', '50');
INSERT INTO `fly_area` VALUES ('1103', '106', '玄武区', '3', '320102', '50');
INSERT INTO `fly_area` VALUES ('1104', '106', '秦淮区', '3', '320104', '50');
INSERT INTO `fly_area` VALUES ('1105', '106', '建邺区', '3', '320105', '50');
INSERT INTO `fly_area` VALUES ('1106', '106', '鼓楼区', '3', '320106', '50');
INSERT INTO `fly_area` VALUES ('1107', '106', '浦口区', '3', '320111', '50');
INSERT INTO `fly_area` VALUES ('1108', '106', '栖霞区', '3', '320113', '50');
INSERT INTO `fly_area` VALUES ('1109', '106', '雨花台区', '3', '320114', '50');
INSERT INTO `fly_area` VALUES ('1110', '106', '江宁区', '3', '320115', '50');
INSERT INTO `fly_area` VALUES ('1111', '106', '六合区', '3', '320116', '50');
INSERT INTO `fly_area` VALUES ('1112', '106', '溧水区', '3', '320117', '50');
INSERT INTO `fly_area` VALUES ('1113', '106', '高淳区', '3', '320118', '50');
INSERT INTO `fly_area` VALUES ('1114', '107', '锡山区', '3', '320205', '50');
INSERT INTO `fly_area` VALUES ('1115', '107', '惠山区', '3', '320206', '50');
INSERT INTO `fly_area` VALUES ('1116', '107', '滨湖区', '3', '320211', '50');
INSERT INTO `fly_area` VALUES ('1117', '107', '梁溪区', '3', '320213', '50');
INSERT INTO `fly_area` VALUES ('1118', '107', '新吴区', '3', '320214', '50');
INSERT INTO `fly_area` VALUES ('1119', '107', '江阴市', '3', '320281', '50');
INSERT INTO `fly_area` VALUES ('1120', '107', '宜兴市', '3', '320282', '50');
INSERT INTO `fly_area` VALUES ('1121', '108', '鼓楼区', '3', '320302', '50');
INSERT INTO `fly_area` VALUES ('1122', '108', '云龙区', '3', '320303', '50');
INSERT INTO `fly_area` VALUES ('1123', '108', '贾汪区', '3', '320305', '50');
INSERT INTO `fly_area` VALUES ('1124', '108', '泉山区', '3', '320311', '50');
INSERT INTO `fly_area` VALUES ('1125', '108', '铜山区', '3', '320312', '50');
INSERT INTO `fly_area` VALUES ('1126', '108', '丰县', '3', '320321', '50');
INSERT INTO `fly_area` VALUES ('1127', '108', '沛县', '3', '320322', '50');
INSERT INTO `fly_area` VALUES ('1128', '108', '睢宁县', '3', '320324', '50');
INSERT INTO `fly_area` VALUES ('1129', '108', '新沂市', '3', '320381', '50');
INSERT INTO `fly_area` VALUES ('1130', '108', '邳州市', '3', '320382', '50');
INSERT INTO `fly_area` VALUES ('1131', '109', '天宁区', '3', '320402', '50');
INSERT INTO `fly_area` VALUES ('1132', '109', '钟楼区', '3', '320404', '50');
INSERT INTO `fly_area` VALUES ('1133', '109', '新北区', '3', '320411', '50');
INSERT INTO `fly_area` VALUES ('1134', '109', '武进区', '3', '320412', '50');
INSERT INTO `fly_area` VALUES ('1135', '109', '金坛区', '3', '320413', '50');
INSERT INTO `fly_area` VALUES ('1136', '109', '溧阳市', '3', '320481', '50');
INSERT INTO `fly_area` VALUES ('1137', '110', '虎丘区', '3', '320505', '50');
INSERT INTO `fly_area` VALUES ('1138', '110', '吴中区', '3', '320506', '50');
INSERT INTO `fly_area` VALUES ('1139', '110', '相城区', '3', '320507', '50');
INSERT INTO `fly_area` VALUES ('1140', '110', '姑苏区', '3', '320508', '50');
INSERT INTO `fly_area` VALUES ('1141', '110', '吴江区', '3', '320509', '50');
INSERT INTO `fly_area` VALUES ('1142', '110', '常熟市', '3', '320581', '50');
INSERT INTO `fly_area` VALUES ('1143', '110', '张家港市', '3', '320582', '50');
INSERT INTO `fly_area` VALUES ('1144', '110', '昆山市', '3', '320583', '50');
INSERT INTO `fly_area` VALUES ('1145', '110', '太仓市', '3', '320585', '50');
INSERT INTO `fly_area` VALUES ('1146', '111', '崇川区', '3', '320602', '50');
INSERT INTO `fly_area` VALUES ('1147', '111', '港闸区', '3', '320611', '50');
INSERT INTO `fly_area` VALUES ('1148', '111', '通州区', '3', '320612', '50');
INSERT INTO `fly_area` VALUES ('1149', '111', '海安县', '3', '320621', '50');
INSERT INTO `fly_area` VALUES ('1150', '111', '如东县', '3', '320623', '50');
INSERT INTO `fly_area` VALUES ('1151', '111', '启东市', '3', '320681', '50');
INSERT INTO `fly_area` VALUES ('1152', '111', '如皋市', '3', '320682', '50');
INSERT INTO `fly_area` VALUES ('1153', '111', '海门市', '3', '320684', '50');
INSERT INTO `fly_area` VALUES ('1154', '112', '连云区', '3', '320703', '50');
INSERT INTO `fly_area` VALUES ('1155', '112', '海州区', '3', '320706', '50');
INSERT INTO `fly_area` VALUES ('1156', '112', '赣榆区', '3', '320707', '50');
INSERT INTO `fly_area` VALUES ('1157', '112', '东海县', '3', '320722', '50');
INSERT INTO `fly_area` VALUES ('1158', '112', '灌云县', '3', '320723', '50');
INSERT INTO `fly_area` VALUES ('1159', '112', '灌南县', '3', '320724', '50');
INSERT INTO `fly_area` VALUES ('1160', '113', '淮安区', '3', '320803', '50');
INSERT INTO `fly_area` VALUES ('1161', '113', '淮阴区', '3', '320804', '50');
INSERT INTO `fly_area` VALUES ('1162', '113', '清江浦区', '3', '320812', '50');
INSERT INTO `fly_area` VALUES ('1163', '113', '洪泽区', '3', '320813', '50');
INSERT INTO `fly_area` VALUES ('1164', '113', '涟水县', '3', '320826', '50');
INSERT INTO `fly_area` VALUES ('1165', '113', '盱眙县', '3', '320830', '50');
INSERT INTO `fly_area` VALUES ('1166', '113', '金湖县', '3', '320831', '50');
INSERT INTO `fly_area` VALUES ('1167', '114', '亭湖区', '3', '320902', '50');
INSERT INTO `fly_area` VALUES ('1168', '114', '盐都区', '3', '320903', '50');
INSERT INTO `fly_area` VALUES ('1169', '114', '大丰区', '3', '320904', '50');
INSERT INTO `fly_area` VALUES ('1170', '114', '响水县', '3', '320921', '50');
INSERT INTO `fly_area` VALUES ('1171', '114', '滨海县', '3', '320922', '50');
INSERT INTO `fly_area` VALUES ('1172', '114', '阜宁县', '3', '320923', '50');
INSERT INTO `fly_area` VALUES ('1173', '114', '射阳县', '3', '320924', '50');
INSERT INTO `fly_area` VALUES ('1174', '114', '建湖县', '3', '320925', '50');
INSERT INTO `fly_area` VALUES ('1175', '114', '东台市', '3', '320981', '50');
INSERT INTO `fly_area` VALUES ('1176', '115', '广陵区', '3', '321002', '50');
INSERT INTO `fly_area` VALUES ('1177', '115', '邗江区', '3', '321003', '50');
INSERT INTO `fly_area` VALUES ('1178', '115', '江都区', '3', '321012', '50');
INSERT INTO `fly_area` VALUES ('1179', '115', '宝应县', '3', '321023', '50');
INSERT INTO `fly_area` VALUES ('1180', '115', '仪征市', '3', '321081', '50');
INSERT INTO `fly_area` VALUES ('1181', '115', '高邮市', '3', '321084', '50');
INSERT INTO `fly_area` VALUES ('1182', '116', '京口区', '3', '321102', '50');
INSERT INTO `fly_area` VALUES ('1183', '116', '润州区', '3', '321111', '50');
INSERT INTO `fly_area` VALUES ('1184', '116', '丹徒区', '3', '321112', '50');
INSERT INTO `fly_area` VALUES ('1185', '116', '丹阳市', '3', '321181', '50');
INSERT INTO `fly_area` VALUES ('1186', '116', '扬中市', '3', '321182', '50');
INSERT INTO `fly_area` VALUES ('1187', '116', '句容市', '3', '321183', '50');
INSERT INTO `fly_area` VALUES ('1188', '117', '海陵区', '3', '321202', '50');
INSERT INTO `fly_area` VALUES ('1189', '117', '高港区', '3', '321203', '50');
INSERT INTO `fly_area` VALUES ('1190', '117', '姜堰区', '3', '321204', '50');
INSERT INTO `fly_area` VALUES ('1191', '117', '兴化市', '3', '321281', '50');
INSERT INTO `fly_area` VALUES ('1192', '117', '靖江市', '3', '321282', '50');
INSERT INTO `fly_area` VALUES ('1193', '117', '泰兴市', '3', '321283', '50');
INSERT INTO `fly_area` VALUES ('1194', '118', '宿城区', '3', '321302', '50');
INSERT INTO `fly_area` VALUES ('1195', '118', '宿豫区', '3', '321311', '50');
INSERT INTO `fly_area` VALUES ('1196', '118', '沭阳县', '3', '321322', '50');
INSERT INTO `fly_area` VALUES ('1197', '118', '泗阳县', '3', '321323', '50');
INSERT INTO `fly_area` VALUES ('1198', '118', '泗洪县', '3', '321324', '50');
INSERT INTO `fly_area` VALUES ('1199', '119', '上城区', '3', '330102', '50');
INSERT INTO `fly_area` VALUES ('1200', '119', '下城区', '3', '330103', '50');
INSERT INTO `fly_area` VALUES ('1201', '119', '江干区', '3', '330104', '50');
INSERT INTO `fly_area` VALUES ('1202', '119', '拱墅区', '3', '330105', '50');
INSERT INTO `fly_area` VALUES ('1203', '119', '西湖区', '3', '330106', '50');
INSERT INTO `fly_area` VALUES ('1204', '119', '滨江区', '3', '330108', '50');
INSERT INTO `fly_area` VALUES ('1205', '119', '萧山区', '3', '330109', '50');
INSERT INTO `fly_area` VALUES ('1206', '119', '余杭区', '3', '330110', '50');
INSERT INTO `fly_area` VALUES ('1207', '119', '富阳区', '3', '330111', '50');
INSERT INTO `fly_area` VALUES ('1208', '119', '桐庐县', '3', '330122', '50');
INSERT INTO `fly_area` VALUES ('1209', '119', '淳安县', '3', '330127', '50');
INSERT INTO `fly_area` VALUES ('1210', '119', '建德市', '3', '330182', '50');
INSERT INTO `fly_area` VALUES ('1211', '119', '临安市', '3', '330185', '50');
INSERT INTO `fly_area` VALUES ('1212', '120', '海曙区', '3', '330203', '50');
INSERT INTO `fly_area` VALUES ('1213', '120', '江东区', '3', '330204', '50');
INSERT INTO `fly_area` VALUES ('1214', '120', '江北区', '3', '330205', '50');
INSERT INTO `fly_area` VALUES ('1215', '120', '北仑区', '3', '330206', '50');
INSERT INTO `fly_area` VALUES ('1216', '120', '镇海区', '3', '330211', '50');
INSERT INTO `fly_area` VALUES ('1217', '120', '鄞州区', '3', '330212', '50');
INSERT INTO `fly_area` VALUES ('1218', '120', '象山县', '3', '330225', '50');
INSERT INTO `fly_area` VALUES ('1219', '120', '宁海县', '3', '330226', '50');
INSERT INTO `fly_area` VALUES ('1220', '120', '余姚市', '3', '330281', '50');
INSERT INTO `fly_area` VALUES ('1221', '120', '慈溪市', '3', '330282', '50');
INSERT INTO `fly_area` VALUES ('1222', '120', '奉化市', '3', '330283', '50');
INSERT INTO `fly_area` VALUES ('1223', '121', '鹿城区', '3', '330302', '50');
INSERT INTO `fly_area` VALUES ('1224', '121', '龙湾区', '3', '330303', '50');
INSERT INTO `fly_area` VALUES ('1225', '121', '瓯海区', '3', '330304', '50');
INSERT INTO `fly_area` VALUES ('1226', '121', '洞头区', '3', '330305', '50');
INSERT INTO `fly_area` VALUES ('1227', '121', '永嘉县', '3', '330324', '50');
INSERT INTO `fly_area` VALUES ('1228', '121', '平阳县', '3', '330326', '50');
INSERT INTO `fly_area` VALUES ('1229', '121', '苍南县', '3', '330327', '50');
INSERT INTO `fly_area` VALUES ('1230', '121', '文成县', '3', '330328', '50');
INSERT INTO `fly_area` VALUES ('1231', '121', '泰顺县', '3', '330329', '50');
INSERT INTO `fly_area` VALUES ('1232', '121', '瑞安市', '3', '330381', '50');
INSERT INTO `fly_area` VALUES ('1233', '121', '乐清市', '3', '330382', '50');
INSERT INTO `fly_area` VALUES ('1234', '122', '南湖区', '3', '330402', '50');
INSERT INTO `fly_area` VALUES ('1235', '122', '秀洲区', '3', '330411', '50');
INSERT INTO `fly_area` VALUES ('1236', '122', '嘉善县', '3', '330421', '50');
INSERT INTO `fly_area` VALUES ('1237', '122', '海盐县', '3', '330424', '50');
INSERT INTO `fly_area` VALUES ('1238', '122', '海宁市', '3', '330481', '50');
INSERT INTO `fly_area` VALUES ('1239', '122', '平湖市', '3', '330482', '50');
INSERT INTO `fly_area` VALUES ('1240', '122', '桐乡市', '3', '330483', '50');
INSERT INTO `fly_area` VALUES ('1241', '123', '吴兴区', '3', '330502', '50');
INSERT INTO `fly_area` VALUES ('1242', '123', '南浔区', '3', '330503', '50');
INSERT INTO `fly_area` VALUES ('1243', '123', '德清县', '3', '330521', '50');
INSERT INTO `fly_area` VALUES ('1244', '123', '长兴县', '3', '330522', '50');
INSERT INTO `fly_area` VALUES ('1245', '123', '安吉县', '3', '330523', '50');
INSERT INTO `fly_area` VALUES ('1246', '124', '越城区', '3', '330602', '50');
INSERT INTO `fly_area` VALUES ('1247', '124', '柯桥区', '3', '330603', '50');
INSERT INTO `fly_area` VALUES ('1248', '124', '上虞区', '3', '330604', '50');
INSERT INTO `fly_area` VALUES ('1249', '124', '新昌县', '3', '330624', '50');
INSERT INTO `fly_area` VALUES ('1250', '124', '诸暨市', '3', '330681', '50');
INSERT INTO `fly_area` VALUES ('1251', '124', '嵊州市', '3', '330683', '50');
INSERT INTO `fly_area` VALUES ('1252', '125', '婺城区', '3', '330702', '50');
INSERT INTO `fly_area` VALUES ('1253', '125', '金东区', '3', '330703', '50');
INSERT INTO `fly_area` VALUES ('1254', '125', '武义县', '3', '330723', '50');
INSERT INTO `fly_area` VALUES ('1255', '125', '浦江县', '3', '330726', '50');
INSERT INTO `fly_area` VALUES ('1256', '125', '磐安县', '3', '330727', '50');
INSERT INTO `fly_area` VALUES ('1257', '125', '兰溪市', '3', '330781', '50');
INSERT INTO `fly_area` VALUES ('1258', '125', '义乌市', '3', '330782', '50');
INSERT INTO `fly_area` VALUES ('1259', '125', '东阳市', '3', '330783', '50');
INSERT INTO `fly_area` VALUES ('1260', '125', '永康市', '3', '330784', '50');
INSERT INTO `fly_area` VALUES ('1261', '126', '柯城区', '3', '330802', '50');
INSERT INTO `fly_area` VALUES ('1262', '126', '衢江区', '3', '330803', '50');
INSERT INTO `fly_area` VALUES ('1263', '126', '常山县', '3', '330822', '50');
INSERT INTO `fly_area` VALUES ('1264', '126', '开化县', '3', '330824', '50');
INSERT INTO `fly_area` VALUES ('1265', '126', '龙游县', '3', '330825', '50');
INSERT INTO `fly_area` VALUES ('1266', '126', '江山市', '3', '330881', '50');
INSERT INTO `fly_area` VALUES ('1267', '127', '定海区', '3', '330902', '50');
INSERT INTO `fly_area` VALUES ('1268', '127', '普陀区', '3', '330903', '50');
INSERT INTO `fly_area` VALUES ('1269', '127', '岱山县', '3', '330921', '50');
INSERT INTO `fly_area` VALUES ('1270', '127', '嵊泗县', '3', '330922', '50');
INSERT INTO `fly_area` VALUES ('1271', '128', '椒江区', '3', '331002', '50');
INSERT INTO `fly_area` VALUES ('1272', '128', '黄岩区', '3', '331003', '50');
INSERT INTO `fly_area` VALUES ('1273', '128', '路桥区', '3', '331004', '50');
INSERT INTO `fly_area` VALUES ('1274', '128', '玉环县', '3', '331021', '50');
INSERT INTO `fly_area` VALUES ('1275', '128', '三门县', '3', '331022', '50');
INSERT INTO `fly_area` VALUES ('1276', '128', '天台县', '3', '331023', '50');
INSERT INTO `fly_area` VALUES ('1277', '128', '仙居县', '3', '331024', '50');
INSERT INTO `fly_area` VALUES ('1278', '128', '温岭市', '3', '331081', '50');
INSERT INTO `fly_area` VALUES ('1279', '128', '临海市', '3', '331082', '50');
INSERT INTO `fly_area` VALUES ('1280', '129', '莲都区', '3', '331102', '50');
INSERT INTO `fly_area` VALUES ('1281', '129', '青田县', '3', '331121', '50');
INSERT INTO `fly_area` VALUES ('1282', '129', '缙云县', '3', '331122', '50');
INSERT INTO `fly_area` VALUES ('1283', '129', '遂昌县', '3', '331123', '50');
INSERT INTO `fly_area` VALUES ('1284', '129', '松阳县', '3', '331124', '50');
INSERT INTO `fly_area` VALUES ('1285', '129', '云和县', '3', '331125', '50');
INSERT INTO `fly_area` VALUES ('1286', '129', '庆元县', '3', '331126', '50');
INSERT INTO `fly_area` VALUES ('1287', '129', '景宁畲族自治县', '3', '331127', '50');
INSERT INTO `fly_area` VALUES ('1288', '129', '龙泉市', '3', '331181', '50');
INSERT INTO `fly_area` VALUES ('1289', '130', '瑶海区', '3', '340102', '50');
INSERT INTO `fly_area` VALUES ('1290', '130', '庐阳区', '3', '340103', '50');
INSERT INTO `fly_area` VALUES ('1291', '130', '蜀山区', '3', '340104', '50');
INSERT INTO `fly_area` VALUES ('1292', '130', '包河区', '3', '340111', '50');
INSERT INTO `fly_area` VALUES ('1293', '130', '长丰县', '3', '340121', '50');
INSERT INTO `fly_area` VALUES ('1294', '130', '肥东县', '3', '340122', '50');
INSERT INTO `fly_area` VALUES ('1295', '130', '肥西县', '3', '340123', '50');
INSERT INTO `fly_area` VALUES ('1296', '130', '庐江县', '3', '340124', '50');
INSERT INTO `fly_area` VALUES ('1297', '130', '巢湖市', '3', '340181', '50');
INSERT INTO `fly_area` VALUES ('1298', '131', '镜湖区', '3', '340202', '50');
INSERT INTO `fly_area` VALUES ('1299', '131', '弋江区', '3', '340203', '50');
INSERT INTO `fly_area` VALUES ('1300', '131', '鸠江区', '3', '340207', '50');
INSERT INTO `fly_area` VALUES ('1301', '131', '三山区', '3', '340208', '50');
INSERT INTO `fly_area` VALUES ('1302', '131', '芜湖县', '3', '340221', '50');
INSERT INTO `fly_area` VALUES ('1303', '131', '繁昌县', '3', '340222', '50');
INSERT INTO `fly_area` VALUES ('1304', '131', '南陵县', '3', '340223', '50');
INSERT INTO `fly_area` VALUES ('1305', '131', '无为县', '3', '340225', '50');
INSERT INTO `fly_area` VALUES ('1306', '132', '龙子湖区', '3', '340302', '50');
INSERT INTO `fly_area` VALUES ('1307', '132', '蚌山区', '3', '340303', '50');
INSERT INTO `fly_area` VALUES ('1308', '132', '禹会区', '3', '340304', '50');
INSERT INTO `fly_area` VALUES ('1309', '132', '淮上区', '3', '340311', '50');
INSERT INTO `fly_area` VALUES ('1310', '132', '怀远县', '3', '340321', '50');
INSERT INTO `fly_area` VALUES ('1311', '132', '五河县', '3', '340322', '50');
INSERT INTO `fly_area` VALUES ('1312', '132', '固镇县', '3', '340323', '50');
INSERT INTO `fly_area` VALUES ('1313', '133', '大通区', '3', '340402', '50');
INSERT INTO `fly_area` VALUES ('1314', '133', '田家庵区', '3', '340403', '50');
INSERT INTO `fly_area` VALUES ('1315', '133', '谢家集区', '3', '340404', '50');
INSERT INTO `fly_area` VALUES ('1316', '133', '八公山区', '3', '340405', '50');
INSERT INTO `fly_area` VALUES ('1317', '133', '潘集区', '3', '340406', '50');
INSERT INTO `fly_area` VALUES ('1318', '133', '凤台县', '3', '340421', '50');
INSERT INTO `fly_area` VALUES ('1319', '133', '寿县', '3', '340422', '50');
INSERT INTO `fly_area` VALUES ('1320', '134', '花山区', '3', '340503', '50');
INSERT INTO `fly_area` VALUES ('1321', '134', '雨山区', '3', '340504', '50');
INSERT INTO `fly_area` VALUES ('1322', '134', '博望区', '3', '340506', '50');
INSERT INTO `fly_area` VALUES ('1323', '134', '当涂县', '3', '340521', '50');
INSERT INTO `fly_area` VALUES ('1324', '134', '含山县', '3', '340522', '50');
INSERT INTO `fly_area` VALUES ('1325', '134', '和县', '3', '340523', '50');
INSERT INTO `fly_area` VALUES ('1326', '135', '杜集区', '3', '340602', '50');
INSERT INTO `fly_area` VALUES ('1327', '135', '相山区', '3', '340603', '50');
INSERT INTO `fly_area` VALUES ('1328', '135', '烈山区', '3', '340604', '50');
INSERT INTO `fly_area` VALUES ('1329', '135', '濉溪县', '3', '340621', '50');
INSERT INTO `fly_area` VALUES ('1330', '136', '铜官区', '3', '340705', '50');
INSERT INTO `fly_area` VALUES ('1331', '136', '义安区', '3', '340706', '50');
INSERT INTO `fly_area` VALUES ('1332', '136', '郊区', '3', '340711', '50');
INSERT INTO `fly_area` VALUES ('1333', '136', '枞阳县', '3', '340722', '50');
INSERT INTO `fly_area` VALUES ('1334', '137', '迎江区', '3', '340802', '50');
INSERT INTO `fly_area` VALUES ('1335', '137', '大观区', '3', '340803', '50');
INSERT INTO `fly_area` VALUES ('1336', '137', '宜秀区', '3', '340811', '50');
INSERT INTO `fly_area` VALUES ('1337', '137', '怀宁县', '3', '340822', '50');
INSERT INTO `fly_area` VALUES ('1338', '137', '潜山县', '3', '340824', '50');
INSERT INTO `fly_area` VALUES ('1339', '137', '太湖县', '3', '340825', '50');
INSERT INTO `fly_area` VALUES ('1340', '137', '宿松县', '3', '340826', '50');
INSERT INTO `fly_area` VALUES ('1341', '137', '望江县', '3', '340827', '50');
INSERT INTO `fly_area` VALUES ('1342', '137', '岳西县', '3', '340828', '50');
INSERT INTO `fly_area` VALUES ('1343', '137', '桐城市', '3', '340881', '50');
INSERT INTO `fly_area` VALUES ('1344', '138', '屯溪区', '3', '341002', '50');
INSERT INTO `fly_area` VALUES ('1345', '138', '黄山区', '3', '341003', '50');
INSERT INTO `fly_area` VALUES ('1346', '138', '徽州区', '3', '341004', '50');
INSERT INTO `fly_area` VALUES ('1347', '138', '歙县', '3', '341021', '50');
INSERT INTO `fly_area` VALUES ('1348', '138', '休宁县', '3', '341022', '50');
INSERT INTO `fly_area` VALUES ('1349', '138', '黟县', '3', '341023', '50');
INSERT INTO `fly_area` VALUES ('1350', '138', '祁门县', '3', '341024', '50');
INSERT INTO `fly_area` VALUES ('1351', '139', '琅琊区', '3', '341102', '50');
INSERT INTO `fly_area` VALUES ('1352', '139', '南谯区', '3', '341103', '50');
INSERT INTO `fly_area` VALUES ('1353', '139', '来安县', '3', '341122', '50');
INSERT INTO `fly_area` VALUES ('1354', '139', '全椒县', '3', '341124', '50');
INSERT INTO `fly_area` VALUES ('1355', '139', '定远县', '3', '341125', '50');
INSERT INTO `fly_area` VALUES ('1356', '139', '凤阳县', '3', '341126', '50');
INSERT INTO `fly_area` VALUES ('1357', '139', '天长市', '3', '341181', '50');
INSERT INTO `fly_area` VALUES ('1358', '139', '明光市', '3', '341182', '50');
INSERT INTO `fly_area` VALUES ('1359', '140', '颍州区', '3', '341202', '50');
INSERT INTO `fly_area` VALUES ('1360', '140', '颍东区', '3', '341203', '50');
INSERT INTO `fly_area` VALUES ('1361', '140', '颍泉区', '3', '341204', '50');
INSERT INTO `fly_area` VALUES ('1362', '140', '临泉县', '3', '341221', '50');
INSERT INTO `fly_area` VALUES ('1363', '140', '太和县', '3', '341222', '50');
INSERT INTO `fly_area` VALUES ('1364', '140', '阜南县', '3', '341225', '50');
INSERT INTO `fly_area` VALUES ('1365', '140', '颍上县', '3', '341226', '50');
INSERT INTO `fly_area` VALUES ('1366', '140', '界首市', '3', '341282', '50');
INSERT INTO `fly_area` VALUES ('1367', '141', '埇桥区', '3', '341302', '50');
INSERT INTO `fly_area` VALUES ('1368', '141', '砀山县', '3', '341321', '50');
INSERT INTO `fly_area` VALUES ('1369', '141', '萧县', '3', '341322', '50');
INSERT INTO `fly_area` VALUES ('1370', '141', '灵璧县', '3', '341323', '50');
INSERT INTO `fly_area` VALUES ('1371', '141', '泗县', '3', '341324', '50');
INSERT INTO `fly_area` VALUES ('1372', '142', '金安区', '3', '341502', '50');
INSERT INTO `fly_area` VALUES ('1373', '142', '裕安区', '3', '341503', '50');
INSERT INTO `fly_area` VALUES ('1374', '142', '叶集区', '3', '341504', '50');
INSERT INTO `fly_area` VALUES ('1375', '142', '霍邱县', '3', '341522', '50');
INSERT INTO `fly_area` VALUES ('1376', '142', '舒城县', '3', '341523', '50');
INSERT INTO `fly_area` VALUES ('1377', '142', '金寨县', '3', '341524', '50');
INSERT INTO `fly_area` VALUES ('1378', '142', '霍山县', '3', '341525', '50');
INSERT INTO `fly_area` VALUES ('1379', '143', '谯城区', '3', '341602', '50');
INSERT INTO `fly_area` VALUES ('1380', '143', '涡阳县', '3', '341621', '50');
INSERT INTO `fly_area` VALUES ('1381', '143', '蒙城县', '3', '341622', '50');
INSERT INTO `fly_area` VALUES ('1382', '143', '利辛县', '3', '341623', '50');
INSERT INTO `fly_area` VALUES ('1383', '144', '贵池区', '3', '341702', '50');
INSERT INTO `fly_area` VALUES ('1384', '144', '东至县', '3', '341721', '50');
INSERT INTO `fly_area` VALUES ('1385', '144', '石台县', '3', '341722', '50');
INSERT INTO `fly_area` VALUES ('1386', '144', '青阳县', '3', '341723', '50');
INSERT INTO `fly_area` VALUES ('1387', '145', '宣州区', '3', '341802', '50');
INSERT INTO `fly_area` VALUES ('1388', '145', '郎溪县', '3', '341821', '50');
INSERT INTO `fly_area` VALUES ('1389', '145', '广德县', '3', '341822', '50');
INSERT INTO `fly_area` VALUES ('1390', '145', '泾县', '3', '341823', '50');
INSERT INTO `fly_area` VALUES ('1391', '145', '绩溪县', '3', '341824', '50');
INSERT INTO `fly_area` VALUES ('1392', '145', '旌德县', '3', '341825', '50');
INSERT INTO `fly_area` VALUES ('1393', '145', '宁国市', '3', '341881', '50');
INSERT INTO `fly_area` VALUES ('1394', '146', '鼓楼区', '3', '350102', '50');
INSERT INTO `fly_area` VALUES ('1395', '146', '台江区', '3', '350103', '50');
INSERT INTO `fly_area` VALUES ('1396', '146', '仓山区', '3', '350104', '50');
INSERT INTO `fly_area` VALUES ('1397', '146', '马尾区', '3', '350105', '50');
INSERT INTO `fly_area` VALUES ('1398', '146', '晋安区', '3', '350111', '50');
INSERT INTO `fly_area` VALUES ('1399', '146', '闽侯县', '3', '350121', '50');
INSERT INTO `fly_area` VALUES ('1400', '146', '连江县', '3', '350122', '50');
INSERT INTO `fly_area` VALUES ('1401', '146', '罗源县', '3', '350123', '50');
INSERT INTO `fly_area` VALUES ('1402', '146', '闽清县', '3', '350124', '50');
INSERT INTO `fly_area` VALUES ('1403', '146', '永泰县', '3', '350125', '50');
INSERT INTO `fly_area` VALUES ('1404', '146', '平潭县', '3', '350128', '50');
INSERT INTO `fly_area` VALUES ('1405', '146', '福清市', '3', '350181', '50');
INSERT INTO `fly_area` VALUES ('1406', '146', '长乐市', '3', '350182', '50');
INSERT INTO `fly_area` VALUES ('1407', '147', '思明区', '3', '350203', '50');
INSERT INTO `fly_area` VALUES ('1408', '147', '海沧区', '3', '350205', '50');
INSERT INTO `fly_area` VALUES ('1409', '147', '湖里区', '3', '350206', '50');
INSERT INTO `fly_area` VALUES ('1410', '147', '集美区', '3', '350211', '50');
INSERT INTO `fly_area` VALUES ('1411', '147', '同安区', '3', '350212', '50');
INSERT INTO `fly_area` VALUES ('1412', '147', '翔安区', '3', '350213', '50');
INSERT INTO `fly_area` VALUES ('1413', '148', '城厢区', '3', '350302', '50');
INSERT INTO `fly_area` VALUES ('1414', '148', '涵江区', '3', '350303', '50');
INSERT INTO `fly_area` VALUES ('1415', '148', '荔城区', '3', '350304', '50');
INSERT INTO `fly_area` VALUES ('1416', '148', '秀屿区', '3', '350305', '50');
INSERT INTO `fly_area` VALUES ('1417', '148', '仙游县', '3', '350322', '50');
INSERT INTO `fly_area` VALUES ('1418', '149', '梅列区', '3', '350402', '50');
INSERT INTO `fly_area` VALUES ('1419', '149', '三元区', '3', '350403', '50');
INSERT INTO `fly_area` VALUES ('1420', '149', '明溪县', '3', '350421', '50');
INSERT INTO `fly_area` VALUES ('1421', '149', '清流县', '3', '350423', '50');
INSERT INTO `fly_area` VALUES ('1422', '149', '宁化县', '3', '350424', '50');
INSERT INTO `fly_area` VALUES ('1423', '149', '大田县', '3', '350425', '50');
INSERT INTO `fly_area` VALUES ('1424', '149', '尤溪县', '3', '350426', '50');
INSERT INTO `fly_area` VALUES ('1425', '149', '沙县', '3', '350427', '50');
INSERT INTO `fly_area` VALUES ('1426', '149', '将乐县', '3', '350428', '50');
INSERT INTO `fly_area` VALUES ('1427', '149', '泰宁县', '3', '350429', '50');
INSERT INTO `fly_area` VALUES ('1428', '149', '建宁县', '3', '350430', '50');
INSERT INTO `fly_area` VALUES ('1429', '149', '永安市', '3', '350481', '50');
INSERT INTO `fly_area` VALUES ('1430', '150', '鲤城区', '3', '350502', '50');
INSERT INTO `fly_area` VALUES ('1431', '150', '丰泽区', '3', '350503', '50');
INSERT INTO `fly_area` VALUES ('1432', '150', '洛江区', '3', '350504', '50');
INSERT INTO `fly_area` VALUES ('1433', '150', '泉港区', '3', '350505', '50');
INSERT INTO `fly_area` VALUES ('1434', '150', '惠安县', '3', '350521', '50');
INSERT INTO `fly_area` VALUES ('1435', '150', '安溪县', '3', '350524', '50');
INSERT INTO `fly_area` VALUES ('1436', '150', '永春县', '3', '350525', '50');
INSERT INTO `fly_area` VALUES ('1437', '150', '德化县', '3', '350526', '50');
INSERT INTO `fly_area` VALUES ('1438', '150', '金门县', '3', '350527', '50');
INSERT INTO `fly_area` VALUES ('1439', '150', '石狮市', '3', '350581', '50');
INSERT INTO `fly_area` VALUES ('1440', '150', '晋江市', '3', '350582', '50');
INSERT INTO `fly_area` VALUES ('1441', '150', '南安市', '3', '350583', '50');
INSERT INTO `fly_area` VALUES ('1442', '151', '芗城区', '3', '350602', '50');
INSERT INTO `fly_area` VALUES ('1443', '151', '龙文区', '3', '350603', '50');
INSERT INTO `fly_area` VALUES ('1444', '151', '云霄县', '3', '350622', '50');
INSERT INTO `fly_area` VALUES ('1445', '151', '漳浦县', '3', '350623', '50');
INSERT INTO `fly_area` VALUES ('1446', '151', '诏安县', '3', '350624', '50');
INSERT INTO `fly_area` VALUES ('1447', '151', '长泰县', '3', '350625', '50');
INSERT INTO `fly_area` VALUES ('1448', '151', '东山县', '3', '350626', '50');
INSERT INTO `fly_area` VALUES ('1449', '151', '南靖县', '3', '350627', '50');
INSERT INTO `fly_area` VALUES ('1450', '151', '平和县', '3', '350628', '50');
INSERT INTO `fly_area` VALUES ('1451', '151', '华安县', '3', '350629', '50');
INSERT INTO `fly_area` VALUES ('1452', '151', '龙海市', '3', '350681', '50');
INSERT INTO `fly_area` VALUES ('1453', '152', '延平区', '3', '350702', '50');
INSERT INTO `fly_area` VALUES ('1454', '152', '建阳区', '3', '350703', '50');
INSERT INTO `fly_area` VALUES ('1455', '152', '顺昌县', '3', '350721', '50');
INSERT INTO `fly_area` VALUES ('1456', '152', '浦城县', '3', '350722', '50');
INSERT INTO `fly_area` VALUES ('1457', '152', '光泽县', '3', '350723', '50');
INSERT INTO `fly_area` VALUES ('1458', '152', '松溪县', '3', '350724', '50');
INSERT INTO `fly_area` VALUES ('1459', '152', '政和县', '3', '350725', '50');
INSERT INTO `fly_area` VALUES ('1460', '152', '邵武市', '3', '350781', '50');
INSERT INTO `fly_area` VALUES ('1461', '152', '武夷山市', '3', '350782', '50');
INSERT INTO `fly_area` VALUES ('1462', '152', '建瓯市', '3', '350783', '50');
INSERT INTO `fly_area` VALUES ('1463', '153', '新罗区', '3', '350802', '50');
INSERT INTO `fly_area` VALUES ('1464', '153', '永定区', '3', '350803', '50');
INSERT INTO `fly_area` VALUES ('1465', '153', '长汀县', '3', '350821', '50');
INSERT INTO `fly_area` VALUES ('1466', '153', '上杭县', '3', '350823', '50');
INSERT INTO `fly_area` VALUES ('1467', '153', '武平县', '3', '350824', '50');
INSERT INTO `fly_area` VALUES ('1468', '153', '连城县', '3', '350825', '50');
INSERT INTO `fly_area` VALUES ('1469', '153', '漳平市', '3', '350881', '50');
INSERT INTO `fly_area` VALUES ('1470', '154', '蕉城区', '3', '350902', '50');
INSERT INTO `fly_area` VALUES ('1471', '154', '霞浦县', '3', '350921', '50');
INSERT INTO `fly_area` VALUES ('1472', '154', '古田县', '3', '350922', '50');
INSERT INTO `fly_area` VALUES ('1473', '154', '屏南县', '3', '350923', '50');
INSERT INTO `fly_area` VALUES ('1474', '154', '寿宁县', '3', '350924', '50');
INSERT INTO `fly_area` VALUES ('1475', '154', '周宁县', '3', '350925', '50');
INSERT INTO `fly_area` VALUES ('1476', '154', '柘荣县', '3', '350926', '50');
INSERT INTO `fly_area` VALUES ('1477', '154', '福安市', '3', '350981', '50');
INSERT INTO `fly_area` VALUES ('1478', '154', '福鼎市', '3', '350982', '50');
INSERT INTO `fly_area` VALUES ('1479', '155', '东湖区', '3', '360102', '50');
INSERT INTO `fly_area` VALUES ('1480', '155', '西湖区', '3', '360103', '50');
INSERT INTO `fly_area` VALUES ('1481', '155', '青云谱区', '3', '360104', '50');
INSERT INTO `fly_area` VALUES ('1482', '155', '湾里区', '3', '360105', '50');
INSERT INTO `fly_area` VALUES ('1483', '155', '青山湖区', '3', '360111', '50');
INSERT INTO `fly_area` VALUES ('1484', '155', '新建区', '3', '360112', '50');
INSERT INTO `fly_area` VALUES ('1485', '155', '南昌县', '3', '360121', '50');
INSERT INTO `fly_area` VALUES ('1486', '155', '安义县', '3', '360123', '50');
INSERT INTO `fly_area` VALUES ('1487', '155', '进贤县', '3', '360124', '50');
INSERT INTO `fly_area` VALUES ('1488', '156', '昌江区', '3', '360202', '50');
INSERT INTO `fly_area` VALUES ('1489', '156', '珠山区', '3', '360203', '50');
INSERT INTO `fly_area` VALUES ('1490', '156', '浮梁县', '3', '360222', '50');
INSERT INTO `fly_area` VALUES ('1491', '156', '乐平市', '3', '360281', '50');
INSERT INTO `fly_area` VALUES ('1492', '157', '安源区', '3', '360302', '50');
INSERT INTO `fly_area` VALUES ('1493', '157', '湘东区', '3', '360313', '50');
INSERT INTO `fly_area` VALUES ('1494', '157', '莲花县', '3', '360321', '50');
INSERT INTO `fly_area` VALUES ('1495', '157', '上栗县', '3', '360322', '50');
INSERT INTO `fly_area` VALUES ('1496', '157', '芦溪县', '3', '360323', '50');
INSERT INTO `fly_area` VALUES ('1497', '158', '濂溪区', '3', '360402', '50');
INSERT INTO `fly_area` VALUES ('1498', '158', '浔阳区', '3', '360403', '50');
INSERT INTO `fly_area` VALUES ('1499', '158', '九江县', '3', '360421', '50');
INSERT INTO `fly_area` VALUES ('1500', '158', '武宁县', '3', '360423', '50');
INSERT INTO `fly_area` VALUES ('1501', '158', '修水县', '3', '360424', '50');
INSERT INTO `fly_area` VALUES ('1502', '158', '永修县', '3', '360425', '50');
INSERT INTO `fly_area` VALUES ('1503', '158', '德安县', '3', '360426', '50');
INSERT INTO `fly_area` VALUES ('1504', '158', '都昌县', '3', '360428', '50');
INSERT INTO `fly_area` VALUES ('1505', '158', '湖口县', '3', '360429', '50');
INSERT INTO `fly_area` VALUES ('1506', '158', '彭泽县', '3', '360430', '50');
INSERT INTO `fly_area` VALUES ('1507', '158', '瑞昌市', '3', '360481', '50');
INSERT INTO `fly_area` VALUES ('1508', '158', '共青城市', '3', '360482', '50');
INSERT INTO `fly_area` VALUES ('1509', '158', '庐山市', '3', '360483', '50');
INSERT INTO `fly_area` VALUES ('1510', '159', '渝水区', '3', '360502', '50');
INSERT INTO `fly_area` VALUES ('1511', '159', '分宜县', '3', '360521', '50');
INSERT INTO `fly_area` VALUES ('1512', '160', '月湖区', '3', '360602', '50');
INSERT INTO `fly_area` VALUES ('1513', '160', '余江县', '3', '360622', '50');
INSERT INTO `fly_area` VALUES ('1514', '160', '贵溪市', '3', '360681', '50');
INSERT INTO `fly_area` VALUES ('1515', '161', '章贡区', '3', '360702', '50');
INSERT INTO `fly_area` VALUES ('1516', '161', '南康区', '3', '360703', '50');
INSERT INTO `fly_area` VALUES ('1517', '161', '赣县', '3', '360721', '50');
INSERT INTO `fly_area` VALUES ('1518', '161', '信丰县', '3', '360722', '50');
INSERT INTO `fly_area` VALUES ('1519', '161', '大余县', '3', '360723', '50');
INSERT INTO `fly_area` VALUES ('1520', '161', '上犹县', '3', '360724', '50');
INSERT INTO `fly_area` VALUES ('1521', '161', '崇义县', '3', '360725', '50');
INSERT INTO `fly_area` VALUES ('1522', '161', '安远县', '3', '360726', '50');
INSERT INTO `fly_area` VALUES ('1523', '161', '龙南县', '3', '360727', '50');
INSERT INTO `fly_area` VALUES ('1524', '161', '定南县', '3', '360728', '50');
INSERT INTO `fly_area` VALUES ('1525', '161', '全南县', '3', '360729', '50');
INSERT INTO `fly_area` VALUES ('1526', '161', '宁都县', '3', '360730', '50');
INSERT INTO `fly_area` VALUES ('1527', '161', '于都县', '3', '360731', '50');
INSERT INTO `fly_area` VALUES ('1528', '161', '兴国县', '3', '360732', '50');
INSERT INTO `fly_area` VALUES ('1529', '161', '会昌县', '3', '360733', '50');
INSERT INTO `fly_area` VALUES ('1530', '161', '寻乌县', '3', '360734', '50');
INSERT INTO `fly_area` VALUES ('1531', '161', '石城县', '3', '360735', '50');
INSERT INTO `fly_area` VALUES ('1532', '161', '瑞金市', '3', '360781', '50');
INSERT INTO `fly_area` VALUES ('1533', '162', '吉州区', '3', '360802', '50');
INSERT INTO `fly_area` VALUES ('1534', '162', '青原区', '3', '360803', '50');
INSERT INTO `fly_area` VALUES ('1535', '162', '吉安县', '3', '360821', '50');
INSERT INTO `fly_area` VALUES ('1536', '162', '吉水县', '3', '360822', '50');
INSERT INTO `fly_area` VALUES ('1537', '162', '峡江县', '3', '360823', '50');
INSERT INTO `fly_area` VALUES ('1538', '162', '新干县', '3', '360824', '50');
INSERT INTO `fly_area` VALUES ('1539', '162', '永丰县', '3', '360825', '50');
INSERT INTO `fly_area` VALUES ('1540', '162', '泰和县', '3', '360826', '50');
INSERT INTO `fly_area` VALUES ('1541', '162', '遂川县', '3', '360827', '50');
INSERT INTO `fly_area` VALUES ('1542', '162', '万安县', '3', '360828', '50');
INSERT INTO `fly_area` VALUES ('1543', '162', '安福县', '3', '360829', '50');
INSERT INTO `fly_area` VALUES ('1544', '162', '永新县', '3', '360830', '50');
INSERT INTO `fly_area` VALUES ('1545', '162', '井冈山市', '3', '360881', '50');
INSERT INTO `fly_area` VALUES ('1546', '163', '袁州区', '3', '360902', '50');
INSERT INTO `fly_area` VALUES ('1547', '163', '奉新县', '3', '360921', '50');
INSERT INTO `fly_area` VALUES ('1548', '163', '万载县', '3', '360922', '50');
INSERT INTO `fly_area` VALUES ('1549', '163', '上高县', '3', '360923', '50');
INSERT INTO `fly_area` VALUES ('1550', '163', '宜丰县', '3', '360924', '50');
INSERT INTO `fly_area` VALUES ('1551', '163', '靖安县', '3', '360925', '50');
INSERT INTO `fly_area` VALUES ('1552', '163', '铜鼓县', '3', '360926', '50');
INSERT INTO `fly_area` VALUES ('1553', '163', '丰城市', '3', '360981', '50');
INSERT INTO `fly_area` VALUES ('1554', '163', '樟树市', '3', '360982', '50');
INSERT INTO `fly_area` VALUES ('1555', '163', '高安市', '3', '360983', '50');
INSERT INTO `fly_area` VALUES ('1556', '164', '临川区', '3', '361002', '50');
INSERT INTO `fly_area` VALUES ('1557', '164', '南城县', '3', '361021', '50');
INSERT INTO `fly_area` VALUES ('1558', '164', '黎川县', '3', '361022', '50');
INSERT INTO `fly_area` VALUES ('1559', '164', '南丰县', '3', '361023', '50');
INSERT INTO `fly_area` VALUES ('1560', '164', '崇仁县', '3', '361024', '50');
INSERT INTO `fly_area` VALUES ('1561', '164', '乐安县', '3', '361025', '50');
INSERT INTO `fly_area` VALUES ('1562', '164', '宜黄县', '3', '361026', '50');
INSERT INTO `fly_area` VALUES ('1563', '164', '金溪县', '3', '361027', '50');
INSERT INTO `fly_area` VALUES ('1564', '164', '资溪县', '3', '361028', '50');
INSERT INTO `fly_area` VALUES ('1565', '164', '东乡县', '3', '361029', '50');
INSERT INTO `fly_area` VALUES ('1566', '164', '广昌县', '3', '361030', '50');
INSERT INTO `fly_area` VALUES ('1567', '165', '信州区', '3', '361102', '50');
INSERT INTO `fly_area` VALUES ('1568', '165', '广丰区', '3', '361103', '50');
INSERT INTO `fly_area` VALUES ('1569', '165', '上饶县', '3', '361121', '50');
INSERT INTO `fly_area` VALUES ('1570', '165', '玉山县', '3', '361123', '50');
INSERT INTO `fly_area` VALUES ('1571', '165', '铅山县', '3', '361124', '50');
INSERT INTO `fly_area` VALUES ('1572', '165', '横峰县', '3', '361125', '50');
INSERT INTO `fly_area` VALUES ('1573', '165', '弋阳县', '3', '361126', '50');
INSERT INTO `fly_area` VALUES ('1574', '165', '余干县', '3', '361127', '50');
INSERT INTO `fly_area` VALUES ('1575', '165', '鄱阳县', '3', '361128', '50');
INSERT INTO `fly_area` VALUES ('1576', '165', '万年县', '3', '361129', '50');
INSERT INTO `fly_area` VALUES ('1577', '165', '婺源县', '3', '361130', '50');
INSERT INTO `fly_area` VALUES ('1578', '165', '德兴市', '3', '361181', '50');
INSERT INTO `fly_area` VALUES ('1579', '166', '历下区', '3', '370102', '50');
INSERT INTO `fly_area` VALUES ('1580', '166', '市中区', '3', '370103', '50');
INSERT INTO `fly_area` VALUES ('1581', '166', '槐荫区', '3', '370104', '50');
INSERT INTO `fly_area` VALUES ('1582', '166', '天桥区', '3', '370105', '50');
INSERT INTO `fly_area` VALUES ('1583', '166', '历城区', '3', '370112', '50');
INSERT INTO `fly_area` VALUES ('1584', '166', '长清区', '3', '370113', '50');
INSERT INTO `fly_area` VALUES ('1585', '166', '平阴县', '3', '370124', '50');
INSERT INTO `fly_area` VALUES ('1586', '166', '济阳县', '3', '370125', '50');
INSERT INTO `fly_area` VALUES ('1587', '166', '商河县', '3', '370126', '50');
INSERT INTO `fly_area` VALUES ('1588', '166', '章丘市', '3', '370181', '50');
INSERT INTO `fly_area` VALUES ('1589', '167', '市南区', '3', '370202', '50');
INSERT INTO `fly_area` VALUES ('1590', '167', '市北区', '3', '370203', '50');
INSERT INTO `fly_area` VALUES ('1591', '167', '黄岛区', '3', '370211', '50');
INSERT INTO `fly_area` VALUES ('1592', '167', '崂山区', '3', '370212', '50');
INSERT INTO `fly_area` VALUES ('1593', '167', '李沧区', '3', '370213', '50');
INSERT INTO `fly_area` VALUES ('1594', '167', '城阳区', '3', '370214', '50');
INSERT INTO `fly_area` VALUES ('1595', '167', '胶州市', '3', '370281', '50');
INSERT INTO `fly_area` VALUES ('1596', '167', '即墨市', '3', '370282', '50');
INSERT INTO `fly_area` VALUES ('1597', '167', '平度市', '3', '370283', '50');
INSERT INTO `fly_area` VALUES ('1598', '167', '莱西市', '3', '370285', '50');
INSERT INTO `fly_area` VALUES ('1599', '168', '淄川区', '3', '370302', '50');
INSERT INTO `fly_area` VALUES ('1600', '168', '张店区', '3', '370303', '50');
INSERT INTO `fly_area` VALUES ('1601', '168', '博山区', '3', '370304', '50');
INSERT INTO `fly_area` VALUES ('1602', '168', '临淄区', '3', '370305', '50');
INSERT INTO `fly_area` VALUES ('1603', '168', '周村区', '3', '370306', '50');
INSERT INTO `fly_area` VALUES ('1604', '168', '桓台县', '3', '370321', '50');
INSERT INTO `fly_area` VALUES ('1605', '168', '高青县', '3', '370322', '50');
INSERT INTO `fly_area` VALUES ('1606', '168', '沂源县', '3', '370323', '50');
INSERT INTO `fly_area` VALUES ('1607', '169', '市中区', '3', '370402', '50');
INSERT INTO `fly_area` VALUES ('1608', '169', '薛城区', '3', '370403', '50');
INSERT INTO `fly_area` VALUES ('1609', '169', '峄城区', '3', '370404', '50');
INSERT INTO `fly_area` VALUES ('1610', '169', '台儿庄区', '3', '370405', '50');
INSERT INTO `fly_area` VALUES ('1611', '169', '山亭区', '3', '370406', '50');
INSERT INTO `fly_area` VALUES ('1612', '169', '滕州市', '3', '370481', '50');
INSERT INTO `fly_area` VALUES ('1613', '170', '东营区', '3', '370502', '50');
INSERT INTO `fly_area` VALUES ('1614', '170', '河口区', '3', '370503', '50');
INSERT INTO `fly_area` VALUES ('1615', '170', '垦利区', '3', '370505', '50');
INSERT INTO `fly_area` VALUES ('1616', '170', '利津县', '3', '370522', '50');
INSERT INTO `fly_area` VALUES ('1617', '170', '广饶县', '3', '370523', '50');
INSERT INTO `fly_area` VALUES ('1618', '171', '芝罘区', '3', '370602', '50');
INSERT INTO `fly_area` VALUES ('1619', '171', '福山区', '3', '370611', '50');
INSERT INTO `fly_area` VALUES ('1620', '171', '牟平区', '3', '370612', '50');
INSERT INTO `fly_area` VALUES ('1621', '171', '莱山区', '3', '370613', '50');
INSERT INTO `fly_area` VALUES ('1622', '171', '长岛县', '3', '370634', '50');
INSERT INTO `fly_area` VALUES ('1623', '171', '龙口市', '3', '370681', '50');
INSERT INTO `fly_area` VALUES ('1624', '171', '莱阳市', '3', '370682', '50');
INSERT INTO `fly_area` VALUES ('1625', '171', '莱州市', '3', '370683', '50');
INSERT INTO `fly_area` VALUES ('1626', '171', '蓬莱市', '3', '370684', '50');
INSERT INTO `fly_area` VALUES ('1627', '171', '招远市', '3', '370685', '50');
INSERT INTO `fly_area` VALUES ('1628', '171', '栖霞市', '3', '370686', '50');
INSERT INTO `fly_area` VALUES ('1629', '171', '海阳市', '3', '370687', '50');
INSERT INTO `fly_area` VALUES ('1630', '172', '潍城区', '3', '370702', '50');
INSERT INTO `fly_area` VALUES ('1631', '172', '寒亭区', '3', '370703', '50');
INSERT INTO `fly_area` VALUES ('1632', '172', '坊子区', '3', '370704', '50');
INSERT INTO `fly_area` VALUES ('1633', '172', '奎文区', '3', '370705', '50');
INSERT INTO `fly_area` VALUES ('1634', '172', '临朐县', '3', '370724', '50');
INSERT INTO `fly_area` VALUES ('1635', '172', '昌乐县', '3', '370725', '50');
INSERT INTO `fly_area` VALUES ('1636', '172', '青州市', '3', '370781', '50');
INSERT INTO `fly_area` VALUES ('1637', '172', '诸城市', '3', '370782', '50');
INSERT INTO `fly_area` VALUES ('1638', '172', '寿光市', '3', '370783', '50');
INSERT INTO `fly_area` VALUES ('1639', '172', '安丘市', '3', '370784', '50');
INSERT INTO `fly_area` VALUES ('1640', '172', '高密市', '3', '370785', '50');
INSERT INTO `fly_area` VALUES ('1641', '172', '昌邑市', '3', '370786', '50');
INSERT INTO `fly_area` VALUES ('1642', '173', '任城区', '3', '370811', '50');
INSERT INTO `fly_area` VALUES ('1643', '173', '兖州区', '3', '370812', '50');
INSERT INTO `fly_area` VALUES ('1644', '173', '微山县', '3', '370826', '50');
INSERT INTO `fly_area` VALUES ('1645', '173', '鱼台县', '3', '370827', '50');
INSERT INTO `fly_area` VALUES ('1646', '173', '金乡县', '3', '370828', '50');
INSERT INTO `fly_area` VALUES ('1647', '173', '嘉祥县', '3', '370829', '50');
INSERT INTO `fly_area` VALUES ('1648', '173', '汶上县', '3', '370830', '50');
INSERT INTO `fly_area` VALUES ('1649', '173', '泗水县', '3', '370831', '50');
INSERT INTO `fly_area` VALUES ('1650', '173', '梁山县', '3', '370832', '50');
INSERT INTO `fly_area` VALUES ('1651', '173', '曲阜市', '3', '370881', '50');
INSERT INTO `fly_area` VALUES ('1652', '173', '邹城市', '3', '370883', '50');
INSERT INTO `fly_area` VALUES ('1653', '174', '泰山区', '3', '370902', '50');
INSERT INTO `fly_area` VALUES ('1654', '174', '岱岳区', '3', '370911', '50');
INSERT INTO `fly_area` VALUES ('1655', '174', '宁阳县', '3', '370921', '50');
INSERT INTO `fly_area` VALUES ('1656', '174', '东平县', '3', '370923', '50');
INSERT INTO `fly_area` VALUES ('1657', '174', '新泰市', '3', '370982', '50');
INSERT INTO `fly_area` VALUES ('1658', '174', '肥城市', '3', '370983', '50');
INSERT INTO `fly_area` VALUES ('1659', '175', '环翠区', '3', '371002', '50');
INSERT INTO `fly_area` VALUES ('1660', '175', '文登区', '3', '371003', '50');
INSERT INTO `fly_area` VALUES ('1661', '175', '荣成市', '3', '371082', '50');
INSERT INTO `fly_area` VALUES ('1662', '175', '乳山市', '3', '371083', '50');
INSERT INTO `fly_area` VALUES ('1663', '176', '东港区', '3', '371102', '50');
INSERT INTO `fly_area` VALUES ('1664', '176', '岚山区', '3', '371103', '50');
INSERT INTO `fly_area` VALUES ('1665', '176', '五莲县', '3', '371121', '50');
INSERT INTO `fly_area` VALUES ('1666', '176', '莒县', '3', '371122', '50');
INSERT INTO `fly_area` VALUES ('1667', '177', '莱城区', '3', '371202', '50');
INSERT INTO `fly_area` VALUES ('1668', '177', '钢城区', '3', '371203', '50');
INSERT INTO `fly_area` VALUES ('1669', '178', '兰山区', '3', '371302', '50');
INSERT INTO `fly_area` VALUES ('1670', '178', '罗庄区', '3', '371311', '50');
INSERT INTO `fly_area` VALUES ('1671', '178', '河东区', '3', '371312', '50');
INSERT INTO `fly_area` VALUES ('1672', '178', '沂南县', '3', '371321', '50');
INSERT INTO `fly_area` VALUES ('1673', '178', '郯城县', '3', '371322', '50');
INSERT INTO `fly_area` VALUES ('1674', '178', '沂水县', '3', '371323', '50');
INSERT INTO `fly_area` VALUES ('1675', '178', '兰陵县', '3', '371324', '50');
INSERT INTO `fly_area` VALUES ('1676', '178', '费县', '3', '371325', '50');
INSERT INTO `fly_area` VALUES ('1677', '178', '平邑县', '3', '371326', '50');
INSERT INTO `fly_area` VALUES ('1678', '178', '莒南县', '3', '371327', '50');
INSERT INTO `fly_area` VALUES ('1679', '178', '蒙阴县', '3', '371328', '50');
INSERT INTO `fly_area` VALUES ('1680', '178', '临沭县', '3', '371329', '50');
INSERT INTO `fly_area` VALUES ('1681', '179', '德城区', '3', '371402', '50');
INSERT INTO `fly_area` VALUES ('1682', '179', '陵城区', '3', '371403', '50');
INSERT INTO `fly_area` VALUES ('1683', '179', '宁津县', '3', '371422', '50');
INSERT INTO `fly_area` VALUES ('1684', '179', '庆云县', '3', '371423', '50');
INSERT INTO `fly_area` VALUES ('1685', '179', '临邑县', '3', '371424', '50');
INSERT INTO `fly_area` VALUES ('1686', '179', '齐河县', '3', '371425', '50');
INSERT INTO `fly_area` VALUES ('1687', '179', '平原县', '3', '371426', '50');
INSERT INTO `fly_area` VALUES ('1688', '179', '夏津县', '3', '371427', '50');
INSERT INTO `fly_area` VALUES ('1689', '179', '武城县', '3', '371428', '50');
INSERT INTO `fly_area` VALUES ('1690', '179', '乐陵市', '3', '371481', '50');
INSERT INTO `fly_area` VALUES ('1691', '179', '禹城市', '3', '371482', '50');
INSERT INTO `fly_area` VALUES ('1692', '180', '东昌府区', '3', '371502', '50');
INSERT INTO `fly_area` VALUES ('1693', '180', '阳谷县', '3', '371521', '50');
INSERT INTO `fly_area` VALUES ('1694', '180', '莘县', '3', '371522', '50');
INSERT INTO `fly_area` VALUES ('1695', '180', '茌平县', '3', '371523', '50');
INSERT INTO `fly_area` VALUES ('1696', '180', '东阿县', '3', '371524', '50');
INSERT INTO `fly_area` VALUES ('1697', '180', '冠县', '3', '371525', '50');
INSERT INTO `fly_area` VALUES ('1698', '180', '高唐县', '3', '371526', '50');
INSERT INTO `fly_area` VALUES ('1699', '180', '临清市', '3', '371581', '50');
INSERT INTO `fly_area` VALUES ('1700', '181', '滨城区', '3', '371602', '50');
INSERT INTO `fly_area` VALUES ('1701', '181', '沾化区', '3', '371603', '50');
INSERT INTO `fly_area` VALUES ('1702', '181', '惠民县', '3', '371621', '50');
INSERT INTO `fly_area` VALUES ('1703', '181', '阳信县', '3', '371622', '50');
INSERT INTO `fly_area` VALUES ('1704', '181', '无棣县', '3', '371623', '50');
INSERT INTO `fly_area` VALUES ('1705', '181', '博兴县', '3', '371625', '50');
INSERT INTO `fly_area` VALUES ('1706', '181', '邹平县', '3', '371626', '50');
INSERT INTO `fly_area` VALUES ('1707', '182', '牡丹区', '3', '371702', '50');
INSERT INTO `fly_area` VALUES ('1708', '182', '定陶区', '3', '371703', '50');
INSERT INTO `fly_area` VALUES ('1709', '182', '曹县', '3', '371721', '50');
INSERT INTO `fly_area` VALUES ('1710', '182', '单县', '3', '371722', '50');
INSERT INTO `fly_area` VALUES ('1711', '182', '成武县', '3', '371723', '50');
INSERT INTO `fly_area` VALUES ('1712', '182', '巨野县', '3', '371724', '50');
INSERT INTO `fly_area` VALUES ('1713', '182', '郓城县', '3', '371725', '50');
INSERT INTO `fly_area` VALUES ('1714', '182', '鄄城县', '3', '371726', '50');
INSERT INTO `fly_area` VALUES ('1715', '182', '东明县', '3', '371728', '50');
INSERT INTO `fly_area` VALUES ('1716', '183', '中原区', '3', '410102', '50');
INSERT INTO `fly_area` VALUES ('1717', '183', '二七区', '3', '410103', '50');
INSERT INTO `fly_area` VALUES ('1718', '183', '管城回族区', '3', '410104', '50');
INSERT INTO `fly_area` VALUES ('1719', '183', '金水区', '3', '410105', '50');
INSERT INTO `fly_area` VALUES ('1720', '183', '上街区', '3', '410106', '50');
INSERT INTO `fly_area` VALUES ('1721', '183', '惠济区', '3', '410108', '50');
INSERT INTO `fly_area` VALUES ('1722', '183', '中牟县', '3', '410122', '50');
INSERT INTO `fly_area` VALUES ('1723', '183', '巩义市', '3', '410181', '50');
INSERT INTO `fly_area` VALUES ('1724', '183', '荥阳市', '3', '410182', '50');
INSERT INTO `fly_area` VALUES ('1725', '183', '新密市', '3', '410183', '50');
INSERT INTO `fly_area` VALUES ('1726', '183', '新郑市', '3', '410184', '50');
INSERT INTO `fly_area` VALUES ('1727', '183', '登封市', '3', '410185', '50');
INSERT INTO `fly_area` VALUES ('1728', '184', '龙亭区', '3', '410202', '50');
INSERT INTO `fly_area` VALUES ('1729', '184', '顺河回族区', '3', '410203', '50');
INSERT INTO `fly_area` VALUES ('1730', '184', '鼓楼区', '3', '410204', '50');
INSERT INTO `fly_area` VALUES ('1731', '184', '禹王台区', '3', '410205', '50');
INSERT INTO `fly_area` VALUES ('1732', '184', '金明区', '3', '410211', '50');
INSERT INTO `fly_area` VALUES ('1733', '184', '祥符区', '3', '410212', '50');
INSERT INTO `fly_area` VALUES ('1734', '184', '杞县', '3', '410221', '50');
INSERT INTO `fly_area` VALUES ('1735', '184', '通许县', '3', '410222', '50');
INSERT INTO `fly_area` VALUES ('1736', '184', '尉氏县', '3', '410223', '50');
INSERT INTO `fly_area` VALUES ('1737', '184', '兰考县', '3', '410225', '50');
INSERT INTO `fly_area` VALUES ('1738', '185', '老城区', '3', '410302', '50');
INSERT INTO `fly_area` VALUES ('1739', '185', '西工区', '3', '410303', '50');
INSERT INTO `fly_area` VALUES ('1740', '185', '瀍河回族区', '3', '410304', '50');
INSERT INTO `fly_area` VALUES ('1741', '185', '涧西区', '3', '410305', '50');
INSERT INTO `fly_area` VALUES ('1742', '185', '吉利区', '3', '410306', '50');
INSERT INTO `fly_area` VALUES ('1743', '185', '洛龙区', '3', '410311', '50');
INSERT INTO `fly_area` VALUES ('1744', '185', '孟津县', '3', '410322', '50');
INSERT INTO `fly_area` VALUES ('1745', '185', '新安县', '3', '410323', '50');
INSERT INTO `fly_area` VALUES ('1746', '185', '栾川县', '3', '410324', '50');
INSERT INTO `fly_area` VALUES ('1747', '185', '嵩县', '3', '410325', '50');
INSERT INTO `fly_area` VALUES ('1748', '185', '汝阳县', '3', '410326', '50');
INSERT INTO `fly_area` VALUES ('1749', '185', '宜阳县', '3', '410327', '50');
INSERT INTO `fly_area` VALUES ('1750', '185', '洛宁县', '3', '410328', '50');
INSERT INTO `fly_area` VALUES ('1751', '185', '伊川县', '3', '410329', '50');
INSERT INTO `fly_area` VALUES ('1752', '185', '偃师市', '3', '410381', '50');
INSERT INTO `fly_area` VALUES ('1753', '186', '新华区', '3', '410402', '50');
INSERT INTO `fly_area` VALUES ('1754', '186', '卫东区', '3', '410403', '50');
INSERT INTO `fly_area` VALUES ('1755', '186', '石龙区', '3', '410404', '50');
INSERT INTO `fly_area` VALUES ('1756', '186', '湛河区', '3', '410411', '50');
INSERT INTO `fly_area` VALUES ('1757', '186', '宝丰县', '3', '410421', '50');
INSERT INTO `fly_area` VALUES ('1758', '186', '叶县', '3', '410422', '50');
INSERT INTO `fly_area` VALUES ('1759', '186', '鲁山县', '3', '410423', '50');
INSERT INTO `fly_area` VALUES ('1760', '186', '郏县', '3', '410425', '50');
INSERT INTO `fly_area` VALUES ('1761', '186', '舞钢市', '3', '410481', '50');
INSERT INTO `fly_area` VALUES ('1762', '186', '汝州市', '3', '410482', '50');
INSERT INTO `fly_area` VALUES ('1763', '187', '文峰区', '3', '410502', '50');
INSERT INTO `fly_area` VALUES ('1764', '187', '北关区', '3', '410503', '50');
INSERT INTO `fly_area` VALUES ('1765', '187', '殷都区', '3', '410505', '50');
INSERT INTO `fly_area` VALUES ('1766', '187', '龙安区', '3', '410506', '50');
INSERT INTO `fly_area` VALUES ('1767', '187', '安阳县', '3', '410522', '50');
INSERT INTO `fly_area` VALUES ('1768', '187', '汤阴县', '3', '410523', '50');
INSERT INTO `fly_area` VALUES ('1769', '187', '滑县', '3', '410526', '50');
INSERT INTO `fly_area` VALUES ('1770', '187', '内黄县', '3', '410527', '50');
INSERT INTO `fly_area` VALUES ('1771', '187', '林州市', '3', '410581', '50');
INSERT INTO `fly_area` VALUES ('1772', '188', '鹤山区', '3', '410602', '50');
INSERT INTO `fly_area` VALUES ('1773', '188', '山城区', '3', '410603', '50');
INSERT INTO `fly_area` VALUES ('1774', '188', '淇滨区', '3', '410611', '50');
INSERT INTO `fly_area` VALUES ('1775', '188', '浚县', '3', '410621', '50');
INSERT INTO `fly_area` VALUES ('1776', '188', '淇县', '3', '410622', '50');
INSERT INTO `fly_area` VALUES ('1777', '189', '红旗区', '3', '410702', '50');
INSERT INTO `fly_area` VALUES ('1778', '189', '卫滨区', '3', '410703', '50');
INSERT INTO `fly_area` VALUES ('1779', '189', '凤泉区', '3', '410704', '50');
INSERT INTO `fly_area` VALUES ('1780', '189', '牧野区', '3', '410711', '50');
INSERT INTO `fly_area` VALUES ('1781', '189', '新乡县', '3', '410721', '50');
INSERT INTO `fly_area` VALUES ('1782', '189', '获嘉县', '3', '410724', '50');
INSERT INTO `fly_area` VALUES ('1783', '189', '原阳县', '3', '410725', '50');
INSERT INTO `fly_area` VALUES ('1784', '189', '延津县', '3', '410726', '50');
INSERT INTO `fly_area` VALUES ('1785', '189', '封丘县', '3', '410727', '50');
INSERT INTO `fly_area` VALUES ('1786', '189', '长垣县', '3', '410728', '50');
INSERT INTO `fly_area` VALUES ('1787', '189', '卫辉市', '3', '410781', '50');
INSERT INTO `fly_area` VALUES ('1788', '189', '辉县市', '3', '410782', '50');
INSERT INTO `fly_area` VALUES ('1789', '190', '解放区', '3', '410802', '50');
INSERT INTO `fly_area` VALUES ('1790', '190', '中站区', '3', '410803', '50');
INSERT INTO `fly_area` VALUES ('1791', '190', '马村区', '3', '410804', '50');
INSERT INTO `fly_area` VALUES ('1792', '190', '山阳区', '3', '410811', '50');
INSERT INTO `fly_area` VALUES ('1793', '190', '修武县', '3', '410821', '50');
INSERT INTO `fly_area` VALUES ('1794', '190', '博爱县', '3', '410822', '50');
INSERT INTO `fly_area` VALUES ('1795', '190', '武陟县', '3', '410823', '50');
INSERT INTO `fly_area` VALUES ('1796', '190', '温县', '3', '410825', '50');
INSERT INTO `fly_area` VALUES ('1797', '190', '沁阳市', '3', '410882', '50');
INSERT INTO `fly_area` VALUES ('1798', '190', '孟州市', '3', '410883', '50');
INSERT INTO `fly_area` VALUES ('1799', '191', '华龙区', '3', '410902', '50');
INSERT INTO `fly_area` VALUES ('1800', '191', '清丰县', '3', '410922', '50');
INSERT INTO `fly_area` VALUES ('1801', '191', '南乐县', '3', '410923', '50');
INSERT INTO `fly_area` VALUES ('1802', '191', '范县', '3', '410926', '50');
INSERT INTO `fly_area` VALUES ('1803', '191', '台前县', '3', '410927', '50');
INSERT INTO `fly_area` VALUES ('1804', '191', '濮阳县', '3', '410928', '50');
INSERT INTO `fly_area` VALUES ('1805', '192', '魏都区', '3', '411002', '50');
INSERT INTO `fly_area` VALUES ('1806', '192', '许昌县', '3', '411023', '50');
INSERT INTO `fly_area` VALUES ('1807', '192', '鄢陵县', '3', '411024', '50');
INSERT INTO `fly_area` VALUES ('1808', '192', '襄城县', '3', '411025', '50');
INSERT INTO `fly_area` VALUES ('1809', '192', '禹州市', '3', '411081', '50');
INSERT INTO `fly_area` VALUES ('1810', '192', '长葛市', '3', '411082', '50');
INSERT INTO `fly_area` VALUES ('1811', '193', '源汇区', '3', '411102', '50');
INSERT INTO `fly_area` VALUES ('1812', '193', '郾城区', '3', '411103', '50');
INSERT INTO `fly_area` VALUES ('1813', '193', '召陵区', '3', '411104', '50');
INSERT INTO `fly_area` VALUES ('1814', '193', '舞阳县', '3', '411121', '50');
INSERT INTO `fly_area` VALUES ('1815', '193', '临颍县', '3', '411122', '50');
INSERT INTO `fly_area` VALUES ('1816', '194', '湖滨区', '3', '411202', '50');
INSERT INTO `fly_area` VALUES ('1817', '194', '陕州区', '3', '411203', '50');
INSERT INTO `fly_area` VALUES ('1818', '194', '渑池县', '3', '411221', '50');
INSERT INTO `fly_area` VALUES ('1819', '194', '卢氏县', '3', '411224', '50');
INSERT INTO `fly_area` VALUES ('1820', '194', '义马市', '3', '411281', '50');
INSERT INTO `fly_area` VALUES ('1821', '194', '灵宝市', '3', '411282', '50');
INSERT INTO `fly_area` VALUES ('1822', '195', '宛城区', '3', '411302', '50');
INSERT INTO `fly_area` VALUES ('1823', '195', '卧龙区', '3', '411303', '50');
INSERT INTO `fly_area` VALUES ('1824', '195', '南召县', '3', '411321', '50');
INSERT INTO `fly_area` VALUES ('1825', '195', '方城县', '3', '411322', '50');
INSERT INTO `fly_area` VALUES ('1826', '195', '西峡县', '3', '411323', '50');
INSERT INTO `fly_area` VALUES ('1827', '195', '镇平县', '3', '411324', '50');
INSERT INTO `fly_area` VALUES ('1828', '195', '内乡县', '3', '411325', '50');
INSERT INTO `fly_area` VALUES ('1829', '195', '淅川县', '3', '411326', '50');
INSERT INTO `fly_area` VALUES ('1830', '195', '社旗县', '3', '411327', '50');
INSERT INTO `fly_area` VALUES ('1831', '195', '唐河县', '3', '411328', '50');
INSERT INTO `fly_area` VALUES ('1832', '195', '新野县', '3', '411329', '50');
INSERT INTO `fly_area` VALUES ('1833', '195', '桐柏县', '3', '411330', '50');
INSERT INTO `fly_area` VALUES ('1834', '195', '邓州市', '3', '411381', '50');
INSERT INTO `fly_area` VALUES ('1835', '196', '梁园区', '3', '411402', '50');
INSERT INTO `fly_area` VALUES ('1836', '196', '睢阳区', '3', '411403', '50');
INSERT INTO `fly_area` VALUES ('1837', '196', '民权县', '3', '411421', '50');
INSERT INTO `fly_area` VALUES ('1838', '196', '睢县', '3', '411422', '50');
INSERT INTO `fly_area` VALUES ('1839', '196', '宁陵县', '3', '411423', '50');
INSERT INTO `fly_area` VALUES ('1840', '196', '柘城县', '3', '411424', '50');
INSERT INTO `fly_area` VALUES ('1841', '196', '虞城县', '3', '411425', '50');
INSERT INTO `fly_area` VALUES ('1842', '196', '夏邑县', '3', '411426', '50');
INSERT INTO `fly_area` VALUES ('1843', '196', '永城市', '3', '411481', '50');
INSERT INTO `fly_area` VALUES ('1844', '197', '浉河区', '3', '411502', '50');
INSERT INTO `fly_area` VALUES ('1845', '197', '平桥区', '3', '411503', '50');
INSERT INTO `fly_area` VALUES ('1846', '197', '罗山县', '3', '411521', '50');
INSERT INTO `fly_area` VALUES ('1847', '197', '光山县', '3', '411522', '50');
INSERT INTO `fly_area` VALUES ('1848', '197', '新县', '3', '411523', '50');
INSERT INTO `fly_area` VALUES ('1849', '197', '商城县', '3', '411524', '50');
INSERT INTO `fly_area` VALUES ('1850', '197', '固始县', '3', '411525', '50');
INSERT INTO `fly_area` VALUES ('1851', '197', '潢川县', '3', '411526', '50');
INSERT INTO `fly_area` VALUES ('1852', '197', '淮滨县', '3', '411527', '50');
INSERT INTO `fly_area` VALUES ('1853', '197', '息县', '3', '411528', '50');
INSERT INTO `fly_area` VALUES ('1854', '198', '川汇区', '3', '411602', '50');
INSERT INTO `fly_area` VALUES ('1855', '198', '扶沟县', '3', '411621', '50');
INSERT INTO `fly_area` VALUES ('1856', '198', '西华县', '3', '411622', '50');
INSERT INTO `fly_area` VALUES ('1857', '198', '商水县', '3', '411623', '50');
INSERT INTO `fly_area` VALUES ('1858', '198', '沈丘县', '3', '411624', '50');
INSERT INTO `fly_area` VALUES ('1859', '198', '郸城县', '3', '411625', '50');
INSERT INTO `fly_area` VALUES ('1860', '198', '淮阳县', '3', '411626', '50');
INSERT INTO `fly_area` VALUES ('1861', '198', '太康县', '3', '411627', '50');
INSERT INTO `fly_area` VALUES ('1862', '198', '鹿邑县', '3', '411628', '50');
INSERT INTO `fly_area` VALUES ('1863', '198', '项城市', '3', '411681', '50');
INSERT INTO `fly_area` VALUES ('1864', '199', '驿城区', '3', '411702', '50');
INSERT INTO `fly_area` VALUES ('1865', '199', '西平县', '3', '411721', '50');
INSERT INTO `fly_area` VALUES ('1866', '199', '上蔡县', '3', '411722', '50');
INSERT INTO `fly_area` VALUES ('1867', '199', '平舆县', '3', '411723', '50');
INSERT INTO `fly_area` VALUES ('1868', '199', '正阳县', '3', '411724', '50');
INSERT INTO `fly_area` VALUES ('1869', '199', '确山县', '3', '411725', '50');
INSERT INTO `fly_area` VALUES ('1870', '199', '泌阳县', '3', '411726', '50');
INSERT INTO `fly_area` VALUES ('1871', '199', '汝南县', '3', '411727', '50');
INSERT INTO `fly_area` VALUES ('1872', '199', '遂平县', '3', '411728', '50');
INSERT INTO `fly_area` VALUES ('1873', '199', '新蔡县', '3', '411729', '50');
INSERT INTO `fly_area` VALUES ('1874', '200', '济源市', '3', '419001', '50');
INSERT INTO `fly_area` VALUES ('1875', '201', '江岸区', '3', '420102', '50');
INSERT INTO `fly_area` VALUES ('1876', '201', '江汉区', '3', '420103', '50');
INSERT INTO `fly_area` VALUES ('1877', '201', '硚口区', '3', '420104', '50');
INSERT INTO `fly_area` VALUES ('1878', '201', '汉阳区', '3', '420105', '50');
INSERT INTO `fly_area` VALUES ('1879', '201', '武昌区', '3', '420106', '50');
INSERT INTO `fly_area` VALUES ('1880', '201', '青山区', '3', '420107', '50');
INSERT INTO `fly_area` VALUES ('1881', '201', '洪山区', '3', '420111', '50');
INSERT INTO `fly_area` VALUES ('1882', '201', '东西湖区', '3', '420112', '50');
INSERT INTO `fly_area` VALUES ('1883', '201', '汉南区', '3', '420113', '50');
INSERT INTO `fly_area` VALUES ('1884', '201', '蔡甸区', '3', '420114', '50');
INSERT INTO `fly_area` VALUES ('1885', '201', '江夏区', '3', '420115', '50');
INSERT INTO `fly_area` VALUES ('1886', '201', '黄陂区', '3', '420116', '50');
INSERT INTO `fly_area` VALUES ('1887', '201', '新洲区', '3', '420117', '50');
INSERT INTO `fly_area` VALUES ('1888', '202', '黄石港区', '3', '420202', '50');
INSERT INTO `fly_area` VALUES ('1889', '202', '西塞山区', '3', '420203', '50');
INSERT INTO `fly_area` VALUES ('1890', '202', '下陆区', '3', '420204', '50');
INSERT INTO `fly_area` VALUES ('1891', '202', '铁山区', '3', '420205', '50');
INSERT INTO `fly_area` VALUES ('1892', '202', '阳新县', '3', '420222', '50');
INSERT INTO `fly_area` VALUES ('1893', '202', '大冶市', '3', '420281', '50');
INSERT INTO `fly_area` VALUES ('1894', '203', '茅箭区', '3', '420302', '50');
INSERT INTO `fly_area` VALUES ('1895', '203', '张湾区', '3', '420303', '50');
INSERT INTO `fly_area` VALUES ('1896', '203', '郧阳区', '3', '420304', '50');
INSERT INTO `fly_area` VALUES ('1897', '203', '郧西县', '3', '420322', '50');
INSERT INTO `fly_area` VALUES ('1898', '203', '竹山县', '3', '420323', '50');
INSERT INTO `fly_area` VALUES ('1899', '203', '竹溪县', '3', '420324', '50');
INSERT INTO `fly_area` VALUES ('1900', '203', '房县', '3', '420325', '50');
INSERT INTO `fly_area` VALUES ('1901', '203', '丹江口市', '3', '420381', '50');
INSERT INTO `fly_area` VALUES ('1902', '204', '西陵区', '3', '420502', '50');
INSERT INTO `fly_area` VALUES ('1903', '204', '伍家岗区', '3', '420503', '50');
INSERT INTO `fly_area` VALUES ('1904', '204', '点军区', '3', '420504', '50');
INSERT INTO `fly_area` VALUES ('1905', '204', '猇亭区', '3', '420505', '50');
INSERT INTO `fly_area` VALUES ('1906', '204', '夷陵区', '3', '420506', '50');
INSERT INTO `fly_area` VALUES ('1907', '204', '远安县', '3', '420525', '50');
INSERT INTO `fly_area` VALUES ('1908', '204', '兴山县', '3', '420526', '50');
INSERT INTO `fly_area` VALUES ('1909', '204', '秭归县', '3', '420527', '50');
INSERT INTO `fly_area` VALUES ('1910', '204', '长阳土家族自治县', '3', '420528', '50');
INSERT INTO `fly_area` VALUES ('1911', '204', '五峰土家族自治县', '3', '420529', '50');
INSERT INTO `fly_area` VALUES ('1912', '204', '宜都市', '3', '420581', '50');
INSERT INTO `fly_area` VALUES ('1913', '204', '当阳市', '3', '420582', '50');
INSERT INTO `fly_area` VALUES ('1914', '204', '枝江市', '3', '420583', '50');
INSERT INTO `fly_area` VALUES ('1915', '205', '襄城区', '3', '420602', '50');
INSERT INTO `fly_area` VALUES ('1916', '205', '樊城区', '3', '420606', '50');
INSERT INTO `fly_area` VALUES ('1917', '205', '襄州区', '3', '420607', '50');
INSERT INTO `fly_area` VALUES ('1918', '205', '南漳县', '3', '420624', '50');
INSERT INTO `fly_area` VALUES ('1919', '205', '谷城县', '3', '420625', '50');
INSERT INTO `fly_area` VALUES ('1920', '205', '保康县', '3', '420626', '50');
INSERT INTO `fly_area` VALUES ('1921', '205', '老河口市', '3', '420682', '50');
INSERT INTO `fly_area` VALUES ('1922', '205', '枣阳市', '3', '420683', '50');
INSERT INTO `fly_area` VALUES ('1923', '205', '宜城市', '3', '420684', '50');
INSERT INTO `fly_area` VALUES ('1924', '206', '梁子湖区', '3', '420702', '50');
INSERT INTO `fly_area` VALUES ('1925', '206', '华容区', '3', '420703', '50');
INSERT INTO `fly_area` VALUES ('1926', '206', '鄂城区', '3', '420704', '50');
INSERT INTO `fly_area` VALUES ('1927', '207', '东宝区', '3', '420802', '50');
INSERT INTO `fly_area` VALUES ('1928', '207', '掇刀区', '3', '420804', '50');
INSERT INTO `fly_area` VALUES ('1929', '207', '京山县', '3', '420821', '50');
INSERT INTO `fly_area` VALUES ('1930', '207', '沙洋县', '3', '420822', '50');
INSERT INTO `fly_area` VALUES ('1931', '207', '钟祥市', '3', '420881', '50');
INSERT INTO `fly_area` VALUES ('1932', '208', '孝南区', '3', '420902', '50');
INSERT INTO `fly_area` VALUES ('1933', '208', '孝昌县', '3', '420921', '50');
INSERT INTO `fly_area` VALUES ('1934', '208', '大悟县', '3', '420922', '50');
INSERT INTO `fly_area` VALUES ('1935', '208', '云梦县', '3', '420923', '50');
INSERT INTO `fly_area` VALUES ('1936', '208', '应城市', '3', '420981', '50');
INSERT INTO `fly_area` VALUES ('1937', '208', '安陆市', '3', '420982', '50');
INSERT INTO `fly_area` VALUES ('1938', '208', '汉川市', '3', '420984', '50');
INSERT INTO `fly_area` VALUES ('1939', '209', '沙市区', '3', '421002', '50');
INSERT INTO `fly_area` VALUES ('1940', '209', '荆州区', '3', '421003', '50');
INSERT INTO `fly_area` VALUES ('1941', '209', '公安县', '3', '421022', '50');
INSERT INTO `fly_area` VALUES ('1942', '209', '监利县', '3', '421023', '50');
INSERT INTO `fly_area` VALUES ('1943', '209', '江陵县', '3', '421024', '50');
INSERT INTO `fly_area` VALUES ('1944', '209', '石首市', '3', '421081', '50');
INSERT INTO `fly_area` VALUES ('1945', '209', '洪湖市', '3', '421083', '50');
INSERT INTO `fly_area` VALUES ('1946', '209', '松滋市', '3', '421087', '50');
INSERT INTO `fly_area` VALUES ('1947', '210', '黄州区', '3', '421102', '50');
INSERT INTO `fly_area` VALUES ('1948', '210', '团风县', '3', '421121', '50');
INSERT INTO `fly_area` VALUES ('1949', '210', '红安县', '3', '421122', '50');
INSERT INTO `fly_area` VALUES ('1950', '210', '罗田县', '3', '421123', '50');
INSERT INTO `fly_area` VALUES ('1951', '210', '英山县', '3', '421124', '50');
INSERT INTO `fly_area` VALUES ('1952', '210', '浠水县', '3', '421125', '50');
INSERT INTO `fly_area` VALUES ('1953', '210', '蕲春县', '3', '421126', '50');
INSERT INTO `fly_area` VALUES ('1954', '210', '黄梅县', '3', '421127', '50');
INSERT INTO `fly_area` VALUES ('1955', '210', '麻城市', '3', '421181', '50');
INSERT INTO `fly_area` VALUES ('1956', '210', '武穴市', '3', '421182', '50');
INSERT INTO `fly_area` VALUES ('1957', '211', '咸安区', '3', '421202', '50');
INSERT INTO `fly_area` VALUES ('1958', '211', '嘉鱼县', '3', '421221', '50');
INSERT INTO `fly_area` VALUES ('1959', '211', '通城县', '3', '421222', '50');
INSERT INTO `fly_area` VALUES ('1960', '211', '崇阳县', '3', '421223', '50');
INSERT INTO `fly_area` VALUES ('1961', '211', '通山县', '3', '421224', '50');
INSERT INTO `fly_area` VALUES ('1962', '211', '赤壁市', '3', '421281', '50');
INSERT INTO `fly_area` VALUES ('1963', '212', '曾都区', '3', '421303', '50');
INSERT INTO `fly_area` VALUES ('1964', '212', '随县', '3', '421321', '50');
INSERT INTO `fly_area` VALUES ('1965', '212', '广水市', '3', '421381', '50');
INSERT INTO `fly_area` VALUES ('1966', '213', '恩施市', '3', '422801', '50');
INSERT INTO `fly_area` VALUES ('1967', '213', '利川市', '3', '422802', '50');
INSERT INTO `fly_area` VALUES ('1968', '213', '建始县', '3', '422822', '50');
INSERT INTO `fly_area` VALUES ('1969', '213', '巴东县', '3', '422823', '50');
INSERT INTO `fly_area` VALUES ('1970', '213', '宣恩县', '3', '422825', '50');
INSERT INTO `fly_area` VALUES ('1971', '213', '咸丰县', '3', '422826', '50');
INSERT INTO `fly_area` VALUES ('1972', '213', '来凤县', '3', '422827', '50');
INSERT INTO `fly_area` VALUES ('1973', '213', '鹤峰县', '3', '422828', '50');
INSERT INTO `fly_area` VALUES ('1974', '214', '仙桃市', '3', '429004', '50');
INSERT INTO `fly_area` VALUES ('1975', '214', '潜江市', '3', '429005', '50');
INSERT INTO `fly_area` VALUES ('1976', '214', '天门市', '3', '429006', '50');
INSERT INTO `fly_area` VALUES ('1977', '214', '神农架林区', '3', '429021', '50');
INSERT INTO `fly_area` VALUES ('1978', '215', '芙蓉区', '3', '430102', '50');
INSERT INTO `fly_area` VALUES ('1979', '215', '天心区', '3', '430103', '50');
INSERT INTO `fly_area` VALUES ('1980', '215', '岳麓区', '3', '430104', '50');
INSERT INTO `fly_area` VALUES ('1981', '215', '开福区', '3', '430105', '50');
INSERT INTO `fly_area` VALUES ('1982', '215', '雨花区', '3', '430111', '50');
INSERT INTO `fly_area` VALUES ('1983', '215', '望城区', '3', '430112', '50');
INSERT INTO `fly_area` VALUES ('1984', '215', '长沙县', '3', '430121', '50');
INSERT INTO `fly_area` VALUES ('1985', '215', '宁乡县', '3', '430124', '50');
INSERT INTO `fly_area` VALUES ('1986', '215', '浏阳市', '3', '430181', '50');
INSERT INTO `fly_area` VALUES ('1987', '216', '荷塘区', '3', '430202', '50');
INSERT INTO `fly_area` VALUES ('1988', '216', '芦淞区', '3', '430203', '50');
INSERT INTO `fly_area` VALUES ('1989', '216', '石峰区', '3', '430204', '50');
INSERT INTO `fly_area` VALUES ('1990', '216', '天元区', '3', '430211', '50');
INSERT INTO `fly_area` VALUES ('1991', '216', '株洲县', '3', '430221', '50');
INSERT INTO `fly_area` VALUES ('1992', '216', '攸县', '3', '430223', '50');
INSERT INTO `fly_area` VALUES ('1993', '216', '茶陵县', '3', '430224', '50');
INSERT INTO `fly_area` VALUES ('1994', '216', '炎陵县', '3', '430225', '50');
INSERT INTO `fly_area` VALUES ('1995', '216', '醴陵市', '3', '430281', '50');
INSERT INTO `fly_area` VALUES ('1996', '217', '雨湖区', '3', '430302', '50');
INSERT INTO `fly_area` VALUES ('1997', '217', '岳塘区', '3', '430304', '50');
INSERT INTO `fly_area` VALUES ('1998', '217', '湘潭县', '3', '430321', '50');
INSERT INTO `fly_area` VALUES ('1999', '217', '湘乡市', '3', '430381', '50');
INSERT INTO `fly_area` VALUES ('2000', '217', '韶山市', '3', '430382', '50');
INSERT INTO `fly_area` VALUES ('2001', '218', '珠晖区', '3', '430405', '50');
INSERT INTO `fly_area` VALUES ('2002', '218', '雁峰区', '3', '430406', '50');
INSERT INTO `fly_area` VALUES ('2003', '218', '石鼓区', '3', '430407', '50');
INSERT INTO `fly_area` VALUES ('2004', '218', '蒸湘区', '3', '430408', '50');
INSERT INTO `fly_area` VALUES ('2005', '218', '南岳区', '3', '430412', '50');
INSERT INTO `fly_area` VALUES ('2006', '218', '衡阳县', '3', '430421', '50');
INSERT INTO `fly_area` VALUES ('2007', '218', '衡南县', '3', '430422', '50');
INSERT INTO `fly_area` VALUES ('2008', '218', '衡山县', '3', '430423', '50');
INSERT INTO `fly_area` VALUES ('2009', '218', '衡东县', '3', '430424', '50');
INSERT INTO `fly_area` VALUES ('2010', '218', '祁东县', '3', '430426', '50');
INSERT INTO `fly_area` VALUES ('2011', '218', '耒阳市', '3', '430481', '50');
INSERT INTO `fly_area` VALUES ('2012', '218', '常宁市', '3', '430482', '50');
INSERT INTO `fly_area` VALUES ('2013', '219', '双清区', '3', '430502', '50');
INSERT INTO `fly_area` VALUES ('2014', '219', '大祥区', '3', '430503', '50');
INSERT INTO `fly_area` VALUES ('2015', '219', '北塔区', '3', '430511', '50');
INSERT INTO `fly_area` VALUES ('2016', '219', '邵东县', '3', '430521', '50');
INSERT INTO `fly_area` VALUES ('2017', '219', '新邵县', '3', '430522', '50');
INSERT INTO `fly_area` VALUES ('2018', '219', '邵阳县', '3', '430523', '50');
INSERT INTO `fly_area` VALUES ('2019', '219', '隆回县', '3', '430524', '50');
INSERT INTO `fly_area` VALUES ('2020', '219', '洞口县', '3', '430525', '50');
INSERT INTO `fly_area` VALUES ('2021', '219', '绥宁县', '3', '430527', '50');
INSERT INTO `fly_area` VALUES ('2022', '219', '新宁县', '3', '430528', '50');
INSERT INTO `fly_area` VALUES ('2023', '219', '城步苗族自治县', '3', '430529', '50');
INSERT INTO `fly_area` VALUES ('2024', '219', '武冈市', '3', '430581', '50');
INSERT INTO `fly_area` VALUES ('2025', '220', '岳阳楼区', '3', '430602', '50');
INSERT INTO `fly_area` VALUES ('2026', '220', '云溪区', '3', '430603', '50');
INSERT INTO `fly_area` VALUES ('2027', '220', '君山区', '3', '430611', '50');
INSERT INTO `fly_area` VALUES ('2028', '220', '岳阳县', '3', '430621', '50');
INSERT INTO `fly_area` VALUES ('2029', '220', '华容县', '3', '430623', '50');
INSERT INTO `fly_area` VALUES ('2030', '220', '湘阴县', '3', '430624', '50');
INSERT INTO `fly_area` VALUES ('2031', '220', '平江县', '3', '430626', '50');
INSERT INTO `fly_area` VALUES ('2032', '220', '汨罗市', '3', '430681', '50');
INSERT INTO `fly_area` VALUES ('2033', '220', '临湘市', '3', '430682', '50');
INSERT INTO `fly_area` VALUES ('2034', '221', '武陵区', '3', '430702', '50');
INSERT INTO `fly_area` VALUES ('2035', '221', '鼎城区', '3', '430703', '50');
INSERT INTO `fly_area` VALUES ('2036', '221', '安乡县', '3', '430721', '50');
INSERT INTO `fly_area` VALUES ('2037', '221', '汉寿县', '3', '430722', '50');
INSERT INTO `fly_area` VALUES ('2038', '221', '澧县', '3', '430723', '50');
INSERT INTO `fly_area` VALUES ('2039', '221', '临澧县', '3', '430724', '50');
INSERT INTO `fly_area` VALUES ('2040', '221', '桃源县', '3', '430725', '50');
INSERT INTO `fly_area` VALUES ('2041', '221', '石门县', '3', '430726', '50');
INSERT INTO `fly_area` VALUES ('2042', '221', '津市市', '3', '430781', '50');
INSERT INTO `fly_area` VALUES ('2043', '222', '永定区', '3', '430802', '50');
INSERT INTO `fly_area` VALUES ('2044', '222', '武陵源区', '3', '430811', '50');
INSERT INTO `fly_area` VALUES ('2045', '222', '慈利县', '3', '430821', '50');
INSERT INTO `fly_area` VALUES ('2046', '222', '桑植县', '3', '430822', '50');
INSERT INTO `fly_area` VALUES ('2047', '223', '资阳区', '3', '430902', '50');
INSERT INTO `fly_area` VALUES ('2048', '223', '赫山区', '3', '430903', '50');
INSERT INTO `fly_area` VALUES ('2049', '223', '南县', '3', '430921', '50');
INSERT INTO `fly_area` VALUES ('2050', '223', '桃江县', '3', '430922', '50');
INSERT INTO `fly_area` VALUES ('2051', '223', '安化县', '3', '430923', '50');
INSERT INTO `fly_area` VALUES ('2052', '223', '沅江市', '3', '430981', '50');
INSERT INTO `fly_area` VALUES ('2053', '224', '北湖区', '3', '431002', '50');
INSERT INTO `fly_area` VALUES ('2054', '224', '苏仙区', '3', '431003', '50');
INSERT INTO `fly_area` VALUES ('2055', '224', '桂阳县', '3', '431021', '50');
INSERT INTO `fly_area` VALUES ('2056', '224', '宜章县', '3', '431022', '50');
INSERT INTO `fly_area` VALUES ('2057', '224', '永兴县', '3', '431023', '50');
INSERT INTO `fly_area` VALUES ('2058', '224', '嘉禾县', '3', '431024', '50');
INSERT INTO `fly_area` VALUES ('2059', '224', '临武县', '3', '431025', '50');
INSERT INTO `fly_area` VALUES ('2060', '224', '汝城县', '3', '431026', '50');
INSERT INTO `fly_area` VALUES ('2061', '224', '桂东县', '3', '431027', '50');
INSERT INTO `fly_area` VALUES ('2062', '224', '安仁县', '3', '431028', '50');
INSERT INTO `fly_area` VALUES ('2063', '224', '资兴市', '3', '431081', '50');
INSERT INTO `fly_area` VALUES ('2064', '225', '零陵区', '3', '431102', '50');
INSERT INTO `fly_area` VALUES ('2065', '225', '冷水滩区', '3', '431103', '50');
INSERT INTO `fly_area` VALUES ('2066', '225', '祁阳县', '3', '431121', '50');
INSERT INTO `fly_area` VALUES ('2067', '225', '东安县', '3', '431122', '50');
INSERT INTO `fly_area` VALUES ('2068', '225', '双牌县', '3', '431123', '50');
INSERT INTO `fly_area` VALUES ('2069', '225', '道县', '3', '431124', '50');
INSERT INTO `fly_area` VALUES ('2070', '225', '江永县', '3', '431125', '50');
INSERT INTO `fly_area` VALUES ('2071', '225', '宁远县', '3', '431126', '50');
INSERT INTO `fly_area` VALUES ('2072', '225', '蓝山县', '3', '431127', '50');
INSERT INTO `fly_area` VALUES ('2073', '225', '新田县', '3', '431128', '50');
INSERT INTO `fly_area` VALUES ('2074', '225', '江华瑶族自治县', '3', '431129', '50');
INSERT INTO `fly_area` VALUES ('2075', '226', '鹤城区', '3', '431202', '50');
INSERT INTO `fly_area` VALUES ('2076', '226', '中方县', '3', '431221', '50');
INSERT INTO `fly_area` VALUES ('2077', '226', '沅陵县', '3', '431222', '50');
INSERT INTO `fly_area` VALUES ('2078', '226', '辰溪县', '3', '431223', '50');
INSERT INTO `fly_area` VALUES ('2079', '226', '溆浦县', '3', '431224', '50');
INSERT INTO `fly_area` VALUES ('2080', '226', '会同县', '3', '431225', '50');
INSERT INTO `fly_area` VALUES ('2081', '226', '麻阳苗族自治县', '3', '431226', '50');
INSERT INTO `fly_area` VALUES ('2082', '226', '新晃侗族自治县', '3', '431227', '50');
INSERT INTO `fly_area` VALUES ('2083', '226', '芷江侗族自治县', '3', '431228', '50');
INSERT INTO `fly_area` VALUES ('2084', '226', '靖州苗族侗族自治县', '3', '431229', '50');
INSERT INTO `fly_area` VALUES ('2085', '226', '通道侗族自治县', '3', '431230', '50');
INSERT INTO `fly_area` VALUES ('2086', '226', '洪江市', '3', '431281', '50');
INSERT INTO `fly_area` VALUES ('2087', '227', '娄星区', '3', '431302', '50');
INSERT INTO `fly_area` VALUES ('2088', '227', '双峰县', '3', '431321', '50');
INSERT INTO `fly_area` VALUES ('2089', '227', '新化县', '3', '431322', '50');
INSERT INTO `fly_area` VALUES ('2090', '227', '冷水江市', '3', '431381', '50');
INSERT INTO `fly_area` VALUES ('2091', '227', '涟源市', '3', '431382', '50');
INSERT INTO `fly_area` VALUES ('2092', '228', '吉首市', '3', '433101', '50');
INSERT INTO `fly_area` VALUES ('2093', '228', '泸溪县', '3', '433122', '50');
INSERT INTO `fly_area` VALUES ('2094', '228', '凤凰县', '3', '433123', '50');
INSERT INTO `fly_area` VALUES ('2095', '228', '花垣县', '3', '433124', '50');
INSERT INTO `fly_area` VALUES ('2096', '228', '保靖县', '3', '433125', '50');
INSERT INTO `fly_area` VALUES ('2097', '228', '古丈县', '3', '433126', '50');
INSERT INTO `fly_area` VALUES ('2098', '228', '永顺县', '3', '433127', '50');
INSERT INTO `fly_area` VALUES ('2099', '228', '龙山县', '3', '433130', '50');
INSERT INTO `fly_area` VALUES ('2100', '229', '荔湾区', '3', '440103', '50');
INSERT INTO `fly_area` VALUES ('2101', '229', '越秀区', '3', '440104', '50');
INSERT INTO `fly_area` VALUES ('2102', '229', '海珠区', '3', '440105', '50');
INSERT INTO `fly_area` VALUES ('2103', '229', '天河区', '3', '440106', '50');
INSERT INTO `fly_area` VALUES ('2104', '229', '白云区', '3', '440111', '50');
INSERT INTO `fly_area` VALUES ('2105', '229', '黄埔区', '3', '440112', '50');
INSERT INTO `fly_area` VALUES ('2106', '229', '番禺区', '3', '440113', '50');
INSERT INTO `fly_area` VALUES ('2107', '229', '花都区', '3', '440114', '50');
INSERT INTO `fly_area` VALUES ('2108', '229', '南沙区', '3', '440115', '50');
INSERT INTO `fly_area` VALUES ('2109', '229', '从化区', '3', '440117', '50');
INSERT INTO `fly_area` VALUES ('2110', '229', '增城区', '3', '440118', '50');
INSERT INTO `fly_area` VALUES ('2111', '230', '武江区', '3', '440203', '50');
INSERT INTO `fly_area` VALUES ('2112', '230', '浈江区', '3', '440204', '50');
INSERT INTO `fly_area` VALUES ('2113', '230', '曲江区', '3', '440205', '50');
INSERT INTO `fly_area` VALUES ('2114', '230', '始兴县', '3', '440222', '50');
INSERT INTO `fly_area` VALUES ('2115', '230', '仁化县', '3', '440224', '50');
INSERT INTO `fly_area` VALUES ('2116', '230', '翁源县', '3', '440229', '50');
INSERT INTO `fly_area` VALUES ('2117', '230', '乳源瑶族自治县', '3', '440232', '50');
INSERT INTO `fly_area` VALUES ('2118', '230', '新丰县', '3', '440233', '50');
INSERT INTO `fly_area` VALUES ('2119', '230', '乐昌市', '3', '440281', '50');
INSERT INTO `fly_area` VALUES ('2120', '230', '南雄市', '3', '440282', '50');
INSERT INTO `fly_area` VALUES ('2121', '231', '罗湖区', '3', '440303', '50');
INSERT INTO `fly_area` VALUES ('2122', '231', '福田区', '3', '440304', '50');
INSERT INTO `fly_area` VALUES ('2123', '231', '南山区', '3', '440305', '50');
INSERT INTO `fly_area` VALUES ('2124', '231', '宝安区', '3', '440306', '50');
INSERT INTO `fly_area` VALUES ('2125', '231', '龙岗区', '3', '440307', '50');
INSERT INTO `fly_area` VALUES ('2126', '231', '盐田区', '3', '440308', '50');
INSERT INTO `fly_area` VALUES ('2127', '232', '香洲区', '3', '440402', '50');
INSERT INTO `fly_area` VALUES ('2128', '232', '斗门区', '3', '440403', '50');
INSERT INTO `fly_area` VALUES ('2129', '232', '金湾区', '3', '440404', '50');
INSERT INTO `fly_area` VALUES ('2130', '233', '龙湖区', '3', '440507', '50');
INSERT INTO `fly_area` VALUES ('2131', '233', '金平区', '3', '440511', '50');
INSERT INTO `fly_area` VALUES ('2132', '233', '濠江区', '3', '440512', '50');
INSERT INTO `fly_area` VALUES ('2133', '233', '潮阳区', '3', '440513', '50');
INSERT INTO `fly_area` VALUES ('2134', '233', '潮南区', '3', '440514', '50');
INSERT INTO `fly_area` VALUES ('2135', '233', '澄海区', '3', '440515', '50');
INSERT INTO `fly_area` VALUES ('2136', '233', '南澳县', '3', '440523', '50');
INSERT INTO `fly_area` VALUES ('2137', '234', '禅城区', '3', '440604', '50');
INSERT INTO `fly_area` VALUES ('2138', '234', '南海区', '3', '440605', '50');
INSERT INTO `fly_area` VALUES ('2139', '234', '顺德区', '3', '440606', '50');
INSERT INTO `fly_area` VALUES ('2140', '234', '三水区', '3', '440607', '50');
INSERT INTO `fly_area` VALUES ('2141', '234', '高明区', '3', '440608', '50');
INSERT INTO `fly_area` VALUES ('2142', '235', '蓬江区', '3', '440703', '50');
INSERT INTO `fly_area` VALUES ('2143', '235', '江海区', '3', '440704', '50');
INSERT INTO `fly_area` VALUES ('2144', '235', '新会区', '3', '440705', '50');
INSERT INTO `fly_area` VALUES ('2145', '235', '台山市', '3', '440781', '50');
INSERT INTO `fly_area` VALUES ('2146', '235', '开平市', '3', '440783', '50');
INSERT INTO `fly_area` VALUES ('2147', '235', '鹤山市', '3', '440784', '50');
INSERT INTO `fly_area` VALUES ('2148', '235', '恩平市', '3', '440785', '50');
INSERT INTO `fly_area` VALUES ('2149', '236', '赤坎区', '3', '440802', '50');
INSERT INTO `fly_area` VALUES ('2150', '236', '霞山区', '3', '440803', '50');
INSERT INTO `fly_area` VALUES ('2151', '236', '坡头区', '3', '440804', '50');
INSERT INTO `fly_area` VALUES ('2152', '236', '麻章区', '3', '440811', '50');
INSERT INTO `fly_area` VALUES ('2153', '236', '遂溪县', '3', '440823', '50');
INSERT INTO `fly_area` VALUES ('2154', '236', '徐闻县', '3', '440825', '50');
INSERT INTO `fly_area` VALUES ('2155', '236', '廉江市', '3', '440881', '50');
INSERT INTO `fly_area` VALUES ('2156', '236', '雷州市', '3', '440882', '50');
INSERT INTO `fly_area` VALUES ('2157', '236', '吴川市', '3', '440883', '50');
INSERT INTO `fly_area` VALUES ('2158', '237', '茂南区', '3', '440902', '50');
INSERT INTO `fly_area` VALUES ('2159', '237', '电白区', '3', '440904', '50');
INSERT INTO `fly_area` VALUES ('2160', '237', '高州市', '3', '440981', '50');
INSERT INTO `fly_area` VALUES ('2161', '237', '化州市', '3', '440982', '50');
INSERT INTO `fly_area` VALUES ('2162', '237', '信宜市', '3', '440983', '50');
INSERT INTO `fly_area` VALUES ('2163', '238', '端州区', '3', '441202', '50');
INSERT INTO `fly_area` VALUES ('2164', '238', '鼎湖区', '3', '441203', '50');
INSERT INTO `fly_area` VALUES ('2165', '238', '高要区', '3', '441204', '50');
INSERT INTO `fly_area` VALUES ('2166', '238', '广宁县', '3', '441223', '50');
INSERT INTO `fly_area` VALUES ('2167', '238', '怀集县', '3', '441224', '50');
INSERT INTO `fly_area` VALUES ('2168', '238', '封开县', '3', '441225', '50');
INSERT INTO `fly_area` VALUES ('2169', '238', '德庆县', '3', '441226', '50');
INSERT INTO `fly_area` VALUES ('2170', '238', '四会市', '3', '441284', '50');
INSERT INTO `fly_area` VALUES ('2171', '239', '惠城区', '3', '441302', '50');
INSERT INTO `fly_area` VALUES ('2172', '239', '惠阳区', '3', '441303', '50');
INSERT INTO `fly_area` VALUES ('2173', '239', '博罗县', '3', '441322', '50');
INSERT INTO `fly_area` VALUES ('2174', '239', '惠东县', '3', '441323', '50');
INSERT INTO `fly_area` VALUES ('2175', '239', '龙门县', '3', '441324', '50');
INSERT INTO `fly_area` VALUES ('2176', '240', '梅江区', '3', '441402', '50');
INSERT INTO `fly_area` VALUES ('2177', '240', '梅县区', '3', '441403', '50');
INSERT INTO `fly_area` VALUES ('2178', '240', '大埔县', '3', '441422', '50');
INSERT INTO `fly_area` VALUES ('2179', '240', '丰顺县', '3', '441423', '50');
INSERT INTO `fly_area` VALUES ('2180', '240', '五华县', '3', '441424', '50');
INSERT INTO `fly_area` VALUES ('2181', '240', '平远县', '3', '441426', '50');
INSERT INTO `fly_area` VALUES ('2182', '240', '蕉岭县', '3', '441427', '50');
INSERT INTO `fly_area` VALUES ('2183', '240', '兴宁市', '3', '441481', '50');
INSERT INTO `fly_area` VALUES ('2184', '241', '城区', '3', '441502', '50');
INSERT INTO `fly_area` VALUES ('2185', '241', '海丰县', '3', '441521', '50');
INSERT INTO `fly_area` VALUES ('2186', '241', '陆河县', '3', '441523', '50');
INSERT INTO `fly_area` VALUES ('2187', '241', '陆丰市', '3', '441581', '50');
INSERT INTO `fly_area` VALUES ('2188', '242', '源城区', '3', '441602', '50');
INSERT INTO `fly_area` VALUES ('2189', '242', '紫金县', '3', '441621', '50');
INSERT INTO `fly_area` VALUES ('2190', '242', '龙川县', '3', '441622', '50');
INSERT INTO `fly_area` VALUES ('2191', '242', '连平县', '3', '441623', '50');
INSERT INTO `fly_area` VALUES ('2192', '242', '和平县', '3', '441624', '50');
INSERT INTO `fly_area` VALUES ('2193', '242', '东源县', '3', '441625', '50');
INSERT INTO `fly_area` VALUES ('2194', '243', '江城区', '3', '441702', '50');
INSERT INTO `fly_area` VALUES ('2195', '243', '阳东区', '3', '441704', '50');
INSERT INTO `fly_area` VALUES ('2196', '243', '阳西县', '3', '441721', '50');
INSERT INTO `fly_area` VALUES ('2197', '243', '阳春市', '3', '441781', '50');
INSERT INTO `fly_area` VALUES ('2198', '244', '清城区', '3', '441802', '50');
INSERT INTO `fly_area` VALUES ('2199', '244', '清新区', '3', '441803', '50');
INSERT INTO `fly_area` VALUES ('2200', '244', '佛冈县', '3', '441821', '50');
INSERT INTO `fly_area` VALUES ('2201', '244', '阳山县', '3', '441823', '50');
INSERT INTO `fly_area` VALUES ('2202', '244', '连山壮族瑶族自治县', '3', '441825', '50');
INSERT INTO `fly_area` VALUES ('2203', '244', '连南瑶族自治县', '3', '441826', '50');
INSERT INTO `fly_area` VALUES ('2204', '244', '英德市', '3', '441881', '50');
INSERT INTO `fly_area` VALUES ('2205', '244', '连州市', '3', '441882', '50');
INSERT INTO `fly_area` VALUES ('2206', '245', '东莞市', '3', '441900', '50');
INSERT INTO `fly_area` VALUES ('2207', '246', '中山市', '3', '442000', '50');
INSERT INTO `fly_area` VALUES ('2208', '247', '湘桥区', '3', '445102', '50');
INSERT INTO `fly_area` VALUES ('2209', '247', '潮安区', '3', '445103', '50');
INSERT INTO `fly_area` VALUES ('2210', '247', '饶平县', '3', '445122', '50');
INSERT INTO `fly_area` VALUES ('2211', '248', '榕城区', '3', '445202', '50');
INSERT INTO `fly_area` VALUES ('2212', '248', '揭东区', '3', '445203', '50');
INSERT INTO `fly_area` VALUES ('2213', '248', '揭西县', '3', '445222', '50');
INSERT INTO `fly_area` VALUES ('2214', '248', '惠来县', '3', '445224', '50');
INSERT INTO `fly_area` VALUES ('2215', '248', '普宁市', '3', '445281', '50');
INSERT INTO `fly_area` VALUES ('2216', '249', '云城区', '3', '445302', '50');
INSERT INTO `fly_area` VALUES ('2217', '249', '云安区', '3', '445303', '50');
INSERT INTO `fly_area` VALUES ('2218', '249', '新兴县', '3', '445321', '50');
INSERT INTO `fly_area` VALUES ('2219', '249', '郁南县', '3', '445322', '50');
INSERT INTO `fly_area` VALUES ('2220', '249', '罗定市', '3', '445381', '50');
INSERT INTO `fly_area` VALUES ('2221', '250', '兴宁区', '3', '450102', '50');
INSERT INTO `fly_area` VALUES ('2222', '250', '青秀区', '3', '450103', '50');
INSERT INTO `fly_area` VALUES ('2223', '250', '江南区', '3', '450105', '50');
INSERT INTO `fly_area` VALUES ('2224', '250', '西乡塘区', '3', '450107', '50');
INSERT INTO `fly_area` VALUES ('2225', '250', '良庆区', '3', '450108', '50');
INSERT INTO `fly_area` VALUES ('2226', '250', '邕宁区', '3', '450109', '50');
INSERT INTO `fly_area` VALUES ('2227', '250', '武鸣区', '3', '450110', '50');
INSERT INTO `fly_area` VALUES ('2228', '250', '隆安县', '3', '450123', '50');
INSERT INTO `fly_area` VALUES ('2229', '250', '马山县', '3', '450124', '50');
INSERT INTO `fly_area` VALUES ('2230', '250', '上林县', '3', '450125', '50');
INSERT INTO `fly_area` VALUES ('2231', '250', '宾阳县', '3', '450126', '50');
INSERT INTO `fly_area` VALUES ('2232', '250', '横县', '3', '450127', '50');
INSERT INTO `fly_area` VALUES ('2233', '251', '城中区', '3', '450202', '50');
INSERT INTO `fly_area` VALUES ('2234', '251', '鱼峰区', '3', '450203', '50');
INSERT INTO `fly_area` VALUES ('2235', '251', '柳南区', '3', '450204', '50');
INSERT INTO `fly_area` VALUES ('2236', '251', '柳北区', '3', '450205', '50');
INSERT INTO `fly_area` VALUES ('2237', '251', '柳江区', '3', '450206', '50');
INSERT INTO `fly_area` VALUES ('2238', '251', '柳城县', '3', '450222', '50');
INSERT INTO `fly_area` VALUES ('2239', '251', '鹿寨县', '3', '450223', '50');
INSERT INTO `fly_area` VALUES ('2240', '251', '融安县', '3', '450224', '50');
INSERT INTO `fly_area` VALUES ('2241', '251', '融水苗族自治县', '3', '450225', '50');
INSERT INTO `fly_area` VALUES ('2242', '251', '三江侗族自治县', '3', '450226', '50');
INSERT INTO `fly_area` VALUES ('2243', '252', '秀峰区', '3', '450302', '50');
INSERT INTO `fly_area` VALUES ('2244', '252', '叠彩区', '3', '450303', '50');
INSERT INTO `fly_area` VALUES ('2245', '252', '象山区', '3', '450304', '50');
INSERT INTO `fly_area` VALUES ('2246', '252', '七星区', '3', '450305', '50');
INSERT INTO `fly_area` VALUES ('2247', '252', '雁山区', '3', '450311', '50');
INSERT INTO `fly_area` VALUES ('2248', '252', '临桂区', '3', '450312', '50');
INSERT INTO `fly_area` VALUES ('2249', '252', '阳朔县', '3', '450321', '50');
INSERT INTO `fly_area` VALUES ('2250', '252', '灵川县', '3', '450323', '50');
INSERT INTO `fly_area` VALUES ('2251', '252', '全州县', '3', '450324', '50');
INSERT INTO `fly_area` VALUES ('2252', '252', '兴安县', '3', '450325', '50');
INSERT INTO `fly_area` VALUES ('2253', '252', '永福县', '3', '450326', '50');
INSERT INTO `fly_area` VALUES ('2254', '252', '灌阳县', '3', '450327', '50');
INSERT INTO `fly_area` VALUES ('2255', '252', '龙胜各族自治县', '3', '450328', '50');
INSERT INTO `fly_area` VALUES ('2256', '252', '资源县', '3', '450329', '50');
INSERT INTO `fly_area` VALUES ('2257', '252', '平乐县', '3', '450330', '50');
INSERT INTO `fly_area` VALUES ('2258', '252', '荔浦县', '3', '450331', '50');
INSERT INTO `fly_area` VALUES ('2259', '252', '恭城瑶族自治县', '3', '450332', '50');
INSERT INTO `fly_area` VALUES ('2260', '253', '万秀区', '3', '450403', '50');
INSERT INTO `fly_area` VALUES ('2261', '253', '长洲区', '3', '450405', '50');
INSERT INTO `fly_area` VALUES ('2262', '253', '龙圩区', '3', '450406', '50');
INSERT INTO `fly_area` VALUES ('2263', '253', '苍梧县', '3', '450421', '50');
INSERT INTO `fly_area` VALUES ('2264', '253', '藤县', '3', '450422', '50');
INSERT INTO `fly_area` VALUES ('2265', '253', '蒙山县', '3', '450423', '50');
INSERT INTO `fly_area` VALUES ('2266', '253', '岑溪市', '3', '450481', '50');
INSERT INTO `fly_area` VALUES ('2267', '254', '海城区', '3', '450502', '50');
INSERT INTO `fly_area` VALUES ('2268', '254', '银海区', '3', '450503', '50');
INSERT INTO `fly_area` VALUES ('2269', '254', '铁山港区', '3', '450512', '50');
INSERT INTO `fly_area` VALUES ('2270', '254', '合浦县', '3', '450521', '50');
INSERT INTO `fly_area` VALUES ('2271', '255', '港口区', '3', '450602', '50');
INSERT INTO `fly_area` VALUES ('2272', '255', '防城区', '3', '450603', '50');
INSERT INTO `fly_area` VALUES ('2273', '255', '上思县', '3', '450621', '50');
INSERT INTO `fly_area` VALUES ('2274', '255', '东兴市', '3', '450681', '50');
INSERT INTO `fly_area` VALUES ('2275', '256', '钦南区', '3', '450702', '50');
INSERT INTO `fly_area` VALUES ('2276', '256', '钦北区', '3', '450703', '50');
INSERT INTO `fly_area` VALUES ('2277', '256', '灵山县', '3', '450721', '50');
INSERT INTO `fly_area` VALUES ('2278', '256', '浦北县', '3', '450722', '50');
INSERT INTO `fly_area` VALUES ('2279', '257', '港北区', '3', '450802', '50');
INSERT INTO `fly_area` VALUES ('2280', '257', '港南区', '3', '450803', '50');
INSERT INTO `fly_area` VALUES ('2281', '257', '覃塘区', '3', '450804', '50');
INSERT INTO `fly_area` VALUES ('2282', '257', '平南县', '3', '450821', '50');
INSERT INTO `fly_area` VALUES ('2283', '257', '桂平市', '3', '450881', '50');
INSERT INTO `fly_area` VALUES ('2284', '258', '玉州区', '3', '450902', '50');
INSERT INTO `fly_area` VALUES ('2285', '258', '福绵区', '3', '450903', '50');
INSERT INTO `fly_area` VALUES ('2286', '258', '容县', '3', '450921', '50');
INSERT INTO `fly_area` VALUES ('2287', '258', '陆川县', '3', '450922', '50');
INSERT INTO `fly_area` VALUES ('2288', '258', '博白县', '3', '450923', '50');
INSERT INTO `fly_area` VALUES ('2289', '258', '兴业县', '3', '450924', '50');
INSERT INTO `fly_area` VALUES ('2290', '258', '北流市', '3', '450981', '50');
INSERT INTO `fly_area` VALUES ('2291', '259', '右江区', '3', '451002', '50');
INSERT INTO `fly_area` VALUES ('2292', '259', '田阳县', '3', '451021', '50');
INSERT INTO `fly_area` VALUES ('2293', '259', '田东县', '3', '451022', '50');
INSERT INTO `fly_area` VALUES ('2294', '259', '平果县', '3', '451023', '50');
INSERT INTO `fly_area` VALUES ('2295', '259', '德保县', '3', '451024', '50');
INSERT INTO `fly_area` VALUES ('2296', '259', '那坡县', '3', '451026', '50');
INSERT INTO `fly_area` VALUES ('2297', '259', '凌云县', '3', '451027', '50');
INSERT INTO `fly_area` VALUES ('2298', '259', '乐业县', '3', '451028', '50');
INSERT INTO `fly_area` VALUES ('2299', '259', '田林县', '3', '451029', '50');
INSERT INTO `fly_area` VALUES ('2300', '259', '西林县', '3', '451030', '50');
INSERT INTO `fly_area` VALUES ('2301', '259', '隆林各族自治县', '3', '451031', '50');
INSERT INTO `fly_area` VALUES ('2302', '259', '靖西市', '3', '451081', '50');
INSERT INTO `fly_area` VALUES ('2303', '260', '八步区', '3', '451102', '50');
INSERT INTO `fly_area` VALUES ('2304', '260', '平桂区', '3', '451103', '50');
INSERT INTO `fly_area` VALUES ('2305', '260', '昭平县', '3', '451121', '50');
INSERT INTO `fly_area` VALUES ('2306', '260', '钟山县', '3', '451122', '50');
INSERT INTO `fly_area` VALUES ('2307', '260', '富川瑶族自治县', '3', '451123', '50');
INSERT INTO `fly_area` VALUES ('2308', '261', '金城江区', '3', '451202', '50');
INSERT INTO `fly_area` VALUES ('2309', '261', '南丹县', '3', '451221', '50');
INSERT INTO `fly_area` VALUES ('2310', '261', '天峨县', '3', '451222', '50');
INSERT INTO `fly_area` VALUES ('2311', '261', '凤山县', '3', '451223', '50');
INSERT INTO `fly_area` VALUES ('2312', '261', '东兰县', '3', '451224', '50');
INSERT INTO `fly_area` VALUES ('2313', '261', '罗城仫佬族自治县', '3', '451225', '50');
INSERT INTO `fly_area` VALUES ('2314', '261', '环江毛南族自治县', '3', '451226', '50');
INSERT INTO `fly_area` VALUES ('2315', '261', '巴马瑶族自治县', '3', '451227', '50');
INSERT INTO `fly_area` VALUES ('2316', '261', '都安瑶族自治县', '3', '451228', '50');
INSERT INTO `fly_area` VALUES ('2317', '261', '大化瑶族自治县', '3', '451229', '50');
INSERT INTO `fly_area` VALUES ('2318', '261', '宜州市', '3', '451281', '50');
INSERT INTO `fly_area` VALUES ('2319', '262', '兴宾区', '3', '451302', '50');
INSERT INTO `fly_area` VALUES ('2320', '262', '忻城县', '3', '451321', '50');
INSERT INTO `fly_area` VALUES ('2321', '262', '象州县', '3', '451322', '50');
INSERT INTO `fly_area` VALUES ('2322', '262', '武宣县', '3', '451323', '50');
INSERT INTO `fly_area` VALUES ('2323', '262', '金秀瑶族自治县', '3', '451324', '50');
INSERT INTO `fly_area` VALUES ('2324', '262', '合山市', '3', '451381', '50');
INSERT INTO `fly_area` VALUES ('2325', '263', '江州区', '3', '451402', '50');
INSERT INTO `fly_area` VALUES ('2326', '263', '扶绥县', '3', '451421', '50');
INSERT INTO `fly_area` VALUES ('2327', '263', '宁明县', '3', '451422', '50');
INSERT INTO `fly_area` VALUES ('2328', '263', '龙州县', '3', '451423', '50');
INSERT INTO `fly_area` VALUES ('2329', '263', '大新县', '3', '451424', '50');
INSERT INTO `fly_area` VALUES ('2330', '263', '天等县', '3', '451425', '50');
INSERT INTO `fly_area` VALUES ('2331', '263', '凭祥市', '3', '451481', '50');
INSERT INTO `fly_area` VALUES ('2332', '264', '秀英区', '3', '460105', '50');
INSERT INTO `fly_area` VALUES ('2333', '264', '龙华区', '3', '460106', '50');
INSERT INTO `fly_area` VALUES ('2334', '264', '琼山区', '3', '460107', '50');
INSERT INTO `fly_area` VALUES ('2335', '264', '美兰区', '3', '460108', '50');
INSERT INTO `fly_area` VALUES ('2336', '265', '市辖区', '3', '460201', '50');
INSERT INTO `fly_area` VALUES ('2337', '265', '海棠区', '3', '460202', '50');
INSERT INTO `fly_area` VALUES ('2338', '265', '吉阳区', '3', '460203', '50');
INSERT INTO `fly_area` VALUES ('2339', '265', '天涯区', '3', '460204', '50');
INSERT INTO `fly_area` VALUES ('2340', '265', '崖州区', '3', '460205', '50');
INSERT INTO `fly_area` VALUES ('2341', '266', '西沙群岛', '3', '460321', '50');
INSERT INTO `fly_area` VALUES ('2342', '266', '南沙群岛', '3', '460322', '50');
INSERT INTO `fly_area` VALUES ('2343', '266', '中沙群岛的岛礁及其海域', '3', '460323', '50');
INSERT INTO `fly_area` VALUES ('2344', '267', '儋州市', '3', '460400', '50');
INSERT INTO `fly_area` VALUES ('2345', '268', '五指山市', '3', '469001', '50');
INSERT INTO `fly_area` VALUES ('2346', '268', '琼海市', '3', '469002', '50');
INSERT INTO `fly_area` VALUES ('2347', '268', '文昌市', '3', '469005', '50');
INSERT INTO `fly_area` VALUES ('2348', '268', '万宁市', '3', '469006', '50');
INSERT INTO `fly_area` VALUES ('2349', '268', '东方市', '3', '469007', '50');
INSERT INTO `fly_area` VALUES ('2350', '268', '定安县', '3', '469021', '50');
INSERT INTO `fly_area` VALUES ('2351', '268', '屯昌县', '3', '469022', '50');
INSERT INTO `fly_area` VALUES ('2352', '268', '澄迈县', '3', '469023', '50');
INSERT INTO `fly_area` VALUES ('2353', '268', '临高县', '3', '469024', '50');
INSERT INTO `fly_area` VALUES ('2354', '268', '白沙黎族自治县', '3', '469025', '50');
INSERT INTO `fly_area` VALUES ('2355', '268', '昌江黎族自治县', '3', '469026', '50');
INSERT INTO `fly_area` VALUES ('2356', '268', '乐东黎族自治县', '3', '469027', '50');
INSERT INTO `fly_area` VALUES ('2357', '268', '陵水黎族自治县', '3', '469028', '50');
INSERT INTO `fly_area` VALUES ('2358', '268', '保亭黎族苗族自治县', '3', '469029', '50');
INSERT INTO `fly_area` VALUES ('2359', '268', '琼中黎族苗族自治县', '3', '469030', '50');
INSERT INTO `fly_area` VALUES ('2360', '269', '万州区', '3', '500101', '50');
INSERT INTO `fly_area` VALUES ('2361', '269', '涪陵区', '3', '500102', '50');
INSERT INTO `fly_area` VALUES ('2362', '269', '渝中区', '3', '500103', '50');
INSERT INTO `fly_area` VALUES ('2363', '269', '大渡口区', '3', '500104', '50');
INSERT INTO `fly_area` VALUES ('2364', '269', '江北区', '3', '500105', '50');
INSERT INTO `fly_area` VALUES ('2365', '269', '沙坪坝区', '3', '500106', '50');
INSERT INTO `fly_area` VALUES ('2366', '269', '九龙坡区', '3', '500107', '50');
INSERT INTO `fly_area` VALUES ('2367', '269', '南岸区', '3', '500108', '50');
INSERT INTO `fly_area` VALUES ('2368', '269', '北碚区', '3', '500109', '50');
INSERT INTO `fly_area` VALUES ('2369', '269', '綦江区', '3', '500110', '50');
INSERT INTO `fly_area` VALUES ('2370', '269', '大足区', '3', '500111', '50');
INSERT INTO `fly_area` VALUES ('2371', '269', '渝北区', '3', '500112', '50');
INSERT INTO `fly_area` VALUES ('2372', '269', '巴南区', '3', '500113', '50');
INSERT INTO `fly_area` VALUES ('2373', '269', '黔江区', '3', '500114', '50');
INSERT INTO `fly_area` VALUES ('2374', '269', '长寿区', '3', '500115', '50');
INSERT INTO `fly_area` VALUES ('2375', '269', '江津区', '3', '500116', '50');
INSERT INTO `fly_area` VALUES ('2376', '269', '合川区', '3', '500117', '50');
INSERT INTO `fly_area` VALUES ('2377', '269', '永川区', '3', '500118', '50');
INSERT INTO `fly_area` VALUES ('2378', '269', '南川区', '3', '500119', '50');
INSERT INTO `fly_area` VALUES ('2379', '269', '璧山区', '3', '500120', '50');
INSERT INTO `fly_area` VALUES ('2380', '269', '铜梁区', '3', '500151', '50');
INSERT INTO `fly_area` VALUES ('2381', '269', '潼南区', '3', '500152', '50');
INSERT INTO `fly_area` VALUES ('2382', '269', '荣昌区', '3', '500153', '50');
INSERT INTO `fly_area` VALUES ('2383', '269', '开州区', '3', '500154', '50');
INSERT INTO `fly_area` VALUES ('2384', '270', '梁平县', '3', '500228', '50');
INSERT INTO `fly_area` VALUES ('2385', '270', '城口县', '3', '500229', '50');
INSERT INTO `fly_area` VALUES ('2386', '270', '丰都县', '3', '500230', '50');
INSERT INTO `fly_area` VALUES ('2387', '270', '垫江县', '3', '500231', '50');
INSERT INTO `fly_area` VALUES ('2388', '270', '武隆县', '3', '500232', '50');
INSERT INTO `fly_area` VALUES ('2389', '270', '忠县', '3', '500233', '50');
INSERT INTO `fly_area` VALUES ('2390', '270', '云阳县', '3', '500235', '50');
INSERT INTO `fly_area` VALUES ('2391', '270', '奉节县', '3', '500236', '50');
INSERT INTO `fly_area` VALUES ('2392', '270', '巫山县', '3', '500237', '50');
INSERT INTO `fly_area` VALUES ('2393', '270', '巫溪县', '3', '500238', '50');
INSERT INTO `fly_area` VALUES ('2394', '270', '石柱土家族自治县', '3', '500240', '50');
INSERT INTO `fly_area` VALUES ('2395', '270', '秀山土家族苗族自治县', '3', '500241', '50');
INSERT INTO `fly_area` VALUES ('2396', '270', '酉阳土家族苗族自治县', '3', '500242', '50');
INSERT INTO `fly_area` VALUES ('2397', '270', '彭水苗族土家族自治县', '3', '500243', '50');
INSERT INTO `fly_area` VALUES ('2398', '271', '锦江区', '3', '510104', '50');
INSERT INTO `fly_area` VALUES ('2399', '271', '青羊区', '3', '510105', '50');
INSERT INTO `fly_area` VALUES ('2400', '271', '金牛区', '3', '510106', '50');
INSERT INTO `fly_area` VALUES ('2401', '271', '武侯区', '3', '510107', '50');
INSERT INTO `fly_area` VALUES ('2402', '271', '成华区', '3', '510108', '50');
INSERT INTO `fly_area` VALUES ('2403', '271', '龙泉驿区', '3', '510112', '50');
INSERT INTO `fly_area` VALUES ('2404', '271', '青白江区', '3', '510113', '50');
INSERT INTO `fly_area` VALUES ('2405', '271', '新都区', '3', '510114', '50');
INSERT INTO `fly_area` VALUES ('2406', '271', '温江区', '3', '510115', '50');
INSERT INTO `fly_area` VALUES ('2407', '271', '双流区', '3', '510116', '50');
INSERT INTO `fly_area` VALUES ('2408', '271', '金堂县', '3', '510121', '50');
INSERT INTO `fly_area` VALUES ('2409', '271', '郫县', '3', '510124', '50');
INSERT INTO `fly_area` VALUES ('2410', '271', '大邑县', '3', '510129', '50');
INSERT INTO `fly_area` VALUES ('2411', '271', '蒲江县', '3', '510131', '50');
INSERT INTO `fly_area` VALUES ('2412', '271', '新津县', '3', '510132', '50');
INSERT INTO `fly_area` VALUES ('2413', '271', '都江堰市', '3', '510181', '50');
INSERT INTO `fly_area` VALUES ('2414', '271', '彭州市', '3', '510182', '50');
INSERT INTO `fly_area` VALUES ('2415', '271', '邛崃市', '3', '510183', '50');
INSERT INTO `fly_area` VALUES ('2416', '271', '崇州市', '3', '510184', '50');
INSERT INTO `fly_area` VALUES ('2417', '271', '简阳市', '3', '510185', '50');
INSERT INTO `fly_area` VALUES ('2418', '272', '自流井区', '3', '510302', '50');
INSERT INTO `fly_area` VALUES ('2419', '272', '贡井区', '3', '510303', '50');
INSERT INTO `fly_area` VALUES ('2420', '272', '大安区', '3', '510304', '50');
INSERT INTO `fly_area` VALUES ('2421', '272', '沿滩区', '3', '510311', '50');
INSERT INTO `fly_area` VALUES ('2422', '272', '荣县', '3', '510321', '50');
INSERT INTO `fly_area` VALUES ('2423', '272', '富顺县', '3', '510322', '50');
INSERT INTO `fly_area` VALUES ('2424', '273', '东区', '3', '510402', '50');
INSERT INTO `fly_area` VALUES ('2425', '273', '西区', '3', '510403', '50');
INSERT INTO `fly_area` VALUES ('2426', '273', '仁和区', '3', '510411', '50');
INSERT INTO `fly_area` VALUES ('2427', '273', '米易县', '3', '510421', '50');
INSERT INTO `fly_area` VALUES ('2428', '273', '盐边县', '3', '510422', '50');
INSERT INTO `fly_area` VALUES ('2429', '274', '江阳区', '3', '510502', '50');
INSERT INTO `fly_area` VALUES ('2430', '274', '纳溪区', '3', '510503', '50');
INSERT INTO `fly_area` VALUES ('2431', '274', '龙马潭区', '3', '510504', '50');
INSERT INTO `fly_area` VALUES ('2432', '274', '泸县', '3', '510521', '50');
INSERT INTO `fly_area` VALUES ('2433', '274', '合江县', '3', '510522', '50');
INSERT INTO `fly_area` VALUES ('2434', '274', '叙永县', '3', '510524', '50');
INSERT INTO `fly_area` VALUES ('2435', '274', '古蔺县', '3', '510525', '50');
INSERT INTO `fly_area` VALUES ('2436', '275', '旌阳区', '3', '510603', '50');
INSERT INTO `fly_area` VALUES ('2437', '275', '中江县', '3', '510623', '50');
INSERT INTO `fly_area` VALUES ('2438', '275', '罗江县', '3', '510626', '50');
INSERT INTO `fly_area` VALUES ('2439', '275', '广汉市', '3', '510681', '50');
INSERT INTO `fly_area` VALUES ('2440', '275', '什邡市', '3', '510682', '50');
INSERT INTO `fly_area` VALUES ('2441', '275', '绵竹市', '3', '510683', '50');
INSERT INTO `fly_area` VALUES ('2442', '276', '涪城区', '3', '510703', '50');
INSERT INTO `fly_area` VALUES ('2443', '276', '游仙区', '3', '510704', '50');
INSERT INTO `fly_area` VALUES ('2444', '276', '安州区', '3', '510705', '50');
INSERT INTO `fly_area` VALUES ('2445', '276', '三台县', '3', '510722', '50');
INSERT INTO `fly_area` VALUES ('2446', '276', '盐亭县', '3', '510723', '50');
INSERT INTO `fly_area` VALUES ('2447', '276', '梓潼县', '3', '510725', '50');
INSERT INTO `fly_area` VALUES ('2448', '276', '北川羌族自治县', '3', '510726', '50');
INSERT INTO `fly_area` VALUES ('2449', '276', '平武县', '3', '510727', '50');
INSERT INTO `fly_area` VALUES ('2450', '276', '江油市', '3', '510781', '50');
INSERT INTO `fly_area` VALUES ('2451', '277', '利州区', '3', '510802', '50');
INSERT INTO `fly_area` VALUES ('2452', '277', '昭化区', '3', '510811', '50');
INSERT INTO `fly_area` VALUES ('2453', '277', '朝天区', '3', '510812', '50');
INSERT INTO `fly_area` VALUES ('2454', '277', '旺苍县', '3', '510821', '50');
INSERT INTO `fly_area` VALUES ('2455', '277', '青川县', '3', '510822', '50');
INSERT INTO `fly_area` VALUES ('2456', '277', '剑阁县', '3', '510823', '50');
INSERT INTO `fly_area` VALUES ('2457', '277', '苍溪县', '3', '510824', '50');
INSERT INTO `fly_area` VALUES ('2458', '278', '船山区', '3', '510903', '50');
INSERT INTO `fly_area` VALUES ('2459', '278', '安居区', '3', '510904', '50');
INSERT INTO `fly_area` VALUES ('2460', '278', '蓬溪县', '3', '510921', '50');
INSERT INTO `fly_area` VALUES ('2461', '278', '射洪县', '3', '510922', '50');
INSERT INTO `fly_area` VALUES ('2462', '278', '大英县', '3', '510923', '50');
INSERT INTO `fly_area` VALUES ('2463', '279', '市中区', '3', '511002', '50');
INSERT INTO `fly_area` VALUES ('2464', '279', '东兴区', '3', '511011', '50');
INSERT INTO `fly_area` VALUES ('2465', '279', '威远县', '3', '511024', '50');
INSERT INTO `fly_area` VALUES ('2466', '279', '资中县', '3', '511025', '50');
INSERT INTO `fly_area` VALUES ('2467', '279', '隆昌县', '3', '511028', '50');
INSERT INTO `fly_area` VALUES ('2468', '280', '市中区', '3', '511102', '50');
INSERT INTO `fly_area` VALUES ('2469', '280', '沙湾区', '3', '511111', '50');
INSERT INTO `fly_area` VALUES ('2470', '280', '五通桥区', '3', '511112', '50');
INSERT INTO `fly_area` VALUES ('2471', '280', '金口河区', '3', '511113', '50');
INSERT INTO `fly_area` VALUES ('2472', '280', '犍为县', '3', '511123', '50');
INSERT INTO `fly_area` VALUES ('2473', '280', '井研县', '3', '511124', '50');
INSERT INTO `fly_area` VALUES ('2474', '280', '夹江县', '3', '511126', '50');
INSERT INTO `fly_area` VALUES ('2475', '280', '沐川县', '3', '511129', '50');
INSERT INTO `fly_area` VALUES ('2476', '280', '峨边彝族自治县', '3', '511132', '50');
INSERT INTO `fly_area` VALUES ('2477', '280', '马边彝族自治县', '3', '511133', '50');
INSERT INTO `fly_area` VALUES ('2478', '280', '峨眉山市', '3', '511181', '50');
INSERT INTO `fly_area` VALUES ('2479', '281', '顺庆区', '3', '511302', '50');
INSERT INTO `fly_area` VALUES ('2480', '281', '高坪区', '3', '511303', '50');
INSERT INTO `fly_area` VALUES ('2481', '281', '嘉陵区', '3', '511304', '50');
INSERT INTO `fly_area` VALUES ('2482', '281', '南部县', '3', '511321', '50');
INSERT INTO `fly_area` VALUES ('2483', '281', '营山县', '3', '511322', '50');
INSERT INTO `fly_area` VALUES ('2484', '281', '蓬安县', '3', '511323', '50');
INSERT INTO `fly_area` VALUES ('2485', '281', '仪陇县', '3', '511324', '50');
INSERT INTO `fly_area` VALUES ('2486', '281', '西充县', '3', '511325', '50');
INSERT INTO `fly_area` VALUES ('2487', '281', '阆中市', '3', '511381', '50');
INSERT INTO `fly_area` VALUES ('2488', '282', '东坡区', '3', '511402', '50');
INSERT INTO `fly_area` VALUES ('2489', '282', '彭山区', '3', '511403', '50');
INSERT INTO `fly_area` VALUES ('2490', '282', '仁寿县', '3', '511421', '50');
INSERT INTO `fly_area` VALUES ('2491', '282', '洪雅县', '3', '511423', '50');
INSERT INTO `fly_area` VALUES ('2492', '282', '丹棱县', '3', '511424', '50');
INSERT INTO `fly_area` VALUES ('2493', '282', '青神县', '3', '511425', '50');
INSERT INTO `fly_area` VALUES ('2494', '283', '翠屏区', '3', '511502', '50');
INSERT INTO `fly_area` VALUES ('2495', '283', '南溪区', '3', '511503', '50');
INSERT INTO `fly_area` VALUES ('2496', '283', '宜宾县', '3', '511521', '50');
INSERT INTO `fly_area` VALUES ('2497', '283', '江安县', '3', '511523', '50');
INSERT INTO `fly_area` VALUES ('2498', '283', '长宁县', '3', '511524', '50');
INSERT INTO `fly_area` VALUES ('2499', '283', '高县', '3', '511525', '50');
INSERT INTO `fly_area` VALUES ('2500', '283', '珙县', '3', '511526', '50');
INSERT INTO `fly_area` VALUES ('2501', '283', '筠连县', '3', '511527', '50');
INSERT INTO `fly_area` VALUES ('2502', '283', '兴文县', '3', '511528', '50');
INSERT INTO `fly_area` VALUES ('2503', '283', '屏山县', '3', '511529', '50');
INSERT INTO `fly_area` VALUES ('2504', '284', '广安区', '3', '511602', '50');
INSERT INTO `fly_area` VALUES ('2505', '284', '前锋区', '3', '511603', '50');
INSERT INTO `fly_area` VALUES ('2506', '284', '岳池县', '3', '511621', '50');
INSERT INTO `fly_area` VALUES ('2507', '284', '武胜县', '3', '511622', '50');
INSERT INTO `fly_area` VALUES ('2508', '284', '邻水县', '3', '511623', '50');
INSERT INTO `fly_area` VALUES ('2509', '284', '华蓥市', '3', '511681', '50');
INSERT INTO `fly_area` VALUES ('2510', '285', '通川区', '3', '511702', '50');
INSERT INTO `fly_area` VALUES ('2511', '285', '达川区', '3', '511703', '50');
INSERT INTO `fly_area` VALUES ('2512', '285', '宣汉县', '3', '511722', '50');
INSERT INTO `fly_area` VALUES ('2513', '285', '开江县', '3', '511723', '50');
INSERT INTO `fly_area` VALUES ('2514', '285', '大竹县', '3', '511724', '50');
INSERT INTO `fly_area` VALUES ('2515', '285', '渠县', '3', '511725', '50');
INSERT INTO `fly_area` VALUES ('2516', '285', '万源市', '3', '511781', '50');
INSERT INTO `fly_area` VALUES ('2517', '286', '雨城区', '3', '511802', '50');
INSERT INTO `fly_area` VALUES ('2518', '286', '名山区', '3', '511803', '50');
INSERT INTO `fly_area` VALUES ('2519', '286', '荥经县', '3', '511822', '50');
INSERT INTO `fly_area` VALUES ('2520', '286', '汉源县', '3', '511823', '50');
INSERT INTO `fly_area` VALUES ('2521', '286', '石棉县', '3', '511824', '50');
INSERT INTO `fly_area` VALUES ('2522', '286', '天全县', '3', '511825', '50');
INSERT INTO `fly_area` VALUES ('2523', '286', '芦山县', '3', '511826', '50');
INSERT INTO `fly_area` VALUES ('2524', '286', '宝兴县', '3', '511827', '50');
INSERT INTO `fly_area` VALUES ('2525', '287', '巴州区', '3', '511902', '50');
INSERT INTO `fly_area` VALUES ('2526', '287', '恩阳区', '3', '511903', '50');
INSERT INTO `fly_area` VALUES ('2527', '287', '通江县', '3', '511921', '50');
INSERT INTO `fly_area` VALUES ('2528', '287', '南江县', '3', '511922', '50');
INSERT INTO `fly_area` VALUES ('2529', '287', '平昌县', '3', '511923', '50');
INSERT INTO `fly_area` VALUES ('2530', '288', '雁江区', '3', '512002', '50');
INSERT INTO `fly_area` VALUES ('2531', '288', '安岳县', '3', '512021', '50');
INSERT INTO `fly_area` VALUES ('2532', '288', '乐至县', '3', '512022', '50');
INSERT INTO `fly_area` VALUES ('2533', '289', '马尔康市', '3', '513201', '50');
INSERT INTO `fly_area` VALUES ('2534', '289', '汶川县', '3', '513221', '50');
INSERT INTO `fly_area` VALUES ('2535', '289', '理县', '3', '513222', '50');
INSERT INTO `fly_area` VALUES ('2536', '289', '茂县', '3', '513223', '50');
INSERT INTO `fly_area` VALUES ('2537', '289', '松潘县', '3', '513224', '50');
INSERT INTO `fly_area` VALUES ('2538', '289', '九寨沟县', '3', '513225', '50');
INSERT INTO `fly_area` VALUES ('2539', '289', '金川县', '3', '513226', '50');
INSERT INTO `fly_area` VALUES ('2540', '289', '小金县', '3', '513227', '50');
INSERT INTO `fly_area` VALUES ('2541', '289', '黑水县', '3', '513228', '50');
INSERT INTO `fly_area` VALUES ('2542', '289', '壤塘县', '3', '513230', '50');
INSERT INTO `fly_area` VALUES ('2543', '289', '阿坝县', '3', '513231', '50');
INSERT INTO `fly_area` VALUES ('2544', '289', '若尔盖县', '3', '513232', '50');
INSERT INTO `fly_area` VALUES ('2545', '289', '红原县', '3', '513233', '50');
INSERT INTO `fly_area` VALUES ('2546', '290', '康定市', '3', '513301', '50');
INSERT INTO `fly_area` VALUES ('2547', '290', '泸定县', '3', '513322', '50');
INSERT INTO `fly_area` VALUES ('2548', '290', '丹巴县', '3', '513323', '50');
INSERT INTO `fly_area` VALUES ('2549', '290', '九龙县', '3', '513324', '50');
INSERT INTO `fly_area` VALUES ('2550', '290', '雅江县', '3', '513325', '50');
INSERT INTO `fly_area` VALUES ('2551', '290', '道孚县', '3', '513326', '50');
INSERT INTO `fly_area` VALUES ('2552', '290', '炉霍县', '3', '513327', '50');
INSERT INTO `fly_area` VALUES ('2553', '290', '甘孜县', '3', '513328', '50');
INSERT INTO `fly_area` VALUES ('2554', '290', '新龙县', '3', '513329', '50');
INSERT INTO `fly_area` VALUES ('2555', '290', '德格县', '3', '513330', '50');
INSERT INTO `fly_area` VALUES ('2556', '290', '白玉县', '3', '513331', '50');
INSERT INTO `fly_area` VALUES ('2557', '290', '石渠县', '3', '513332', '50');
INSERT INTO `fly_area` VALUES ('2558', '290', '色达县', '3', '513333', '50');
INSERT INTO `fly_area` VALUES ('2559', '290', '理塘县', '3', '513334', '50');
INSERT INTO `fly_area` VALUES ('2560', '290', '巴塘县', '3', '513335', '50');
INSERT INTO `fly_area` VALUES ('2561', '290', '乡城县', '3', '513336', '50');
INSERT INTO `fly_area` VALUES ('2562', '290', '稻城县', '3', '513337', '50');
INSERT INTO `fly_area` VALUES ('2563', '290', '得荣县', '3', '513338', '50');
INSERT INTO `fly_area` VALUES ('2564', '291', '西昌市', '3', '513401', '50');
INSERT INTO `fly_area` VALUES ('2565', '291', '木里藏族自治县', '3', '513422', '50');
INSERT INTO `fly_area` VALUES ('2566', '291', '盐源县', '3', '513423', '50');
INSERT INTO `fly_area` VALUES ('2567', '291', '德昌县', '3', '513424', '50');
INSERT INTO `fly_area` VALUES ('2568', '291', '会理县', '3', '513425', '50');
INSERT INTO `fly_area` VALUES ('2569', '291', '会东县', '3', '513426', '50');
INSERT INTO `fly_area` VALUES ('2570', '291', '宁南县', '3', '513427', '50');
INSERT INTO `fly_area` VALUES ('2571', '291', '普格县', '3', '513428', '50');
INSERT INTO `fly_area` VALUES ('2572', '291', '布拖县', '3', '513429', '50');
INSERT INTO `fly_area` VALUES ('2573', '291', '金阳县', '3', '513430', '50');
INSERT INTO `fly_area` VALUES ('2574', '291', '昭觉县', '3', '513431', '50');
INSERT INTO `fly_area` VALUES ('2575', '291', '喜德县', '3', '513432', '50');
INSERT INTO `fly_area` VALUES ('2576', '291', '冕宁县', '3', '513433', '50');
INSERT INTO `fly_area` VALUES ('2577', '291', '越西县', '3', '513434', '50');
INSERT INTO `fly_area` VALUES ('2578', '291', '甘洛县', '3', '513435', '50');
INSERT INTO `fly_area` VALUES ('2579', '291', '美姑县', '3', '513436', '50');
INSERT INTO `fly_area` VALUES ('2580', '291', '雷波县', '3', '513437', '50');
INSERT INTO `fly_area` VALUES ('2581', '292', '南明区', '3', '520102', '50');
INSERT INTO `fly_area` VALUES ('2582', '292', '云岩区', '3', '520103', '50');
INSERT INTO `fly_area` VALUES ('2583', '292', '花溪区', '3', '520111', '50');
INSERT INTO `fly_area` VALUES ('2584', '292', '乌当区', '3', '520112', '50');
INSERT INTO `fly_area` VALUES ('2585', '292', '白云区', '3', '520113', '50');
INSERT INTO `fly_area` VALUES ('2586', '292', '观山湖区', '3', '520115', '50');
INSERT INTO `fly_area` VALUES ('2587', '292', '开阳县', '3', '520121', '50');
INSERT INTO `fly_area` VALUES ('2588', '292', '息烽县', '3', '520122', '50');
INSERT INTO `fly_area` VALUES ('2589', '292', '修文县', '3', '520123', '50');
INSERT INTO `fly_area` VALUES ('2590', '292', '清镇市', '3', '520181', '50');
INSERT INTO `fly_area` VALUES ('2591', '293', '钟山区', '3', '520201', '50');
INSERT INTO `fly_area` VALUES ('2592', '293', '六枝特区', '3', '520203', '50');
INSERT INTO `fly_area` VALUES ('2593', '293', '水城县', '3', '520221', '50');
INSERT INTO `fly_area` VALUES ('2594', '293', '盘县', '3', '520222', '50');
INSERT INTO `fly_area` VALUES ('2595', '294', '红花岗区', '3', '520302', '50');
INSERT INTO `fly_area` VALUES ('2596', '294', '汇川区', '3', '520303', '50');
INSERT INTO `fly_area` VALUES ('2597', '294', '播州区', '3', '520304', '50');
INSERT INTO `fly_area` VALUES ('2598', '294', '桐梓县', '3', '520322', '50');
INSERT INTO `fly_area` VALUES ('2599', '294', '绥阳县', '3', '520323', '50');
INSERT INTO `fly_area` VALUES ('2600', '294', '正安县', '3', '520324', '50');
INSERT INTO `fly_area` VALUES ('2601', '294', '道真仡佬族苗族自治县', '3', '520325', '50');
INSERT INTO `fly_area` VALUES ('2602', '294', '务川仡佬族苗族自治县', '3', '520326', '50');
INSERT INTO `fly_area` VALUES ('2603', '294', '凤冈县', '3', '520327', '50');
INSERT INTO `fly_area` VALUES ('2604', '294', '湄潭县', '3', '520328', '50');
INSERT INTO `fly_area` VALUES ('2605', '294', '余庆县', '3', '520329', '50');
INSERT INTO `fly_area` VALUES ('2606', '294', '习水县', '3', '520330', '50');
INSERT INTO `fly_area` VALUES ('2607', '294', '赤水市', '3', '520381', '50');
INSERT INTO `fly_area` VALUES ('2608', '294', '仁怀市', '3', '520382', '50');
INSERT INTO `fly_area` VALUES ('2609', '295', '西秀区', '3', '520402', '50');
INSERT INTO `fly_area` VALUES ('2610', '295', '平坝区', '3', '520403', '50');
INSERT INTO `fly_area` VALUES ('2611', '295', '普定县', '3', '520422', '50');
INSERT INTO `fly_area` VALUES ('2612', '295', '镇宁布依族苗族自治县', '3', '520423', '50');
INSERT INTO `fly_area` VALUES ('2613', '295', '关岭布依族苗族自治县', '3', '520424', '50');
INSERT INTO `fly_area` VALUES ('2614', '295', '紫云苗族布依族自治县', '3', '520425', '50');
INSERT INTO `fly_area` VALUES ('2615', '296', '七星关区', '3', '520502', '50');
INSERT INTO `fly_area` VALUES ('2616', '296', '大方县', '3', '520521', '50');
INSERT INTO `fly_area` VALUES ('2617', '296', '黔西县', '3', '520522', '50');
INSERT INTO `fly_area` VALUES ('2618', '296', '金沙县', '3', '520523', '50');
INSERT INTO `fly_area` VALUES ('2619', '296', '织金县', '3', '520524', '50');
INSERT INTO `fly_area` VALUES ('2620', '296', '纳雍县', '3', '520525', '50');
INSERT INTO `fly_area` VALUES ('2621', '296', '威宁彝族回族苗族自治县', '3', '520526', '50');
INSERT INTO `fly_area` VALUES ('2622', '296', '赫章县', '3', '520527', '50');
INSERT INTO `fly_area` VALUES ('2623', '297', '碧江区', '3', '520602', '50');
INSERT INTO `fly_area` VALUES ('2624', '297', '万山区', '3', '520603', '50');
INSERT INTO `fly_area` VALUES ('2625', '297', '江口县', '3', '520621', '50');
INSERT INTO `fly_area` VALUES ('2626', '297', '玉屏侗族自治县', '3', '520622', '50');
INSERT INTO `fly_area` VALUES ('2627', '297', '石阡县', '3', '520623', '50');
INSERT INTO `fly_area` VALUES ('2628', '297', '思南县', '3', '520624', '50');
INSERT INTO `fly_area` VALUES ('2629', '297', '印江土家族苗族自治县', '3', '520625', '50');
INSERT INTO `fly_area` VALUES ('2630', '297', '德江县', '3', '520626', '50');
INSERT INTO `fly_area` VALUES ('2631', '297', '沿河土家族自治县', '3', '520627', '50');
INSERT INTO `fly_area` VALUES ('2632', '297', '松桃苗族自治县', '3', '520628', '50');
INSERT INTO `fly_area` VALUES ('2633', '298', '兴义市', '3', '522301', '50');
INSERT INTO `fly_area` VALUES ('2634', '298', '兴仁县', '3', '522322', '50');
INSERT INTO `fly_area` VALUES ('2635', '298', '普安县', '3', '522323', '50');
INSERT INTO `fly_area` VALUES ('2636', '298', '晴隆县', '3', '522324', '50');
INSERT INTO `fly_area` VALUES ('2637', '298', '贞丰县', '3', '522325', '50');
INSERT INTO `fly_area` VALUES ('2638', '298', '望谟县', '3', '522326', '50');
INSERT INTO `fly_area` VALUES ('2639', '298', '册亨县', '3', '522327', '50');
INSERT INTO `fly_area` VALUES ('2640', '298', '安龙县', '3', '522328', '50');
INSERT INTO `fly_area` VALUES ('2641', '299', '凯里市', '3', '522601', '50');
INSERT INTO `fly_area` VALUES ('2642', '299', '黄平县', '3', '522622', '50');
INSERT INTO `fly_area` VALUES ('2643', '299', '施秉县', '3', '522623', '50');
INSERT INTO `fly_area` VALUES ('2644', '299', '三穗县', '3', '522624', '50');
INSERT INTO `fly_area` VALUES ('2645', '299', '镇远县', '3', '522625', '50');
INSERT INTO `fly_area` VALUES ('2646', '299', '岑巩县', '3', '522626', '50');
INSERT INTO `fly_area` VALUES ('2647', '299', '天柱县', '3', '522627', '50');
INSERT INTO `fly_area` VALUES ('2648', '299', '锦屏县', '3', '522628', '50');
INSERT INTO `fly_area` VALUES ('2649', '299', '剑河县', '3', '522629', '50');
INSERT INTO `fly_area` VALUES ('2650', '299', '台江县', '3', '522630', '50');
INSERT INTO `fly_area` VALUES ('2651', '299', '黎平县', '3', '522631', '50');
INSERT INTO `fly_area` VALUES ('2652', '299', '榕江县', '3', '522632', '50');
INSERT INTO `fly_area` VALUES ('2653', '299', '从江县', '3', '522633', '50');
INSERT INTO `fly_area` VALUES ('2654', '299', '雷山县', '3', '522634', '50');
INSERT INTO `fly_area` VALUES ('2655', '299', '麻江县', '3', '522635', '50');
INSERT INTO `fly_area` VALUES ('2656', '299', '丹寨县', '3', '522636', '50');
INSERT INTO `fly_area` VALUES ('2657', '300', '都匀市', '3', '522701', '50');
INSERT INTO `fly_area` VALUES ('2658', '300', '福泉市', '3', '522702', '50');
INSERT INTO `fly_area` VALUES ('2659', '300', '荔波县', '3', '522722', '50');
INSERT INTO `fly_area` VALUES ('2660', '300', '贵定县', '3', '522723', '50');
INSERT INTO `fly_area` VALUES ('2661', '300', '瓮安县', '3', '522725', '50');
INSERT INTO `fly_area` VALUES ('2662', '300', '独山县', '3', '522726', '50');
INSERT INTO `fly_area` VALUES ('2663', '300', '平塘县', '3', '522727', '50');
INSERT INTO `fly_area` VALUES ('2664', '300', '罗甸县', '3', '522728', '50');
INSERT INTO `fly_area` VALUES ('2665', '300', '长顺县', '3', '522729', '50');
INSERT INTO `fly_area` VALUES ('2666', '300', '龙里县', '3', '522730', '50');
INSERT INTO `fly_area` VALUES ('2667', '300', '惠水县', '3', '522731', '50');
INSERT INTO `fly_area` VALUES ('2668', '300', '三都水族自治县', '3', '522732', '50');
INSERT INTO `fly_area` VALUES ('2669', '301', '五华区', '3', '530102', '50');
INSERT INTO `fly_area` VALUES ('2670', '301', '盘龙区', '3', '530103', '50');
INSERT INTO `fly_area` VALUES ('2671', '301', '官渡区', '3', '530111', '50');
INSERT INTO `fly_area` VALUES ('2672', '301', '西山区', '3', '530112', '50');
INSERT INTO `fly_area` VALUES ('2673', '301', '东川区', '3', '530113', '50');
INSERT INTO `fly_area` VALUES ('2674', '301', '呈贡区', '3', '530114', '50');
INSERT INTO `fly_area` VALUES ('2675', '301', '晋宁县', '3', '530122', '50');
INSERT INTO `fly_area` VALUES ('2676', '301', '富民县', '3', '530124', '50');
INSERT INTO `fly_area` VALUES ('2677', '301', '宜良县', '3', '530125', '50');
INSERT INTO `fly_area` VALUES ('2678', '301', '石林彝族自治县', '3', '530126', '50');
INSERT INTO `fly_area` VALUES ('2679', '301', '嵩明县', '3', '530127', '50');
INSERT INTO `fly_area` VALUES ('2680', '301', '禄劝彝族苗族自治县', '3', '530128', '50');
INSERT INTO `fly_area` VALUES ('2681', '301', '寻甸回族彝族自治县', '3', '530129', '50');
INSERT INTO `fly_area` VALUES ('2682', '301', '安宁市', '3', '530181', '50');
INSERT INTO `fly_area` VALUES ('2683', '302', '麒麟区', '3', '530302', '50');
INSERT INTO `fly_area` VALUES ('2684', '302', '沾益区', '3', '530303', '50');
INSERT INTO `fly_area` VALUES ('2685', '302', '马龙县', '3', '530321', '50');
INSERT INTO `fly_area` VALUES ('2686', '302', '陆良县', '3', '530322', '50');
INSERT INTO `fly_area` VALUES ('2687', '302', '师宗县', '3', '530323', '50');
INSERT INTO `fly_area` VALUES ('2688', '302', '罗平县', '3', '530324', '50');
INSERT INTO `fly_area` VALUES ('2689', '302', '富源县', '3', '530325', '50');
INSERT INTO `fly_area` VALUES ('2690', '302', '会泽县', '3', '530326', '50');
INSERT INTO `fly_area` VALUES ('2691', '302', '宣威市', '3', '530381', '50');
INSERT INTO `fly_area` VALUES ('2692', '303', '红塔区', '3', '530402', '50');
INSERT INTO `fly_area` VALUES ('2693', '303', '江川区', '3', '530403', '50');
INSERT INTO `fly_area` VALUES ('2694', '303', '澄江县', '3', '530422', '50');
INSERT INTO `fly_area` VALUES ('2695', '303', '通海县', '3', '530423', '50');
INSERT INTO `fly_area` VALUES ('2696', '303', '华宁县', '3', '530424', '50');
INSERT INTO `fly_area` VALUES ('2697', '303', '易门县', '3', '530425', '50');
INSERT INTO `fly_area` VALUES ('2698', '303', '峨山彝族自治县', '3', '530426', '50');
INSERT INTO `fly_area` VALUES ('2699', '303', '新平彝族傣族自治县', '3', '530427', '50');
INSERT INTO `fly_area` VALUES ('2700', '303', '元江哈尼族彝族傣族自治县', '3', '530428', '50');
INSERT INTO `fly_area` VALUES ('2701', '304', '隆阳区', '3', '530502', '50');
INSERT INTO `fly_area` VALUES ('2702', '304', '施甸县', '3', '530521', '50');
INSERT INTO `fly_area` VALUES ('2703', '304', '龙陵县', '3', '530523', '50');
INSERT INTO `fly_area` VALUES ('2704', '304', '昌宁县', '3', '530524', '50');
INSERT INTO `fly_area` VALUES ('2705', '304', '腾冲市', '3', '530581', '50');
INSERT INTO `fly_area` VALUES ('2706', '305', '昭阳区', '3', '530602', '50');
INSERT INTO `fly_area` VALUES ('2707', '305', '鲁甸县', '3', '530621', '50');
INSERT INTO `fly_area` VALUES ('2708', '305', '巧家县', '3', '530622', '50');
INSERT INTO `fly_area` VALUES ('2709', '305', '盐津县', '3', '530623', '50');
INSERT INTO `fly_area` VALUES ('2710', '305', '大关县', '3', '530624', '50');
INSERT INTO `fly_area` VALUES ('2711', '305', '永善县', '3', '530625', '50');
INSERT INTO `fly_area` VALUES ('2712', '305', '绥江县', '3', '530626', '50');
INSERT INTO `fly_area` VALUES ('2713', '305', '镇雄县', '3', '530627', '50');
INSERT INTO `fly_area` VALUES ('2714', '305', '彝良县', '3', '530628', '50');
INSERT INTO `fly_area` VALUES ('2715', '305', '威信县', '3', '530629', '50');
INSERT INTO `fly_area` VALUES ('2716', '305', '水富县', '3', '530630', '50');
INSERT INTO `fly_area` VALUES ('2717', '306', '古城区', '3', '530702', '50');
INSERT INTO `fly_area` VALUES ('2718', '306', '玉龙纳西族自治县', '3', '530721', '50');
INSERT INTO `fly_area` VALUES ('2719', '306', '永胜县', '3', '530722', '50');
INSERT INTO `fly_area` VALUES ('2720', '306', '华坪县', '3', '530723', '50');
INSERT INTO `fly_area` VALUES ('2721', '306', '宁蒗彝族自治县', '3', '530724', '50');
INSERT INTO `fly_area` VALUES ('2722', '307', '思茅区', '3', '530802', '50');
INSERT INTO `fly_area` VALUES ('2723', '307', '宁洱哈尼族彝族自治县', '3', '530821', '50');
INSERT INTO `fly_area` VALUES ('2724', '307', '墨江哈尼族自治县', '3', '530822', '50');
INSERT INTO `fly_area` VALUES ('2725', '307', '景东彝族自治县', '3', '530823', '50');
INSERT INTO `fly_area` VALUES ('2726', '307', '景谷傣族彝族自治县', '3', '530824', '50');
INSERT INTO `fly_area` VALUES ('2727', '307', '镇沅彝族哈尼族拉祜族自治县', '3', '530825', '50');
INSERT INTO `fly_area` VALUES ('2728', '307', '江城哈尼族彝族自治县', '3', '530826', '50');
INSERT INTO `fly_area` VALUES ('2729', '307', '孟连傣族拉祜族佤族自治县', '3', '530827', '50');
INSERT INTO `fly_area` VALUES ('2730', '307', '澜沧拉祜族自治县', '3', '530828', '50');
INSERT INTO `fly_area` VALUES ('2731', '307', '西盟佤族自治县', '3', '530829', '50');
INSERT INTO `fly_area` VALUES ('2732', '308', '临翔区', '3', '530902', '50');
INSERT INTO `fly_area` VALUES ('2733', '308', '凤庆县', '3', '530921', '50');
INSERT INTO `fly_area` VALUES ('2734', '308', '云县', '3', '530922', '50');
INSERT INTO `fly_area` VALUES ('2735', '308', '永德县', '3', '530923', '50');
INSERT INTO `fly_area` VALUES ('2736', '308', '镇康县', '3', '530924', '50');
INSERT INTO `fly_area` VALUES ('2737', '308', '双江拉祜族佤族布朗族傣族自治县', '3', '530925', '50');
INSERT INTO `fly_area` VALUES ('2738', '308', '耿马傣族佤族自治县', '3', '530926', '50');
INSERT INTO `fly_area` VALUES ('2739', '308', '沧源佤族自治县', '3', '530927', '50');
INSERT INTO `fly_area` VALUES ('2740', '309', '楚雄市', '3', '532301', '50');
INSERT INTO `fly_area` VALUES ('2741', '309', '双柏县', '3', '532322', '50');
INSERT INTO `fly_area` VALUES ('2742', '309', '牟定县', '3', '532323', '50');
INSERT INTO `fly_area` VALUES ('2743', '309', '南华县', '3', '532324', '50');
INSERT INTO `fly_area` VALUES ('2744', '309', '姚安县', '3', '532325', '50');
INSERT INTO `fly_area` VALUES ('2745', '309', '大姚县', '3', '532326', '50');
INSERT INTO `fly_area` VALUES ('2746', '309', '永仁县', '3', '532327', '50');
INSERT INTO `fly_area` VALUES ('2747', '309', '元谋县', '3', '532328', '50');
INSERT INTO `fly_area` VALUES ('2748', '309', '武定县', '3', '532329', '50');
INSERT INTO `fly_area` VALUES ('2749', '309', '禄丰县', '3', '532331', '50');
INSERT INTO `fly_area` VALUES ('2750', '310', '个旧市', '3', '532501', '50');
INSERT INTO `fly_area` VALUES ('2751', '310', '开远市', '3', '532502', '50');
INSERT INTO `fly_area` VALUES ('2752', '310', '蒙自市', '3', '532503', '50');
INSERT INTO `fly_area` VALUES ('2753', '310', '弥勒市', '3', '532504', '50');
INSERT INTO `fly_area` VALUES ('2754', '310', '屏边苗族自治县', '3', '532523', '50');
INSERT INTO `fly_area` VALUES ('2755', '310', '建水县', '3', '532524', '50');
INSERT INTO `fly_area` VALUES ('2756', '310', '石屏县', '3', '532525', '50');
INSERT INTO `fly_area` VALUES ('2757', '310', '泸西县', '3', '532527', '50');
INSERT INTO `fly_area` VALUES ('2758', '310', '元阳县', '3', '532528', '50');
INSERT INTO `fly_area` VALUES ('2759', '310', '红河县', '3', '532529', '50');
INSERT INTO `fly_area` VALUES ('2760', '310', '金平苗族瑶族傣族自治县', '3', '532530', '50');
INSERT INTO `fly_area` VALUES ('2761', '310', '绿春县', '3', '532531', '50');
INSERT INTO `fly_area` VALUES ('2762', '310', '河口瑶族自治县', '3', '532532', '50');
INSERT INTO `fly_area` VALUES ('2763', '311', '文山市', '3', '532601', '50');
INSERT INTO `fly_area` VALUES ('2764', '311', '砚山县', '3', '532622', '50');
INSERT INTO `fly_area` VALUES ('2765', '311', '西畴县', '3', '532623', '50');
INSERT INTO `fly_area` VALUES ('2766', '311', '麻栗坡县', '3', '532624', '50');
INSERT INTO `fly_area` VALUES ('2767', '311', '马关县', '3', '532625', '50');
INSERT INTO `fly_area` VALUES ('2768', '311', '丘北县', '3', '532626', '50');
INSERT INTO `fly_area` VALUES ('2769', '311', '广南县', '3', '532627', '50');
INSERT INTO `fly_area` VALUES ('2770', '311', '富宁县', '3', '532628', '50');
INSERT INTO `fly_area` VALUES ('2771', '312', '景洪市', '3', '532801', '50');
INSERT INTO `fly_area` VALUES ('2772', '312', '勐海县', '3', '532822', '50');
INSERT INTO `fly_area` VALUES ('2773', '312', '勐腊县', '3', '532823', '50');
INSERT INTO `fly_area` VALUES ('2774', '313', '大理市', '3', '532901', '50');
INSERT INTO `fly_area` VALUES ('2775', '313', '漾濞彝族自治县', '3', '532922', '50');
INSERT INTO `fly_area` VALUES ('2776', '313', '祥云县', '3', '532923', '50');
INSERT INTO `fly_area` VALUES ('2777', '313', '宾川县', '3', '532924', '50');
INSERT INTO `fly_area` VALUES ('2778', '313', '弥渡县', '3', '532925', '50');
INSERT INTO `fly_area` VALUES ('2779', '313', '南涧彝族自治县', '3', '532926', '50');
INSERT INTO `fly_area` VALUES ('2780', '313', '巍山彝族回族自治县', '3', '532927', '50');
INSERT INTO `fly_area` VALUES ('2781', '313', '永平县', '3', '532928', '50');
INSERT INTO `fly_area` VALUES ('2782', '313', '云龙县', '3', '532929', '50');
INSERT INTO `fly_area` VALUES ('2783', '313', '洱源县', '3', '532930', '50');
INSERT INTO `fly_area` VALUES ('2784', '313', '剑川县', '3', '532931', '50');
INSERT INTO `fly_area` VALUES ('2785', '313', '鹤庆县', '3', '532932', '50');
INSERT INTO `fly_area` VALUES ('2786', '314', '瑞丽市', '3', '533102', '50');
INSERT INTO `fly_area` VALUES ('2787', '314', '芒市', '3', '533103', '50');
INSERT INTO `fly_area` VALUES ('2788', '314', '梁河县', '3', '533122', '50');
INSERT INTO `fly_area` VALUES ('2789', '314', '盈江县', '3', '533123', '50');
INSERT INTO `fly_area` VALUES ('2790', '314', '陇川县', '3', '533124', '50');
INSERT INTO `fly_area` VALUES ('2791', '315', '泸水市', '3', '533301', '50');
INSERT INTO `fly_area` VALUES ('2792', '315', '福贡县', '3', '533323', '50');
INSERT INTO `fly_area` VALUES ('2793', '315', '贡山独龙族怒族自治县', '3', '533324', '50');
INSERT INTO `fly_area` VALUES ('2794', '315', '兰坪白族普米族自治县', '3', '533325', '50');
INSERT INTO `fly_area` VALUES ('2795', '316', '香格里拉市', '3', '533401', '50');
INSERT INTO `fly_area` VALUES ('2796', '316', '德钦县', '3', '533422', '50');
INSERT INTO `fly_area` VALUES ('2797', '316', '维西傈僳族自治县', '3', '533423', '50');
INSERT INTO `fly_area` VALUES ('2798', '317', '城关区', '3', '540102', '50');
INSERT INTO `fly_area` VALUES ('2799', '317', '堆龙德庆区', '3', '540103', '50');
INSERT INTO `fly_area` VALUES ('2800', '317', '林周县', '3', '540121', '50');
INSERT INTO `fly_area` VALUES ('2801', '317', '当雄县', '3', '540122', '50');
INSERT INTO `fly_area` VALUES ('2802', '317', '尼木县', '3', '540123', '50');
INSERT INTO `fly_area` VALUES ('2803', '317', '曲水县', '3', '540124', '50');
INSERT INTO `fly_area` VALUES ('2804', '317', '达孜县', '3', '540126', '50');
INSERT INTO `fly_area` VALUES ('2805', '317', '墨竹工卡县', '3', '540127', '50');
INSERT INTO `fly_area` VALUES ('2806', '318', '桑珠孜区', '3', '540202', '50');
INSERT INTO `fly_area` VALUES ('2807', '318', '南木林县', '3', '540221', '50');
INSERT INTO `fly_area` VALUES ('2808', '318', '江孜县', '3', '540222', '50');
INSERT INTO `fly_area` VALUES ('2809', '318', '定日县', '3', '540223', '50');
INSERT INTO `fly_area` VALUES ('2810', '318', '萨迦县', '3', '540224', '50');
INSERT INTO `fly_area` VALUES ('2811', '318', '拉孜县', '3', '540225', '50');
INSERT INTO `fly_area` VALUES ('2812', '318', '昂仁县', '3', '540226', '50');
INSERT INTO `fly_area` VALUES ('2813', '318', '谢通门县', '3', '540227', '50');
INSERT INTO `fly_area` VALUES ('2814', '318', '白朗县', '3', '540228', '50');
INSERT INTO `fly_area` VALUES ('2815', '318', '仁布县', '3', '540229', '50');
INSERT INTO `fly_area` VALUES ('2816', '318', '康马县', '3', '540230', '50');
INSERT INTO `fly_area` VALUES ('2817', '318', '定结县', '3', '540231', '50');
INSERT INTO `fly_area` VALUES ('2818', '318', '仲巴县', '3', '540232', '50');
INSERT INTO `fly_area` VALUES ('2819', '318', '亚东县', '3', '540233', '50');
INSERT INTO `fly_area` VALUES ('2820', '318', '吉隆县', '3', '540234', '50');
INSERT INTO `fly_area` VALUES ('2821', '318', '聂拉木县', '3', '540235', '50');
INSERT INTO `fly_area` VALUES ('2822', '318', '萨嘎县', '3', '540236', '50');
INSERT INTO `fly_area` VALUES ('2823', '318', '岗巴县', '3', '540237', '50');
INSERT INTO `fly_area` VALUES ('2824', '319', '卡若区', '3', '540302', '50');
INSERT INTO `fly_area` VALUES ('2825', '319', '江达县', '3', '540321', '50');
INSERT INTO `fly_area` VALUES ('2826', '319', '贡觉县', '3', '540322', '50');
INSERT INTO `fly_area` VALUES ('2827', '319', '类乌齐县', '3', '540323', '50');
INSERT INTO `fly_area` VALUES ('2828', '319', '丁青县', '3', '540324', '50');
INSERT INTO `fly_area` VALUES ('2829', '319', '察雅县', '3', '540325', '50');
INSERT INTO `fly_area` VALUES ('2830', '319', '八宿县', '3', '540326', '50');
INSERT INTO `fly_area` VALUES ('2831', '319', '左贡县', '3', '540327', '50');
INSERT INTO `fly_area` VALUES ('2832', '319', '芒康县', '3', '540328', '50');
INSERT INTO `fly_area` VALUES ('2833', '319', '洛隆县', '3', '540329', '50');
INSERT INTO `fly_area` VALUES ('2834', '319', '边坝县', '3', '540330', '50');
INSERT INTO `fly_area` VALUES ('2835', '320', '巴宜区', '3', '540402', '50');
INSERT INTO `fly_area` VALUES ('2836', '320', '工布江达县', '3', '540421', '50');
INSERT INTO `fly_area` VALUES ('2837', '320', '米林县', '3', '540422', '50');
INSERT INTO `fly_area` VALUES ('2838', '320', '墨脱县', '3', '540423', '50');
INSERT INTO `fly_area` VALUES ('2839', '320', '波密县', '3', '540424', '50');
INSERT INTO `fly_area` VALUES ('2840', '320', '察隅县', '3', '540425', '50');
INSERT INTO `fly_area` VALUES ('2841', '320', '朗县', '3', '540426', '50');
INSERT INTO `fly_area` VALUES ('2842', '321', '乃东区', '3', '540502', '50');
INSERT INTO `fly_area` VALUES ('2843', '321', '扎囊县', '3', '540521', '50');
INSERT INTO `fly_area` VALUES ('2844', '321', '贡嘎县', '3', '540522', '50');
INSERT INTO `fly_area` VALUES ('2845', '321', '桑日县', '3', '540523', '50');
INSERT INTO `fly_area` VALUES ('2846', '321', '琼结县', '3', '540524', '50');
INSERT INTO `fly_area` VALUES ('2847', '321', '曲松县', '3', '540525', '50');
INSERT INTO `fly_area` VALUES ('2848', '321', '措美县', '3', '540526', '50');
INSERT INTO `fly_area` VALUES ('2849', '321', '洛扎县', '3', '540527', '50');
INSERT INTO `fly_area` VALUES ('2850', '321', '加查县', '3', '540528', '50');
INSERT INTO `fly_area` VALUES ('2851', '321', '隆子县', '3', '540529', '50');
INSERT INTO `fly_area` VALUES ('2852', '321', '错那县', '3', '540530', '50');
INSERT INTO `fly_area` VALUES ('2853', '321', '浪卡子县', '3', '540531', '50');
INSERT INTO `fly_area` VALUES ('2854', '322', '那曲县', '3', '542421', '50');
INSERT INTO `fly_area` VALUES ('2855', '322', '嘉黎县', '3', '542422', '50');
INSERT INTO `fly_area` VALUES ('2856', '322', '比如县', '3', '542423', '50');
INSERT INTO `fly_area` VALUES ('2857', '322', '聂荣县', '3', '542424', '50');
INSERT INTO `fly_area` VALUES ('2858', '322', '安多县', '3', '542425', '50');
INSERT INTO `fly_area` VALUES ('2859', '322', '申扎县', '3', '542426', '50');
INSERT INTO `fly_area` VALUES ('2860', '322', '索县', '3', '542427', '50');
INSERT INTO `fly_area` VALUES ('2861', '322', '班戈县', '3', '542428', '50');
INSERT INTO `fly_area` VALUES ('2862', '322', '巴青县', '3', '542429', '50');
INSERT INTO `fly_area` VALUES ('2863', '322', '尼玛县', '3', '542430', '50');
INSERT INTO `fly_area` VALUES ('2864', '322', '双湖县', '3', '542431', '50');
INSERT INTO `fly_area` VALUES ('2865', '323', '普兰县', '3', '542521', '50');
INSERT INTO `fly_area` VALUES ('2866', '323', '札达县', '3', '542522', '50');
INSERT INTO `fly_area` VALUES ('2867', '323', '噶尔县', '3', '542523', '50');
INSERT INTO `fly_area` VALUES ('2868', '323', '日土县', '3', '542524', '50');
INSERT INTO `fly_area` VALUES ('2869', '323', '革吉县', '3', '542525', '50');
INSERT INTO `fly_area` VALUES ('2870', '323', '改则县', '3', '542526', '50');
INSERT INTO `fly_area` VALUES ('2871', '323', '措勤县', '3', '542527', '50');
INSERT INTO `fly_area` VALUES ('2872', '324', '新城区', '3', '610102', '50');
INSERT INTO `fly_area` VALUES ('2873', '324', '碑林区', '3', '610103', '50');
INSERT INTO `fly_area` VALUES ('2874', '324', '莲湖区', '3', '610104', '50');
INSERT INTO `fly_area` VALUES ('2875', '324', '灞桥区', '3', '610111', '50');
INSERT INTO `fly_area` VALUES ('2876', '324', '未央区', '3', '610112', '50');
INSERT INTO `fly_area` VALUES ('2877', '324', '雁塔区', '3', '610113', '50');
INSERT INTO `fly_area` VALUES ('2878', '324', '阎良区', '3', '610114', '50');
INSERT INTO `fly_area` VALUES ('2879', '324', '临潼区', '3', '610115', '50');
INSERT INTO `fly_area` VALUES ('2880', '324', '长安区', '3', '610116', '50');
INSERT INTO `fly_area` VALUES ('2881', '324', '高陵区', '3', '610117', '50');
INSERT INTO `fly_area` VALUES ('2882', '324', '蓝田县', '3', '610122', '50');
INSERT INTO `fly_area` VALUES ('2883', '324', '周至县', '3', '610124', '50');
INSERT INTO `fly_area` VALUES ('2884', '324', '户县', '3', '610125', '50');
INSERT INTO `fly_area` VALUES ('2885', '325', '王益区', '3', '610202', '50');
INSERT INTO `fly_area` VALUES ('2886', '325', '印台区', '3', '610203', '50');
INSERT INTO `fly_area` VALUES ('2887', '325', '耀州区', '3', '610204', '50');
INSERT INTO `fly_area` VALUES ('2888', '325', '宜君县', '3', '610222', '50');
INSERT INTO `fly_area` VALUES ('2889', '326', '渭滨区', '3', '610302', '50');
INSERT INTO `fly_area` VALUES ('2890', '326', '金台区', '3', '610303', '50');
INSERT INTO `fly_area` VALUES ('2891', '326', '陈仓区', '3', '610304', '50');
INSERT INTO `fly_area` VALUES ('2892', '326', '凤翔县', '3', '610322', '50');
INSERT INTO `fly_area` VALUES ('2893', '326', '岐山县', '3', '610323', '50');
INSERT INTO `fly_area` VALUES ('2894', '326', '扶风县', '3', '610324', '50');
INSERT INTO `fly_area` VALUES ('2895', '326', '眉县', '3', '610326', '50');
INSERT INTO `fly_area` VALUES ('2896', '326', '陇县', '3', '610327', '50');
INSERT INTO `fly_area` VALUES ('2897', '326', '千阳县', '3', '610328', '50');
INSERT INTO `fly_area` VALUES ('2898', '326', '麟游县', '3', '610329', '50');
INSERT INTO `fly_area` VALUES ('2899', '326', '凤县', '3', '610330', '50');
INSERT INTO `fly_area` VALUES ('2900', '326', '太白县', '3', '610331', '50');
INSERT INTO `fly_area` VALUES ('2901', '327', '秦都区', '3', '610402', '50');
INSERT INTO `fly_area` VALUES ('2902', '327', '杨陵区', '3', '610403', '50');
INSERT INTO `fly_area` VALUES ('2903', '327', '渭城区', '3', '610404', '50');
INSERT INTO `fly_area` VALUES ('2904', '327', '三原县', '3', '610422', '50');
INSERT INTO `fly_area` VALUES ('2905', '327', '泾阳县', '3', '610423', '50');
INSERT INTO `fly_area` VALUES ('2906', '327', '乾县', '3', '610424', '50');
INSERT INTO `fly_area` VALUES ('2907', '327', '礼泉县', '3', '610425', '50');
INSERT INTO `fly_area` VALUES ('2908', '327', '永寿县', '3', '610426', '50');
INSERT INTO `fly_area` VALUES ('2909', '327', '彬县', '3', '610427', '50');
INSERT INTO `fly_area` VALUES ('2910', '327', '长武县', '3', '610428', '50');
INSERT INTO `fly_area` VALUES ('2911', '327', '旬邑县', '3', '610429', '50');
INSERT INTO `fly_area` VALUES ('2912', '327', '淳化县', '3', '610430', '50');
INSERT INTO `fly_area` VALUES ('2913', '327', '武功县', '3', '610431', '50');
INSERT INTO `fly_area` VALUES ('2914', '327', '兴平市', '3', '610481', '50');
INSERT INTO `fly_area` VALUES ('2915', '328', '临渭区', '3', '610502', '50');
INSERT INTO `fly_area` VALUES ('2916', '328', '华州区', '3', '610503', '50');
INSERT INTO `fly_area` VALUES ('2917', '328', '潼关县', '3', '610522', '50');
INSERT INTO `fly_area` VALUES ('2918', '328', '大荔县', '3', '610523', '50');
INSERT INTO `fly_area` VALUES ('2919', '328', '合阳县', '3', '610524', '50');
INSERT INTO `fly_area` VALUES ('2920', '328', '澄城县', '3', '610525', '50');
INSERT INTO `fly_area` VALUES ('2921', '328', '蒲城县', '3', '610526', '50');
INSERT INTO `fly_area` VALUES ('2922', '328', '白水县', '3', '610527', '50');
INSERT INTO `fly_area` VALUES ('2923', '328', '富平县', '3', '610528', '50');
INSERT INTO `fly_area` VALUES ('2924', '328', '韩城市', '3', '610581', '50');
INSERT INTO `fly_area` VALUES ('2925', '328', '华阴市', '3', '610582', '50');
INSERT INTO `fly_area` VALUES ('2926', '329', '宝塔区', '3', '610602', '50');
INSERT INTO `fly_area` VALUES ('2927', '329', '安塞区', '3', '610603', '50');
INSERT INTO `fly_area` VALUES ('2928', '329', '延长县', '3', '610621', '50');
INSERT INTO `fly_area` VALUES ('2929', '329', '延川县', '3', '610622', '50');
INSERT INTO `fly_area` VALUES ('2930', '329', '子长县', '3', '610623', '50');
INSERT INTO `fly_area` VALUES ('2931', '329', '志丹县', '3', '610625', '50');
INSERT INTO `fly_area` VALUES ('2932', '329', '吴起县', '3', '610626', '50');
INSERT INTO `fly_area` VALUES ('2933', '329', '甘泉县', '3', '610627', '50');
INSERT INTO `fly_area` VALUES ('2934', '329', '富县', '3', '610628', '50');
INSERT INTO `fly_area` VALUES ('2935', '329', '洛川县', '3', '610629', '50');
INSERT INTO `fly_area` VALUES ('2936', '329', '宜川县', '3', '610630', '50');
INSERT INTO `fly_area` VALUES ('2937', '329', '黄龙县', '3', '610631', '50');
INSERT INTO `fly_area` VALUES ('2938', '329', '黄陵县', '3', '610632', '50');
INSERT INTO `fly_area` VALUES ('2939', '330', '汉台区', '3', '610702', '50');
INSERT INTO `fly_area` VALUES ('2940', '330', '南郑县', '3', '610721', '50');
INSERT INTO `fly_area` VALUES ('2941', '330', '城固县', '3', '610722', '50');
INSERT INTO `fly_area` VALUES ('2942', '330', '洋县', '3', '610723', '50');
INSERT INTO `fly_area` VALUES ('2943', '330', '西乡县', '3', '610724', '50');
INSERT INTO `fly_area` VALUES ('2944', '330', '勉县', '3', '610725', '50');
INSERT INTO `fly_area` VALUES ('2945', '330', '宁强县', '3', '610726', '50');
INSERT INTO `fly_area` VALUES ('2946', '330', '略阳县', '3', '610727', '50');
INSERT INTO `fly_area` VALUES ('2947', '330', '镇巴县', '3', '610728', '50');
INSERT INTO `fly_area` VALUES ('2948', '330', '留坝县', '3', '610729', '50');
INSERT INTO `fly_area` VALUES ('2949', '330', '佛坪县', '3', '610730', '50');
INSERT INTO `fly_area` VALUES ('2950', '331', '榆阳区', '3', '610802', '50');
INSERT INTO `fly_area` VALUES ('2951', '331', '横山区', '3', '610803', '50');
INSERT INTO `fly_area` VALUES ('2952', '331', '神木县', '3', '610821', '50');
INSERT INTO `fly_area` VALUES ('2953', '331', '府谷县', '3', '610822', '50');
INSERT INTO `fly_area` VALUES ('2954', '331', '靖边县', '3', '610824', '50');
INSERT INTO `fly_area` VALUES ('2955', '331', '定边县', '3', '610825', '50');
INSERT INTO `fly_area` VALUES ('2956', '331', '绥德县', '3', '610826', '50');
INSERT INTO `fly_area` VALUES ('2957', '331', '米脂县', '3', '610827', '50');
INSERT INTO `fly_area` VALUES ('2958', '331', '佳县', '3', '610828', '50');
INSERT INTO `fly_area` VALUES ('2959', '331', '吴堡县', '3', '610829', '50');
INSERT INTO `fly_area` VALUES ('2960', '331', '清涧县', '3', '610830', '50');
INSERT INTO `fly_area` VALUES ('2961', '331', '子洲县', '3', '610831', '50');
INSERT INTO `fly_area` VALUES ('2962', '332', '汉滨区', '3', '610902', '50');
INSERT INTO `fly_area` VALUES ('2963', '332', '汉阴县', '3', '610921', '50');
INSERT INTO `fly_area` VALUES ('2964', '332', '石泉县', '3', '610922', '50');
INSERT INTO `fly_area` VALUES ('2965', '332', '宁陕县', '3', '610923', '50');
INSERT INTO `fly_area` VALUES ('2966', '332', '紫阳县', '3', '610924', '50');
INSERT INTO `fly_area` VALUES ('2967', '332', '岚皋县', '3', '610925', '50');
INSERT INTO `fly_area` VALUES ('2968', '332', '平利县', '3', '610926', '50');
INSERT INTO `fly_area` VALUES ('2969', '332', '镇坪县', '3', '610927', '50');
INSERT INTO `fly_area` VALUES ('2970', '332', '旬阳县', '3', '610928', '50');
INSERT INTO `fly_area` VALUES ('2971', '332', '白河县', '3', '610929', '50');
INSERT INTO `fly_area` VALUES ('2972', '333', '商州区', '3', '611002', '50');
INSERT INTO `fly_area` VALUES ('2973', '333', '洛南县', '3', '611021', '50');
INSERT INTO `fly_area` VALUES ('2974', '333', '丹凤县', '3', '611022', '50');
INSERT INTO `fly_area` VALUES ('2975', '333', '商南县', '3', '611023', '50');
INSERT INTO `fly_area` VALUES ('2976', '333', '山阳县', '3', '611024', '50');
INSERT INTO `fly_area` VALUES ('2977', '333', '镇安县', '3', '611025', '50');
INSERT INTO `fly_area` VALUES ('2978', '333', '柞水县', '3', '611026', '50');
INSERT INTO `fly_area` VALUES ('2979', '334', '城关区', '3', '620102', '50');
INSERT INTO `fly_area` VALUES ('2980', '334', '七里河区', '3', '620103', '50');
INSERT INTO `fly_area` VALUES ('2981', '334', '西固区', '3', '620104', '50');
INSERT INTO `fly_area` VALUES ('2982', '334', '安宁区', '3', '620105', '50');
INSERT INTO `fly_area` VALUES ('2983', '334', '红古区', '3', '620111', '50');
INSERT INTO `fly_area` VALUES ('2984', '334', '永登县', '3', '620121', '50');
INSERT INTO `fly_area` VALUES ('2985', '334', '皋兰县', '3', '620122', '50');
INSERT INTO `fly_area` VALUES ('2986', '334', '榆中县', '3', '620123', '50');
INSERT INTO `fly_area` VALUES ('2987', '335', '嘉峪关市', '3', '620201', '50');
INSERT INTO `fly_area` VALUES ('2988', '336', '金川区', '3', '620302', '50');
INSERT INTO `fly_area` VALUES ('2989', '336', '永昌县', '3', '620321', '50');
INSERT INTO `fly_area` VALUES ('2990', '337', '白银区', '3', '620402', '50');
INSERT INTO `fly_area` VALUES ('2991', '337', '平川区', '3', '620403', '50');
INSERT INTO `fly_area` VALUES ('2992', '337', '靖远县', '3', '620421', '50');
INSERT INTO `fly_area` VALUES ('2993', '337', '会宁县', '3', '620422', '50');
INSERT INTO `fly_area` VALUES ('2994', '337', '景泰县', '3', '620423', '50');
INSERT INTO `fly_area` VALUES ('2995', '338', '秦州区', '3', '620502', '50');
INSERT INTO `fly_area` VALUES ('2996', '338', '麦积区', '3', '620503', '50');
INSERT INTO `fly_area` VALUES ('2997', '338', '清水县', '3', '620521', '50');
INSERT INTO `fly_area` VALUES ('2998', '338', '秦安县', '3', '620522', '50');
INSERT INTO `fly_area` VALUES ('2999', '338', '甘谷县', '3', '620523', '50');
INSERT INTO `fly_area` VALUES ('3000', '338', '武山县', '3', '620524', '50');
INSERT INTO `fly_area` VALUES ('3001', '338', '张家川回族自治县', '3', '620525', '50');
INSERT INTO `fly_area` VALUES ('3002', '339', '凉州区', '3', '620602', '50');
INSERT INTO `fly_area` VALUES ('3003', '339', '民勤县', '3', '620621', '50');
INSERT INTO `fly_area` VALUES ('3004', '339', '古浪县', '3', '620622', '50');
INSERT INTO `fly_area` VALUES ('3005', '339', '天祝藏族自治县', '3', '620623', '50');
INSERT INTO `fly_area` VALUES ('3006', '340', '甘州区', '3', '620702', '50');
INSERT INTO `fly_area` VALUES ('3007', '340', '肃南裕固族自治县', '3', '620721', '50');
INSERT INTO `fly_area` VALUES ('3008', '340', '民乐县', '3', '620722', '50');
INSERT INTO `fly_area` VALUES ('3009', '340', '临泽县', '3', '620723', '50');
INSERT INTO `fly_area` VALUES ('3010', '340', '高台县', '3', '620724', '50');
INSERT INTO `fly_area` VALUES ('3011', '340', '山丹县', '3', '620725', '50');
INSERT INTO `fly_area` VALUES ('3012', '341', '崆峒区', '3', '620802', '50');
INSERT INTO `fly_area` VALUES ('3013', '341', '泾川县', '3', '620821', '50');
INSERT INTO `fly_area` VALUES ('3014', '341', '灵台县', '3', '620822', '50');
INSERT INTO `fly_area` VALUES ('3015', '341', '崇信县', '3', '620823', '50');
INSERT INTO `fly_area` VALUES ('3016', '341', '华亭县', '3', '620824', '50');
INSERT INTO `fly_area` VALUES ('3017', '341', '庄浪县', '3', '620825', '50');
INSERT INTO `fly_area` VALUES ('3018', '341', '静宁县', '3', '620826', '50');
INSERT INTO `fly_area` VALUES ('3019', '342', '肃州区', '3', '620902', '50');
INSERT INTO `fly_area` VALUES ('3020', '342', '金塔县', '3', '620921', '50');
INSERT INTO `fly_area` VALUES ('3021', '342', '瓜州县', '3', '620922', '50');
INSERT INTO `fly_area` VALUES ('3022', '342', '肃北蒙古族自治县', '3', '620923', '50');
INSERT INTO `fly_area` VALUES ('3023', '342', '阿克塞哈萨克族自治县', '3', '620924', '50');
INSERT INTO `fly_area` VALUES ('3024', '342', '玉门市', '3', '620981', '50');
INSERT INTO `fly_area` VALUES ('3025', '342', '敦煌市', '3', '620982', '50');
INSERT INTO `fly_area` VALUES ('3026', '343', '西峰区', '3', '621002', '50');
INSERT INTO `fly_area` VALUES ('3027', '343', '庆城县', '3', '621021', '50');
INSERT INTO `fly_area` VALUES ('3028', '343', '环县', '3', '621022', '50');
INSERT INTO `fly_area` VALUES ('3029', '343', '华池县', '3', '621023', '50');
INSERT INTO `fly_area` VALUES ('3030', '343', '合水县', '3', '621024', '50');
INSERT INTO `fly_area` VALUES ('3031', '343', '正宁县', '3', '621025', '50');
INSERT INTO `fly_area` VALUES ('3032', '343', '宁县', '3', '621026', '50');
INSERT INTO `fly_area` VALUES ('3033', '343', '镇原县', '3', '621027', '50');
INSERT INTO `fly_area` VALUES ('3034', '344', '安定区', '3', '621102', '50');
INSERT INTO `fly_area` VALUES ('3035', '344', '通渭县', '3', '621121', '50');
INSERT INTO `fly_area` VALUES ('3036', '344', '陇西县', '3', '621122', '50');
INSERT INTO `fly_area` VALUES ('3037', '344', '渭源县', '3', '621123', '50');
INSERT INTO `fly_area` VALUES ('3038', '344', '临洮县', '3', '621124', '50');
INSERT INTO `fly_area` VALUES ('3039', '344', '漳县', '3', '621125', '50');
INSERT INTO `fly_area` VALUES ('3040', '344', '岷县', '3', '621126', '50');
INSERT INTO `fly_area` VALUES ('3041', '345', '武都区', '3', '621202', '50');
INSERT INTO `fly_area` VALUES ('3042', '345', '成县', '3', '621221', '50');
INSERT INTO `fly_area` VALUES ('3043', '345', '文县', '3', '621222', '50');
INSERT INTO `fly_area` VALUES ('3044', '345', '宕昌县', '3', '621223', '50');
INSERT INTO `fly_area` VALUES ('3045', '345', '康县', '3', '621224', '50');
INSERT INTO `fly_area` VALUES ('3046', '345', '西和县', '3', '621225', '50');
INSERT INTO `fly_area` VALUES ('3047', '345', '礼县', '3', '621226', '50');
INSERT INTO `fly_area` VALUES ('3048', '345', '徽县', '3', '621227', '50');
INSERT INTO `fly_area` VALUES ('3049', '345', '两当县', '3', '621228', '50');
INSERT INTO `fly_area` VALUES ('3050', '346', '临夏市', '3', '622901', '50');
INSERT INTO `fly_area` VALUES ('3051', '346', '临夏县', '3', '622921', '50');
INSERT INTO `fly_area` VALUES ('3052', '346', '康乐县', '3', '622922', '50');
INSERT INTO `fly_area` VALUES ('3053', '346', '永靖县', '3', '622923', '50');
INSERT INTO `fly_area` VALUES ('3054', '346', '广河县', '3', '622924', '50');
INSERT INTO `fly_area` VALUES ('3055', '346', '和政县', '3', '622925', '50');
INSERT INTO `fly_area` VALUES ('3056', '346', '东乡族自治县', '3', '622926', '50');
INSERT INTO `fly_area` VALUES ('3057', '346', '积石山保安族东乡族撒拉族自治县', '3', '622927', '50');
INSERT INTO `fly_area` VALUES ('3058', '347', '合作市', '3', '623001', '50');
INSERT INTO `fly_area` VALUES ('3059', '347', '临潭县', '3', '623021', '50');
INSERT INTO `fly_area` VALUES ('3060', '347', '卓尼县', '3', '623022', '50');
INSERT INTO `fly_area` VALUES ('3061', '347', '舟曲县', '3', '623023', '50');
INSERT INTO `fly_area` VALUES ('3062', '347', '迭部县', '3', '623024', '50');
INSERT INTO `fly_area` VALUES ('3063', '347', '玛曲县', '3', '623025', '50');
INSERT INTO `fly_area` VALUES ('3064', '347', '碌曲县', '3', '623026', '50');
INSERT INTO `fly_area` VALUES ('3065', '347', '夏河县', '3', '623027', '50');
INSERT INTO `fly_area` VALUES ('3066', '348', '城东区', '3', '630102', '50');
INSERT INTO `fly_area` VALUES ('3067', '348', '城中区', '3', '630103', '50');
INSERT INTO `fly_area` VALUES ('3068', '348', '城西区', '3', '630104', '50');
INSERT INTO `fly_area` VALUES ('3069', '348', '城北区', '3', '630105', '50');
INSERT INTO `fly_area` VALUES ('3070', '348', '大通回族土族自治县', '3', '630121', '50');
INSERT INTO `fly_area` VALUES ('3071', '348', '湟中县', '3', '630122', '50');
INSERT INTO `fly_area` VALUES ('3072', '348', '湟源县', '3', '630123', '50');
INSERT INTO `fly_area` VALUES ('3073', '349', '乐都区', '3', '630202', '50');
INSERT INTO `fly_area` VALUES ('3074', '349', '平安区', '3', '630203', '50');
INSERT INTO `fly_area` VALUES ('3075', '349', '民和回族土族自治县', '3', '630222', '50');
INSERT INTO `fly_area` VALUES ('3076', '349', '互助土族自治县', '3', '630223', '50');
INSERT INTO `fly_area` VALUES ('3077', '349', '化隆回族自治县', '3', '630224', '50');
INSERT INTO `fly_area` VALUES ('3078', '349', '循化撒拉族自治县', '3', '630225', '50');
INSERT INTO `fly_area` VALUES ('3079', '350', '门源回族自治县', '3', '632221', '50');
INSERT INTO `fly_area` VALUES ('3080', '350', '祁连县', '3', '632222', '50');
INSERT INTO `fly_area` VALUES ('3081', '350', '海晏县', '3', '632223', '50');
INSERT INTO `fly_area` VALUES ('3082', '350', '刚察县', '3', '632224', '50');
INSERT INTO `fly_area` VALUES ('3083', '351', '同仁县', '3', '632321', '50');
INSERT INTO `fly_area` VALUES ('3084', '351', '尖扎县', '3', '632322', '50');
INSERT INTO `fly_area` VALUES ('3085', '351', '泽库县', '3', '632323', '50');
INSERT INTO `fly_area` VALUES ('3086', '351', '河南蒙古族自治县', '3', '632324', '50');
INSERT INTO `fly_area` VALUES ('3087', '352', '共和县', '3', '632521', '50');
INSERT INTO `fly_area` VALUES ('3088', '352', '同德县', '3', '632522', '50');
INSERT INTO `fly_area` VALUES ('3089', '352', '贵德县', '3', '632523', '50');
INSERT INTO `fly_area` VALUES ('3090', '352', '兴海县', '3', '632524', '50');
INSERT INTO `fly_area` VALUES ('3091', '352', '贵南县', '3', '632525', '50');
INSERT INTO `fly_area` VALUES ('3092', '353', '玛沁县', '3', '632621', '50');
INSERT INTO `fly_area` VALUES ('3093', '353', '班玛县', '3', '632622', '50');
INSERT INTO `fly_area` VALUES ('3094', '353', '甘德县', '3', '632623', '50');
INSERT INTO `fly_area` VALUES ('3095', '353', '达日县', '3', '632624', '50');
INSERT INTO `fly_area` VALUES ('3096', '353', '久治县', '3', '632625', '50');
INSERT INTO `fly_area` VALUES ('3097', '353', '玛多县', '3', '632626', '50');
INSERT INTO `fly_area` VALUES ('3098', '354', '玉树市', '3', '632701', '50');
INSERT INTO `fly_area` VALUES ('3099', '354', '杂多县', '3', '632722', '50');
INSERT INTO `fly_area` VALUES ('3100', '354', '称多县', '3', '632723', '50');
INSERT INTO `fly_area` VALUES ('3101', '354', '治多县', '3', '632724', '50');
INSERT INTO `fly_area` VALUES ('3102', '354', '囊谦县', '3', '632725', '50');
INSERT INTO `fly_area` VALUES ('3103', '354', '曲麻莱县', '3', '632726', '50');
INSERT INTO `fly_area` VALUES ('3104', '355', '格尔木市', '3', '632801', '50');
INSERT INTO `fly_area` VALUES ('3105', '355', '德令哈市', '3', '632802', '50');
INSERT INTO `fly_area` VALUES ('3106', '355', '乌兰县', '3', '632821', '50');
INSERT INTO `fly_area` VALUES ('3107', '355', '都兰县', '3', '632822', '50');
INSERT INTO `fly_area` VALUES ('3108', '355', '天峻县', '3', '632823', '50');
INSERT INTO `fly_area` VALUES ('3109', '356', '兴庆区', '3', '640104', '50');
INSERT INTO `fly_area` VALUES ('3110', '356', '西夏区', '3', '640105', '50');
INSERT INTO `fly_area` VALUES ('3111', '356', '金凤区', '3', '640106', '50');
INSERT INTO `fly_area` VALUES ('3112', '356', '永宁县', '3', '640121', '50');
INSERT INTO `fly_area` VALUES ('3113', '356', '贺兰县', '3', '640122', '50');
INSERT INTO `fly_area` VALUES ('3114', '356', '灵武市', '3', '640181', '50');
INSERT INTO `fly_area` VALUES ('3115', '357', '大武口区', '3', '640202', '50');
INSERT INTO `fly_area` VALUES ('3116', '357', '惠农区', '3', '640205', '50');
INSERT INTO `fly_area` VALUES ('3117', '357', '平罗县', '3', '640221', '50');
INSERT INTO `fly_area` VALUES ('3118', '358', '利通区', '3', '640302', '50');
INSERT INTO `fly_area` VALUES ('3119', '358', '红寺堡区', '3', '640303', '50');
INSERT INTO `fly_area` VALUES ('3120', '358', '盐池县', '3', '640323', '50');
INSERT INTO `fly_area` VALUES ('3121', '358', '同心县', '3', '640324', '50');
INSERT INTO `fly_area` VALUES ('3122', '358', '青铜峡市', '3', '640381', '50');
INSERT INTO `fly_area` VALUES ('3123', '359', '原州区', '3', '640402', '50');
INSERT INTO `fly_area` VALUES ('3124', '359', '西吉县', '3', '640422', '50');
INSERT INTO `fly_area` VALUES ('3125', '359', '隆德县', '3', '640423', '50');
INSERT INTO `fly_area` VALUES ('3126', '359', '泾源县', '3', '640424', '50');
INSERT INTO `fly_area` VALUES ('3127', '359', '彭阳县', '3', '640425', '50');
INSERT INTO `fly_area` VALUES ('3128', '360', '沙坡头区', '3', '640502', '50');
INSERT INTO `fly_area` VALUES ('3129', '360', '中宁县', '3', '640521', '50');
INSERT INTO `fly_area` VALUES ('3130', '360', '海原县', '3', '640522', '50');
INSERT INTO `fly_area` VALUES ('3131', '361', '天山区', '3', '650102', '50');
INSERT INTO `fly_area` VALUES ('3132', '361', '沙依巴克区', '3', '650103', '50');
INSERT INTO `fly_area` VALUES ('3133', '361', '新市区', '3', '650104', '50');
INSERT INTO `fly_area` VALUES ('3134', '361', '水磨沟区', '3', '650105', '50');
INSERT INTO `fly_area` VALUES ('3135', '361', '头屯河区', '3', '650106', '50');
INSERT INTO `fly_area` VALUES ('3136', '361', '达坂城区', '3', '650107', '50');
INSERT INTO `fly_area` VALUES ('3137', '361', '米东区', '3', '650109', '50');
INSERT INTO `fly_area` VALUES ('3138', '361', '乌鲁木齐县', '3', '650121', '50');
INSERT INTO `fly_area` VALUES ('3139', '362', '独山子区', '3', '650202', '50');
INSERT INTO `fly_area` VALUES ('3140', '362', '克拉玛依区', '3', '650203', '50');
INSERT INTO `fly_area` VALUES ('3141', '362', '白碱滩区', '3', '650204', '50');
INSERT INTO `fly_area` VALUES ('3142', '362', '乌尔禾区', '3', '650205', '50');
INSERT INTO `fly_area` VALUES ('3143', '363', '高昌区', '3', '650402', '50');
INSERT INTO `fly_area` VALUES ('3144', '363', '鄯善县', '3', '650421', '50');
INSERT INTO `fly_area` VALUES ('3145', '363', '托克逊县', '3', '650422', '50');
INSERT INTO `fly_area` VALUES ('3146', '364', '伊州区', '3', '650502', '50');
INSERT INTO `fly_area` VALUES ('3147', '364', '巴里坤哈萨克自治县', '3', '650521', '50');
INSERT INTO `fly_area` VALUES ('3148', '364', '伊吾县', '3', '650522', '50');
INSERT INTO `fly_area` VALUES ('3149', '365', '昌吉市', '3', '652301', '50');
INSERT INTO `fly_area` VALUES ('3150', '365', '阜康市', '3', '652302', '50');
INSERT INTO `fly_area` VALUES ('3151', '365', '呼图壁县', '3', '652323', '50');
INSERT INTO `fly_area` VALUES ('3152', '365', '玛纳斯县', '3', '652324', '50');
INSERT INTO `fly_area` VALUES ('3153', '365', '奇台县', '3', '652325', '50');
INSERT INTO `fly_area` VALUES ('3154', '365', '吉木萨尔县', '3', '652327', '50');
INSERT INTO `fly_area` VALUES ('3155', '365', '木垒哈萨克自治县', '3', '652328', '50');
INSERT INTO `fly_area` VALUES ('3156', '366', '博乐市', '3', '652701', '50');
INSERT INTO `fly_area` VALUES ('3157', '366', '阿拉山口市', '3', '652702', '50');
INSERT INTO `fly_area` VALUES ('3158', '366', '精河县', '3', '652722', '50');
INSERT INTO `fly_area` VALUES ('3159', '366', '温泉县', '3', '652723', '50');
INSERT INTO `fly_area` VALUES ('3160', '367', '库尔勒市', '3', '652801', '50');
INSERT INTO `fly_area` VALUES ('3161', '367', '轮台县', '3', '652822', '50');
INSERT INTO `fly_area` VALUES ('3162', '367', '尉犁县', '3', '652823', '50');
INSERT INTO `fly_area` VALUES ('3163', '367', '若羌县', '3', '652824', '50');
INSERT INTO `fly_area` VALUES ('3164', '367', '且末县', '3', '652825', '50');
INSERT INTO `fly_area` VALUES ('3165', '367', '焉耆回族自治县', '3', '652826', '50');
INSERT INTO `fly_area` VALUES ('3166', '367', '和静县', '3', '652827', '50');
INSERT INTO `fly_area` VALUES ('3167', '367', '和硕县', '3', '652828', '50');
INSERT INTO `fly_area` VALUES ('3168', '367', '博湖县', '3', '652829', '50');
INSERT INTO `fly_area` VALUES ('3169', '368', '阿克苏市', '3', '652901', '50');
INSERT INTO `fly_area` VALUES ('3170', '368', '温宿县', '3', '652922', '50');
INSERT INTO `fly_area` VALUES ('3171', '368', '库车县', '3', '652923', '50');
INSERT INTO `fly_area` VALUES ('3172', '368', '沙雅县', '3', '652924', '50');
INSERT INTO `fly_area` VALUES ('3173', '368', '新和县', '3', '652925', '50');
INSERT INTO `fly_area` VALUES ('3174', '368', '拜城县', '3', '652926', '50');
INSERT INTO `fly_area` VALUES ('3175', '368', '乌什县', '3', '652927', '50');
INSERT INTO `fly_area` VALUES ('3176', '368', '阿瓦提县', '3', '652928', '50');
INSERT INTO `fly_area` VALUES ('3177', '368', '柯坪县', '3', '652929', '50');
INSERT INTO `fly_area` VALUES ('3178', '369', '阿图什市', '3', '653001', '50');
INSERT INTO `fly_area` VALUES ('3179', '369', '阿克陶县', '3', '653022', '50');
INSERT INTO `fly_area` VALUES ('3180', '369', '阿合奇县', '3', '653023', '50');
INSERT INTO `fly_area` VALUES ('3181', '369', '乌恰县', '3', '653024', '50');
INSERT INTO `fly_area` VALUES ('3182', '370', '喀什市', '3', '653101', '50');
INSERT INTO `fly_area` VALUES ('3183', '370', '疏附县', '3', '653121', '50');
INSERT INTO `fly_area` VALUES ('3184', '370', '疏勒县', '3', '653122', '50');
INSERT INTO `fly_area` VALUES ('3185', '370', '英吉沙县', '3', '653123', '50');
INSERT INTO `fly_area` VALUES ('3186', '370', '泽普县', '3', '653124', '50');
INSERT INTO `fly_area` VALUES ('3187', '370', '莎车县', '3', '653125', '50');
INSERT INTO `fly_area` VALUES ('3188', '370', '叶城县', '3', '653126', '50');
INSERT INTO `fly_area` VALUES ('3189', '370', '麦盖提县', '3', '653127', '50');
INSERT INTO `fly_area` VALUES ('3190', '370', '岳普湖县', '3', '653128', '50');
INSERT INTO `fly_area` VALUES ('3191', '370', '伽师县', '3', '653129', '50');
INSERT INTO `fly_area` VALUES ('3192', '370', '巴楚县', '3', '653130', '50');
INSERT INTO `fly_area` VALUES ('3193', '370', '塔什库尔干塔吉克自治县', '3', '653131', '50');
INSERT INTO `fly_area` VALUES ('3194', '371', '和田市', '3', '653201', '50');
INSERT INTO `fly_area` VALUES ('3195', '371', '和田县', '3', '653221', '50');
INSERT INTO `fly_area` VALUES ('3196', '371', '墨玉县', '3', '653222', '50');
INSERT INTO `fly_area` VALUES ('3197', '371', '皮山县', '3', '653223', '50');
INSERT INTO `fly_area` VALUES ('3198', '371', '洛浦县', '3', '653224', '50');
INSERT INTO `fly_area` VALUES ('3199', '371', '策勒县', '3', '653225', '50');
INSERT INTO `fly_area` VALUES ('3200', '371', '于田县', '3', '653226', '50');
INSERT INTO `fly_area` VALUES ('3201', '371', '民丰县', '3', '653227', '50');
INSERT INTO `fly_area` VALUES ('3202', '372', '伊宁市', '3', '654002', '50');
INSERT INTO `fly_area` VALUES ('3203', '372', '奎屯市', '3', '654003', '50');
INSERT INTO `fly_area` VALUES ('3204', '372', '霍尔果斯市', '3', '654004', '50');
INSERT INTO `fly_area` VALUES ('3205', '372', '伊宁县', '3', '654021', '50');
INSERT INTO `fly_area` VALUES ('3206', '372', '察布查尔锡伯自治县', '3', '654022', '50');
INSERT INTO `fly_area` VALUES ('3207', '372', '霍城县', '3', '654023', '50');
INSERT INTO `fly_area` VALUES ('3208', '372', '巩留县', '3', '654024', '50');
INSERT INTO `fly_area` VALUES ('3209', '372', '新源县', '3', '654025', '50');
INSERT INTO `fly_area` VALUES ('3210', '372', '昭苏县', '3', '654026', '50');
INSERT INTO `fly_area` VALUES ('3211', '372', '特克斯县', '3', '654027', '50');
INSERT INTO `fly_area` VALUES ('3212', '372', '尼勒克县', '3', '654028', '50');
INSERT INTO `fly_area` VALUES ('3213', '373', '塔城市', '3', '654201', '50');
INSERT INTO `fly_area` VALUES ('3214', '373', '乌苏市', '3', '654202', '50');
INSERT INTO `fly_area` VALUES ('3215', '373', '额敏县', '3', '654221', '50');
INSERT INTO `fly_area` VALUES ('3216', '373', '沙湾县', '3', '654223', '50');
INSERT INTO `fly_area` VALUES ('3217', '373', '托里县', '3', '654224', '50');
INSERT INTO `fly_area` VALUES ('3218', '373', '裕民县', '3', '654225', '50');
INSERT INTO `fly_area` VALUES ('3219', '373', '和布克赛尔蒙古自治县', '3', '654226', '50');
INSERT INTO `fly_area` VALUES ('3220', '374', '阿勒泰市', '3', '654301', '50');
INSERT INTO `fly_area` VALUES ('3221', '374', '布尔津县', '3', '654321', '50');
INSERT INTO `fly_area` VALUES ('3222', '374', '富蕴县', '3', '654322', '50');
INSERT INTO `fly_area` VALUES ('3223', '374', '福海县', '3', '654323', '50');
INSERT INTO `fly_area` VALUES ('3224', '374', '哈巴河县', '3', '654324', '50');
INSERT INTO `fly_area` VALUES ('3225', '374', '青河县', '3', '654325', '50');
INSERT INTO `fly_area` VALUES ('3226', '374', '吉木乃县', '3', '654326', '50');
INSERT INTO `fly_area` VALUES ('3227', '375', '石河子市', '3', '659001', '50');
INSERT INTO `fly_area` VALUES ('3228', '375', '阿拉尔市', '3', '659002', '50');
INSERT INTO `fly_area` VALUES ('3229', '375', '图木舒克市', '3', '659003', '50');
INSERT INTO `fly_area` VALUES ('3230', '375', '五家渠市', '3', '659004', '50');
INSERT INTO `fly_area` VALUES ('3231', '375', '铁门关市', '3', '659006', '50');

-- ----------------------------
-- Table structure for `fly_company`
-- ----------------------------
DROP TABLE IF EXISTS `fly_company`;
CREATE TABLE `fly_company` (
  `id` bigint(20) NOT NULL,
  `short_name` varchar(20) DEFAULT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  `company_type` varchar(100) DEFAULT NULL COMMENT '公司类型',
  `industry` bigint(20) DEFAULT '0' COMMENT '公司所属行业',
  `mode` varchar(100) DEFAULT NULL COMMENT '经营模式',
  `found_time` datetime DEFAULT NULL COMMENT '成立时间',
  `capital` double(11,2) DEFAULT '0.00' COMMENT '注册资本',
  `regunit` varchar(15) DEFAULT NULL COMMENT '注册资本货币单位',
  `business` varchar(255) DEFAULT NULL COMMENT '经营范围',
  `scale` bigint(20) DEFAULT '0' COMMENT '公司规模',
  `sell` varchar(255) DEFAULT NULL COMMENT '销售的产品(提供的服务)',
  `buy` varchar(255) DEFAULT NULL COMMENT '采购的产品(需要的服务）',
  `introduce` text COMMENT '企业介绍',
  `contact_id` bigint(20) DEFAULT '0' COMMENT '公司地址id',
  `sort_order` int(11) DEFAULT '0' COMMENT '排序',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `verified` tinyint(1) DEFAULT '0' COMMENT '认证状态，0未认证，1通过认证',
  `recommend` int(5) DEFAULT '0' COMMENT '推荐设置',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of fly_company
-- ----------------------------
INSERT INTO `fly_company` VALUES ('389481100151631908', '新纶科技', '深圳市新纶科技股份有限公司', null, '0', null, null, '0.00', null, null, '0', null, null, null, '0', '0', '2019-11-06 15:44:18', '2019-11-06 15:44:21', '0', '0', '0');
INSERT INTO `fly_company` VALUES ('401335316046151680', '公司简称', '公司名称', null, '0', null, null, '0.00', null, null, '0', null, null, '丰东股份的鬼地方<span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">公司简称</span>', '0', '0', '2019-12-09 08:44:21', '2019-12-09 10:20:43', '0', '0', '0');

-- ----------------------------
-- Table structure for `fly_company_user`
-- ----------------------------
DROP TABLE IF EXISTS `fly_company_user`;
CREATE TABLE `fly_company_user` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `company_id` bigint(20) NOT NULL COMMENT '公司id',
  `user_type` int(2) DEFAULT '0' COMMENT '类型(0:会员,1:管理员)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of fly_company_user
-- ----------------------------
INSERT INTO `fly_company_user` VALUES ('363663265068220416', '366638723963551744', '389481100151631908', '0');
INSERT INTO `fly_company_user` VALUES ('401335316134232064', '378556618222075904', '401335316046151680', '1');

-- ----------------------------
-- Table structure for `fly_configure`
-- ----------------------------
DROP TABLE IF EXISTS `fly_configure`;
CREATE TABLE `fly_configure` (
  `id` bigint(20) NOT NULL,
  `key_code` varchar(200) NOT NULL COMMENT '参数名,程序内调用',
  `description` char(200) DEFAULT NULL COMMENT '说明描述',
  `key_value` varchar(2000) DEFAULT NULL COMMENT '值',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`,`key_code`),
  KEY `system_index` (`id`,`key_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='系统变量，系统的一些参数相关，比如系统名字等';

-- ----------------------------
-- Records of fly_configure
-- ----------------------------
INSERT INTO `fly_configure` VALUES ('396262328448069632', 'user_site_verify', '用户添加网站时，网站审核状态，0未审核，1已审核，2未通过', '1', null, '2019-08-17 20:32:10');
INSERT INTO `fly_configure` VALUES ('396262328448069633', 'global_site_name', '网站名称', '网·市场', null, '2019-08-17 20:32:13');
INSERT INTO `fly_configure` VALUES ('396262328448069634', 'global_site_keywords', '网站SEO搜索的关键字，首页根内页没有设置keywords的都默认用此', 'IW', null, '2019-08-17 20:32:16');
INSERT INTO `fly_configure` VALUES ('396262328448069635', 'global_site_description', '网站SEO描述，首页根内页没有设置description的都默认用此', '孙先生', null, '2019-08-17 20:32:19');
INSERT INTO `fly_configure` VALUES ('396262328448069636', 'user_site_number', '用户试用网站可建立数量', '1', null, '2019-08-17 20:32:21');
INSERT INTO `fly_configure` VALUES ('396262328448069641', 'role_user_id', '普通用户的角色id，其值对应角色 role.id', '366301311295303684', null, '2019-08-17 20:32:36');
INSERT INTO `fly_configure` VALUES ('396262328448069642', 'ROLE_SUPERADMIN_ID', '超级管理员的角色id，其值对应角色 role.id', '9', null, '2019-08-17 20:32:39');
INSERT INTO `fly_configure` VALUES ('396262328448069643', 'BBS_DEFAULT_PUBLISH_CLASSID', '论坛中，如果帖子发布时，没有指明要发布到哪个论坛板块，那么默认选中哪个板块(分类)，这里便是分类的id，即数据表中的 post_class.id', '3', null, '2019-08-17 20:32:44');
INSERT INTO `fly_configure` VALUES ('396262328448069644', 'USER_HEAD_PATH', '用户头像(User.head)上传OSS或服务器进行存储的路径，存储于哪个文件夹中。<br/><b>注意</b><br/>1.这里最前面不要加/，最后要带/，如 head/<br/>2.使用中时，中途最好别改动，不然改动之前的用户设置好的头像就都没了', 'head/', null, '2019-08-17 20:32:46');
INSERT INTO `fly_configure` VALUES ('396262328448069645', 'ALLOW_USER_REG', '是否允许用户自行注册。<br/>1：允许用户自行注册<br/>0：禁止用户自行注册', '1', null, '2019-08-17 20:32:49');
INSERT INTO `fly_configure` VALUES ('396262328448069646', 'LIST_EVERYPAGE_NUMBER', '所有列表页面，每页显示的列表条数。', '15', null, '2019-08-17 20:32:53');
INSERT INTO `fly_configure` VALUES ('396262328448069647', 'SERVICE_MAIL', '网站管理员的邮箱。<br/>当网站出现什么问题，或者什么提醒时，会自动向管理员邮箱发送提示信息', '79678111@qq.com', null, '2019-08-17 20:32:55');
INSERT INTO `fly_configure` VALUES ('396262328448069648', 'AGENCY_ROLE', '代理商得角色id', '10', null, '2019-08-17 20:32:58');
INSERT INTO `fly_configure` VALUES ('396262328448069651', 'user_site_probation', '用户添加网站时试用时间，day一天，week一周，month一个月，year一年', 'week', null, '2019-08-17 20:33:59');
INSERT INTO `fly_configure` VALUES ('396262328448069653', 'ALIYUN_LOG_SITESIZECHANGE', '站币变动的日志记录。此项无需改动', 'sitemoneychange', null, '2019-08-17 20:33:50');
INSERT INTO `fly_configure` VALUES ('396262328448069654', 'AUTO_ASSIGN_DOMAIN', '网站生成后，会自动分配给网站一个二级域名。这里便是泛解析的主域名。<br/>如果分配有多个二级域名，则用,分割。并且第一个是作为主域名会显示给用户看到。后面的其他的域名用户不会看到，只可以使用访问网站。', '97560.com', null, '2019-08-17 20:33:48');
INSERT INTO `fly_configure` VALUES ('396262328448069656', 'ATTACHMENT_FILE_URL', '设置当前建站系统中，上传的图片、附件的访问域名。若后续想要将附件转到云上存储、或开通CDN加速，可平滑上云使用。', 'http://127.0.0.1/', null, '2019-08-17 20:33:41');
INSERT INTO `fly_configure` VALUES ('396262328448069657', 'ATTACHMENT_FILE_MODE', '当前文件附件存储使用的模式，用的阿里云oss，还是服务器本身磁盘进行存储。<br/>可选一：aliyunOSS：阿里云OSS模式存储<br/>可选二：localFile：服务器本身磁盘进行附件存储', 'localFile', null, '2019-08-17 20:33:30');
INSERT INTO `fly_configure` VALUES ('396262328448069658', 'SITEUSER_FIRST_USE_EXPLAIN_URL', '网站建站用户第一天登陆网站管理后台时，在欢迎页面会自动通过iframe引入的入门使用说明的视频，这里便是播放的视频的页面网址', '//video.leimingyun.com/sitehelp/sitehelp.html', null, '2019-08-17 20:33:33');
INSERT INTO `fly_configure` VALUES ('396262328448069659', 'AGENCYUSER_FIRST_USE_EXPLAIN_URL', '代理用户前15天登陆代理后台时，会自动弹出使用教程的提示。这里便是教程的链接地址', '//www.97560.com/jianzhanDemo.html', null, '2019-08-17 20:33:17');
INSERT INTO `fly_configure` VALUES ('396262328448069660', 'SITE_TEMPLATE_DEVELOP_URL', '模版开发说明，模版开发入门', '//tag.97560.com/4192.html', null, '2019-08-17 20:33:21');
INSERT INTO `fly_configure` VALUES ('396262328448069662', 'global_templets_skin', '模板路径', 'http://abc.com', null, '2019-08-17 20:33:08');
INSERT INTO `fly_configure` VALUES ('396262328448069664', 'fly_mail_smtpServer', '邮件服务器', null, null, '2019-09-02 01:31:48');
INSERT INTO `fly_configure` VALUES ('396262328448069665', 'fly_mail_smtpUsermail', '发件人的账号', null, null, '2019-09-02 01:31:52');
INSERT INTO `fly_configure` VALUES ('396262328448069666', 'fly_mail_smtpPassword', '访问SMTP服务时需要提供的密码', null, null, '2019-09-02 12:47:09');
INSERT INTO `fly_configure` VALUES ('396262328448069667', 'fly_mail_smtpPort', '邮件服务器smtp发送端口', null, null, '2019-09-02 12:47:12');
INSERT INTO `fly_configure` VALUES ('396262328448069668', 'home_sms_user_reg', '用户注册短信模板', '317765676100235265', null, '2019-09-02 12:47:15');
INSERT INTO `fly_configure` VALUES ('396262328448069669', 'hold_domain', '保留二级域名', 'www,bbs,web,888,weixiaobao,wxb,admin', null, '2019-11-16 01:03:57');
INSERT INTO `fly_configure` VALUES ('396262328448069670', 'default_site', '默认官网id', '363539057487118336', null, '2019-11-20 16:13:40');
INSERT INTO `fly_configure` VALUES ('396262328448069671', 'system_prefix', '系统数据库标签等前缀', 'fly_', null, null);

-- ----------------------------
-- Table structure for `fly_email_templet`
-- ----------------------------
DROP TABLE IF EXISTS `fly_email_templet`;
CREATE TABLE `fly_email_templet` (
  `id` bigint(20) unsigned NOT NULL,
  `tp_code` varchar(100) NOT NULL COMMENT '模板指定查询代码',
  `title` varchar(100) DEFAULT NULL COMMENT '邮件标题',
  `content` text COMMENT '邮件内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统邮件模板设置';

-- ----------------------------
-- Records of fly_email_templet
-- ----------------------------
INSERT INTO `fly_email_templet` VALUES ('1', 'reg_email', '免费获取开源之家注册邮件验证码', '<table width=\"100%\" style=\"background:#ffffff;\" bgcolor=\"\" cellspacing=\"0\" cellpadding=\"0\"><tbody><tr><td style=\"background:#ffffff;width:137px;\"><strong style=\"font-size:14px;line-height:24px;color:#333333;font-family:arial,sans-serif;\">开源之家</strong></td></tr><tr><td><table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"> <tbody><tr><td style=\"background:#ffffff;border-bottom:2px solid #dfdfdf;width:15px;\"></tr></tbody></table><tr><td><table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"> <tbody><tr><td width=\"25\" style=\"width:25px;\"> <td align=\"\"><div style=\"line-height:40px;height:40px;\"> </div><p style=\"margin:0px;padding:0px;\"><strong style=\"font-size:14px;line-height:24px;color:#333333;font-family:arial,sans-serif;\">亲爱的开源之家用户：</strong></p><p style=\"margin:0px;padding:0px;line-height:24px;font-size:12px;color:#333333;font-family:\'宋体\',arial,sans-serif;\">您好！</p><p style=\"margin:0px;padding:0px;line-height:24px;font-size:12px;color:#333333;font-family:\'宋体\',arial,sans-serif;\">\n            您正在注册开源之家帐号（${userEmail}）获取验证码，请复制下面的注册码完成后续注册步骤。</p>\n            <p style=\"margin:0px;padding:0px;\">验证码：<span style=\"margin:0px;padding:0px;line-height:24px;font-size:18px;color:#333333;font-family:\'宋体\',arial,sans-serif;\"><strong>${code}</strong></span></p><p style=\"margin:0px;padding:0px;line-height:24px;font-size:12px;color:#333333;font-family:\'宋体\',arial,sans-serif;\">该验证码有效期为5分钟，账户完成注册后将立即失效。</p><div style=\"line-height:80px;height:80px;\"> </div><p style=\"margin:0px;padding:0px;line-height:24px;font-size:12px;color:#333333;font-family:\'宋体\',arial,sans-serif;\">开源之家团队</p><p style=\"margin:0px;padding:0px;line-height:24px;font-size:12px;color:#333333;font-family:\'宋体\',arial,sans-serif;\">${createTime}</p></tr></tbody></table><tr><td><table width=\"100%\" style=\"border-top:1px solid #dfdfdf\" cellspacing=\"0\" cellpadding=\"0\"><tbody><tr><td width=\"25\" style=\"width:25px;\"> <td align=\"\"><div style=\"line-height:40px;height:40px;\"> </div><p style=\"margin:0px;padding:0px;line-height:24px;font-size:12px;color:#979797;font-family:\'宋体\',arial,sans-serif;\">绑定手机、绑定邮箱、设置密码保护问题将更好地保障您的帐号安全，建议您立即绑定。</p><p style=\"margin:0px;padding:0px;line-height:24px;font-size:12px;color:#979797;font-family:\'宋体\',arial,sans-serif;\">如果您没有进行过人工申诉，则可能是其他用户的误操作，建议您<a style=\"color:#0000cc\" href=\"http://www.28844.com/login\">立即登录</a>，进入开源之家安全中心（帐号设置）->密保工具，绑定手机或邮箱。感谢您对开源之家的支持！</p></tr></tbody></table></tr></tbody></table>');
INSERT INTO `fly_email_templet` VALUES ('2', 'reset_email', '开源之家账号重置密码', '<table width=\"100%\" style=\"background:#FFFFFF;\" bgcolor=\"\" cellspacing=\"0\" cellpadding=\"0\">\n	<tbody>\n		<tr>\n			<td style=\"background:#FFFFFF;width:137px;\">\n				<strong style=\"color:#333333;line-height:24px;font-family:arial,sans-serif;font-size:14px;\">开源之家</strong> \n			</td>\n		</tr>\n		<tr>\n			<td>\n				<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n					<tbody>\n						<tr>\n							<td style=\"background:#FFFFFF;width:15px;border-bottom-color:#DFDFDF;border-bottom-width:2px;border-bottom-style:solid;\">\n									</tr>\n										</tbody>\n											</table>\n											<tr>\n												<td>\n													<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n														<tbody>\n															<tr>\n																<td width=\"25\" style=\"width:25px;\">\n																	<td align=\"\">\n																		<div style=\"height:40px;line-height:40px;\">\n																		</div>\n																		<p style=\"margin:0px;padding:0px;\">\n																			<strong style=\"color:#333333;line-height:24px;font-family:arial,sans-serif;font-size:14px;\">亲爱的开源之家用户：</strong> \n																		</p>\n																		<p style=\"margin:0px;padding:0px;color:#333333;line-height:24px;font-family:\"font-size:12px;\">\n																			您好！\n																		</p>\n																		<p style=\"margin:0px;padding:0px;color:#333333;line-height:24px;font-family:\"font-size:12px;\">\n																			您正在重置开源之家密码帐号（${userEmail}）获取验证码，请复制下面的注册码完成后续注册步骤。\n																		</p>\n																		<p style=\"margin:0px;padding:0px;\">\n																			验证码：<span style=\"margin:0px;padding:0px;color:#333333;line-height:24px;font-family:\"font-size:18px;\"><strong>${code}</strong></span> \n																		</p>\n																		<p style=\"margin:0px;padding:0px;color:#333333;line-height:24px;font-family:\"font-size:12px;\">\n																			该验证码有效期为5分钟，账户完成注册后将立即失效。\n																		</p>\n																		<div style=\"height:80px;line-height:80px;\">\n																		</div>\n																		<p style=\"margin:0px;padding:0px;color:#333333;line-height:24px;font-family:\"font-size:12px;\">\n																			开源之家团队\n																		</p>\n																		<p style=\"margin:0px;padding:0px;color:#333333;line-height:24px;font-family:\"font-size:12px;\">\n																			${createTime}\n																		</p>\n																			</tr>\n																				</tbody>\n																					</table>\n																					<tr>\n																						<td>\n																							<table width=\"100%\" style=\"border-top-color:#DFDFDF;border-top-width:1px;border-top-style:solid;\" cellspacing=\"0\" cellpadding=\"0\">\n																								<tbody>\n																									<tr>\n																										<td width=\"25\" style=\"width:25px;\">\n																											<td align=\"\">\n																												<div style=\"height:40px;line-height:40px;\">\n																												</div>\n																												<p style=\"margin:0px;padding:0px;color:#979797;line-height:24px;font-family:\"font-size:12px;\">\n																													绑定手机、绑定邮箱、设置密码保护问题将更好地保障您的帐号安全，建议您立即绑定。\n																												</p>\n																												<p style=\"margin:0px;padding:0px;color:#979797;line-height:24px;font-family:\"font-size:12px;\">\n																													如果您没有进行过人工申诉，则可能是其他用户的误操作，建议您<a style=\"color:#0000CC;\" href=\"http://www.28844.com/login\">立即登录</a>，进入开源之家安全中心（帐号设置）->密保工具，绑定手机或邮箱。感谢您对开源之家的支持！\n																												</p>\n																													</tr>\n																														</tbody>\n																															</table>\n																																</tr>\n																																	</tbody>\n																																		</table>');
INSERT INTO `fly_email_templet` VALUES ('3', 'safe_email', '开源之家绑定安全邮箱验证码', '<table width=\"100%\" style=\"background:#ffffff;\" bgcolor=\"\" cellspacing=\"0\" cellpadding=\"0\">\n	<tbody>\n		<tr>\n			<td style=\"background:#ffffff;width:137px;\">\n				<strong style=\"font-size:14px;line-height:24px;color:#333333;font-family:arial,sans-serif;\">开源之家</strong> \n			</td>\n		</tr>\n		<tr>\n			<td>\n				<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n					<tbody>\n						<tr>\n							<td style=\"background:#ffffff;border-bottom:2px solid #dfdfdf;width:15px;\">\n							</td>\n						</tr>\n					</tbody>\n				</table>\n			</td>\n		</tr>\n		<tr>\n			<td>\n				<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n					<tbody>\n						<tr>\n							<td width=\"25\" style=\"width:25px;\">\n							</td>\n							<td align=\"\">\n								<div style=\"line-height:40px;height:40px;\">\n								</div>\n								<p style=\"margin:0px;padding:0px;\">\n									<strong style=\"font-size:14px;line-height:24px;color:#333333;font-family:arial,sans-serif;\">亲爱的开源之家用户：</strong> \n								</p>\n								<p style=\"margin:0px;padding:0px;line-height:24px;font-size:12px;color:#333333;font-family:\'宋体\',arial,sans-serif;\">\n									您好！\n								</p>\n								<p style=\"margin:0px;padding:0px;line-height:24px;font-size:12px;color:#333333;font-family:\'宋体\',arial,sans-serif;\">\n									您正在绑定开源之家安全邮箱（${userEmail}）获取验证码，请复制下面的验证码完成后续绑定步骤。\n								</p>\n								<p style=\"margin:0px;padding:0px;\">\n									验证码：<span style=\"margin:0px;padding:0px;line-height:24px;font-size:18px;color:#333333;font-family:\'宋体\',arial,sans-serif;\"><strong>${code}</strong></span> \n								</p>\n								<p style=\"margin:0px;padding:0px;line-height:24px;font-size:12px;color:#333333;font-family:\'宋体\',arial,sans-serif;\">\n									该验证码有效期为5分钟，账户完成注册后将立即失效。\n								</p>\n								<div style=\"line-height:80px;height:80px;\">\n								</div>\n								<p style=\"margin:0px;padding:0px;line-height:24px;font-size:12px;color:#333333;font-family:\'宋体\',arial,sans-serif;\">\n									开源之家团队\n								</p>\n								<p style=\"margin:0px;padding:0px;line-height:24px;font-size:12px;color:#333333;font-family:\'宋体\',arial,sans-serif;\">\n									${createTime}\n								</p>\n							</td>\n						</tr>\n					</tbody>\n				</table>\n			</td>\n		</tr>\n		<tr>\n			<td>\n				<table width=\"100%\" style=\"border-top:1px solid #dfdfdf;\" cellspacing=\"0\" cellpadding=\"0\">\n					<tbody>\n						<tr>\n							<td width=\"25\" style=\"width:25px;\">\n							</td>\n							<td align=\"\">\n								<div style=\"line-height:40px;height:40px;\">\n								</div>\n								<p style=\"margin:0px;padding:0px;line-height:24px;font-size:12px;color:#979797;font-family:\'宋体\',arial,sans-serif;\">\n									绑定手机、绑定邮箱、设置密码保护问题将更好地保障您的帐号安全，建议您立即绑定。\n								</p>\n								<p style=\"margin:0px;padding:0px;line-height:24px;font-size:12px;color:#979797;font-family:\'宋体\',arial,sans-serif;\">\n									如果您没有进行过人工申诉，则可能是其他用户的误操作，建议您<a style=\"color:#0000cc;\" href=\"http://www.28844.com/login\">立即登录</a>，进入开源之家安全中心（帐号设置）-&gt;密保工具，绑定手机或邮箱。感谢您对开源之家的支持！\n								</p>\n							</td>\n						</tr>\n					</tbody>\n				</table>\n			</td>\n		</tr>\n	</tbody>\n</table>');
INSERT INTO `fly_email_templet` VALUES ('4', 'tese_email', '邮件测试标题', '邮件测试内容');

-- ----------------------------
-- Table structure for `fly_feedback`
-- ----------------------------
DROP TABLE IF EXISTS `fly_feedback`;
CREATE TABLE `fly_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `addtime` int(11) DEFAULT '0' COMMENT '添加时间',
  `userid` int(11) DEFAULT NULL COMMENT '哪个用户提出的',
  `text` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '问题反馈的反馈内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='问题反馈（暂未用到，已废弃）';

-- ----------------------------
-- Records of fly_feedback
-- ----------------------------

-- ----------------------------
-- Table structure for `fly_help`
-- ----------------------------
DROP TABLE IF EXISTS `fly_help`;
CREATE TABLE `fly_help` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` bigint(20) unsigned DEFAULT NULL COMMENT '帮助分类，如果为0则代表着是下面的帮助单页',
  `site_id` bigint(20) DEFAULT NULL COMMENT '所属站点，对应site.id',
  `title` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `sort_order` smallint(5) NOT NULL DEFAULT '99' COMMENT '顺序',
  `create_time` datetime DEFAULT NULL COMMENT '发布时间',
  `deleted` tinyint(1) DEFAULT '1' COMMENT '逻辑删除，1显示，0删除',
  PRIMARY KEY (`id`),
  KEY `cat_id` (`cat_id`),
  KEY `sort` (`sort_order`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COMMENT='帮助内容';

-- ----------------------------
-- Records of fly_help
-- ----------------------------
INSERT INTO `fly_help` VALUES ('4', '3', null, '购物流程', '<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、搜索商品</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">为您提供了方便快捷的商品搜索功能：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）您可以通过在首页输入关键字的方法来搜索您想要购买的商品</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）您还可以通过</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">的分类导航栏来找到您想要购买的商品分类，根据分类找到您的商品</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）观看搜索商品演示</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">&nbsp;</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、放入购物车在您想要购买的商品的详情页点击“购买”，商品会添加到您的购物车中；您还可以继续挑选商品放入购物车，一起结算。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）在购物车中，系统默认每件商品的订购数量为一件，如果您想购买多件商品，可修改购买数量</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）在购物车中，您可以将商品移至收藏，或是选择删除</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）在购物车中，您可以直接查看到商品的优惠折和参加促销活动的商品名称、促销主题</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">4</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）购物车页面下方的商品是</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">根据您挑选的商品为您作出的推荐，若有您喜爱的商品，点击“放入购物车”即可</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">温馨提示：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）商品价格会不定期调整，最终价格以您提交订单后订单中的价格为准</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）优惠政策、配送时间、运费收取标准等都有可能进行调整，最终成交信息以您提交订单时网站公布的最新信息为准</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">&nbsp;</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、选择订单</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">和商家的商品需要分别提交订单订购</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）不同商家的商品需要分别提交订单订购</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">&nbsp;</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">4</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、注册登陆</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）老顾客：请在“登陆”页面输入</span><span lang=\\\"EN-US\\\">Email</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">地址或昵称、注册密码进行登陆</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）新顾客：请在“新用户注册”页面按照提示完成注册</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">&nbsp;</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">5</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、填写收货人信息</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）请填写正确完整的收货人姓名、收货人联系方式、详细的收货地址和邮编，否则将会影响您订单的处理或配送</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）您可以进入“我的</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">—帐户管理—收货地址簿”编辑常用收货地址，保存成功后，再订购时，可以直接选择使用</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">&nbsp;</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">6</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、选择收货方式</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">提供多种收货方式：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）普通快递送货上门</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）加急快递送货上门</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）普通邮递</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">4</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）邮政特快专递</span><span lang=\\\"EN-US\\\">(EMS)</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">详情请点击查看配送范围、时间及运费</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">&nbsp;</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">7</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、选择支付方式</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">提供多种支付方式，订购过程中您可以选择：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）货到付款</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）网上支付</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）银行转帐</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">4</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）邮局汇款</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">点击查看各种支付方式订单的支付期限</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">&nbsp;</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">8</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、索取发票</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">请点击“索取发票”，填写正确的发票抬头、选择正确的发票内容，发票选择成功后，将于订单货物一起送达</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">点击查看发票制度</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">&nbsp;</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">9</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、提交订单</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）以上信息核实无误后，请点击“提交订单”，系统生成一个订单号，就说明您已经成功提交订单</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）订单提交成功后，您可以登陆“我的</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">”查看订单信息或为订单进行网上支付</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">特别提示</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、若您帐户中有礼品卡，可以在“支付方式”处选择使用礼品卡支付，详情请点击查看</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">礼品卡</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、若您帐户中有符合支付该订单的礼券，在结算页面会有“使用礼券”按钮，您点击选择礼券即可，点击查看礼券使用规则</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">当您选择了礼券并点击“确定使用”后，便无法再取消使用该礼券</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、在订单提交高峰时段，新订单可能一段时间之后才会在“我的</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">”中显示。如果您在“我的</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;\\\">”中暂未找到这张订单，请您耐心等待</span></p>\r\n<p class=\\\"MsoNormal\\\"><br />\r\n</p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;\\\"><br />\r\n</span></p>', '0', '2019-08-17 21:39:20', '1');
INSERT INTO `fly_help` VALUES ('26', '3', null, '会员制度', '<div>会员级别共分七级，具体为：注册会员、铁牌会员、铜牌会员、银牌会员、金牌会员、钻石会员、双钻石会员，级别升降均由系统自动实现，无需申请。</div>\r\n<div><br />\r\n</div>\r\n<div>注册会员：</div>\r\n<div>申请条件：任何愿意到iWebShop购物的用户都可以免费注册。</div>\r\n<div>待　　遇：可以享受注册会员所能购买的产品及服务。</div>\r\n<div>铁牌会员：</div>\r\n<div>申请条件：一年内有过成功消费的会员，金额不限。</div>\r\n<div>待　　遇：可以享受铁牌会员级别所能购买的产品及服务。</div>\r\n<div>铜牌会员：</div>\r\n<div>申请条件：一年内消费金额超过2000元（含）的会员。</div>\r\n<div>待　　遇：可以享受铜牌会员级别所能购买的产品及服务。</div>\r\n<div>其它要求：</div>\r\n<div>身份有效期为一年，一年有效期满后，如在该年度内累计消费金额不满1000元或一年内未完成10个（含）以上不同日期的订单，则系统自动将身份降为铁牌会员。</div>\r\n<div>银牌会员：</div>\r\n<div>申请条件：一年内消费金额超过5000元（含），需填写本人真实的身份证号码进行升级</div>\r\n<div>待　　遇：可以享受银牌会员级别所能购买的产品及服务。</div>\r\n<div>其它要求：</div>\r\n<div>身份有效期为一年，一年有效期满后，如在该年度内累计消费金额在1000元（含）——2500元或一年内未完成10个（含）以上不同日期的订单，则系统自动将身份降为铜牌会员；如消费金额不满1000元或一年内未完成10个（含）以上不同日期的订单，则系统自动将身份降为铁牌会员。</div>\r\n<div>金牌会员：</div>\r\n<div>申请条件： 一年内累计消费金额超过10000 元（含）。</div>\r\n<div>待　　遇：</div>\r\n<div>享有优先购物权 —— 对国内少见的优秀产品或者其它比较紧俏的产品具有优先购买权。</div>\r\n<div>享受运费优惠政策（详见这里）</div>\r\n<div>享有一年两次的特别针对金牌会员抽奖的权利</div>\r\n<div>不定期举办个别产品针对金牌会员的优惠活动。</div>\r\n<div>享有支付66元DIY装机服务费的权利。</div>\r\n<div>其它相关要求：</div>\r\n<div>身份有效期为一年，一年有效期满后，如在该年度内累计消费金额在2500元（含）——5000元或一年内未完成10个（含）以上不同日期的订单，则系统自动将身份降为银牌会员；如消费金额在1000元（含）——2500元或一年内未完成10个（含）以上不同日期的订单，则系统自动将身份降为铜牌会员；如消费金额不满1000元或一年内未完成10个（含）以上不同日期的订单，则系统自动将身份降为铁牌会员。　</div>\r\n<div>钻石会员：</div>\r\n<div>申请条件：一年内累计消费金额达到 30000 元（含）</div>\r\n<div>享受金牌会员全部待遇。</div>\r\n<div>享受运费优惠政策（详见这里）</div>\r\n<div>享有支付30元DIY装机服务费的权利。</div>\r\n<div>享受一定范围内免返修品快递运费的服务。（详情请查看售后返修品运费规定）</div>\r\n<div>其它要求：</div>\r\n<div>身份有效期为一年，一年有效期满后，如在该年度内累计消费金额在5000元（含）——15000元或一年内未完成10个（含）以上不同日期的订单，则系统自动将身份降为金牌会员；如消费金额在2500元（含）——5000元或一年内未完成10个（含）以上不同日期的订单，则系统自动将身份降为银牌会员；如消费金额在1000元（含）——2500元或一年内未完成10个（含）以上不同日期的订单，则系统自动将身份降为铜牌会员；如消费金额不满1000元或一年内未完成10个（含）以上不同日期的订单，则系统自动将身份降为铁牌会员。&nbsp;</div>\r\n<div>双钻石会员：</div>\r\n<div>申请条件：个人用户，年消费金额在10万元（含）以上。</div>\r\n<div>待　　遇：</div>\r\n<div>钻石会员的全部待遇都可以享受。</div>\r\n<div>享有iWebShop网站高管定期提供的沟通服务。</div>\r\n<div>享有不需审核，只需报名，即可参加iWebShop网站举办的网友见面会等网友活动。</div>\r\n<div>享有客服专员定期回访征询意见服务。</div>\r\n<div>其它要求：</div>\r\n<div>身份有效期为一年，一年有效期满后，如在该年度内累计消费金额在15000元（含）——50000元或一年内未完成10个（含）以上不同日期的订单，则系统自动将身份降为钻石会员；如消费金额在5000元（含）——15000元或一年内未完成10个（含）以上不同日期的订单，则系统自动将身份降为金牌会员；如消费金额在2500元（含）——5000元或一年内未完成10个（含）以上不同日期的订单，则系统自动将身份降为银牌会员；如消费金额在1000元（含）——2500元或一年内未完成10个（含）以上不同日期的订单，则系统自动将身份降为铜牌会员；如消费金额不满1000元或一年内未完成10个（含）以上不同日期的订单，则系统自动将身份降为铁牌会员。&nbsp;</div>\r\n<div><br />\r\n</div>\r\n<div>注：针对各个级别会员特别声明：</div>\r\n<div>会员账号禁止转借或转让他人使用，如因转借或转让他人使用所带来的一切后果，iWebShop网站概不负责，如被iWebShop网站发现有转借或转让使用情况，iWebShop网站则有权立即取消此会员账号的相应级别资格。</div>\r\n<div>如iWebShop网站发现相应的级别中有经销商，则iWebShop网站有权立即取消此会员帐号的相应级别资格。</div>\r\n<div><br />\r\n</div>', '0', '2019-08-17 21:39:23', '1');
INSERT INTO `fly_help` VALUES ('27', '3', null, '积分说明', '<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">所有会员在</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">购物均可获得积分，积分可以用来参与兑换活动。</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">会不定期推出各类积分兑换活动，请随时关注关于积分的活动告知。详情请点击查看以下各项说明。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">积分获得</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、每一张成功交易的订单，所付现金部分（含</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">礼品卡）都可获得积分，不同商品积分标准不同，获得积分以订单提交时所注明的积分为准。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、贵宾会员购物时，将额外获得相应级别的级别赠分。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、阶段性的积分促销活动，也会给您带来额外的促销赠分，详见积分活动。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">4</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、促销商品不能获得积分。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">…………………………………………………………………………………………</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">积分有效期</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">积分有效期：获得之日起到次年年底。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">…………………………………………………………………………………………</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">查询积分</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">积分有效期：获得之日起到次年年底。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">您可以在</span><span lang=\\\"EN-US\\\">\\\"</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">我的</span><span lang=\\\"EN-US\\\">iWebShop-</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">我的积分</span><span lang=\\\"EN-US\\\">\\\"</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">中，查看您的累计积分。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">…………………………………………………………………………………………</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">积分活动</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">会不定期地推出各种积分活动，请随时关注关于积分促销的告知。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、会员可以用积分参与</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">指定的各种活动，参与后会扣减相应的积分。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、积分不可用于兑换现金，仅限参加</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">指定兑换物品、参与抽奖等各种活动。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">…………………………………………………………………………………………</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">会员积分计划细则</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">不同帐户积分不可合并使用；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">·本计划只适用于个人用途而进行的购物，不适用于团体购物、以营利或销售为目的的购买行为、其它非个人用途购买行为。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">·会员积分计划及原</span><span lang=\\\"EN-US\\\">VIP</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">制度的最终解释权归</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">所有。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">…………………………………………………………………………………………</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">免责条款</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">感谢您访问</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">的会员积分计划，本计划由</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站</span><span lang=\\\"EN-US\\\">/</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">或其关联企业提供。以上计划条款和条件，连同计划有关的任何促销内容的相应条款和条件，构成本计划会员与</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">之间关于制度的完整协议。如果您使用</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">，您就参加了本计划并接受了这些条款、条件、限制和要求。请注意，您对</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">站的使用以及您的会员资格还受制于</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">站上时常更新的所有条款、条件、限制和要求，请仔细阅读这些条款和条件。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">协议的变更</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">可以在没有特殊通知的情况下自行变更本条款、</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">的任何其它条款和条件、或您的计划会员资格的任何方面。对这些条款的任何修改将被包含在</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">的更新的条款中。如果任何变更被认定为无效、废止或因任何原因不可执行，则该变更是可分割的，且不影响其它变更或条件的有效性或可执行性。在我们变更这些条款后，您对</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">的继续使用，构成您对变更的接受。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">终止</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">可以不经通知而自行决定终止全部或部分计划，或终止您的计划会员资格。即使</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">没有要求或强制您严格遵守这些条款，也并不构成对属于</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">的任何权利的放弃。如果您在</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">的客户帐户被关闭，那么您也将丧失您的会员资格。对于该会员资格的丧失，您对</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">不能主张任何权利或为此索赔。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">责任限制</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">除了</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">的使用条件中规定的其它限制和除外情况之外，在中国法律法规所允许的限度内，对于因会员积分计划而引起的或与之有关的任何直接的、间接的、特殊的、附带的、后果性的或惩罚性的损害，或任何其它性质的损害，</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">、</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">的董事、管理人员、雇员、代理或其它代表在任何情况下都不承担责任。</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">的全部责任，不论是合同、保证、侵权（包括过失）项下的还是其它的责任，均不超过您所购买的与该索赔有关的商品价值额。这些责任排除和限制条款将在法律所允许的最大限度内适用，并在您的计划会员资格被撤销或终止后仍继续有效。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\"><br />\r\n</span></p>', '0', '2019-08-17 21:39:35', '1');
INSERT INTO `fly_help` VALUES ('28', '3', null, '交易条款', '<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站交易条款</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站和您之间的契约</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1.iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站将尽最大努力保证您所购商品与网站上公布的价格一致，但价目表和声明并不构成要约。</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站有权在发现了其网站上显现的产品及订单的明显错误或缺货的情况下，单方面撤回。</span><span lang=\\\"EN-US\\\">(</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">参见下面相关条款</span><span lang=\\\"EN-US\\\">)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站保留对产品订购的数量的限制权。</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family: Calibri\\\">在下订单的同时，您也同时承认了您拥有购买这些产品的权利能力和行为能力，并且您对您在订单中提供的所有信息的真实性负责。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2. </span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">价格变化和缺货</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">产品的价格和可获性都在</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站上指明。这类信息将随时更改且不发任何通知。商品的价格都包含了增值税。送货费将另外结算，费用根据您选择的送货方式的不同而异。如果发生了意外情况，在确认了您的订单后，由于供应商提价，税额变化引起的价格变化，或是由于网站的错误等造成商品价格变化，您有权取消您的订单，并希望您能及时通过电子邮件或电话通知</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">客户服务部。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">您所订购的商品，如果发生缺货，您有权取消订单。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3. </span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">邮件</span><span lang=\\\"EN-US\\\">/</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">短信服务</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站保留通过邮件和短信的形式，对本网站注册、购物用户发送订单信息、促销活动等告知服务的权利。如果您在</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站注册、购物，表明您已默示同意接受此项服务。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">若您不希望接收</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站的邮件，请在邮件下方输入您的</span><span lang=\\\"EN-US\\\">E-mail</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">地址自助完成退阅；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">若您不希望接收</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站的短信，请提供您的手机号码</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family: Calibri\\\">联系客服</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">处理。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">4. </span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">送货</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站将会把产品送到您所指定的送货地址。所有在</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站上列出的送货时间为参考时间，参考时间的计算是根据库存状况、正常的处理过程和送货时间、送货地点的基础上估计得出的。参考时间不代表等同于到货时间。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">5.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">退款政策</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">退货或换货商品缺货时产生的现金款项，退回方式视支付方式的不同而不同：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网上支付的订单，退款退回至原支付卡；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">银行转帐或邮局汇款支付的订单，退款退回至下订单账户的账户余额中。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">6. </span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">条款的修正</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">这些交易条件的条款适用于</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站为您提供的产品销售服务。这些条款将有可能不时的被修正。任何修正条款的发生，</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站都将会及时公布。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">7. </span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">条款的可执行性</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">如果出于任何原因，这些条款及其条件的部分不能得以执行，其他条款及其条件的有效性将不受影响。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">8. </span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">适用的法律和管辖权</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">您和</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站之间的契约将适用中华人民共和国的法律，所有的争端将诉诸于</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站所在地的人民法院。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">9</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站会员制计划（</span><span lang=\\\"EN-US\\\">VIP</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">计划）协议的变更</span><span lang=\\\"EN-US\\\">/</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">终止</span><span lang=\\\"EN-US\\\">/</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">责任限制</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站的会员制计划（</span><span lang=\\\"EN-US\\\">VIP</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">计划），本计划由</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站</span><span lang=\\\"EN-US\\\">/</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">或其关联企业提供。以上计划条款和条件，连同计划有关的任何促销内容的相应条款和条件，构成本计划会员与</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">之间关于制度的完整协议。如果您参加计划，您就接受了这些条款、条件、限制和要求。请注意，您对</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站的使用以及您的会员资格还受制于</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站上时常更新的所有条款、条件、限制和要求，请仔细阅读这些条款和条件。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">协议的变更</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站可以在没有特殊通知的情况下自行变更本条款、</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站的任何其它条款和条件、或您的计划会员资格的任何方面。</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">对这些条款的任何修改将被包含在</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站的更新的条款中。如果任何变更被认定为无效、废止或因任何原因不可执行，则该变更是可分割的，且不影响其它变更或条件的有效性或可执行性。在我们变更这些条款后，您对</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站的继续使用，构成您对变更的接受。如果您不同意本使用交易条款中的任何一条，您可以不使用</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">10</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站的帐户余额自助提现功能</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站为您提供了帐户余额自助提现功能，在提交提现申请单时，您也同时承认了您拥有提现账户余额的权利能力和行为能力，并且将对您在申请单中提供的所有信息的真实性负责。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">用户在申请使用</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站网络服务时，必须准确提供必要的资料，如资料有任何变动</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family: Calibri\\\">，请在</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站产品网站上及时更新。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">用户注册成功后，</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站将为其开通一个账户，为用户在</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站交易及使用</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站服务时的唯一身份标识，该账户的登录名和密码由用户负责保管；用户应当对以其账户进行的所有活动和事件负法律责任。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">终止</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站可以不经通知而自行决定终止全部或部分计划，或终止您的计划会员资格。即使</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站没有要求或强制您严格遵守这些条款，也并不构成对属于</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站的任何权利的放弃。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">如果您在</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站的客户账户被关闭，那么您也将丧失您的会员资格。对于该会员资格的丧失，您对</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站不能主张任何权利或为此索赔。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">责任限制</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">除了</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站的使用条件中规定的其它限制和除外情况之外，在中国法律法规所允许的限度内，对于因</span><span lang=\\\"EN-US\\\">VIP</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">计划而引起的或与之有关的任何直接的、间接的、特殊的、附带的、后果性的或惩罚性的损害，或任何其它性质的损害，</span><span lang=\\\"EN-US\\\"> iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站、</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站的董事、管理人员、雇员、代理或其它代表在任何情况下都不承担责任。</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站的全部责任</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">，不论是合同、保证、侵权（包括过失）项下的还是其它的责任，均不超过您所购买的与该索赔有关的商品价值额。这些责任排除和限制条款将在法律所允许的最大限度内适用，并在您的计划会员资格被撤销或终止后仍继续有效。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">隐私声明</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">电子通讯</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">当您访问</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站或给我们发送电子邮件时，您与我们用电子方式进行联系。您同意以电子方式接受我们的信息。我们将用电子邮件或通过在</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站上发布通知的方式与您进行联系。您同意我们用电子方式提供给您的所有协议、通知、披露和其他信息是符合此类通讯必须是书面形式的法定要求的。如果</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站能够证明以电子形式的信息已经发送给您或者</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站立即在</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站上张贴这样的通知，将被视为您已收到所有协议、声明、披露和其他信息</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">版权声明</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站上的所有内容诸如文字、图表、标识、按钮图标、图像、声音文件片段、数字下载、数据编辑和软件、商标都是</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站或其关联公司或其内容提供者的财产，受中国和国际版权法的保护。未经</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站书面授权或许可</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">，不得以任何目的对</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站或其任何部分进行复制、复印、仿造、出售、转售、访问、或以其他方式加以利用。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">您的账户</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">如果您使用</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站，您有责任对您的账户和密码保守秘密并对进入您的计算机作出限制，并且您同意对在您的账户和密码下发生的所有活动承担责任。</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站的确销售供儿童使用的产品，但只将它们销售给成年人。如果您在</span><span lang=\\\"EN-US\\\">18</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">岁以下，您只能在父母或监护人的参与下才能使用</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站。</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站及其关联公司保留在中华人民共和国法律允许的范围内独自决定拒绝服务、关闭账户、清除或编辑内容或取消订单的权利。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">评论、意见、消息和其他内容</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">访问者可以张贴评论、意见及其他内容，以及提出建议、主意、意见、问题或其他信息，只要内容不是非法、淫秽、威胁、诽谤、侵犯隐私、侵犯知识产权或以其他形式对第三者构成伤害或侵犯或令公众讨厌，也不包含软件病毒、政治宣传、商业招揽、连锁信、大宗邮件或任何形式的</span><span lang=\\\"EN-US\\\">\\\"</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">垃圾邮件</span><span lang=\\\"EN-US\\\">\\\"</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">。您不可以使用虚假的电子邮件地址、冒充任何他人或实体或以其它方式对卡片或其他内容的来源进行误导。</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站保留清除或编辑这些内容的权利（但非义务），但不对所张贴的内容进行经常性的审查。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">如果您确实张贴了内容或提交了材料，除非我们有相反指示，您授予</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站及其关联公司非排他的、免费的、永久的、不可撤销的和完全的再许可权而在全世界范围内任何媒体上使用、复制、修改、改写、出版、翻译、创作衍生作品、分发和展示这样的内容。您授予</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站及其关联公司和被转许可人使用您所提交的与这些内容有关的名字的权利，如果他们选择这样做的话。您声明并担保您拥有或以其它方式控制您所张贴内容的权利，内容是准确的，对您所提供内容的使用不违反本政策并不会对任何人和实体造成伤害。您声明并保证对于因您所提供的内容引起的对</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站或其关联公司的损害进行赔偿。</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站有权（但非义务）监控和编辑或清除任何活动或内容。</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站对您或任何第三方所张贴的内容不承担责任。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">合同缔结</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">如果您通过我们网站订购产品，您的订单就成为一种购买产品的申请或要约。我们将发送给您一封确认收到订单的电子邮件，其中载明订单的细节。但是只有当我们向您发出送货确认的电子邮件通知您我们已将产品发出时，我们对您合同申请的批准与接受才成立。如果您在一份订单里订购了多种产品并且我们只给您发出了关于其中一部分的发货确认电子邮件，那么直到我们发出关于其他产品的发货确认电子邮件，关于那部分产品的合同才成立。当您所购买的商品离开了</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站或其关联公司的库房时，该物品的所有权和灭失风险即转移到您这一方。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">产品说明</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站及其关联公司努力使产品说明尽可能准确。不过，我们并不保证产品说明或</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站上的其他内容是准确的、完整的、可靠的、最新的或无错误的。如果</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站提供的产品本身并非如说明所说，您唯一的救济是将该未经使用过的产品退还我们。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">价格</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">直到您发出订单，我们才能确认商品的价格。尽管我们做出最大的努力，我们的商品目录里的一小部分商品可能会有定价错误。如果我们发现错误定价，我们将采取下列之一措施：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">i</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family: Calibri\\\">如果某一商品的正确定价低于我们的错误定价，我们将按照较低的定价向您销售交付该商品。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">ii</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family: Calibri\\\">如果某一商品的正确定价高于我们的错误定价，我们会根据我们的情况决定</span><span lang=\\\"EN-US\\\">,</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">是否在交付前联系您寻求您的指示</span><span lang=\\\"EN-US\\\">, </span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">或者取消订单并通知您。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">其他企业</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站及其关联企业之外的其他人可能在</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站上经营商店、提供服务或者销售产品。另外，我们提供与关联公司和其他企业的链接。我们不负责审查和评估也不担保任何这些企业或个人的待售商品及它们网站的内容。我们对所有这些企业或任何其他第三人或其网站的行为、产品和内容不承担责任。您应仔细阅读它们自己的隐私政策及使用条件。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>', '0', '2019-08-17 21:39:38', '1');
INSERT INTO `fly_help` VALUES ('29', '3', null, '订单状态', '<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">一个</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站的新订单从下单到订单完成，会经历各种状态，我们会将各种状态显示在订单详情页面，希望以此种方式让您更好的了解订单情况，及时跟踪订单状态，打消疑虑并顺利完成购物。以下是订单状态的简单说明：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">等待付款：如果您选择“在线支付”“银行卡转账”“邮局汇款”“公司转账”“分期付款”“高校</span><span lang=\\\"EN-US\\\">-</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">自己支付”“高校</span><span lang=\\\"EN-US\\\">-</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">代理垫付”这几种支付方式，在成功支付且得到财务确认之前，订单状态会显示为等待付款；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">正在配货：该状态说明</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站正在为您的订单进行配货，包括</span><span lang=\\\"EN-US\\\">5</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">个子状态</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）打印：将您订购的商品打印成单，便于出库员取货</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）出库：出库员找到您订购的商品并出库</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）扫描：扫描员扫描您订购的商品并确认商品成功出库</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">4</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）打包：打包员将您订购的商品放入包裹以便运输</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">5</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）发货：发货员将您的包裹发货运输</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站送货：您订购的商品已经发货，正在运送途中</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">收货并确认：货物已发出一段时间，如果您已收到货物可以点击确认按钮进行确认</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">上门自提：该状态说明您订购的商品已经送至相应自提点，请您尽快到自提点提货</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">已完成：此次交易已经完成，希望能得到您的满意</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">已锁定：如果您修改了订单但没有修改成功，则系统会自动锁定您的订单，您可以在订单列表页面点击操作栏中的“解锁订单”使订单恢复正常</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">订单待审核：该状态说明您订购的某类商品缺货，需要</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站将货物备齐后订单才会恢复正常状态，此状态下请您不要进行付款操作，以免货物无法备齐而占用您的货款</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">修改订单常见问题：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、什么时候允许修改订单？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">您在</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站下单后，</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站后台程序会通过一系列算法来判断您的订单是否可以修改，如果可以修改，您在订单操作一列可以看到“修改订单”链接，此时说明订单可以修改。如果没有此链接，说明该订单不可修改。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">一般来说，在您选购的商品没有打印完毕之前，都是可以修改订单的。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、我能修改订单的哪些内容？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">修改购物车内的商品数量，增加或删除商品；（暂不支持添加套装）</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">修改收货人信息、配送方式、发票信息、订单备注；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">添加优惠券或删除已使用的优惠券；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注：由于目前暂不支持修改支付方式，所以一些与支付方式相关联的收货地址可能也无法修改。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、修改订单时，订单为什么会被锁定？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">为了避免您在修改订单的同时，您的订单继续被程序处理和执行，我们会在您修改订单过程中锁定您的订单，直到您完成修改并点击了“提交订单”按钮。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">如果您在修改过程当中放弃了修改，建议您返回订单列表页面点击操作栏中的“解锁订单”，否则您的订单将在</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">个小时后解锁，将影响您订单的生产时间和收货时间。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">4</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、如果购物车里某一款商品下单时的价格和修改订单当时的价格不一致，按哪个来算商品价格呢？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">如果您不修改该商品的购买数量，那么价格和赠品都会维持您下单时的状态不变；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">如果您修改了该商品购买数量或者添加了新商品，那么价格和赠品都会与</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站最新显示的价格和赠品一致。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">如果您添加了新商品，那么新商品的价格与</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站最新显示的价格和赠品一致。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">5</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、可以先申请价保后再修改订单吗？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">不可以，如果你对某商品申请了价保，那么该商品将不能进行修改和删除，除非您删除整个订单。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>', '0', '2019-08-17 21:39:41', '1');
INSERT INTO `fly_help` VALUES ('30', '4', null, '货到付款', '<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">货到付款：货物送到订单指定的收货地址后，由收货人支付货款给送货人员</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">货到付款适用于加急快递、普通快递送货上门的订单。请您在订购过程的“付款方式”处，选择货到付款</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">温馨提示：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、货到付款仅限支付现金</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、签收时，请您仔细核兑款项、务必作到货款两清，若事后发现款项错误，我们将无法再核实确认点击查看当当网签收验货政策</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、部分商店街的商家不支持货到付款，请您通过网上支付、邮局汇款、银行转帐方式支付</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>', '0', '2019-08-17 21:39:44', '1');
INSERT INTO `fly_help` VALUES ('31', '4', null, '在线支付', '<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站提供的在线支付方式</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站目前有以下支付平台可供选择：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）工商银行网上银行支付平台，支持工商银行银行卡网上在线支付</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）招商银行网上银行支付平台，支持招商银行银行卡网上在线支付</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）建设银行网上银行支付平台，支持建设银行银行卡网上在线支付</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">4</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）农业银行网上银行支付平台，支持</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family: Calibri\\\">农业银行银行卡网上在线支付</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">5</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）支付宝支付平台，关于支付宝的支付帮助请查看</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">6</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）财付通支付平台，关于财付通的支付帮助请查看</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">7</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）快钱</span><span lang=\\\"EN-US\\\">99Bill</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">支付平台，关于快钱支付平台的支付帮助请查看</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">8</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）环迅</span><span lang=\\\"EN-US\\\">IPS</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">，关于环迅支付平台的支付帮助请查看</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">9</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站虚拟账户支付</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、如您是第一次进行网上在线支付，建议事先拨打银行卡所属发卡银行的热线电话，详细咨询可在其网上进行在线支付的银行卡种类及相关开通手续。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>', '0', '2019-08-17 21:39:47', '1');
INSERT INTO `fly_help` VALUES ('32', '4', null, '银行电汇', '<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">请在电汇单“汇款用途”一栏处注明您的订单号，银行汇款到账通常需要</span><span lang=\\\"EN-US\\\">2~3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">个工作日的时间，我们将在款到后当日为您发货。否则我们无法及时核对审核，这将延误您的发货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">使用银行电汇支付，请务必在</span><span lang=\\\"EN-US\\\">3*24</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">小时之内支付，逾时订单将会自动作废。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">如果有些银行网点不能提供填写订单号，请汇款后联系我们。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">银行电汇账户信息：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">银行</span><span lang=\\\"EN-US\\\"> <span style=\\\"mso-tab-count:1;\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">账户信息</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">工商银行</span><span lang=\\\"EN-US\\\"> <span style=\\\"mso-tab-count:1;\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">户</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">名：</span><span lang=\\\"EN-US\\\">*****</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">有限公司</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">开户行：工商银行</span><span lang=\\\"EN-US\\\">*****</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">支行</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">账</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">号：</span><span lang=\\\"EN-US\\\">1001*****</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">建设银行</span><span lang=\\\"EN-US\\\"> <span style=\\\"mso-tab-count:1;\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">户</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">名：</span><span lang=\\\"EN-US\\\">*****</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">有限公司</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">开户行：建设银行</span><span lang=\\\"EN-US\\\">*****</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">支行</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">账</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">号：</span><span lang=\\\"EN-US\\\">3100*****</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">招商银行</span><span lang=\\\"EN-US\\\"> <span style=\\\"mso-tab-count:1;\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">户</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">名：</span><span lang=\\\"EN-US\\\">*****</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">有限公司</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">开户行：招商银行</span><span lang=\\\"EN-US\\\">*****</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">支行</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">账</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">号：</span><span lang=\\\"EN-US\\\">1219*****</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">交通银行</span><span lang=\\\"EN-US\\\"> <span style=\\\"mso-tab-count:1;\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">户</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">名：</span><span lang=\\\"EN-US\\\">*****</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">有限公司</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">开户行：交通银行</span><span lang=\\\"EN-US\\\">*****</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">支行</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">账</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">号：</span><span lang=\\\"EN-US\\\">3100*****</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">汇单范例</span></p>', '0', '2019-08-17 21:39:50', '1');
INSERT INTO `fly_help` VALUES ('33', '4', null, '余额支付', '<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">用户还可以通过使用账户中心中的余额来对订单进行支付</span></p>', '0', '2019-08-17 21:39:53', '1');
INSERT INTO `fly_help` VALUES ('34', '6', null, '配送范围及运费', '<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站购物满</span><span lang=\\\"EN-US\\\">29</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元免运费，查看详情</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">普通快递送货上门</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">覆盖全国</span><span lang=\\\"EN-US\\\">800</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">多个城市，运费</span><span lang=\\\"EN-US\\\">5</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元</span><span lang=\\\"EN-US\\\">/</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">包裹，购物满</span><span lang=\\\"EN-US\\\">29</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元免运费</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">加急快递送货上门</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">支持北京、天津、上海、广州、深圳、廊坊，限当地发货订单，运费</span><span lang=\\\"EN-US\\\">10</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元</span><span lang=\\\"EN-US\\\">/</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">包裹</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">圆通快递</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">北京地区：运费</span><span lang=\\\"EN-US\\\">10</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元</span><span lang=\\\"EN-US\\\">/</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">单</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">4.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">普通邮递</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">大陆地区：运费</span><span lang=\\\"EN-US\\\">5</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元</span><span lang=\\\"EN-US\\\">/</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">包裹，购物满</span><span lang=\\\"EN-US\\\">29</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元免运费</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">港澳地区：运费为商品原价总金额的</span><span lang=\\\"EN-US\\\">30%</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">，最低</span><span lang=\\\"EN-US\\\">20</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">海外地区：运费为商品原价总金额的</span><span lang=\\\"EN-US\\\">50%</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">，最低</span><span lang=\\\"EN-US\\\">50</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">5.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">邮政特快专递</span><span lang=\\\"EN-US\\\">(EMS)</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">北京地区：运费为订单总金额的</span><span lang=\\\"EN-US\\\">50%</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">，最低</span><span lang=\\\"EN-US\\\">20</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">大陆其它地区：运费为订单总金额的</span><span lang=\\\"EN-US\\\">100%</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">，最低</span><span lang=\\\"EN-US\\\">20</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">港澳台地区：运费为商品原价总金额的</span><span lang=\\\"EN-US\\\">70%</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">，最低</span><span lang=\\\"EN-US\\\">60</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>', '0', '2019-08-17 21:39:57', '1');
INSERT INTO `fly_help` VALUES ('35', '6', null, '上门自提', '<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注意事项：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">自提时间：周一至周日，</span><span lang=\\\"EN-US\\\">09:00</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">－</span><span lang=\\\"EN-US\\\">19:00</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（如遇国家法定节假日，则以</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站新闻发布放假时间为准，请大家届时关注）</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品到达自提点后，我们将为您保留三天，超过三天不上门提货，则视为默认取消订单；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">钱、货需客户当面点清，离开提货前台后</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站将不再对钱、货数量负责；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">货物价保需客户在自提当场提出，离开提货前台后</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站不再对自提货物提供价保服务；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">普通发票：每张订单需在自提当日开具发票，</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站不提供累计开具发票的服务；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">增值税发票：选择</span><span lang=\\\"EN-US\\\">POS</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">机刷卡，不能开具增票；增票当日无法开具，需订单完成后三个工作日左右</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站按订单地址将增票快递给客户，如订单中地址有误请及时通知</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站客服人员。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">特殊说明：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">上门自提的订单，请在规定的时间内到自提点提取货物。上门自提订单原则上免收配送费用，但如果一个</span><span lang=\\\"EN-US\\\">ID</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">帐号在一个月内有过</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">次以上或一年内有过</span><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">次以上，在规定的时间内无理由不履约提货，我司将在相应的</span><span lang=\\\"EN-US\\\">ID</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">帐户里每单扣除</span><span lang=\\\"EN-US\\\">50</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">个积分做为运费；时间计算方法为：成功提交订单后向前推算</span><span lang=\\\"EN-US\\\">30</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">天为一个月，成功提交订单后向前推算</span><span lang=\\\"EN-US\\\">365</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">天为一年，不以自然月和自然年计算；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">对于上门自提的客户，</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站可以接受现金、支票（北京和上海的自提点支持，其他城市的自提点不支持）和</span><span lang=\\\"EN-US\\\">POS</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">机刷卡三种付款方式。选择支票支付方式，需要客户自行将支票内容填写完整（货款在</span><span lang=\\\"EN-US\\\">5000</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元或</span><span lang=\\\"EN-US\\\">5000</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元以上，需要款到帐后方可提货）；</span><span lang=\\\"EN-US\\\">POS</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">机刷卡只支持带有银联标识的银行卡。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">信用卡</span><span lang=\\\"EN-US\\\">POS</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">机刷卡消费超过</span><span lang=\\\"EN-US\\\">4500</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元时，发卡银行按照相关规定有可能不向您赠送积分，具体信息请致电发卡行确认。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">自提点</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">自提点适用范围：以下地区用户均可到相应自提点付款提货，无需支付运费（大家电产品限物流中心自提点）。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">以下各自提点均不接收由于各种原因被客户邮寄退回的商品，否则出现的一切后果</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站概不负责。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">友好提示</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">下单之后可以更换自提点或更换配送方式吗？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">可以更换自提点但无法更换配送方式。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">方法：我的</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站</span><span lang=\\\"EN-US\\\">-</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">订单中心</span><span lang=\\\"EN-US\\\">-</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">点“查看”</span><span lang=\\\"EN-US\\\">-</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">进入订单详细页面</span><span lang=\\\"EN-US\\\">-</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">订单操作</span><span lang=\\\"EN-US\\\">-</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">修改订单</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">一般来说，在您选购的商品没有打印完毕之前，都是可以修改订单的。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">自提时验货发现问题，可以当场换货吗？如何处理？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">不可以。自提时如果发现货品有问题请当场反映给</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站工作人员，由工作人员帮您处理。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">自提价格和其它配送方式价格是否一样？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">不管您选用哪种配送方式，商品的价格是一样的。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">4.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">可以到自提点付款，贵公司工作人员送货到家吗？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">不可以，自提点采用的是上门付款提货方式。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">5.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">为何结算时找不到某某自提点？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）可能是您所下订单的收货地址与该自提点不在一个省市；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）或者该自提点已经更换名称、地址；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）或者是您所购买的商品是大家电，该分类下部分商品只支持部分地区物流总部自提。详情以下单时所支持配送方式为准。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">6.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">自提点可以先验货后付款吗？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">不可以，我司不管哪种配送方式都是采取先付款后验货的方式。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">7.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">任何商品都可以自提吗？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">除了虚拟商品和服务类商品（如网络版杀毒软件）及部分大家电无法自提外，其它都可以自提。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">8.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">上门自提能用支付宝支付吗？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">不可以。对于上门自提的客户，</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站可以接受现金、支票和</span><span lang=\\\"EN-US\\\">POS</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">机刷卡三种付款方式。选择支票支付方式，需要客户自行将支票内容填写完整（货款在</span><span lang=\\\"EN-US\\\">5000</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元或</span><span lang=\\\"EN-US\\\">5000</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元以上，需要款到帐后方可提货）；</span><span lang=\\\"EN-US\\\">POS</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">机刷卡只支持带有银联标识的银行卡。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">9.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">自提点装机需要自带什么，比如：系统盘等等</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">自提点装机服务负责仅配置单里的散件组装，如您希望安装系统请自带系统盘，我们会指导您安装。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">10.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">购买配件，上门自提时可以提供安装服务吗？（比如内存条）</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">购买配件，自提时不提供安装服务，希望您能理解。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>', '0', '2019-08-17 21:39:59', '1');
INSERT INTO `fly_help` VALUES ('36', '6', null, '加急快递', '<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">如何正确选择加急配送服务</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">北京、天津、上海、广州、深圳、廊坊</span><span lang=\\\"EN-US\\\">6</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">个城市地区的用户，并且为当地发货订单，用户可在结算中心“送货方式”部分选择加急快递送货上门服务。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">常见问题解答：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">我的订单什么时候可以送到？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">具体配送时间根据不同城市略有不同，请查看配送范围及运费</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2.\\\"</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">加急快递送货上门</span><span lang=\\\"EN-US\\\">\\\"</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">的费收取标准？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">北京、天津、上海、广州、深圳、廊坊</span><span lang=\\\"EN-US\\\">6</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">个城市的“加急快递送货上门”配送费为</span><span lang=\\\"EN-US\\\">10</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元</span><span lang=\\\"EN-US\\\">/</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">单。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>', '0', '2019-08-17 21:40:03', '1');
INSERT INTO `fly_help` VALUES ('37', '6', null, '商品验货与签收', '<p class=\\\"\\\\&quot;MsoNormal\\\\&quot;\\\"><span style=\\\"\\\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\\&quot;\\\">快递送货上门、圆通快递的订单</span></p>\r\n<p class=\\\"\\\\&quot;MsoNormal\\\\&quot;\\\"><span lang=\\\"\\\\&quot;EN-US\\\\&quot;\\\">1</span><span style=\\\"\\\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\\&quot;\\\">、签收时仔细核对：商品及配件、商品数量、</span><span lang=\\\"\\\\&quot;EN-US\\\\&quot;\\\">iWebShop</span><span style=\\\"\\\\&quot;font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\\&quot;\\\">网站的发货清单、发票（如有）、三包凭证（如有）等</span></p>\r\n<p class=\\\"\\\\&quot;MsoNormal\\\\&quot;\\\"><span lang=\\\"\\\\&quot;EN-US\\\\&quot;\\\">2</span><span style=\\\"\\\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\\&quot;\\\">、若存在包装破损、商品错误、商品少发、商品有表面质量问题等影响签收的因素，请您一定要当面向送货员说明情况并当场整单退货</span></p>\r\n<p class=\\\"\\\\&quot;MsoNormal\\\\&quot;\\\"><span lang=\\\"\\\\&quot;EN-US\\\\&quot;\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"\\\\&quot;MsoNormal\\\\&quot;\\\"><span style=\\\"\\\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\\&quot;\\\">邮局邮寄的订单</span></p>\r\n<p class=\\\"\\\\&quot;MsoNormal\\\\&quot;\\\"><span lang=\\\"\\\\&quot;EN-US\\\\&quot;\\\">1</span><span style=\\\"\\\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\\&quot;\\\">、请您一定要小心开包，以免尖锐物件损伤到包裹内的商品</span></p>\r\n<p class=\\\"\\\\&quot;MsoNormal\\\\&quot;\\\"><span lang=\\\"\\\\&quot;EN-US\\\\&quot;\\\">2</span><span style=\\\"\\\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\\&quot;\\\">、签收时仔细核对：商品及配件、商品数量、</span><span lang=\\\"\\\\&quot;EN-US\\\\&quot;\\\">iWebShop</span><span style=\\\"\\\\&quot;font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\\&quot;\\\">网站的发货清单、发票（如有）、三包凭证（如有）等</span></p>\r\n<p class=\\\"\\\\&quot;MsoNormal\\\\&quot;\\\"><span lang=\\\"\\\\&quot;EN-US\\\\&quot;\\\">3</span><span style=\\\"\\\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\\&quot;\\\">、若包装破损、商品错误、商品少发、商品存在表面质量问题等，您可以选择整单退货；或是求邮局开具相关证明后签收，然后登陆</span><span lang=\\\"\\\\&quot;EN-US\\\\&quot;\\\">iWebShop</span><span style=\\\"\\\\&quot;font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\\&quot;\\\">网站申请退货或申请换货</span></p>\r\n<p class=\\\"\\\\&quot;MsoNormal\\\\&quot;\\\"><span lang=\\\"\\\\&quot;EN-US\\\\&quot;\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"\\\\&quot;MsoNormal\\\\&quot;\\\"><span style=\\\"\\\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\\&quot;\\\">温馨提示</span></p>\r\n<p class=\\\"\\\\&quot;MsoNormal\\\\&quot;\\\"><span lang=\\\"\\\\&quot;EN-US\\\\&quot;\\\">1</span><span style=\\\"\\\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\\&quot;\\\">、货到付款的订单送达时，请您当面与送货员核兑商品与款项，确保货款两清；若事后发现款项有误，</span><span lang=\\\"\\\\&quot;EN-US\\\\&quot;\\\">iWebShop</span><span style=\\\"\\\\&quot;font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\\&quot;\\\">网站将无法为您处理</span></p>\r\n<p class=\\\"\\\\&quot;MsoNormal\\\\&quot;\\\"><span lang=\\\"\\\\&quot;EN-US\\\\&quot;\\\">2</span><span style=\\\"\\\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\\&quot;\\\">、请收货时务必认真核对，若您或您的委托人已签收，则说明订单商品正确无误且不存在影响使用的因素，</span><span lang=\\\"\\\\&quot;EN-US\\\\&quot;\\\">iWebShop</span><span style=\\\"\\\\&quot;font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\\&quot;\\\">网站有权不受理因包装或商品破损、商品错漏发、商品表面质量问题、商品附带品及赠品少发为由的退换货申请</span></p>\r\n<p class=\\\"\\\\&quot;MsoNormal\\\\&quot;\\\"><span lang=\\\"\\\\&quot;EN-US\\\\&quot;\\\">3</span><span style=\\\"\\\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\\&quot;\\\">、部分商品由商店街的商家提供</span><span lang=\\\"\\\\&quot;EN-US\\\\&quot;\\\">,</span><span style=\\\"\\\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\\&quot;\\\">这部分商品的验货验收不在</span><span lang=\\\"\\\\&quot;EN-US\\\\&quot;\\\">iWebShop</span><span style=\\\"\\\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\\&quot;\\\">网站承诺的范围内</span></p>\r\n<p class=\\\"\\\\&quot;MsoNormal\\\\&quot;\\\"><span lang=\\\"\\\\&quot;EN-US\\\\&quot;\\\"><o:p>&nbsp;</o:p></span></p>', '0', '2019-08-17 21:40:06', '1');
INSERT INTO `fly_help` VALUES ('38', '6', null, 'EMS/邮政普包', '<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站目前除提供</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站快递以及上门自提服务以外，还提供了更多样的配送方式，支持更多地区的配送服务。目前开通的快递有圆通快递、宅急送、邮政普包和邮政</span><span lang=\\\"EN-US\\\">EMS</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（邮政特快专递）等。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">　</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">邮政普包运费标准</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">区域</span><span lang=\\\"EN-US\\\"><span style=\\\"mso-tab-count:1;\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">省</span><span lang=\\\"EN-US\\\">/</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">市</span><span lang=\\\"EN-US\\\"><span style=\\\"mso-tab-count: 1\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">运费</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">一区</span><span lang=\\\"EN-US\\\"><span style=\\\"mso-tab-count:1;\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">北京、上海、广东、江苏、浙江、山东、湖北、陕西、四川</span><span lang=\\\"EN-US\\\"><span style=\\\"mso-tab-count:1;\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>5</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元</span><span lang=\\\"EN-US\\\">/</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">单</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">二区</span><span lang=\\\"EN-US\\\"><span style=\\\"mso-tab-count:1;\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">天津、重庆、黑龙江、吉林、辽宁、河北、河南、山西、安徽、江西、湖南、福建、广西、海南</span><span lang=\\\"EN-US\\\"><span style=\\\"mso-tab-count:1;\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>10</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">公斤以下：</span><span lang=\\\"EN-US\\\">5</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元</span><span lang=\\\"EN-US\\\">/</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">单</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">10</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（含）公斤以上：</span><span lang=\\\"EN-US\\\">6</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元</span><span lang=\\\"EN-US\\\">/</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">单</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">三区</span><span lang=\\\"EN-US\\\"><span style=\\\"mso-tab-count:1;\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">内蒙古、甘肃、宁夏、云南、贵州、青海、新疆、西藏</span><span lang=\\\"EN-US\\\"><span style=\\\"mso-tab-count:1;\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>10</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">公斤以下：</span><span lang=\\\"EN-US\\\">10</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元</span><span lang=\\\"EN-US\\\">/</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">单</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">10</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（含）公斤以上：</span><span lang=\\\"EN-US\\\">15</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元</span><span lang=\\\"EN-US\\\">/</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">单</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">邮政普包到货时间</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">根据邮政系统服务时限，邮政普包的货物到货（到客户所在地邮政局）时间为</span><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">～</span><span lang=\\\"EN-US\\\">15</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">个工作日，在到货后需要凭包裹单据去包裹所在邮政局领取。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">邮政普包跟踪查询</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">邮政普包跟踪查询请点击此处</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">http://yjcx.chinapost.com.cn/queryMail.do?action=batchQueryMail</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">邮政</span><span lang=\\\"EN-US\\\">EMS</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">运费标准</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">按优惠资费起重</span><span lang=\\\"EN-US\\\">500</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">克以内</span><span lang=\\\"EN-US\\\">16</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元，续重不同省市资费会有所不同。以</span><span lang=\\\"EN-US\\\">500</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">克为计算资费单位，即每件包裹重量尾数不满</span><span lang=\\\"EN-US\\\">500</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">克的，应进整按</span><span lang=\\\"EN-US\\\">500</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">克计算资费。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">　</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">邮政</span><span lang=\\\"EN-US\\\">EMS</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">到货时间</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">根据邮政系统服务时限，邮政</span><span lang=\\\"EN-US\\\">EMS</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">的货物到货时间为</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">～</span><span lang=\\\"EN-US\\\">5</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">个工作日（节假日除外）。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">　</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">邮政</span><span lang=\\\"EN-US\\\">EMS</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">跟踪查询</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">邮政</span><span lang=\\\"EN-US\\\">EMS</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">跟踪查询请点击此处</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">http://www.ems.com.cn/qcgzOutQueryNewAction.do?reqCode=gotoSearch</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">　</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">温馨提示</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">由于第三方物流公司配送区域变动频繁，请您采用以上配送方式时先行查阅配送公司的配送范围，以保证您的订单可以及时到达。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">在您成功提交订单后，</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站在确认到款的情况下会尽快安排商品的出库，您的货物运单号也会在货物出库后的第二至三个工作日添加到您的帐户中心</span><span lang=\\\"EN-US\\\">,</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">您可以登陆帐户中心的订单查询页面进行查询跟踪您的货物情况。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">音响设备江浙沪地区以外的地方需要空运，请选择</span><span lang=\\\"EN-US\\\">EMS</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">，选择圆通可能会造成不必要的延误和退件。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">4.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">关于送货时间</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">货物如未在您选定送货方式规定的最长送货时间内送达，您可以选择以下方式处理；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">您可以进入</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站网帐户中心，选择“订单管理”，根据您的订单号码查询到自己的运单号，然后直接进入圆通快递公司网站</span><span lang=\\\"EN-US\\\">http://www.yto.net.cn/</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">，输入运单号，在线查询配送状况；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">您可以进入相应快递公司网站，查询您所在地的快递公司分部联系电话，拨打查询；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">您可以点击网站首页右侧的在线客服给我们留言，告知订单号，我们将为您及时处理；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">您可以拨打服务电话</span><span lang=\\\"EN-US\\\">400-820-4400</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">通知我们，我们将为您及时处理。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">选择邮政配送方式的客户在收到产品后可在邮局工作人员的面前拆包，如产品损坏，可直接在签收单上注明：内件损坏，本人拒收字样，由邮局再返回</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站，</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站和邮局协商赔偿事宜。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">如签收后未当面开封，产品出现问题，</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站很难和邮局协商赔偿，为了客户的利益，希望客户能够执行并理解。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">选择其他方式的客户在收到货物时，请您认真检查外包装。如有明显损坏迹象，您可以拒收该货品，并及时通知我们。我们会处理并承担由此而产生的运输费用，请客户不必担心。如您签收有明显损坏迹象的外包装后再投诉货物有误或有损坏，恕我们不能受理。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>', '0', '2019-08-17 21:40:15', '1');
INSERT INTO `fly_help` VALUES ('39', '5', null, '换货说明', '<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站承诺自顾客收到商品之日起</span><span lang=\\\"EN-US\\\">15</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">日内（以发票日期为准，如无发票以</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站发货清单的日期为准），如符合以下条件，我们提供换货服务：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、商品及商品本身的外包装没有损坏，保持</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站出售时的原质原样；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、注明换货原因，如果商品存在质量问题，请务必说明；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、确保商品及配件、附带品或者赠品、保修卡、三包凭证、发票、</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站发货清单齐全；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">4</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、如果成套商品中有部分商品存在质量问题，在办理换货时，必须提供成套商品；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">5</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站中的部分商品是由与</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站签订合同的商家提供的，这些商品的换货请与商家联系</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">以下情况不予办理换货：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、任何非由</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站出售的商品；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、任何已使用过的商品，但有质量问题除外；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、任何因非正常使用及保管导致出现质量问题的商品。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">4</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、所有未经客服确认擅自退回的商品，换货申请无法受理。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">特殊说明：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、食品、保健食品类：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">食品类商品不予换货，但有质量问题除外；如商品过期或距离保质期结束不到</span><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">个月。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、美妆个护类：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">化妆品及个人护理用品属于特殊商品不予换货，但有质量问题除外，如商品包装破损，商品过期或离过期不到</span><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">个月。我们保证商品的进货渠道和质量，如果您在使用时对商品质量表示置疑，请出具书面鉴定，我们会按照国家法律规定予以处理。因个人喜好（气味，色泽、型号，外观）和个人肤质不同要求的换货将无法受理。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、母婴用品：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品签收后不予换货，但有质量问题除外，洗涤方法参考说明，正常缩水</span><span lang=\\\"EN-US\\\">10%</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">以内正常，不属于质量问题。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">因个人原因造成的商品损坏（如自行修改尺寸，洗涤，长时间穿着等），不予换货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品吊牌，包装破损，发货单、商品附件（如纽扣等）、说明书、保修单、标签等丢失，不予换货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">：图片及信息仅供参考，不属质量问题。因拍摄灯光、显示器分辨率等原因可能会造成轻微色差，在网购中属于正常现象，一切以实物为准。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">：品牌商品按其三包约定执行。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">4</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、服装类商品：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">内衣类商品，如内衣裤，袜子，文胸类商品，除质量问题除外，不予换货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">因个人原因造成的商品损坏（如自行修改尺寸，洗涤，长时间穿着等），不予换货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品吊牌，包装破损，发货单、商品附件（如纽扣等）、说明书、保修单、标签等丢失，不予换货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">：图片及信息仅供参考，不属质量问题。因拍摄灯光、显示器分辨率等原因可能会造成轻微色差，在网购中属于正常现象，一切以实物为准。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">：品牌商品按其三包约定执行。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">5</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、鞋帽箱包类：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品吊牌，包装破损，发货单、商品配件（如配饰挂坠等）、说明书、保修单、标签等丢失，不予换货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">：图片及信息仅供参考，不属质量问题。因拍摄灯光、显示器分辨率等原因可能会造成轻微色差，在网购中属于正常现象，一切以实物为准。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">：品牌商品按其三包约定执行。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">6</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、玩具类：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品签收后不予换货，但有质量问题除外。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">：图片及信息仅供参考，不属质量问题。因拍摄灯光、显示器分辨率等原因可能会造成轻微色差，在网购中属于正常现象，一切以实物为准。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">：品牌商品按其三包约定执行。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">7</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、家居类商品：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">因个人原因造成的商品损坏（如自行修改尺寸，洗涤），不予换货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品吊牌，包装破损，发货单、商品配件、说明书、保修单、标签等丢失，不予换货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">：图片及信息仅供参考，不属质量问题。因拍摄灯光、显示器分辨率等原因可能会造成轻微色差，在网购中属于正常现象，一切以实物为准。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">：品牌商品按其三包约定执行。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">8</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、手表类商品：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">手表类商品换货说明请您以商品的单品页面说明为准；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">以下情况不予办理换货：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">如果商品自身携带的产品序列号与</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站售出的不符；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">缺少随商品附带的保修卡、发票、配件等；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品已打开塑封包装或撕开开箱即损贴纸者，但有质量问题除外；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">4)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">将商品存储、暴露在超出商品适宜的环境中；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">5)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">未经授权的修理、误用、疏忽、滥用、事故、改动、不正确的安装；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">6)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">食物或液体溅落造成损坏；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">7)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品使用中出现的磨损，非质量问题。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">8)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">手表表带经过调整，但有质量问题除外。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">9)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">非质量问题。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">9</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、珠宝首饰类及礼品类商品：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">对于附带国家级宝玉石鉴定中心出具的鉴定证书的，非质量问题不予换货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">顾客在收到商品之日起（以发票日期为准）</span><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">个月内，如果出现质量问题，请到当地的质量监督部门</span><span lang=\\\"EN-US\\\">-</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">珠宝玉石质量检验中心进行检测，如检测报告确认属于质量问题，请与</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站客服中心联系办理换货事宜。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">换货时，请您务必将商品的外包装、内带附件、鉴定证书、说明书等随同商品一起退回。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">对于高档首饰都附带国家级宝玉石鉴定中心出具的鉴定证书，如果您对此有任何质疑，请到出具该证书的鉴定机构进行复检。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">瑞士军刀、</span><span lang=\\\"EN-US\\\">zippo</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">打火机、钻石、</span><span lang=\\\"EN-US\\\">18K</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">金，如无质量问题不换货，有质量问题请出示检测报告，方可换货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">为了保证您的利益，请您在收到商品时，仔细检查，如果您发现有任何问题，请您当时指出，并办理换货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">10</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、软件类商品：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">软件类商品换货说明请您以商品的单品页面说明为准；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">如出现质量问题请您直接按照说明书上的联系方式与厂家的售后部门联系解决；已打开塑封包装，不予退换货，但有质量问题除外。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">11</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、手机、数码相机、数码摄像机、笔记本电脑等商品：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">顾客收到商品之日起（以发票日期为准）七日内，有非人为质量问题凭有效检测报告可选择退换货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">顾客收到商品之日起（以发票日期为准）八至十五日内，有非人为质量问题凭有效检测报告可选择更换同型号商品。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">顾客收到商品之日起（以发票日期为准）十六日至一年内，有非人为质量问题可在当地保修点免费保修。配件保修请参阅保修卡。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">4)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">为了您的自身权益请妥善保存发票和保修卡，如有发生质量问题请携带发票和保修卡及时到当地检测点检测，以免给您造成不必要的损失。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">5)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">退换货要求：保修卡、发票、</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站发货清单、有效检测报告一律齐全，并且配件完整，包装盒完好，否则将不予受理。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">6)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">全国各地检测、保修点请在保修卡中查找。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">7)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">不接受无检测报告</span><span lang=\\\"EN-US\\\">,</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">并且不在规定时间内的退换货要求。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">8)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">换机产生的邮费由买卖双方各自承担。换货商品一律以邮寄的方式发出。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">9)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">换货地址及联系电话详见各商品页面“售后服务”。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">10)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">如需换货请您先联系</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站客服，在客服人员指导下，一律以邮寄方式完成换货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注：平邮客户以包裹单上的签收日期为主</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">12</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、数码类（手机、数码相机、数码摄像机、笔记本电脑除外）</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品换货说明请您以商品的单品页面说明为准；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品如出现质量问题，请先行按照说明书上的联系方式与厂家的售后部门联系。如果确认属于质量问题，请与</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站客服中心联系办理换货事宜。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">换货时，请您务必将商品的外包装、内带附件、保修卡、说明书、发票等随同商品一起退回。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注：平邮客户以包裹单上的签收日期为主。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">13</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、电脑办公类</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品换货说明请您以商品的单品页面说明为准；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品如出现质量问题，请先行按照说明书上的联系方式与厂家的售后部门联系。如果确认属于质量问题，请与</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站客服中心联系办理换货事宜。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">换货时，请您务必将商品的外包装、内带附件、保修卡、说明书、发票等随同商品一起退回。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注：平邮客户以包裹单上的签收日期为主。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">14</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、家电类</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品如出现质量问题，请先行按照说明书上的联系方式与厂家的售后部门联系，如果确认属于质量问题，持厂家出具质量问题检测报告与</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站客服中心联系办理换货事宜。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">换货时，请您务必将商品的外包装、内带附件、保修卡、说明书、发票等随同商品一起退回。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注：平邮客户以包裹单上的签收日期为主。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">15</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、康体保健器材类商品</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品换货说明请您以商品的单品页面说明为准；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品如出现质量问题，请先行按照说明书上的联系方式与厂家的售后部门联系。如果确认属于质量问题，请与</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站客服中心联系办理换货事宜。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">换货时，请您务必将商品的外包装、内带附件、保修卡、说明书、发票等随同商品一起退回。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注：平邮客户以包裹单上的签收日期为主。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">16</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、汽车用品类</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">汽车养护用品、汽车耗材开封后不换货。（例如车蜡、防护贴膜、清洗剂、车内空气净化、车用油品等）</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品换货说明请您以商品的单品页面说明为准；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品如出现质量问题，请先行按照说明书上的联系方式与厂家的售后部门联系。如果确认属于质量问题，请与</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站客服中心联系办理换货事宜。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">换货时，请您务必将商品的外包装、内带附件、保修卡、说明书、发票等随同商品一起退回。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注：平邮客户以包裹单上的签收日期为主。</span></p>', '0', '2019-08-17 21:40:20', '1');
INSERT INTO `fly_help` VALUES ('40', '5', null, '退货说明', '<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站承诺自顾客收到商品之日起</span><span lang=\\\"EN-US\\\">7</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">日内（以发票日期为准，如无发票以</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站发货清单的日期为准），如符合以下条件，我们将提供全款退货的服务：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、商品及商品本身的外包装没有损坏，保持</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站出售时的原质原样；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、注明退货原因，如果商品存在质量问题，请务必说明；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、确保商品及配件、附带品或者赠品、保修卡、三包凭证、发票、</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站发货清单齐全；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">4</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、如果成套商品中有部分商品存在质量问题，在办理退货时，必须提供成套商品；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">5</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站中的部分商品是由与</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站签订合同的商家提供的，这些商品的退货请与商家联系</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">以下情况不予办理退货：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、任何非由</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站出售的商品；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、任何已使用过的商品，但有质量问题除外；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、任何因非正常使用及保管导致出现质量问题的商品。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">特殊说明：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、食品、保健食品类：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">食品类商品不予退货，但有质量问题除外；如商品过期或距离保质期结束不到</span><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">个月。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、美妆个护类：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">化妆品及个人护理用品属于特殊商品不予退货，但有质量问题除外，如商品包装破损，商品过期或离过期不到</span><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">个月。我们保证商品的进货渠道和质量，如果您在使用时对商品质量表示置疑，请出具书面鉴定，我们会按照国家法律规定予以处理。因个人喜好（气味，色泽、型号，外观）和个人肤质不同要求的退货将无法受理。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、母婴用品：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品签收后不予退货，但有质量问题除外，洗涤方法参考说明，正常缩水</span><span lang=\\\"EN-US\\\">10%</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">以内正常，不属于质量问题。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">因个人原因造成的商品损坏（如自行修改尺寸，洗涤，长时间穿着等），不予退货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品吊牌，包装破损，发货单、商品附件（如纽扣等）、说明书、保修单、标签等丢失，不予退货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">：图片及信息仅供参考，不属质量问题。因拍摄灯光、显示器分辨率等原因可能会造成轻微色差，在网购中属于正常现象，一切以实物为准。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">：品牌商品按其三包约定执行。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">4</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、服装类商品：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">内衣类商品，如内衣裤，袜子，文胸类商品，除质量问题除外，不予退货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">因个人原因造成的商品损坏（如自行修改尺寸，洗涤，长时间穿着等），不予退货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品吊牌，包装破损，发货单、商品附件（如纽扣等）、说明书、保修单、标签等丢失，不予退货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">：图片及信息仅供参考，不属质量问题。因拍摄灯光、显示器分辨率等原因可能会造成轻微色差，在网购中属于正常现象，一切以实物为准。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">：品牌商品按其三包约定执行。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">5</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、鞋帽箱包类：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品吊牌，包装破损，发货单、商品配件（如配饰挂坠等）、说明书、保修单、标签等丢失，不予退货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">：图片及信息仅供参考，不属质量问题。因拍摄灯光、显示器分辨率等原因可能会造成轻微色差，在网购中属于正常现象，一切以实物为准。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">：品牌商品按其三包约定执行。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">6</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、玩具类：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品签收后不予退货，但有质量问题除外。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">：图片及信息仅供参考，不属质量问题。因拍摄灯光、显示器分辨率等原因可能会造成轻微色差，在网购中属于正常现象，一切以实物为准。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">：品牌商品按其三包约定执行。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">7</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、家居类商品：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">因个人原因造成的商品损坏（如自行修改尺寸，洗涤），不予退货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品吊牌，包装破损，发货单、商品配件、说明书、保修单、标签等丢失，不予退货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">：图片及信息仅供参考，不属质量问题。因拍摄灯光、显示器分辨率等原因可能会造成轻微色差，在网购中属于正常现象，一切以实物为准。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">：品牌商品按其三包约定执行。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">8</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、手表类商品：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">手表类商品退货说明请您以商品的单品页面说明为准；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">以下情况不予办理退货：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">如果商品自身携带的产品序列号与</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站售出的不符；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">缺少随商品附带的保修卡、发票、配件等；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品已打开塑封包装或撕开开箱即损贴纸者，但有质量问题除外；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">4)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">将商品存储、暴露在超出商品适宜的环境中；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">5)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">未经授权的修理、误用、疏忽、滥用、事故、改动、不正确的安装；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">6)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">食物或液体溅落造成损坏；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">7)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品使用中出现的磨损，非质量问题。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">8)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">手表表带经过调整，但有质量问题除外。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">9)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">非质量问题。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">9</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、珠宝首饰类及礼品类商品：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">对于附带国家级宝玉石鉴定中心出具的鉴定证书的，非质量问题不予退货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">顾客在收到商品之日起（以发票日期为准）</span><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">个月内，如果出现质量问题，请到当地的质量监督部门</span><span lang=\\\"EN-US\\\">-</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">珠宝玉石质量检验中心进行检测，如检测报告确认属于质量问题，请与</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站客服中心联系办理退货事宜。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">退货时，请您务必将商品的外包装、内带附件、鉴定证书、说明书等随同商品一起退回。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">对于高档首饰都附带国家级宝玉石鉴定中心出具的鉴定证书，如果您对此有任何质疑，请到出具该证书的鉴定机构进行复检。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">瑞士军刀、</span><span lang=\\\"EN-US\\\">zippo</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">打火机、钻石、</span><span lang=\\\"EN-US\\\">18K</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">金，如无质量问题不退货，有质量问题请出示检测报告，方可退货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">为了保证您的利益，请您在收到商品时，仔细检查，如果您发现有任何问题，请您当时指出，并办理退货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">10</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、软件类商品：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">软件类商品退货说明请您以商品的单品页面说明为准；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">如出现质量问题请您直接按照说明书上的联系方式与厂家的售后部门联系解决；已打开塑封包装，不予退换货，但有质量问题除外。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">11</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、手机、数码相机、数码摄像机、笔记本电脑等商品：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">顾客收到商品之日起（以发票日期为准）七日内，有非人为质量问题凭有效检测报告可选择退货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">顾客收到商品之日起（以发票日期为准）八至十五日内，有非人为质量问题凭有效检测报告可选择更换同型号商品。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">顾客收到商品之日起（以发票日期为准）十六日至一年内，有非人为质量问题可在当地保修点免费保修。配件保修请参阅保修卡。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">4)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">为了您的自身权益请妥善保存发票和保修卡，如有发生质量问题请携带发票和保修卡及时到当地检测点检测，以免给您造成不必要的损失。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">5)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">退换货要求：保修卡、发票、</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站发货清单、有效检测报告一律齐全，并且配件完整，包装盒完好，否则将不予受理。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">6)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">全国各地检测、保修点请在保修卡中查找。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">7)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">不接受无检测报告</span><span lang=\\\"EN-US\\\">,</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">并且不在规定时间内的退换货要求。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">8)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">换机产生的邮费由买卖双方各自承担。换货商品一律以邮寄的方式发出。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">9)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">换货地址及联系电话详见各商品页面“售后服务”。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">10)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">如需退货请您先联系</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站客服，在客服人员指导下，一律以邮寄方式完成退货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注：平邮客户以包裹单上的签收日期为主</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">12</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、数码类（手机、数码相机、数码摄像机、笔记本电脑除外）</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品退货说明请您以商品的单品页面说明为准；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品如出现质量问题，请先行按照说明书上的联系方式与厂家的售后部门联系。如果确认属于质量问题，请与</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站客服中心联系办理退货事宜。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">退货时，请您务必将商品的外包装、内带附件、保修卡、说明书、发票等随同商品一起退回。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注：平邮客户以包裹单上的签收日期为主。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">13</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、电脑办公类</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品退货说明请您以商品的单品页面说明为准；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品如出现质量问题，请先行按照说明书上的联系方式与厂家的售后部门联系。如果确认属于质量问题，请与</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站客服中心联系办理退货事宜。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">退货时，请您务必将商品的外包装、内带附件、保修卡、说明书、发票等随同商品一起退回。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注：平邮客户以包裹单上的签收日期为主。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">14</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、家电类</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品如出现质量问题，请先行按照说明书上的联系方式与厂家的售后部门联系，如果确认属于质量问题，持厂家出具质量问题检测报告与</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站客服中心联系办理退货事宜。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">退货时，请您务必将商品的外包装、内带附件、保修卡、说明书、发票等随同商品一起退回。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注：平邮客户以包裹单上的签收日期为主。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">15</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、康体保健器材类商品</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品退货说明请您以商品的单品页面说明为准；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品如出现质量问题，请先行按照说明书上的联系方式与厂家的售后部门联系。如果确认属于质量问题，请与</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站客服中心联系办理退货事宜。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">退货时，请您务必将商品的外包装、内带附件、保修卡、说明书、发票等随同商品一起退回。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注：平邮客户以包裹单上的签收日期为主。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">16</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、汽车用品类</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">汽车养护用品、汽车耗材开封后不予退货。（例如车蜡、防护贴膜、清洗剂、车内空气净化、车用油品等）</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品退货说明请您以商品的单品页面说明为准；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">商品如出现质量问题，请先行按照说明书上的联系方式与厂家的售后部门联系。如果确认属于质量问题，请与</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站客服中心联系办理退货事宜。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">退货时，请您务必将商品的外包装、内带附件、保修卡、说明书、发票等随同商品一起退回。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注：平邮客户以包裹单上的签收日期为主。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\"><br />\r\n</span></p>', '0', '2019-08-17 21:40:24', '1');
INSERT INTO `fly_help` VALUES ('41', '5', null, '退/换货注意事项', '<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、邮寄时请认真填写以下信息，否则将影响您的退</span><span lang=\\\"EN-US\\\">/</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">换货办理：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">　·您的姓名</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">　·收货地址、</span><span lang=\\\"EN-US\\\">Email</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">　·订单号、商品名称和型号</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">　·退</span><span lang=\\\"EN-US\\\">/</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">换货原因</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、如需检验报告的商品</span><span lang=\\\"EN-US\\\">,</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">您还需要提供检验报告，查看退货说明、换货说明；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、请您在收到商品后尽快进行“确认收货”操作，否则将会影响您的退</span><span lang=\\\"EN-US\\\">/</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">换货的办理；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">4</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站的部分商品是由与</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站合作的商家提供的，此商品的退</span><span lang=\\\"EN-US\\\">/</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">换货流程请直接与商家联系。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>', '0', '2019-08-17 21:40:27', '1');
INSERT INTO `fly_help` VALUES ('42', '5', null, '余额的使用与提现', '<p class=\"\\&quot;MsoNormal\\&quot;\"><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">一、账户余额支付：</span></p>\r\n<p class=\"\\&quot;MsoNormal\\&quot;\"><span lang=\"\\&quot;EN-US\\&quot;\">1</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">、“我的</span><span lang=\"\\&quot;EN-US\\&quot;\">iWebShop</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\&quot;\">网站</span><span lang=\"\\&quot;EN-US\\&quot;\">-</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">账户管理”是您在</span><span lang=\"\\&quot;EN-US\\&quot;\">iWebShop</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\&quot;\">网站上的专用帐户。</span></p>\r\n<p class=\"\\&quot;MsoNormal\\&quot;\"><span lang=\"\\&quot;EN-US\\&quot;\">2</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">、账户内的金额是顾客在</span><span lang=\"\\&quot;EN-US\\&quot;\">iWebShop</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\&quot;\">网站购物后余下的现金或通过邮局、银行多余汇款的总和，如下图：</span></p>\r\n<p class=\"\\&quot;MsoNormal\\&quot;\"><span lang=\"\\&quot;EN-US\\&quot;\">3</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">、您可登录“我的</span><span lang=\"\\&quot;EN-US\\&quot;\">iWebShop</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\&quot;\">网站”查询余额。</span></p>\r\n<p class=\"\\&quot;MsoNormal\\&quot;\"><span lang=\"\\&quot;EN-US\\&quot;\">4</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">、在订单结算时，系统将自动使用您的账户余额，您只需支付其余货款：</span></p>\r\n<p class=\"\\&quot;MsoNormal\\&quot;\"><span lang=\"\\&quot;EN-US\\&quot;\">5</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">、如果您的账户余额足以支付订单，您仍需选择一种支付方式，否则将无法提交订单。</span></p>\r\n<p class=\"\\&quot;MsoNormal\\&quot;\"><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">二、账户余额提现</span></p>\r\n<p class=\"\\&quot;MsoNormal\\&quot;\"><span lang=\"\\&quot;EN-US\\&quot;\">iWebShop</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">网站为您提供了账户余额提现功能，您可以将您在</span><span lang=\"\\&quot;EN-US\\&quot;\">iWebShop</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\&quot;\">网站账户余额中的可用余额提取为现金，我们会已邮局汇款的方式退给您。</span></p>\r\n<p class=\"\\&quot;MsoNormal\\&quot;\"><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">账户余额提现的流程：</span></p>\r\n<p class=\"\\&quot;MsoNormal\\&quot;\"><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">温馨提示：</span></p>\r\n<p class=\"\\&quot;MsoNormal\\&quot;\"><span lang=\"\\&quot;EN-US\\&quot;\">1</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">、账户余额内的现金只能以邮局汇款方式提现；</span></p>\r\n<p class=\"\\&quot;MsoNormal\\&quot;\"><span lang=\"\\&quot;EN-US\\&quot;\">2</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">、每日提现次数不超过</span><span lang=\"\\&quot;EN-US\\&quot;\">1</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">次；</span></p>\r\n<p class=\"\\&quot;MsoNormal\\&quot;\"><span lang=\"\\&quot;EN-US\\&quot;\">3</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">、提现账户余额，需向邮局支付一定比例的手续费：</span><span lang=\"\\&quot;EN-US\\&quot;\">200</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">元以下</span><span lang=\"\\&quot;EN-US\\&quot;\">2</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">元，</span><span lang=\"\\&quot;EN-US\\&quot;\">200</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">元以上</span><span lang=\"\\&quot;EN-US\\&quot;\">1%</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">，最高不超过</span><span lang=\"\\&quot;EN-US\\&quot;\">50</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">元；</span></p>\r\n<p class=\"\\&quot;MsoNormal\\&quot;\"><span lang=\"\\&quot;EN-US\\&quot;\">4</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">、账户余额提现服务暂不支持国外和港澳台地区；</span></p>\r\n<p class=\"\\&quot;MsoNormal\\&quot;\"><span lang=\"\\&quot;EN-US\\&quot;\">5</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">、若您提现失败，邮局不退回相应的手续费；</span></p>\r\n<p class=\"\\&quot;MsoNormal\\&quot;\"><span lang=\"\\&quot;EN-US\\&quot;\">6</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">、提现金额不可大于可用余额；</span></p>\r\n<p class=\"\\&quot;MsoNormal\\&quot;\"><span lang=\"\\&quot;EN-US\\&quot;\">7</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">、申请提现后，</span><span lang=\"\\&quot;EN-US\\&quot;\">iWebShop</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\&quot;\">网站处理时限是</span><span lang=\"\\&quot;EN-US\\&quot;\">3</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">个工作日，邮局处理时限是</span><span lang=\"\\&quot;EN-US\\&quot;\">14</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">个工作日；</span></p>\r\n<p class=\"\\&quot;MsoNormal\\&quot;\"><span lang=\"\\&quot;EN-US\\&quot;\">8</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">、常见提现失败原因：</span><span lang=\"\\&quot;EN-US\\&quot;\">1</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">）逾期退汇；</span><span lang=\"\\&quot;EN-US\\&quot;\">2</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">）地址不详；</span><span lang=\"\\&quot;EN-US\\&quot;\">3</span><span style=\"\\&quot;font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\&quot;\">）原地址查无此人</span></p>\r\n<p class=\"\\&quot;MsoNormal\\&quot;\"><span lang=\"\\&quot;EN-US\\&quot;\"><o:p>&nbsp;</o:p></span></p>', '0', '2019-08-17 21:40:30', '1');
INSERT INTO `fly_help` VALUES ('43', '5', null, '发票制度', '<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">发票政策</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、发票性质</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站提供的是“</span><span lang=\\\"EN-US\\\">*****</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">公司销售商品专用发票”或“</span><span lang=\\\"EN-US\\\">*****</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">有限公司销售商品专用发票”</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、发票信息</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">发票抬头：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）发票抬头不能为空；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）您可填写：“个人”、您的姓名、或您的单位名称</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">发票内容：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站可开具的发票内容：图书、音像、游戏、软件、资料、办公用品、</span><span lang=\\\"EN-US\\\">IT</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">数码、通讯器材、体育休闲、礼品、饰品、汽车用品、化妆品、家用电器、玩具、箱包皮具，请您根据需要选择</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）数码、手机、家电类商品的发票内容只能开具商品名称和型号，无法开具其它内容</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">发票金额：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站仅开具现金购物金额的发票，不含运费、礼券、礼品卡金额</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">温馨提示：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）请您在收货时向送货员索取运费发票</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）此政策仅适用于</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站自营，若您订购商店街的商品，请与商家联系确认</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">索取发票</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">请在提交订单时的结算页面，选择“索取发票”，按照提示填写发票抬头、选择发票内容，发票将会随您的订单商品一起送达：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">温馨提示：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">若您订购了数码、手机、家电类商品，</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站只能将发票内容开具商品的名称和型号</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）补开</span><span lang=\\\"EN-US\\\">/</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">换开发票期限：订单发货后一年以内</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）若您提交订单时未选择发票，请接收到商品后在补开发票期限内</span><span lang=\\\"EN-US\\\">,</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">发邮件至客服邮箱</span><span lang=\\\"EN-US\\\">service@cs.dangdang.com</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">，并注明您的订单号、发票抬头、发票内容、邮寄地址、邮编及收件人，我们会在五个工作日内为您开具发票并以平信方式为您寄出</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）若您接收到的发票信息有误，请在换开发票期限内，将原发票寄至以下地址，同时请务必注明您的订单号、正确的发票抬头、内容、新发票的邮寄地址、邮编、收件人，我们收到后，会在五个工作日内为您重新开具发票以平信方式寄出</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">邮寄地址：</span><span lang=\\\"EN-US\\\">*****</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">信箱</span><span lang=\\\"EN-US\\\">,</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">邮编：</span><span lang=\\\"EN-US\\\">000000</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">温馨提示：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）若您订购的是数码、手机、家电类商品，发票内容只能开具商品名称和型号，无法开具其他内容</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（</span><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）若您订购商店街的商品，请与商家联系索取发票</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>', '0', '2019-08-17 21:40:51', '1');
INSERT INTO `fly_help` VALUES ('44', '7', null, '关于我们', '<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">aircheng</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">技术团队</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">济南爱程网络科技有限公司（</span><span lang=\\\"EN-US\\\">aircheng Tech</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">）专注于基于开源协作的云计算及云服务技术，其高负载高扩展能力的分布式计算与服务技术体系，已成为开源社区软件领域领先的云计算技术平台，该平台通用产品化的名称为</span><span lang=\\\"EN-US\\\">iWeb SuperInteraction</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">，简称</span><span lang=\\\"EN-US\\\">iWebSI</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">。</span><span lang=\\\"EN-US\\\">aircheng Tech</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">的云计算技术框架可涵盖</span><span lang=\\\"EN-US\\\">Internet</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">和移动互联网。依托具有自主知识产权的云技术平台，</span><span lang=\\\"EN-US\\\">aircheng Tech</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">发起了一系列高负载高度交互类的开源软件产品，其产品线由社会化网络服务</span><span lang=\\\"EN-US\\\">(iWebSNS)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">、社区电子商务</span><span lang=\\\"EN-US\\\">(iWebShop&amp;Mall)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、即时通讯服务</span><span lang=\\\"EN-US\\\">(iWebIM)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">等产品构成。由于领先的高负载技术体系和先进的产品理念，</span><span lang=\\\"EN-US\\\">aircheng Tech</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">从一开始运作就得到天使投资人的注资。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">aircheng Tech</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">的产品愿景和团队理念是“开放、分享、共赢、丰富互联网”。</span><span lang=\\\"EN-US\\\">aircheng Tech</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">提倡互联网的开放、创新和共创机遇，而不单单像盈利或者商业组织那样只强调商业利益。并且，</span><span lang=\\\"EN-US\\\">aircheng Tech</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">把成功定义为丰富、繁荣的互联网市场面貌。在这种环境下，互联网上的众多站点应该呈现出不同的形态、风貌，体现出个性化，真正塑造每一个站点的“性格”，让每一个站点“</span><span lang=\\\"EN-US\\\">Live</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">起来”，避免同质化。从而真正做到用技术实现创意，用创意丰富生活，使人们的网络生活变得更加丰富多彩。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">airchengTech</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">遵循开源社区互动提升产品的原则，不闭门造车。这也是开源软件的精髓所在。所以和</span><span lang=\\\"EN-US\\\">aircheng Tech</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">在一起，即便是个人站长，也可以自信的说：“我不是一个人！”做永远的</span><span lang=\\\"EN-US\\\">Beta</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">版，</span><span lang=\\\"EN-US\\\">aircheng Tech</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">正在、也会一直这样：通过与用户构建良好互动，倾听用户的意见和批评，吸取大众的智慧，来改善产品，形成产品发布</span><span lang=\\\"EN-US\\\">-</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">用户使用</span><span lang=\\\"EN-US\\\">-</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">用户反馈</span><span lang=\\\"EN-US\\\">-</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">产品改进的良性循环。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">aircheng</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">的团队宗旨</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">致力于帮助在线企业平滑实现规模化。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">aircheng</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">的服务口号</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">用我们领先的技术，服务于您的全球客户。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>', '0', '2019-08-17 21:40:55', '1');
INSERT INTO `fly_help` VALUES ('45', '7', null, '常见问题', '<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站所售商品都是正品行货吗？有售后保修吗？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站所售商品都是正品行货，均自带机打发票。凭</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站发票，所有商品都可以享受生产厂家的全国联保服务。</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站将严格按照国家三包政策，针对所售商品履行保修、换货和退货的义务。您也可以到</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站任一分公司售后部享受售后服务。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：购买的商品能开发票？如果是公司购买，可以开增值税发票吗？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站所售商品都是正品行货，每张订单均自带中文机打的“商品专用发票”，此发票可用作单位报销凭证。发票会随包裹一同发出，发票金额含配送费金额。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">企业客户在提供《一般纳税人证书》、《营业执照》、《税务登记证》、《开户许可证》四类证件复印件后，可向</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站开取增值税发票，开好后，</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站会以快递方式为您寄出。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：各种库存状态是什么意思？下单多久后可以发货？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：现货：库存有货，下单后会尽快发货，您可以立即下单；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">在途：商品正在内部配货，一般</span><span lang=\\\"EN-US\\\">1-2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">天有货，您可立即下单；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">预订：商品正在备货，一般下单后</span><span lang=\\\"EN-US\\\">2-20</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">天可发货，您可立即下单；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">无货：商品已售完，相应物流中心覆盖地区内的用户不能下单购买。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：无货商品什么时候能到货？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：无货商品的到货时间根据配货情况而不同，无法准确估计，但您可以使用“到货通知”功能，一旦商品有货，我们会通过电子邮件等方式通知您。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：下单后何时可以收到货？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：在商品有现货的情况下，下单后一般</span><span lang=\\\"EN-US\\\">24</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">小时内可收到货（郊区县配送时间可能会更长一些）；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">其它地区用户，将根据您的收货地址及所选择的配送方式而不同，一般到货时间在</span><span lang=\\\"EN-US\\\">1-7</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">天（极偏远地区配送时间可能会更长一些）；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">如果商品处于预订或在途状态，那么还应加上调配货时间。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：快递费是多少？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：凡选用“</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站快递”或“快递运输”的会员即可享受免运费优惠。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：在线支付支持哪些银行卡？支持大额支付吗？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：我们为您提供几乎全部银行的银行卡及信用卡在线支付，只要您开通了“网上支付”功能，即可进行在线支付，无需手续费，实时到帐，方便快捷。（如客户原因取消订单退款，则需要客户承担</span><span lang=\\\"EN-US\\\">1%</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">平台手续费）如您订单金额较大，可以使用快钱支付中的招行、工行、建行、农行、广发进行一次性大额支付（一万元以下）。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：在</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站购物支持信用卡分期付款吗？如何申请？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站目前支持中国银行、招商银行两家银行的信用卡分期付款，只要商品单价在</span><span lang=\\\"EN-US\\\">500</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">元以上，您即可点击“信用卡分期付款”按钮申请分期付款</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：上门提货、货到付款支持刷卡吗？周末可以自提吗？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站全部自提点均支持现金及刷卡支付，绝大部分货到付款地区支持现金及刷卡支付</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站自提点营业时间一律为：周一至周日，</span><span lang=\\\"EN-US\\\">09:00</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">－</span><span lang=\\\"EN-US\\\">19:00</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">（如遇法定假日，以商城新闻公告为准）。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：下单时可以指定送货时间吗？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：可以，您下单时可以选择“只工作日送货</span><span lang=\\\"EN-US\\\">(</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">双休日、假日不用送</span><span lang=\\\"EN-US\\\">)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">”、“工作日、双休日与假日均可送货”、“只双休日、假日送货</span><span lang=\\\"EN-US\\\">(</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">工作日不用送</span><span lang=\\\"EN-US\\\">)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">”等时间类型，并选择是否提前电话确认。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">另外，您还可以在订单备注里填写更具体的需求，我们会尽量满足您的要求。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：哪些地区支持货到付款？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站已在多个省市开通了货到付款</span><span lang=\\\"EN-US\\\">(</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">其它城市正陆续开通</span><span lang=\\\"EN-US\\\">)</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">，您可使用现金、移动</span><span lang=\\\"EN-US\\\">POS</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">机当面付款收货</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：收货时发现问题可以拒收吗？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：在签收货物时如发现货物有损坏，请直接拒收退回我公司，相关人员将为您重新安排发货。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：如果我刚刚下单商品就降价了，能给我补偿吗？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站的商品价格随市场价格的波动每日都会有涨价、降价或者优惠等变化。如果下完订单后价格发生了变化，可到“我的</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站”自主申请价格保护</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：下单后，我能做什么？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：如果是在线支付方式，请您尽快完成付款，待付款被确认后我们会立即为您发货，</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">如果选择自提或货到付款，您可以进入“我的</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站”，在“订单列表”中找到您的订单，然后可随时查看订单处理状态，做好收货或者上门自提的准备。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">在您成功购物后，您还可以发表商品评价，向其他用户分享商品使用心得。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：为什么我无法登陆商城？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：首先要检查您的用户名、密码是否正确，确认您的浏览器是否支持</span><span lang=\\\"EN-US\\\">COOKIE</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：产品如何保修？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站销售的商品都以商品说明中的保修承诺为准。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：订单得到确认后我该做什么？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：按照订单所提示的实际应汇款金额，汇款至该订单所在的公司账号内，汇款交易成功后，登陆“我的</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站”查看您的订单，在订单中的“汇款备注”中输入您的相关汇款信息</span><span lang=\\\"EN-US\\\">(</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">例如：汇入行、汇入我司银行账号的实际金额、汇款日期和汇入账号、订单号等），等待我司财务人员确认汇款。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：汇款确认后多久能够将货物发出？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：正常情况下会在工作时间</span><span lang=\\\"EN-US\\\">24-48</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">小时内可以将您的货物发出。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：非商品自身质量问题是否可以退货？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：部分商品在不影响二次销售的情况下，加收一定的退货手续费，是可以办理退货的，详情请查看“退换货政策”</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：在哪能填写汇款信息？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：首先要在</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站首页的“会员登录”中输入用户名和密码进行登陆，登陆后点击“我的</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站”，点击左侧的“订单中心”，即可查看到您所有的订单，点击汇款订单后面的“查看”，打开后下拉页面，有“付款信息未完成</span><span lang=\\\"EN-US\\\">,</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">请您尽快填写</span><span lang=\\\"EN-US\\\">.</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">”一项，直接在里面填写汇款信息，然后提交即可，相关人员在查收到您的汇款信息后会进行核实，无异议的汇款会在三个工作小时内确认完毕，如有问题，相关人员会电话与您联系。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：怎样咨询商品的详细信息？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：请您在该商品页面下方“购买咨询”处进行提问，相关商品管理员会为您回复。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：在哪进行在线支付？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：在</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站首页的“会员登录”中输入用户名和密码进行登陆，登陆后点击“我的</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站”，进入后点击左侧的“在线支付”，点击进入后就可以进行在线支付了。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：工作时间？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：客服中心受理热线电话及订单处理时间为</span><span lang=\\\"EN-US\\\">7x24</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">小时全天候服务；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">自提接待时间为周一至周日</span><span lang=\\\"EN-US\\\">9</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">：</span><span lang=\\\"EN-US\\\">00-19</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">：</span><span lang=\\\"EN-US\\\">00</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注：如遇国家法定节假日，则以</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站新闻发布放假时间为准，请大家届时关注。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：如何将退款打回银行卡？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：在投诉中心留言相关信息，如银行卡的开户行</span><span lang=\\\"EN-US\\\">(</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">详细到支行）、开户姓名、卡号，相关人员会为您处理，退款周期视您的货物是否发出而定，如果货物未出库发出，退款会在三个工作日内完成；如果货物已发出，则需货物返回我司物流中心后为您办理退款。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：商品包装问题？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：我司所发送商品均由专人进行打包，商品在未签收前都由我司负责，如在收到商品时发现包装有破损或是其它方面问题，请直接致电我司客服</span><span lang=\\\"EN-US\\\">*****</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">，客服人员会帮您解决。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：怎样申请高校代理送货？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：在</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站首页的“会员登录”中输入用户名和密码进行登陆，登陆后点击“我的</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站”，进入后点击左侧的“个人资料”，在“所在学校”一栏中选择您所在的院校，</span><span lang=\\\"EN-US\\\">(</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">如没有您所在的院校，则说明您的学校暂未开通高校代理，您将无法选择高校代理送货），然后点击底部的“修改”，我司相关人员在收到申请后的</span><span lang=\\\"EN-US\\\">24</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">个工作小时内进行审核，审核通过后，您下单时就可以选择高校代理送货了，高校代理订单的运费按照钻石（双钻）会员普通快递运费标准收取，具体请您参照帮助中心中快递运输页面的“普通快递收取标准一览表”，货物由代理直接送达，货款由高校代理收取。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：拍卖成功后如何转成订单？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：在</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站首页的“会员登录”中输入用户名和密码进行登陆，登陆后点击“我的</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站”，进入后点击左侧的“我的拍卖”，在“操作”处有一个“转成订单”按钮，点击该按钮就可以转成订单了，在左侧“订单中心”处可查询到该订单，和商品订单一样，您可以直接进行支付。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：订单付款后，如果长时间未收到货，我是否可以申请办理退款？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：非</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站快递覆盖区域内，由第三方快递公司负责直接送达的订单，如圆通快递，自发货时间算起超过</span><span lang=\\\"EN-US\\\">10</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">天仍未收到货或收货地址超出第三方快递覆盖的区域，由第三方快递转邮政，如圆通转</span><span lang=\\\"EN-US\\\">EMS</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">等，自发货时间算起超过</span><span lang=\\\"EN-US\\\">20</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">天仍未收到货，可致电客服中心，由客服人员为您申请办理退款事宜。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">问：如果我有问题或建议是否可以通过邮件向你们反馈？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">答：可以。</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站受理建议或投诉的邮箱是：</span><span lang=\\\"EN-US\\\">service@iwebshop.com</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>', '0', '2019-08-17 21:41:01', '1');
INSERT INTO `fly_help` VALUES ('46', '7', null, '找回密码', '<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">忘记了帐户密码？</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">不用担心，</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站提供找回密码服务，您点击</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family: Calibri\\\">忘记密码</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">按照系统提示操作即可。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">操作步骤详解如下：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、在</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站登陆页面，点击</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">忘记密码</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、按照提示，填写您在</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站的注册邮箱及验证码</span><span lang=\\\"EN-US\\\"><span style=\\\"mso-spacerun:yes;\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、系统提示成功发送“密码重置”邮件，若您长时间未收到，可以点击“重新发送”</span><span lang=\\\"EN-US\\\"><span style=\\\"mso-spacerun:yes;\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">4</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、登陆您的个人邮箱，找到“</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站新密码重置确认信</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站新密码重置确认信”点击“设置新密码”</span><span lang=\\\"EN-US\\\"><span style=\\\"mso-spacerun:yes;\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">5</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、按照系统提示，设置新密码即可</span><span lang=\\\"EN-US\\\"><span style=\\\"mso-spacerun:yes;\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">温馨提示：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">为了确保顾客注册信息的安全，</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站只提供网上找回密码服务，若您忘记</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站注册邮箱或是忘记注册邮箱的登陆密码，请</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">注册新用户</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>', '0', '2019-08-17 21:41:04', '1');
INSERT INTO `fly_help` VALUES ('47', '7', null, '退订邮件/短信', '<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站保留通过邮件和短信的形式，对本网站注册、购物用户发送订单信息、促销活动等告知服务的权利。如果您在</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:calibri;mso-hansi-font-family:Calibri;\\\">网站注册、购物，表明您已默示接受此项服务。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">若您不希望接收</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站的邮件，请在邮件下方输入您的</span><span lang=\\\"EN-US\\\">E-mail</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">地址自助完成退阅；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">若您不希望接收</span><span lang=\\\"EN-US\\\">iWebShop</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">网站的短信，请提供您的手机号码</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family: Calibri\\\">联系客服</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">处理。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>', '0', '2019-08-17 21:41:09', '1');
INSERT INTO `fly_help` VALUES ('48', '7', null, '联系客服', '<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">邮件联系</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">Kefu@aircheng.com</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">电话联系</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">客服中心电话热线工作时间：</span><span lang=\\\"EN-US\\\">24</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">小时全天候</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">客服热线：</span><span lang=\\\"EN-US\\\">*****</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">客服传真：</span><span lang=\\\"EN-US\\\">*****</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">邮局汇款地址：</span><span lang=\\\"EN-US\\\">*****</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">信箱</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family: Calibri\\\">邮编：</span><span lang=\\\"EN-US\\\">*****</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">邮政信箱地址：</span><span lang=\\\"EN-US\\\">*****</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">分箱</span> <span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family: Calibri\\\">邮编：</span><span lang=\\\"EN-US\\\">*****</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">在线问答</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\">iWebIM</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">在线客服</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>', '0', '2019-08-17 21:41:14', '1');
INSERT INTO `fly_help` VALUES ('49', '7', null, '诚聘英才', '<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">测试工程师</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">岗位职责：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><span style=\\\"mso-spacerun:yes;\\\">&nbsp;&nbsp;&nbsp; </span>1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、负责公司互联网产品和项目的测试工作，搭建测试环境，确保产品和项目质量；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><span style=\\\"mso-spacerun:yes;\\\">&nbsp;&nbsp;&nbsp; </span>2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、编写测试计划，测试大纲和测试用例；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><span style=\\\"mso-spacerun:yes;\\\">&nbsp;&nbsp;&nbsp; </span>3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、对测试过程中发现的问题进行跟踪分析和报告并推动问题及时合理地解决；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><span style=\\\"mso-spacerun:yes;\\\">&nbsp;&nbsp;&nbsp; </span>4</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、按照测试计划编写测试脚本和测试程序对产品进行功能、强度、性能测试；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><span style=\\\"mso-spacerun:yes;\\\">&nbsp;&nbsp;&nbsp; </span>5</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、通过对产品的测试，保证产品质量达到指定质量目标，并能够提出进一步改进的要求并依照执行。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">岗位要求：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><span style=\\\"mso-spacerun:yes;\\\">&nbsp;&nbsp;&nbsp; </span>1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、相关专业专科以上学历，至少</span><span lang=\\\"EN-US\\\">1</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">年以上测试经验；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><span style=\\\"mso-spacerun:yes;\\\">&nbsp;&nbsp;&nbsp; </span>2</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、熟悉测试流程，测试用例与测试计划的编写；熟悉各种</span><span lang=\\\"EN-US\\\">Bug</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">管理和测试管理工具；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><span style=\\\"mso-spacerun:yes;\\\">&nbsp;&nbsp;&nbsp; </span>3</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、较强的发现问题，分析问题的能力；较强的语言表达和文档撰写能力，能根据产品需求编写测试用例；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><span style=\\\"mso-spacerun:yes;\\\">&nbsp;&nbsp;&nbsp; </span>4</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、工作责任心强，细致，耐心；</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><span style=\\\"mso-spacerun:yes;\\\">&nbsp;&nbsp;&nbsp; </span>5</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">、能承受较大的工作压力。</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">联系人：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">联系方式：</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>', '0', '2019-08-17 21:41:18', '1');
INSERT INTO `fly_help` VALUES ('50', '7', null, '友情链接', '<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">申请友链</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">链接显示的顺序以提交的先后顺序为准</span><span lang=\\\"EN-US\\\">.</span></p>\r\n<p class=\\\"MsoNormal\\\"><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">申请链接请将你的网站名称</span><span lang=\\\"EN-US\\\">\\\\logo\\\\</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">链接地址</span><span lang=\\\"EN-US\\\">\\\\</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">联系人等信息发至信箱：</span><span lang=\\\"EN-US\\\">admin@iwebshop.com,</span><span style=\\\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\\\">经我们网站管理员审核后再更新上线</span></p>\r\n<p class=\\\"MsoNormal\\\"><span lang=\\\"EN-US\\\"><o:p>&nbsp;</o:p></span></p>', '0', '2019-08-17 21:41:23', '1');
INSERT INTO `fly_help` VALUES ('52', '5', null, '售后服务', '<div style=\"display:block;\" class=\"mc tabcon hide\">\r\n                    本产品全国联保，享受三包服务，质保期为：十五天质保<br />\r\n				</div>\r\n				\r\n				\r\n				<div id=\"promises\">\r\n					<strong>Iweb商城服务承诺：</strong><br />\r\n<strong>Iweb</strong>商城向您保证所售商品均为正品行货，自带机打发票，与商品一起寄送。凭质保证书及<strong>Iweb</strong>商城发票，可享受全国联保服务，与您亲临商场选购的商品享受相同的质量保证。<br />\r\n<strong>Iweb</strong>商城还为您提供具有竞争力的商品价格和免运费政策，请您放心购买！ \r\n				</div>\r\n				<div id=\"state\"><strong>声明:</strong>因厂家会在没有任何提前通知的情况下更改产品包装、产地或者一些附件，本司不能确保客户收到的货物与商城图片、产地、附件说明完全一致。只能确保为原厂正货！并且保证与当时市场上同样主流新品一致。若本商城没有及时更新，请大家谅解！</div>', '0', '2019-08-17 21:41:26', '1');
INSERT INTO `fly_help` VALUES ('53', '4', null, '支付帮助', 'Iweb商城为您提供以下7种支付方式<br />\r\n货到付款： &nbsp;&nbsp;&nbsp; <br />\r\n<br />\r\n我们在以下省市开通了货到付款(其他城市正陆续开通)，您可使用现金、移动POS机（部分地区支持刷卡）当面付款收货，点击城市名可查看详细配送范围及运费：<br />\r\n北京&nbsp;&nbsp; 上海&nbsp;&nbsp; 广州&nbsp;&nbsp; 广东（不含广州）&nbsp;&nbsp; 天津&nbsp;&nbsp; 杭州&nbsp;&nbsp; 山东&nbsp;&nbsp; 厦门&nbsp;&nbsp; 武汉&nbsp;&nbsp; 成都&nbsp;&nbsp; 深圳&nbsp;&nbsp; 西安&nbsp;&nbsp; 宁波&nbsp;&nbsp; 东莞&nbsp;&nbsp; 沈阳&nbsp;&nbsp; 福州&nbsp;&nbsp; 重庆&nbsp;&nbsp; 温州&nbsp;&nbsp; 长沙&nbsp;&nbsp; 哈尔滨&nbsp;&nbsp; 佛山&nbsp;&nbsp; 郑州&nbsp;&nbsp; 嘉兴&nbsp;&nbsp; 廊坊&nbsp;&nbsp; 绍兴&nbsp;&nbsp; 金华&nbsp;&nbsp; 珠海&nbsp;&nbsp; 太原&nbsp;&nbsp; 大连&nbsp;&nbsp; 长春&nbsp;&nbsp; 南昌&nbsp;&nbsp; 合肥&nbsp;&nbsp; 昆明&nbsp;&nbsp; 石家庄&nbsp;&nbsp; 浙江&nbsp;&nbsp; 贵州&nbsp;&nbsp; 兰州&nbsp;&nbsp; 南宁&nbsp;&nbsp; 呼和浩特&nbsp;&nbsp; 江苏&nbsp;&nbsp; 四川&nbsp;&nbsp; 惠州&nbsp;&nbsp; 烟台<br />\r\n在线支付： &nbsp;&nbsp;&nbsp; <br />\r\n<br />\r\n我们为您提供几乎全部银行的银行卡及信用卡在线支付，只要您开通了\"网上支付\"功能，即可进行在线支付，无需手续费，实时到帐，方便快捷，支付限额说明&gt;&gt;<br />\r\n<br />\r\n您还可以使用以下支付平台进行在线支付及帐户余额付款：<br />\r\n来Iweb自提： &nbsp;&nbsp;&nbsp; <br />\r\n<br />\r\n我们在以下城市开通了自提点(其他城市正陆续开通)，您可就近选择自提点当面付款提货，无需支付运费，点击城市名可查看详细地点及公交线路：<br />\r\n北京&nbsp;&nbsp; 上海&nbsp;&nbsp; 广州&nbsp;&nbsp; 深圳&nbsp;&nbsp; 东莞&nbsp;&nbsp; 佛山&nbsp;&nbsp; 珠海&nbsp;&nbsp; 惠州&nbsp;&nbsp; 天津&nbsp;&nbsp; 苏州&nbsp;&nbsp; 无锡&nbsp;&nbsp; 南京&nbsp;&nbsp; 宿迁&nbsp;&nbsp; 昆山&nbsp;&nbsp; 南通&nbsp;&nbsp; 常州&nbsp;&nbsp; 常熟&nbsp;&nbsp; 杭州&nbsp;&nbsp; 宁波&nbsp;&nbsp; 温州&nbsp;&nbsp; 嘉兴&nbsp;&nbsp; 绍兴&nbsp;&nbsp; 金华&nbsp;&nbsp; 济南&nbsp;&nbsp; 青岛&nbsp;&nbsp; 烟台&nbsp;&nbsp; 厦门&nbsp;&nbsp; 福州&nbsp;&nbsp; 武汉&nbsp;&nbsp; 成都&nbsp;&nbsp; 绵阳&nbsp;&nbsp; 西安&nbsp;&nbsp; 沈阳&nbsp;&nbsp; 大连&nbsp;&nbsp; 重庆&nbsp;&nbsp; 长沙&nbsp;&nbsp; 哈尔滨&nbsp;&nbsp; 郑州&nbsp;&nbsp; 廊坊&nbsp;&nbsp; 太原&nbsp;&nbsp; 长春&nbsp;&nbsp; 南昌&nbsp;&nbsp; 合肥&nbsp;&nbsp; 昆明&nbsp;&nbsp; 石家庄&nbsp;&nbsp; 贵阳&nbsp;&nbsp; 兰州&nbsp;&nbsp; 南宁&nbsp;&nbsp; 呼和浩特<br />\r\n分期付款： &nbsp;&nbsp;&nbsp; <br />\r\n<br />\r\n单个商品价格在500元以上，可使用中国银行、招商银行发行的信用卡申请分期付款，支持3期、6期、12期付款，查看详细说明&gt;&gt;<br />\r\n公司转帐： &nbsp;&nbsp;&nbsp; <br />\r\n<br />\r\n您可以向Iweb公司的三个公司帐户汇款，到帐时间一般为款汇出后的1-5个工作日，查看公司帐户&gt;&gt;<br />\r\n邮局汇款： &nbsp;&nbsp;&nbsp; 您可通过邮局向Iweb商城付款，到帐时间一般为款汇出后的1-5个工作日，查看汇款地址&gt;&gt;<br />', '0', '2019-08-17 21:41:29', '1');

-- ----------------------------
-- Table structure for `fly_help_column`
-- ----------------------------
DROP TABLE IF EXISTS `fly_help_column`;
CREATE TABLE `fly_help_column` (
  `id` bigint(20) NOT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `site_id` bigint(20) DEFAULT NULL,
  `short_url` varchar(10) DEFAULT NULL COMMENT '短连接',
  `column_title` varchar(100) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `sort_order` int(4) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `verify` tinyint(2) DEFAULT NULL,
  `deleted` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of fly_help_column
-- ----------------------------

-- ----------------------------
-- Table structure for `fly_log`
-- ----------------------------
DROP TABLE IF EXISTS `fly_log`;
CREATE TABLE `fly_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `addtime` int(11) DEFAULT NULL,
  `goalid` int(11) DEFAULT NULL,
  `isdelete` smallint(6) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='用户动作日志（暂未用到，已废弃，由阿里云日志服务取代）';

-- ----------------------------
-- Records of fly_log
-- ----------------------------

-- ----------------------------
-- Table structure for `fly_message`
-- ----------------------------
DROP TABLE IF EXISTS `fly_message`;
CREATE TABLE `fly_message` (
  `id` bigint(20) NOT NULL,
  `sender_id` bigint(20) unsigned NOT NULL COMMENT '自己的userid，发信人的userid，若是为0则为系统信息',
  `recipient_id` bigint(20) unsigned NOT NULL COMMENT '给谁发信，这就是谁的userid，目标用户的userid，收信人id',
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '短信主题',
  `content` varchar(4000) COLLATE utf8_unicode_ci NOT NULL,
  `add_time` datetime NOT NULL COMMENT '此信息的发送时间',
  `update_time` datetime DEFAULT NULL COMMENT '最后修改之间',
  `state` tinyint(3) unsigned NOT NULL COMMENT '0:未发送，1:已发送未读，2:已读 ',
  `deleted` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否已经被删除。0正常，1已删除，',
  PRIMARY KEY (`id`),
  KEY `self` (`sender_id`,`recipient_id`,`add_time`,`state`,`deleted`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='发信箱，发信，用户发信表，站内信。（暂未用到，预留）';

-- ----------------------------
-- Records of fly_message
-- ----------------------------

-- ----------------------------
-- Table structure for `fly_news`
-- ----------------------------
DROP TABLE IF EXISTS `fly_news`;
CREATE TABLE `fly_news` (
  `id` bigint(20) NOT NULL,
  `site_id` bigint(20) DEFAULT '0' COMMENT '所属站点，对应site.id',
  `column_id` bigint(20) DEFAULT '0' COMMENT '所属栏目id，对应site_column.id',
  `news_type` tinyint(2) DEFAULT '0' COMMENT '1新闻；2图文；3独立页面',
  `title` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `shorttitle` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '简短标题',
  `titlepic` char(100) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '头图',
  `writer` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '作者',
  `source` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '文章来源',
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '新闻内容',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '简介,从内容正文里自动剪切出开始的160个汉字',
  `keywords` varchar(120) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'SEO关键词',
  `count_digg` int(11) NOT NULL DEFAULT '0' COMMENT '支持的总数量',
  `count_burys` int(11) DEFAULT '0' COMMENT '反对的总数量',
  `count_view` int(11) DEFAULT '0' COMMENT '阅读的总数量',
  `count_comment` int(11) DEFAULT '0' COMMENT '评论的总数量',
  `status` tinyint(2) DEFAULT '1' COMMENT '1：正常显示；2：隐藏不显示',
  `user_id` bigint(20) DEFAULT '0' COMMENT '文章发布人',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `legitimate` tinyint(2) DEFAULT '1' COMMENT '是否是合法的，1是，0不是，涉嫌',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`),
  KEY `userid` (`news_type`,`count_view`,`count_comment`,`column_id`,`status`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='网站的新闻、产品、独立页面等，都是存储在这';

-- ----------------------------
-- Records of fly_news
-- ----------------------------
INSERT INTO `fly_news` VALUES ('382654249068408856', '378190751797870592', '368263538935857152', '0', '的发送到发送到', '对方水电费水电费水电费', '', null, null, '第三方士大夫士大夫', null, null, '0', '0', '0', '0', '1', '366638723963551744', '2019-10-18 19:34:45', '2019-10-18 19:34:41', '1', '0');
INSERT INTO `fly_news` VALUES ('389949475503734784', '378190751797870592', '381768105317105664', '0', 'dfsdfsdfsdfsdsdf', null, '', null, null, 'dfdsdfsdfsdfsdf', 'dfsfsdfsdfd', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-07 22:41:06', null, '1', '0');
INSERT INTO `fly_news` VALUES ('389949720929239040', '378190751797870592', '381768105317105664', '0', '测试测试测试测试测试测试测试测试测试测试测试测试', null, '', null, null, '测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试', '测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-07 22:42:04', null, '1', '0');
INSERT INTO `fly_news` VALUES ('389949996838944768', '378190751797870592', '381768105317105664', '0', '风格化风格化风格化风格化', null, '', null, null, '风格化风格化风格化风格化法国风格化', '风格恢复好风格化风格化风格化', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-07 22:43:10', null, '1', '0');
INSERT INTO `fly_news` VALUES ('390163928786665472', '378190751797870592', '381767072113229824', '0', '文章标题文章标题文章标题文章标题文章标题文章标题', '简短标题', 'http://abc.com/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg', '文章作者', '文章来源', '<span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span><span style=\"color:#666666;font-family:&quot;font-size:14px;text-align:right;white-space:normal;background-color:#FFFFFF;\">文章内容</span>', '文章描述文章描述文章描述文章描述文章描述文章描述文章描述', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-08 12:53:15', null, '1', '0');
INSERT INTO `fly_news` VALUES ('390659770522009600', '378190751797870592', '381768105317105664', '0', 'dfsdfsdfsdf', 'sdfsdfsdf', '/upload/2019/11/7/caf69bb99818657be8921978aa00abe3.jpg', 'sdfsdfsdfsdf', 'dsfsdfsdf', 'sdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdf', 'sdfsdfsdfsdfsdf', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-09 21:43:33', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391029297218846720', '378190751797870592', '381768105317105664', '0', 'jkhjkhjkhjkhj', 'hjkhjkhjkhjk', '/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg', 'hjkhjkhjk', 'hjkhjkhjk', null, 'hjkhjkhjkhjkhjk', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-10 22:11:55', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391030292095172608', '378190751797870592', '381768056327634944', '0', 'gfhfghfgh', 'fghfghfghfgh', '/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg', 'rtyrtytry', 'hyfrtyrt', null, 'rtyrtyrtyrty', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-10 22:15:52', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391587723586043904', '378190751797870592', '381768056327634944', '0', 'werwerwerewr', 'werwerwerwerew', '/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg', 'werwerwer', 'werwerwer', null, 'wewerwerwer', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 11:10:54', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391589102828388352', '378190751797870592', '381767072113229824', '0', 'dfrgdfgdfg', 'dfgdfgdfdfg', '/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg', 'dfgfdgfd', 'dfgdg', null, 'dfgdfg', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 11:16:23', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391591299297312768', '378190751797870592', '381767072113229824', '0', 'tertert', 'ertertertert', '/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg', 'ertertre', 'dgrredter', null, 'erterertertert', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 11:25:07', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391592771099557888', '378190751797870592', '381767072113229824', '0', 'fgdfgdfgfddfgdfgfd', 'dfgdffgf', '/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg', 'dfgdfg', 'fdgdfgd', null, 'dfgdfgdfgdfg', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 11:30:58', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391597839903358976', '378190751797870592', '381767072113229824', '0', 'dfgdfgdf', 'fdgdfgdfgf', '/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg', 'dfgdfg', 'dfgfdg', null, 'dfgdfgdfgf', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 11:51:06', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391600944887889920', '378190751797870592', '381767072113229824', '0', 'fgdgdfg', 'dfgdfgdfg', '/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg', 'dfgdfgfd', 'dfdfgdfg', null, 'dfgdfgdfgdfgdfg', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 12:03:26', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391602384809230336', '378190751797870592', '368263538935857152', '0', 'bvfcvbcv', 'cvbcvbcvb', '/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg', 'dfgdfgfd', 'fgdfgdf', null, 'dfgdfgdfgdf', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 12:09:10', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391604113290297344', '378190751797870592', '381768105317105664', '0', 'dvdxfgsdg', 'dgfrdfgfd', '/upload/2019/11/12/0cd23f8d50aa837ac15211782c82cc96.jpg', 'dfgdfgdf', 'dfgdfgfd', 'dfgdfgdfgdfg<img src=\"/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg\" alt=\"\" />', 'dfgdfgdfgdfg', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 12:16:02', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391605654676045824', '378190751797870592', '381767141860311040', '0', '435345345', '3454354', '', '345345', '453454', null, '345345345', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 12:22:10', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391615270927466496', '378190751797870592', '368263538935857152', '0', 'fgdfgdfg', 'dfgdfgdfgdfg', '/upload/2019/11/12/0cd23f8d50aa837ac15211782c82cc96.jpg', 'fghfghfg', 'fghgfhfg', null, 'fghfghfghfghfgh', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 13:00:22', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391621115279048704', '378190751797870592', '381767072113229824', '0', 'dfgdgdfg', 'dfgdfgfdg', '/upload/2019/11/12/0cd23f8d50aa837ac15211782c82cc96.jpg', 'dfgdfg', 'dfgdfg', null, 'dfgdfgdfgdfg', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 13:23:36', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391624059441381376', '378190751797870592', '368263538935857152', '0', 'drfgdertert', 'erterterter', '', 'ertert', 'ertert', null, 'ertertert', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 13:35:18', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391624516180115456', '378190751797870592', '368263538935857152', '0', 'fgdfg', 'dfgdfgdfg', '', 'dfgdfg', 'fdgdfg', null, 'dfgdfg', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 13:37:07', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391625056838483968', '378190751797870592', '368263538935857152', '0', 'fgdfgdfgqweqweqwewqewq', 'dfgdfgdfg', '', 'dfgdfgdfg', 'dfgdfg', null, 'dfgdfgdfgdfgfdg', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 13:39:15', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391648034707996672', '378190751797870592', '381767072113229824', '0', 'tryrytry', 'rftyrtyrtyt', '/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg', 'fghfgh', 'fghf', null, 'fghffghgfh', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 15:10:34', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391665370563346432', '378190751797870592', '368263538935857152', '0', 'ftyrfytry', 'rtyrtyrty', '', 'rtyrty', 'rtytr', null, 'rtyrtyrt', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 16:19:27', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391666953950855168', '378190751797870592', '381767141860311040', '0', 'tytty', 'tytytyytyt', '', 'tyty', 'tyyt', null, 'tytyytyt', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 16:25:45', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391667666475024384', '378190751797870592', '368263538935857152', '0', 'tyttyy', 'tytytyyt', '', 'tytyyt', 'tyytyt', null, 'tytyyt', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 16:28:34', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391672554625957888', '378190751797870592', '368263538935857152', '0', 'sdafsdfsdf', 'sdfsdfsdf', '', 'sdfsdf', 'dsfsd', null, 'sdfsdfsdf', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 16:48:00', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391673363971440640', '378190751797870592', '368263538935857152', '0', 'sdfdsfsdf', 'sdfsdfsdf', '', 'sdfsdfsd', 'sdfsdfsd', null, 'fsdfsdfsdf', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 16:51:13', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391674436467556352', '378190751797870592', '368263538935857152', '0', 'easrwrwe', 'werwerwerwer', '', 'werwerwer', 'werwerew', null, null, null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 16:55:29', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391681815749853184', '378190751797870592', '368263538935857152', '0', 'sfdgsdfgdfg', 'dfgdfgdfg', '', 'sdfsdfsdf', 'sdfsdfsd', null, 'sdfsdfsdf', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 17:24:48', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391746767869181952', '378190751797870592', '368263538935857152', '0', 'fgdgdgd', 'dfgfdgdfgdf', '', 'dfgdfg', 'dfgdf', null, 'dfgdgfd', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 21:42:54', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391747721146400768', '378190751797870592', '381767072113229824', '0', 'hrtyrtytr', 'rtyrtyrty', '', 'rtyrtyrty', 'rtyrtytr', null, 'rtyrtytry', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 21:46:41', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391748768870957056', '378190751797870592', '381767141860311040', '0', '45345345345345', '345345345345', '', 'ertertertert', 'retertert', null, 'ertertertert', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 21:50:51', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391749956450385920', '378190751797870592', '368263538935857152', '0', 'sdfsdf', 'sdfsdfsdf', '', 'sdfsdfsd', 'dsfsdf', null, 'sdfsdfsdfsd', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 21:55:34', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391751853894795264', '378190751797870592', '381768056327634944', '0', 'tryrtyrty', 'rtyrtyrtyrty', '', 'rtyrtytr', 'tryrtyrtytr', null, 'rtyrtrty', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 22:03:06', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391754719644614656', '378190751797870592', '381767141860311040', '0', 'rdftewtet', 'ertertert', '', 'ertert', 'ertert', null, 'ertert', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 22:14:30', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391758428650864640', '378190751797870592', '381507005895933952', '0', 'werewrwe', 'werwerwer', '', 'werwer', 'werwerwe', null, 'werwerwerew', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 22:29:14', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391759512408686592', '378190751797870592', '381767141860311040', '0', 'tyrtyrtyrt', 'rtyrtyrty', '', 'ertertr', 'ertert', null, 'ertertertert', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 22:33:33', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391766889426059264', '378190751797870592', '381766907604238336', '0', 'retertertertertert', 'erterterterter', '', 'ertert', 'retert', null, 'ertertert', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 23:02:51', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391770034348752896', '378190751797870592', '381767141860311040', '0', 'rterdtert', 'etrertert', '', 'dfgdfg', 'dfgdfg', null, 'dfgdfgfdg', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 23:15:21', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391770936992333824', '378190751797870592', '381767141860311040', '0', 'cghfghfgh', 'fghfghfgh', '/upload/2019/11/12/0cd23f8d50aa837ac15211782c82cc96.jpg', 'ghjghj', 'hjgj', null, 'ghjghj', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-12 23:18:56', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391918357189754880', '378190751797870592', '368263538935857152', '0', 'ghfhf', 'fghfghfghfgh', '/upload/2019/11/12/0cd23f8d50aa837ac15211782c82cc96.jpg', 'hfghfgh', 'fghfg', null, 'fghfghfgh', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-13 09:04:44', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391919692031197184', '378190751797870592', '368263538935857152', '0', 'dfgdfg', 'dfgdfg', '/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg', 'fdg', 'fdgfd', null, 'fdgdfg', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-13 09:10:02', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391920267552620544', '378190751797870592', '381768056327634944', '0', 'rtretret', 'erterterter', '/upload/2019/11/12/0cd23f8d50aa837ac15211782c82cc96.jpg', 'ertert', 'ertert', '<pre style=\"background-color:#FFFFFF;font-family:宋体;font-size:9pt;\">\n<h2 style=\"box-sizing:border-box;outline:0px;margin:8px 0px 16px;padding:0px;font-size:24px;font-family:&quot;color:#4F4F4F;line-height:32px;word-wrap:break-word;white-space:normal;background-color:#FFFFFF;\">\n	1 异常描述\n</h2>\n\n<p style=\"box-sizing:border-box;outline:0px;margin-top:0px;margin-bottom:16px;padding:0px;font-family:&quot;font-size:16px;color:#4D4D4D;line-height:26px;overflow-x:auto;white-space:normal;background-color:#FFFFFF;\">\n	在通过 IP 地址及端口号调用远程方法，进行单元测试的时候，报出如下异常：\n</p>\n\n<p style=\"box-sizing:border-box;outline:0px;margin-top:0px;margin-bottom:16px;padding:0px;font-family:&quot;font-size:16px;color:#4D4D4D;line-height:26px;overflow-x:auto;white-space:normal;background-color:#FFFFFF;\">\n	<img src=\"/upload/2019/11/13/f35c4f730b12fa4311b357f1b770b96d.png\"  alt=\"1\" title=\"\" style=\"box-sizing:border-box;outline:0px;margin:0px;padding:0px;max-width:100%;word-wrap:break-word;cursor:zoom-in;\" />\n</p>\n\n<h2 style=\"box-sizing:border-box;outline:0px;margin:8px 0px 16px;padding:0px;font-size:24px;font-family:&quot;color:#4F4F4F;line-height:32px;word-wrap:break-word;white-space:normal;background-color:#FFFFFF;\">\n	<a name=\"t1\"></a><a style=\"box-sizing:border-box;outline:0px;margin:0px;padding:0px;color:#4EA1DB;cursor:pointer;word-wrap:break-word;\"></a>2 异常原因\n</h2>\n\n<p style=\"box-sizing:border-box;outline:0px;margin-top:0px;margin-bottom:16px;padding:0px;font-family:&quot;font-size:16px;color:#4D4D4D;line-height:26px;overflow-x:auto;white-space:normal;background-color:#FFFFFF;\">\n	通过观察上图标记出来的异常描述，咱们可以知道：\n</p>\n</pre>', 'ertertert', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-13 09:12:19', null, '1', '0');
INSERT INTO `fly_news` VALUES ('391921316430610432', '378190751797870592', '381767072113229824', '0', 'dfgdfgfdg', 'dfgdfgdfg', '/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg', 'dfgdfgfdg', 'fdgdfgd', '<h2 style=\"box-sizing:border-box;outline:0px;margin:8px 0px 16px;padding:0px;font-size:24px;font-family:&quot;color:#4F4F4F;line-height:32px;word-wrap:break-word;white-space:normal;background-color:#FFFFFF;\">\n	1 异常描述\n</h2>\n<p style=\"box-sizing:border-box;outline:0px;margin-top:0px;margin-bottom:16px;padding:0px;font-family:&quot;font-size:16px;color:#4D4D4D;line-height:26px;overflow-x:auto;white-space:normal;background-color:#FFFFFF;\">\n	在通过 IP 地址及端口号调用远程方法，进行单元测试的时候，报出如下异常：\n</p>\n<p style=\"box-sizing:border-box;outline:0px;margin-top:0px;margin-bottom:16px;padding:0px;font-family:&quot;font-size:16px;color:#4D4D4D;line-height:26px;overflow-x:auto;white-space:normal;background-color:#FFFFFF;\">\n	<img src=\"/upload/2019/11/13/f35c4f730b12fa4311b357f1b770b96d.png\"  alt=\"1\" title=\"\" style=\"box-sizing:border-box;outline:0px;margin:0px;padding:0px;max-width:100%;word-wrap:break-word;cursor:zoom-in;\" />\n</p>\n<h2 style=\"box-sizing:border-box;outline:0px;margin:8px 0px 16px;padding:0px;font-size:24px;font-family:&quot;color:#4F4F4F;line-height:32px;word-wrap:break-word;white-space:normal;background-color:#FFFFFF;\">\n	<a name=\"t1\"></a><a style=\"box-sizing:border-box;outline:0px;margin:0px;padding:0px;color:#4EA1DB;cursor:pointer;word-wrap:break-word;\"></a>2 异常原因\n</h2>\n<p style=\"box-sizing:border-box;outline:0px;margin-top:0px;margin-bottom:16px;padding:0px;font-family:&quot;font-size:16px;color:#4D4D4D;line-height:26px;overflow-x:auto;white-space:normal;background-color:#FFFFFF;\">\n	通过观察上图标记出来的异常描述，咱们可以知道：\n</p>', 'fdgdfgdfg', null, '0', '0', '0', '0', '1', '366638723963551744', '2019-11-13 09:16:29', null, '1', '0');

-- ----------------------------
-- Table structure for `fly_news_comment`
-- ----------------------------
DROP TABLE IF EXISTS `fly_news_comment`;
CREATE TABLE `fly_news_comment` (
  `id` bigint(20) NOT NULL,
  `news_id` bigint(20) DEFAULT NULL COMMENT '关联news.id',
  `user_id` bigint(20) DEFAULT NULL COMMENT '关联user.id，评论用户的id',
  `text` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '评论内容',
  `addtime` datetime DEFAULT NULL COMMENT '添加时间',
  `status` tinyint(1) DEFAULT '0' COMMENT '审核状态，0未审核，1已审核',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`),
  KEY `newsid` (`news_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='news的评论（暂未用到，预留）';

-- ----------------------------
-- Records of fly_news_comment
-- ----------------------------

-- ----------------------------
-- Table structure for `fly_order`
-- ----------------------------
DROP TABLE IF EXISTS `fly_order`;
CREATE TABLE `fly_order` (
  `id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '购买的企业id',
  `commodity_name` varchar(100) DEFAULT NULL COMMENT '商品名称',
  `market_type` int(5) DEFAULT NULL COMMENT '产品类型',
  `coupon_id` bigint(20) DEFAULT '0' COMMENT '优惠券id',
  `coupon_price` decimal(11,2) DEFAULT '0.00' COMMENT '优惠券减免',
  `integral_price` int(11) DEFAULT '0' COMMENT '用户积分减免',
  `product_id` bigint(20) DEFAULT NULL COMMENT '购买的商品ID',
  `order_sn` varchar(100) DEFAULT NULL COMMENT '订单编号',
  `order_status` int(2) DEFAULT '0' COMMENT '支付状态',
  `original_amount` decimal(11,2) unsigned zerofill DEFAULT '000000000.00' COMMENT '原价',
  `pay_amount` decimal(11,2) DEFAULT '0.00' COMMENT '应付价格',
  `end_time` datetime DEFAULT NULL COMMENT '订单关闭时间',
  `pay_time` datetime DEFAULT NULL COMMENT '订单支付/开通时间',
  `create_time` datetime DEFAULT NULL COMMENT '订单创建时间',
  `deleted` tinyint(1) DEFAULT '1' COMMENT '逻辑删除，1显示，0删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='网站订单表';

-- ----------------------------
-- Records of fly_order
-- ----------------------------
INSERT INTO `fly_order` VALUES ('363663265068220492', '401335316046151680', '专业版网站', '1', '0', '0.00', '0', '401359718385188864', '4353453434345345', '1', '000000000.00', '0.00', null, '2019-12-09 23:51:23', '2019-12-09 23:51:27', '1');

-- ----------------------------
-- Table structure for `fly_picture`
-- ----------------------------
DROP TABLE IF EXISTS `fly_picture`;
CREATE TABLE `fly_picture` (
  `id` bigint(20) NOT NULL,
  `img_url` varchar(255) CHARACTER SET utf8 NOT NULL,
  `img_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `img_width` int(11) DEFAULT NULL,
  `img_height` int(11) DEFAULT NULL,
  `signature` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `sort` int(5) DEFAULT NULL,
  `picflag` int(1) DEFAULT NULL,
  `count_info` int(11) DEFAULT '0' COMMENT '图片使用次数',
  `deleted` int(1) DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of fly_picture
-- ----------------------------
INSERT INTO `fly_picture` VALUES ('389857477149065216', '/upload/2019/11/7/caf69bb99818657be8921978aa00abe3.jpg', 'eeee.jpg', null, '141', '750', '1206', 'caf69bb99818657be8921978aa00abe3', '2019-11-07 16:35:31', null, null, '0', null);
INSERT INTO `fly_picture` VALUES ('389859700738359296', '/upload/2019/11/7/caf69bb99818657be8921978aa00abe3.jpg', 'eeee.jpg', null, '141', '750', '1206', 'caf69bb99818657be8921978aa00abe3', '2019-11-07 16:44:22', null, null, '0', null);
INSERT INTO `fly_picture` VALUES ('390153143876321280', '/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg', 'eeee.jpg', null, '141', '750', '1206', 'caf69bb99818657be8921978aa00abe3', '2019-11-08 12:10:24', null, null, '0', null);
INSERT INTO `fly_picture` VALUES ('390154410455465984', '/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg', 'eeee.jpg', null, '141', '750', '1206', 'caf69bb99818657be8921978aa00abe3', '2019-11-08 12:15:26', null, null, '0', null);
INSERT INTO `fly_picture` VALUES ('390158032274718720', '/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg', 'eeee.jpg', null, '141', '750', '1206', 'caf69bb99818657be8921978aa00abe3', '2019-11-08 12:29:49', null, null, '0', null);
INSERT INTO `fly_picture` VALUES ('390158561772044288', '/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg', 'eeee.jpg', null, '141', '750', '1206', 'caf69bb99818657be8921978aa00abe3', '2019-11-08 12:31:56', null, null, '0', null);
INSERT INTO `fly_picture` VALUES ('390160664678629376', '/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg', 'eeee.jpg', null, '141', '750', '1206', 'caf69bb99818657be8921978aa00abe3', '2019-11-08 12:40:17', null, null, '0', null);
INSERT INTO `fly_picture` VALUES ('390161415622623232', '/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg', 'eeee.jpg', null, '141', '750', '1206', 'caf69bb99818657be8921978aa00abe3', '2019-11-08 12:43:16', null, null, '0', null);
INSERT INTO `fly_picture` VALUES ('390161780069892096', '/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg', 'eeee.jpg', null, '141', '750', '1206', 'caf69bb99818657be8921978aa00abe3', '2019-11-08 12:44:43', null, null, '0', null);
INSERT INTO `fly_picture` VALUES ('390162493277732864', '/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg', 'eeee.jpg', null, '141', '750', '1206', 'caf69bb99818657be8921978aa00abe3', '2019-11-08 12:47:33', null, null, '0', null);
INSERT INTO `fly_picture` VALUES ('390163293727096832', '/upload/2019/11/8/caf69bb99818657be8921978aa00abe3.jpg', 'eeee.jpg', null, '141', '750', '1206', 'caf69bb99818657be8921978aa00abe3', '2019-11-08 12:50:44', null, null, '14', null);
INSERT INTO `fly_picture` VALUES ('391029283474112512', '/upload/2019/11/10/a52e656c65b5763ceef738038697d20d.jpg', '网站授权原理.jpg', null, '45', '1080', '712', 'a52e656c65b5763ceef738038697d20d', '2019-11-10 22:11:52', null, null, '0', '0');
INSERT INTO `fly_picture` VALUES ('391604054066724864', '/upload/2019/11/12/0cd23f8d50aa837ac15211782c82cc96.jpg', 'qrcode.jpg', null, '11', '140', '140', '0cd23f8d50aa837ac15211782c82cc96', '2019-11-12 12:15:48', null, null, '3', '0');
INSERT INTO `fly_picture` VALUES ('391920268961906688', '/upload/2019/11/13/f35c4f730b12fa4311b357f1b770b96d.png', 'f35c4f730b12fa4311b357f1b770b96d.png', null, '78', '947', '237', 'f35c4f730b12fa4311b357f1b770b96d', '2019-11-13 09:12:19', null, null, '2', '0');

-- ----------------------------
-- Table structure for `fly_picture_company`
-- ----------------------------
DROP TABLE IF EXISTS `fly_picture_company`;
CREATE TABLE `fly_picture_company` (
  `id` bigint(20) NOT NULL,
  `picture_id` bigint(20) NOT NULL COMMENT '图片表id',
  `company_id` bigint(20) NOT NULL COMMENT '公司id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='企业与图片库关联表';

-- ----------------------------
-- Records of fly_picture_company
-- ----------------------------
INSERT INTO `fly_picture_company` VALUES ('363663265068220416', '378556618222075904', '389481100151631908');
INSERT INTO `fly_picture_company` VALUES ('389857477241339904', '389857477149065216', '389481100151631908');
INSERT INTO `fly_picture_company` VALUES ('389859700792885248', '389859700738359296', '389481100151631908');
INSERT INTO `fly_picture_company` VALUES ('390153143989567488', '390153143876321280', '389481100151631908');
INSERT INTO `fly_picture_company` VALUES ('390154410476437504', '390154410455465984', '389481100151631908');
INSERT INTO `fly_picture_company` VALUES ('390158032308273152', '390158032274718720', '389481100151631908');
INSERT INTO `fly_picture_company` VALUES ('390158561801404416', '390158561772044288', '389481100151631908');
INSERT INTO `fly_picture_company` VALUES ('390160664691212288', '390160664678629376', '389481100151631908');
INSERT INTO `fly_picture_company` VALUES ('390161415631011840', '390161415622623232', '389481100151631908');
INSERT INTO `fly_picture_company` VALUES ('390161780082475008', '390161780069892096', '389481100151631908');
INSERT INTO `fly_picture_company` VALUES ('390162493298704384', '390162493277732864', '389481100151631908');
INSERT INTO `fly_picture_company` VALUES ('390163293739679744', '390163293727096832', '389481100151631908');
INSERT INTO `fly_picture_company` VALUES ('391029283599941632', '391029283474112512', '389481100151631908');
INSERT INTO `fly_picture_company` VALUES ('391604054121250816', '391604054066724864', '389481100151631908');
INSERT INTO `fly_picture_company` VALUES ('391920268999655424', '391920268961906688', '389481100151631908');

-- ----------------------------
-- Table structure for `fly_picture_info`
-- ----------------------------
DROP TABLE IF EXISTS `fly_picture_info`;
CREATE TABLE `fly_picture_info` (
  `id` bigint(20) NOT NULL,
  `type_id` int(11) DEFAULT '0' COMMENT '信息类型',
  `picture_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '图片表id',
  `info_id` bigint(20) NOT NULL COMMENT '公司id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='企业与图片库关联表';

-- ----------------------------
-- Records of fly_picture_info
-- ----------------------------
INSERT INTO `fly_picture_info` VALUES ('390617047417237520', '0', '390163293727096832', '390163928786665472');
INSERT INTO `fly_picture_info` VALUES ('390617047417237521', '0', '390163293727096832', '389949996838944768');
INSERT INTO `fly_picture_info` VALUES ('391029297277566976', '1', '390163293727096832', '391029297218846720');
INSERT INTO `fly_picture_info` VALUES ('391030292162281472', '1', '390163293727096832', '391030292095172608');
INSERT INTO `fly_picture_info` VALUES ('391587723703484416', '1', '390163293727096832', '391587723586043904');
INSERT INTO `fly_picture_info` VALUES ('391589102878720000', '1', '390163293727096832', '391589102828388352');
INSERT INTO `fly_picture_info` VALUES ('391591299364421632', '1', '390163293727096832', '391591299297312768');
INSERT INTO `fly_picture_info` VALUES ('391592771158278144', '1', '390163293727096832', '391592771099557888');
INSERT INTO `fly_picture_info` VALUES ('391597839962079232', '1', '390163293727096832', '391597839903358976');
INSERT INTO `fly_picture_info` VALUES ('391600944963387392', '1', '390163293727096832', '391600944887889920');
INSERT INTO `fly_picture_info` VALUES ('391602384863756288', '1', '390163293727096832', '391602384809230336');
INSERT INTO `fly_picture_info` VALUES ('391604113311268864', '1', '390163293727096832', '391604113290297344');
INSERT INTO `fly_picture_info` VALUES ('391604113340628992', '1', '390163293727096832', '391604113290297344');
INSERT INTO `fly_picture_info` VALUES ('391604113382572032', '1', '391604054066724864', '391604113290297344');
INSERT INTO `fly_picture_info` VALUES ('391615272571633664', '1', '391604054066724864', '391615270927466496');
INSERT INTO `fly_picture_info` VALUES ('391621116726083584', '1', '391604054066724864', '391621115279048704');
INSERT INTO `fly_picture_info` VALUES ('391648036121477120', '1', '390163293727096832', '391648034707996672');
INSERT INTO `fly_picture_info` VALUES ('391920269196787712', '1', '391920268961906688', '391920267552620544');
INSERT INTO `fly_picture_info` VALUES ('391921317370134528', '1', '391920268961906688', '391921316430610432');

-- ----------------------------
-- Table structure for `fly_product`
-- ----------------------------
DROP TABLE IF EXISTS `fly_product`;
CREATE TABLE `fly_product` (
  `id` bigint(20) NOT NULL,
  `site_id` bigint(20) DEFAULT '0' COMMENT '文档ID',
  `column_id` bigint(20) DEFAULT NULL,
  `titlepic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品主图',
  `title` varchar(76) CHARACTER SET utf8 DEFAULT NULL COMMENT '产品标题',
  `keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '关键词',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品简介,从内容正文里自动剪切出开始的160个汉字',
  `content` longtext CHARACTER SET utf8 COMMENT '内容详情',
  `create_time` datetime DEFAULT NULL COMMENT '新增时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `status` tinyint(2) DEFAULT '1' COMMENT '1：正常显示；2：隐藏不显示',
  `deleted` tinyint(2) DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`),
  KEY `news_id` (`site_id`,`id`,`column_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='产品附加表';

-- ----------------------------
-- Records of fly_product
-- ----------------------------
INSERT INTO `fly_product` VALUES ('382656320815509518', '378190751797870592', '368262732157288448', null, '产品测试', null, null, '产品测试产品测试产品测试产品测试产品测试产品测试产品测试产品测试产品测试产品测试产品测试产品测试产品测试产品测试产品测试产品测试产品测试产品测试产品测试产品测试产品测试产品测试产品测试产品测试产品测试产品测试产品测试产品测试', '2019-10-19 00:36:37', null, '1', '0');
INSERT INTO `fly_product` VALUES ('393799033635733504', '378190751797870592', null, null, 'erwerewrwerwerwerwe', null, 'werwerwerwer', 'werwerwerwerwerewr', '2019-11-18 13:37:52', null, '1', '0');

-- ----------------------------
-- Table structure for `fly_product_attr`
-- ----------------------------
DROP TABLE IF EXISTS `fly_product_attr`;
CREATE TABLE `fly_product_attr` (
  `product_attr_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '产品属性id自增',
  `aid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '产品id',
  `attr_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '属性id',
  `attr_value` text COMMENT '属性值',
  `attr_price` varchar(255) DEFAULT '' COMMENT '属性价格',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`product_attr_id`),
  KEY `aid` (`aid`) USING BTREE,
  KEY `attr_id` (`attr_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COMMENT='产品表单属性值';

-- ----------------------------
-- Records of fly_product_attr
-- ----------------------------
INSERT INTO `fly_product_attr` VALUES ('5', '28', '5', '13.3', '0', '1526613498', '1526613498');
INSERT INTO `fly_product_attr` VALUES ('6', '28', '6', '3KG', '0', '1526613498', '1526613498');
INSERT INTO `fly_product_attr` VALUES ('7', '29', '7', 'AKG&amp;HUAWEI', '0', '1526613820', '1526613820');
INSERT INTO `fly_product_attr` VALUES ('8', '29', '8', '支持', '0', '1526613820', '1526613820');
INSERT INTO `fly_product_attr` VALUES ('17', '37', '2', '苹果', '', '1527507984', '1527507984');
INSERT INTO `fly_product_attr` VALUES ('18', '37', '1', '牛逼', '', '1527507984', '1527507984');
INSERT INTO `fly_product_attr` VALUES ('19', '37', '3', '触摸', '', '1527507984', '1527507984');
INSERT INTO `fly_product_attr` VALUES ('20', '37', '4', '234234', '', '1527507984', '1527507984');
INSERT INTO `fly_product_attr` VALUES ('21', '27', '2', 'EMUI 4.1 + Android 6.0', '', '1531726843', '1531726843');
INSERT INTO `fly_product_attr` VALUES ('22', '27', '1', 'EMUI 4.1', '', '1531726843', '1531726843');
INSERT INTO `fly_product_attr` VALUES ('23', '27', '3', '虚拟键盘', '', '1531726843', '1531726843');
INSERT INTO `fly_product_attr` VALUES ('24', '27', '4', 'EDI-AL10', '', '1531726843', '1531726843');
INSERT INTO `fly_product_attr` VALUES ('25', '53', '17', 'AKG&amp;HUAWEI', '', '1545268991', '1545268991');
INSERT INTO `fly_product_attr` VALUES ('26', '53', '18', 'Support', '', '1545268991', '1545268991');
INSERT INTO `fly_product_attr` VALUES ('27', '54', '15', '13.3', '', '1545270139', '1545270139');
INSERT INTO `fly_product_attr` VALUES ('28', '54', '16', '3KG', '', '1545270139', '1545270139');
INSERT INTO `fly_product_attr` VALUES ('29', '55', '12', 'EMUI 4.1 + Android 6.0', '', '1545270361', '1545270361');
INSERT INTO `fly_product_attr` VALUES ('30', '55', '11', 'EMUI 4.1', '', '1545270361', '1545270361');
INSERT INTO `fly_product_attr` VALUES ('31', '55', '13', 'Virtual keyboard', '', '1545270361', '1545270361');
INSERT INTO `fly_product_attr` VALUES ('32', '55', '14', 'EDI-AL10', '', '1545270361', '1545270361');
INSERT INTO `fly_product_attr` VALUES ('33', '56', '12', 'iOS 9.0', '', '1545270634', '1545270634');
INSERT INTO `fly_product_attr` VALUES ('34', '56', '11', '4.7 inch display screen', '', '1545270634', '1545270634');
INSERT INTO `fly_product_attr` VALUES ('35', '56', '13', 'Virtual keyboard', '', '1545270634', '1545270634');
INSERT INTO `fly_product_attr` VALUES ('36', '56', '14', '6S', '', '1545270634', '1545270634');

-- ----------------------------
-- Table structure for `fly_product_attribute`
-- ----------------------------
DROP TABLE IF EXISTS `fly_product_attribute`;
CREATE TABLE `fly_product_attribute` (
  `attr_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '属性id',
  `attr_name` varchar(60) DEFAULT '' COMMENT '属性名称',
  `typeid` int(11) unsigned DEFAULT '0' COMMENT '栏目id',
  `attr_index` tinyint(1) unsigned DEFAULT '0' COMMENT '0不需要检索 1关键字检索 2范围检索',
  `attr_input_type` tinyint(1) unsigned DEFAULT '0' COMMENT ' 0=文本框，1=下拉框，2=多行文本框',
  `attr_values` text COMMENT '可选值列表',
  `sort_order` int(11) unsigned DEFAULT '0' COMMENT '属性排序',
  `lang` varchar(50) DEFAULT 'cn' COMMENT '语言标识',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否已删除，0=否，1=是',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`attr_id`),
  KEY `cat_id` (`typeid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='产品表单属性表';

-- ----------------------------
-- Records of fly_product_attribute
-- ----------------------------
INSERT INTO `fly_product_attribute` VALUES ('1', '用户界面', '24', '0', '0', '', '100', 'cn', '0', '1526612774', '1526612774');
INSERT INTO `fly_product_attribute` VALUES ('2', '操作系统', '24', '0', '0', '', '10', 'cn', '0', '1526612785', '1526612785');
INSERT INTO `fly_product_attribute` VALUES ('3', '键盘类型', '24', '0', '0', '', '100', 'cn', '0', '1526613004', '1526613004');
INSERT INTO `fly_product_attribute` VALUES ('4', ' 型号', '24', '0', '0', '', '100', 'cn', '0', '1526613011', '1526613011');
INSERT INTO `fly_product_attribute` VALUES ('5', '屏幕大小', '26', '0', '0', '', '100', 'cn', '0', '1526613252', '1526613252');
INSERT INTO `fly_product_attribute` VALUES ('6', '重量', '26', '0', '0', '', '100', 'cn', '0', '1526613259', '1526613259');
INSERT INTO `fly_product_attribute` VALUES ('7', '型号', '27', '0', '0', '', '100', 'cn', '0', '1526613668', '1526613668');
INSERT INTO `fly_product_attribute` VALUES ('8', '支持蓝牙', '27', '0', '0', '', '100', 'cn', '0', '1526613732', '1526613732');
INSERT INTO `fly_product_attribute` VALUES ('11', 'User Interface', '43', '0', '0', '', '100', 'en', '0', '1526612774', '1545274001');
INSERT INTO `fly_product_attribute` VALUES ('12', 'operating system', '43', '0', '0', '', '10', 'en', '0', '1526612785', '1545273990');
INSERT INTO `fly_product_attribute` VALUES ('13', 'Keyboard type', '43', '0', '0', '', '100', 'en', '0', '1526613004', '1545274014');
INSERT INTO `fly_product_attribute` VALUES ('14', 'model', '43', '0', '0', '', '100', 'en', '0', '1526613011', '1545274025');
INSERT INTO `fly_product_attribute` VALUES ('15', 'Screen size', '46', '0', '0', '', '100', 'en', '0', '1526613252', '1545270158');
INSERT INTO `fly_product_attribute` VALUES ('16', 'Weight', '46', '0', '0', '', '100', 'en', '0', '1526613259', '1545270171');
INSERT INTO `fly_product_attribute` VALUES ('17', 'Model', '48', '0', '0', '', '100', 'en', '0', '1526613668', '1545268934');
INSERT INTO `fly_product_attribute` VALUES ('18', 'Support Bluetooth', '48', '0', '0', '', '100', 'en', '0', '1526613732', '1545268951');

-- ----------------------------
-- Table structure for `fly_site`
-- ----------------------------
DROP TABLE IF EXISTS `fly_site`;
CREATE TABLE `fly_site` (
  `id` bigint(20) NOT NULL,
  `site_name` varchar(150) NOT NULL COMMENT '站点名字',
  `show_banner` tinyint(1) DEFAULT '1' COMMENT '是否在首页显示Banner  1显示 0不显示',
  `template_id` bigint(20) DEFAULT '0' COMMENT '模版编号',
  `template_index` bigint(20) DEFAULT NULL COMMENT '首页模板编号',
  `domain` char(30) DEFAULT NULL COMMENT '二级域名',
  `bind_domain` char(30) DEFAULT NULL COMMENT '用户自己绑定的域名',
  `logo` char(80) DEFAULT '' COMMENT 'PC端的LOGO',
  `title` varchar(128) DEFAULT NULL COMMENT '网站标题',
  `keywords` char(38) DEFAULT '' COMMENT '搜索的关键词',
  `description` varchar(255) DEFAULT NULL COMMENT '网站描述',
  `copyright` varchar(255) DEFAULT NULL COMMENT '网站版权设置',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `expire_time` datetime DEFAULT NULL COMMENT '到期时间',
  `update_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `status` tinyint(1) DEFAULT '0' COMMENT '站点状态，1正常；0关闭',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '逻辑删除',
  `verify` tinyint(2) DEFAULT '0' COMMENT '管理员审核状态：0未审核，1已审核，2审核未通过，3已过期',
  PRIMARY KEY (`id`),
  KEY `site_index` (`id`,`site_name`,`template_id`,`template_index`,`domain`,`bind_domain`,`title`,`create_time`,`expire_time`,`update_time`,`status`,`deleted`,`verify`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='网站，每个站点都会存为一条记录';

-- ----------------------------
-- Records of fly_site
-- ----------------------------
INSERT INTO `fly_site` VALUES ('363539057487118336', '企业之家', '1', '1', '368933812476063754', 'www', 'rtyry', '', '网站建设-品牌网站制作,免费自助建站,网页设计开发', '自助建站系统,SEO建站系统,企业建站系统', '企业之家采用先进的模板处理技术建站模式，且拥有主流SEO自助建站系统全部实用功能。适用于中小企业建站，企业在线销售型网站，多语言外贸网站等。', '© 2019 97560.com MIT license', '2019-08-26 17:35:31', null, null, '1', '0', '0');
INSERT INTO `fly_site` VALUES ('363542464264404992', 'retert', '1', '1', null, 'erterter', 'rterter', '', null, 'ertertert', 'layuiAdmin 是 layui 官方出品的通用型后台模板解决方案，提供了单页版和 iframe 版两种开发模式。layuiAdmin 是目前非常流行的后台模板框架，广泛用于各类管理平台。', '© 2019 97560.com MIT license', '2019-08-26 17:49:03', null, null, '1', '0', '0');
INSERT INTO `fly_site` VALUES ('363542800895049728', 'reterterete', '1', '1', null, 'erterter', 'rterter', '', null, 'ertertert', 'layuiAdmin 是 layui 官方出品的通用型后台模板解决方案，提供了单页版和 iframe 版两种开发模式。layuiAdmin 是目前非常流行的后台模板框架，广泛用于各类管理平台。', '© 2019 97560.com MIT license', '2019-08-26 17:50:24', null, null, '0', '0', '0');
INSERT INTO `fly_site` VALUES ('363663264883671040', 'wwwfdfdfdfdfd', '1', '1', null, 'erterter', 'rterter', '', null, 'ertertert', 'layuiAdmin 是 layui 官方出品的通用型后台模板解决方案，提供了单页版和 iframe 版两种开发模式。layuiAdmin 是目前非常流行的后台模板框架，广泛用于各类管理平台。', '© 2019 97560.com MIT license', '2019-08-27 01:49:05', null, null, '0', '0', '0');
INSERT INTO `fly_site` VALUES ('367356661297315840', 'csfdcfff', '1', '1', null, 'FjeMZ3', 'dfgdfgdfgdfggfd', '', null, 'dfgdfgdfgdf', 'dfgdfgdfgdfgdf', '© 2019 97560.com MIT license', '2019-09-06 14:25:19', null, null, '0', '0', '0');
INSERT INTO `fly_site` VALUES ('367474558459969536', '测试网站顶顶顶顶顶顶顶', '1', '1', null, 'YZVv2i', 'dfgdfgdfgdfggfd', '', null, 'dfgdfgdfgdf', 'dfgdfgdfgdfgdf', '© 2019 97560.com MIT license', '2019-09-06 22:13:48', null, null, '0', '0', '0');
INSERT INTO `fly_site` VALUES ('367866150970720256', '企业测试网站', '1', '371083359947849728', null, 'Urqiyi', 'fghfghfg', '', null, 'fghfghfghfgh', 'fghfghfghfgh', '© 2019 97560.com MIT license', '2019-09-08 00:09:51', null, null, '0', '0', '0');
INSERT INTO `fly_site` VALUES ('378189674327310336', 'dfsfdsdfsdf', '1', '0', null, '34234', null, '', null, 'dfgdfg', 'dfgdfg', '© 2019 97560.com MIT license', '2019-10-06 11:51:50', null, null, '0', '0', '0');
INSERT INTO `fly_site` VALUES ('378190289216471040', 'gdfgdfgfd', '1', '0', null, 'dfgdfg', null, '', null, 'dfgdfg', 'dfgdfgdfg', '© 2019 97560.com MIT license', '2019-10-06 11:54:17', null, null, '0', '0', '0');
INSERT INTO `fly_site` VALUES ('378190751797870592', '演示站', '1', '371083359947849728', '368933812476063754', 'ceshi', 'dfgdfg', '', 'dfgdfg', '新纶科技', '新纶科技', '© 2019 97560.com MIT license', '2019-10-06 11:56:07', null, null, '0', '0', '0');
INSERT INTO `fly_site` VALUES ('381800822650187789', '测试演示自定义模版站', '1', null, null, 'cs', '', null, null, '测试演示的自定义模版站', null, null, '2019-08-23 14:23:56', '2019-08-23 15:19:56', null, '0', '0', '0');
INSERT INTO `fly_site` VALUES ('381800822650187790', '测试网站', '1', '0', null, '3932948', '', '', null, 'ceshi', null, null, '2019-08-23 14:24:00', '2019-08-23 15:19:59', null, '0', '0', '0');
INSERT INTO `fly_site` VALUES ('400623766293446656', 'fghfghfgh', '1', '0', null, 'fghfg', null, '', 'fghfgh', 'fghfgh', 'fghfghgh', '© 2019 97560.com MIT license', '2019-12-07 09:36:55', null, null, '0', '0', '0');
INSERT INTO `fly_site` VALUES ('401359718385188864', 'fddfsd', '1', '371083359947849728', null, 'sdfsdf', null, '', 'sdfsdfsdf', 'sdfsdf', 'sdfsdfdf', '© 2019 97560.com MIT license', '2019-12-09 10:21:19', '2019-12-11 12:59:02', null, '1', '0', '1');
INSERT INTO `fly_site` VALUES ('401751717164613632', 'fghfhf', '1', '371083359947849728', null, 'fghghfg', null, '', null, '', null, null, '2019-12-10 12:18:59', '2019-12-17 12:18:59', null, '1', '0', '0');

-- ----------------------------
-- Table structure for `fly_site_column`
-- ----------------------------
DROP TABLE IF EXISTS `fly_site_column`;
CREATE TABLE `fly_site_column` (
  `id` bigint(20) NOT NULL,
  `parent_id` bigint(20) DEFAULT '0' COMMENT '上级栏目的id，若是顶级栏目，则为0',
  `site_id` bigint(20) NOT NULL COMMENT '对应的站点id,site.id',
  `channel` bigint(20) DEFAULT '0' COMMENT '对应模型表fly_system_module里的id，每个id对应一个模型',
  `column_title` char(60) NOT NULL COMMENT '栏目名称',
  `column_code` varchar(100) DEFAULT NULL,
  `column_url` char(100) DEFAULT '' COMMENT '链接地址',
  `new_window` tinyint(1) DEFAULT '0' COMMENT '是否新窗口打开，0是原窗口，1是新窗口',
  `column_icon` char(100) DEFAULT NULL COMMENT '本栏目的图片、图标，可在模版中使用{siteColumn.icon}进行调用此图以显示',
  `column_hidden` int(2) DEFAULT '1' COMMENT '导航中是否显示。0影藏，1显示',
  `column_type` int(2) DEFAULT '1' COMMENT '1频道封面（频道首页）模板，2列表模板，3内容模板，4单独页面模板',
  `tempindex` bigint(20) DEFAULT NULL COMMENT '当前栏目所使用的首页模版名字。当site.templateName有值时，整个网站使用的自定义模版，这里的才会生效',
  `templist` bigint(20) DEFAULT NULL COMMENT '当前栏目所使用的列表模版名字。当site.templateName有值时，整个网站使用的自定义模版，这里的才会生效',
  `tempcontent` bigint(20) DEFAULT NULL COMMENT '当前栏目所使用的内容模版名字',
  `title` varchar(100) DEFAULT NULL COMMENT '单页面的时候调用的标题',
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `content` longtext COMMENT '单页面的时候调用此内容',
  `sort_order` int(4) DEFAULT '0' COMMENT '排序,数字越小越往前',
  `create_time` datetime DEFAULT NULL COMMENT '栏目添加时间',
  `update_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `status` tinyint(1) DEFAULT '1' COMMENT '是否启用。1启用，0不启用',
  `deleted` tinyint(4) DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`),
  KEY `rank` (`sort_order`,`status`,`site_id`,`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='栏目表，网站上的栏目';

-- ----------------------------
-- Records of fly_site_column
-- ----------------------------
INSERT INTO `fly_site_column` VALUES ('368262732157288448', '0', '378190751797870592', '368208528827232271', '产品与服务', null, '', '1', null, '1', '1', '368933812476063755', '368933812476063757', null, null, null, null, null, '0', '2019-09-09 02:25:43', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('368263538935857152', '0', '378190751797870592', '368208528827232271', '投资者关系', null, '', '1', null, '1', '1', '368933812476063755', '368933812476063757', null, null, null, null, null, '1', '2019-09-09 02:28:55', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('369289385704685568', '0', '378190751797870592', '368208528827232271', '产业发展', null, '', '1', null, '1', '1', '368933812476063755', '368933812476063757', null, null, null, null, null, '0', '2019-09-11 22:25:16', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('369305640453013504', '0', '378190751797870592', '368208528827232271', '技术创新', null, '', '1', null, '1', '1', '368933812476063755', '368933812476063757', null, null, null, null, null, '0', '2019-09-11 23:29:52', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('381507005895933952', '0', '378190751797870592', '368208528827232271', '人力资源', null, '', '1', null, '1', '1', '368933812476063755', '368933812476063757', null, null, null, null, null, '0', '2019-10-15 15:33:44', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('381766907604238336', '369289385704685568', '378190751797870592', '368208528827232271', '产业概览', null, '', '1', null, '1', '1', '368933812476063755', '368933812476063757', null, null, null, null, null, '0', '2019-10-16 08:46:29', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('381767015699841024', '369289385704685568', '378190751797870592', '368208528827232271', '先进材料', null, '', '1', null, '1', '1', '368933812476063755', '368933812476063757', null, null, null, null, null, '0', '2019-10-16 08:46:55', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('381767072113229824', '369289385704685568', '378190751797870592', '368208528827232271', '洁净工程与超净产品', null, '', '1', null, '1', '1', '368933812476063755', '368933812476063757', null, null, null, null, null, '0', '2019-10-16 08:47:09', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('381767141860311040', '369289385704685568', '378190751797870592', '368208528827232271', '材料衍生智造', null, '', '1', null, '1', '1', '368933812476063755', '368933812476063757', null, null, null, null, null, '0', '2019-10-16 08:47:25', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('381767884252119040', '368262732157288448', '378190751797870592', '368208528827232271', '事业本部介绍', null, '', '1', null, '1', '1', '368933812476063755', '368933812476063757', null, null, null, null, null, '0', '2019-10-16 08:50:22', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('381767956964573184', '368262732157288448', '378190751797870592', '368208528827232272', '产品系列', null, '', '1', null, '1', '1', null, null, null, null, null, null, null, '0', '2019-10-16 08:50:39', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('381768010077044736', '368262732157288448', '378190751797870592', '368208528827232271', '行业应用', null, '', '1', null, '1', '1', '368933812476063755', '368933812476063757', null, null, null, null, null, '0', '2019-10-16 08:50:52', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('381768056327634944', '368262732157288448', '378190751797870592', '368208528827232271', '核心优势', null, '', '1', null, '1', '1', '368933812476063755', '368933812476063757', null, null, null, null, null, '0', '2019-10-16 08:51:03', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('381768105317105664', '368262732157288448', '378190751797870592', '368208528827232271', '生产环境', null, '', '1', null, '1', '1', '368933812476063755', '368933812476063757', null, null, null, null, null, '0', '2019-10-16 08:51:15', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('381768152251367424', '368262732157288448', '378190751797870592', '368208528827232271', '联系销售', null, '', '1', null, '1', '1', '368933812476063755', '368933812476063757', null, null, null, null, null, '0', '2019-10-16 08:51:26', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('381768652141101056', '369305640453013504', '378190751797870592', '368208528827232271', '技术开发中心介绍', null, '', '1', null, '1', '1', '368933812476063755', '368933812476063757', null, null, null, null, null, '0', '2019-10-16 08:53:25', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('381768731841265664', '369305640453013504', '378190751797870592', '368208528827232271', '研究领域', null, '', '1', null, '1', '1', '368933812476063755', '368933812476063757', null, null, null, null, null, '0', '2019-10-16 08:53:44', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('381768775130677248', '369305640453013504', '378190751797870592', '368208528827232271', '科研合作', null, '', '1', null, '1', '1', '368933812476063755', '368933812476063757', null, null, null, null, null, '0', '2019-10-16 08:53:55', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('381768821486125056', '369305640453013504', '378190751797870592', '368208528827232271', '研发成果', null, '', '1', null, '1', '1', '368933812476063755', '368933812476063757', null, null, null, null, null, '0', '2019-10-16 08:54:06', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('381768878130200576', '369305640453013504', '378190751797870592', '368208528827232271', '产品与技术服务', null, '', '1', null, '1', '1', '368933812476063755', '368933812476063757', null, null, null, null, null, '0', '2019-10-16 08:54:19', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('381768927404883968', '369305640453013504', '378190751797870592', '368208528827232271', '中心荣誉', null, '', '1', null, '1', '1', '368933812476063755', '368933812476063757', null, null, null, null, null, '0', '2019-10-16 08:54:31', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('381800822641799168', '0', '378190751797870592', '368208528827232271', '新闻中心', null, null, '1', 'http://cdn.weiunity.com/res/glyph-icons/world.png', '1', '1', '368933812476063755', '368933812476063757', null, null, null, null, null, '0', '2019-12-12 15:09:56', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('381800822645993472', '0', '378190751797870592', '368208528827232271', '关于新纶', null, null, '1', 'http://cdn.weiunity.com/res/glyph-icons/world.png', '1', '1', '368933812476063755', '368933812476063757', null, null, null, null, null, '0', '2019-12-12 15:09:58', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('381800822645993473', '0', '381800822650187790', '368208528827232271', '产品', null, null, '0', 'http://cdn.weiunity.com/res/glyph-icons/world.png', '1', '1', null, null, null, null, null, null, null, '0', '2019-12-12 15:10:01', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('381800822645993474', '0', '381800822650187790', '368208528827232271', '复制产品', null, null, '0', 'http://cdn.weiunity.com/res/glyph-icons/world.png', '1', '1', null, null, null, null, null, null, null, '0', '2019-12-12 15:10:04', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('381800822645993475', '0', '381800822650187790', '368208528827232271', 'aaa', null, null, '0', 'http://cdn.weiunity.com/res/glyph-icons/world.png', '1', '1', null, null, null, null, null, null, null, '0', '2019-12-12 15:10:07', null, '1', '0');
INSERT INTO `fly_site_column` VALUES ('402517902718140416', '0', '401359718385188864', '368208528827232271', 'dfgdfgdfg', null, 'dfgdfgfd', '0', null, '1', '2', '368933812476063755', null, null, 'dfgdfgdfg', 'fdgdfg', 'dfgdgdf', 'dfgdfgfdgf', '0', '2019-12-12 15:03:32', null, '1', '0');

-- ----------------------------
-- Table structure for `fly_site_company`
-- ----------------------------
DROP TABLE IF EXISTS `fly_site_company`;
CREATE TABLE `fly_site_company` (
  `id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL COMMENT '企业id',
  `site_id` bigint(20) NOT NULL COMMENT '网站id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of fly_site_company
-- ----------------------------
INSERT INTO `fly_site_company` VALUES ('363663265068220416', '389481100151631908', '363663264883671040');
INSERT INTO `fly_site_company` VALUES ('367356661469282304', '389481100151631908', '367356661297315840');
INSERT INTO `fly_site_company` VALUES ('367474558610964480', '389481100151631908', '367474558459969536');
INSERT INTO `fly_site_company` VALUES ('367866151184629760', '389481100151631908', '367866150970720256');
INSERT INTO `fly_site_company` VALUES ('378189674436362240', '389481100151631908', '378189674327310336');
INSERT INTO `fly_site_company` VALUES ('378190289287774208', '389481100151631908', '378190289216471040');
INSERT INTO `fly_site_company` VALUES ('378190751869173760', '389481100151631908', '378190751797870592');
INSERT INTO `fly_site_company` VALUES ('381800822650187791', '389481100151631908', '381800822650187789');
INSERT INTO `fly_site_company` VALUES ('381800822650187792', '389481100151631908', '381800822650187790');
INSERT INTO `fly_site_company` VALUES ('401359718456492032', '401335316046151680', '401359718385188864');
INSERT INTO `fly_site_company` VALUES ('401751717206556672', '401335316046151680', '401751717164613632');

-- ----------------------------
-- Table structure for `fly_site_links`
-- ----------------------------
DROP TABLE IF EXISTS `fly_site_links`;
CREATE TABLE `fly_site_links` (
  `id` bigint(20) unsigned NOT NULL,
  `link_type` int(2) DEFAULT '0' COMMENT '有钱链接类型：0文字链接，1logo链接',
  `site_id` bigint(20) NOT NULL DEFAULT '0',
  `link_name` varchar(100) DEFAULT NULL COMMENT '网站名称',
  `link_url` varchar(255) DEFAULT NULL COMMENT '网站地址',
  `link_logo` varchar(255) DEFAULT NULL COMMENT '网站logo地址',
  `is_show` int(2) DEFAULT '0' COMMENT '是否显示，0不显示，1显示',
  `sort_order` int(5) DEFAULT '0' COMMENT '排序',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `deleted` tinyint(1) DEFAULT '1' COMMENT '0删除，1显示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='友情链接表';

-- ----------------------------
-- Records of fly_site_links
-- ----------------------------
INSERT INTO `fly_site_links` VALUES ('381800822645993493', '0', '378190751797870592', '开源之家', 'http://www.28844.com', null, '1', '0', '2018-11-12 16:25:50', '1');

-- ----------------------------
-- Table structure for `fly_site_serve`
-- ----------------------------
DROP TABLE IF EXISTS `fly_site_serve`;
CREATE TABLE `fly_site_serve` (
  `id` bigint(20) DEFAULT NULL,
  `serve_name` varchar(100) DEFAULT NULL,
  `content` text,
  `create_time` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0' COMMENT '审核状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='服务项目表';

-- ----------------------------
-- Records of fly_site_serve
-- ----------------------------

-- ----------------------------
-- Table structure for `fly_site_serve_sku`
-- ----------------------------
DROP TABLE IF EXISTS `fly_site_serve_sku`;
CREATE TABLE `fly_site_serve_sku` (
  `id` bigint(20) NOT NULL,
  `serve_id` bigint(20) DEFAULT NULL COMMENT '服务项目id',
  `use_time` int(2) unsigned zerofill DEFAULT NULL COMMENT '使用时间，1为一周,2一年,3两年,4三年，5五年',
  `price` decimal(8,2) DEFAULT '0.00' COMMENT '销售价格',
  `stock_number` int(11) DEFAULT '0' COMMENT '库存数量',
  `sold_number` int(11) DEFAULT '0' COMMENT '已售数量'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='服务库存设置';

-- ----------------------------
-- Records of fly_site_serve_sku
-- ----------------------------

-- ----------------------------
-- Table structure for `fly_site_template_merge`
-- ----------------------------
DROP TABLE IF EXISTS `fly_site_template_merge`;
CREATE TABLE `fly_site_template_merge` (
  `id` bigint(20) NOT NULL,
  `site_id` bigint(20) DEFAULT NULL COMMENT '网站id',
  `template_id` bigint(20) DEFAULT NULL COMMENT '模板id',
  `template_name` varchar(100) DEFAULT NULL COMMENT '模板名称',
  `template_catalog` varchar(255) DEFAULT NULL COMMENT '模板目录',
  `add_time` datetime DEFAULT NULL,
  `expire_time` datetime DEFAULT NULL COMMENT '到期时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of fly_site_template_merge
-- ----------------------------
INSERT INTO `fly_site_template_merge` VALUES ('397799559121874953', '363539057487118336', '366783118428090393', 'website', '/2019/11/29/', '2019-11-29 14:37:48', '2020-11-29 14:37:59');
INSERT INTO `fly_site_template_merge` VALUES ('397799559121874954', '378190751797870592', '371083359947849728', 'wxb002', '/2019/11/15/', '2019-11-29 16:26:43', '2019-11-29 16:26:48');

-- ----------------------------
-- Table structure for `fly_site_template_order`
-- ----------------------------
DROP TABLE IF EXISTS `fly_site_template_order`;
CREATE TABLE `fly_site_template_order` (
  `id` bigint(20) NOT NULL,
  `site_id` bigint(20) DEFAULT NULL COMMENT '网站id',
  `template_id` bigint(20) DEFAULT NULL COMMENT '模板id',
  `add_time` datetime DEFAULT NULL,
  `expire_time` datetime DEFAULT NULL COMMENT '到期时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of fly_site_template_order
-- ----------------------------

-- ----------------------------
-- Table structure for `fly_site_template_page`
-- ----------------------------
DROP TABLE IF EXISTS `fly_site_template_page`;
CREATE TABLE `fly_site_template_page` (
  `id` bigint(20) NOT NULL,
  `site_id` bigint(20) DEFAULT NULL,
  `template_id` bigint(20) NOT NULL COMMENT '模板主表id',
  `module_id` bigint(20) DEFAULT '0' COMMENT '模型fly_system_module表id；对应的如新闻模块id，产品模块id',
  `page_type` int(5) DEFAULT '0' COMMENT '模板类型，0网站首页，1频道封面（频道首页）模板，2列表模板，3内容模板，4单独页面模板',
  `template_name` char(20) DEFAULT NULL COMMENT '所属模版的名字',
  `template_filename` varchar(255) NOT NULL COMMENT '文件名称',
  `template_page` mediumtext COMMENT '模板内容',
  `template_catalog` varchar(255) DEFAULT NULL COMMENT '模板目录',
  `radio` smallint(2) DEFAULT '0' COMMENT '单选择，0为不选择，1为选择',
  `remark` char(30) DEFAULT NULL COMMENT '备注，限制30个字以内',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `deleted` tinyint(2) DEFAULT '0' COMMENT '是否已经被删除。0正常，1已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户购买后的模板';

-- ----------------------------
-- Records of fly_site_template_page
-- ----------------------------
INSERT INTO `fly_site_template_page` VALUES ('368933812476063754', '378190751797870592', '371083359947849728', '368208528827232271', '0', 'lmyboke1', 'index', '<!DOCTYPE html>\r\n<html lang=\"zh-cn\">\r\n<head>\r\n	<meta charset=\"UTF-8\" />\r\n	<title>${global_site_name}</title>\r\n    <meta name=\"description\" content=\"【国家高新技术企业】千助,北京网站建设领跑者,10年专注高端网站建设,网页设计联盟金牌企业,自主研发13项软件著作权.有针对性的提供北京网站建设报价和方案.网站建设咨询: 4006-123-011\" />\r\n    <meta name=\"keywords\" content=\"网站建设,北京网站建设,网站制作,北京网站制作,网站设计,北京网站设计,网页设计,北京网页设计,北京网站建设公司,高端网站建设,做网站,建网站,北京高端网站建设公司\" />\r\n    <meta name=\"viewport\" content=\"width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no\" />\r\n    <meta name=\"copyright\" content=\"Copyright 1000zhu.com 版权所有\" />\r\n    <link href=\"${global_templets_skin}/skin/h5/qzjz/css/bootstrap.min.css\" rel=\"stylesheet\" />\r\n    <link href=\"${global_templets_skin}/skin/h5/qzjz/css/index.min.css\" rel=\"stylesheet\" />\r\n    <link href=\"${global_templets_skin}/skin/h5/qzjz/css/index.css\" rel=\"stylesheet\" />\r\n    <!--[if lt IE 9]>\r\n    <script src=\"${global_templets_skin}/skin/h5/qzjz/js/html5shiv.min.js\"></script>\r\n    <![endif]-->\r\n    <script src=\"${global_templets_skin}/skin/h5/qzjz/js/jquery.min.js\"></script>\r\n    <script src=\"${global_templets_skin}/skin/h5/qzjz/js/index.min.js\"></script>\r\n</head>\r\n\r\n<body>\r\n    <header>\r\n        <div class=\"logo\">\r\n          <a href=\"http://www.1000zhu.com/\"><img src=\"${global_templets_skin}/skin/h5/qzjz/picture/logo.png\" alt=\"北京网站建设_网站制作_千助\" class=\"img-responsive ori\" /></a>\r\n        </div>\r\n        <nav class=\"menu\">\r\n          <ul class=\"list-inline\">\r\n            <li class=\"active\"><a>首页</a></li>\r\n            <li><a>业务</a></li>\r\n            <li><a>案例</a></li>\r\n            <li><a>客户</a></li>\r\n            <li><a>品质</a></li>\r\n            <li><a>增值</a></li>\r\n            <li><a>关于</a></li>\r\n            <li><a>联系</a></li>\r\n          </ul>\r\n        </nav>\r\n        <div class=\"hotline\">\r\n          <a href=\"tel:4006123011\" title=\"网站建设免费咨询热线\"><span>4006-123-011</span></a><u></u>\r\n        </div>\r\n        <div class=\"menu-icon\">\r\n  			<a href=\"tel:4006123011\" title=\"点击直拨高端网站设计热线\"><span class=\"glyphicon glyphicon-earphone\"></span></a>\r\n            <span class=\"glyphicon glyphicon-th-large\"></span>\r\n        </div>\r\n    </header>\r\n    \r\n    <div class=\"welcome\"><p><u></u></p></div>\r\n    \r\n    <section class=\"video\">\r\n    	<div class=\"swiper-container\">\r\n            <div class=\"swiper-wrapper\">\r\n              <div class=\"swiper-slide nth1\">\r\n                <div class=\"box\">\r\n                    <div class=\"r\">\r\n                        <h1>高端定制网站建设</h1>\r\n                    </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"swiper-slide nth2\">\r\n              	<div class=\"box\">\r\n                    <div class=\"r\">\r\n                        <span>中标京城地铁网站建设项目</span>\r\n                    </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"swiper-slide nth3\">\r\n                  <div class=\"box\">\r\n                      <div class=\"r\">\r\n                          <span>祝贺中国开发性金融促进会官方网站建设全新上线</span>\r\n                      </div>\r\n                  </div>\r\n              </div>\r\n              <div class=\"swiper-slide nth4\">\r\n                  <div class=\"box\">\r\n                      <div class=\"r\">\r\n                          <span>全面中标铭泰集团官方网站设计及旗下子公司的网站制作站群项目</span>\r\n                      </div>\r\n                  </div>\r\n              </div>\r\n              <div class=\"swiper-slide nth5\">\r\n                  <div class=\"box\">\r\n                      <div class=\"r\">\r\n                          <span>联合开发坏猴子影业官方网站建设项目</span>\r\n                      </div>\r\n                  </div>\r\n              </div>\r\n              <div class=\"swiper-slide nth6\">\r\n                  <div class=\"box\">\r\n                      <div class=\"r\">\r\n                          <span>成功上线中国扶贫开发协会官方网站</span>\r\n                      </div>\r\n                  </div>\r\n              </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"innerBox\">\r\n        	<div class=\"news\">\r\n            	<span>NEWS :</span>\r\n                <a href=\"/article/\" title=\"更多网站建设相关文章\" class=\"more\" target=\"_blank\">more</a>\r\n                <ul>\r\n                  <li><a href=\"/article/152.html\" target=\"_blank\" title=\"给你看八个网页特效，让你的网站建设更加引人入胜！\">给你看八个网页特效，让你的网站建设更 ...</a></li>\r\n<li><a href=\"/article/151.html\" target=\"_blank\" title=\"如何使用以图像为中心的设计来提高网站转化率\">如何使用以图像为中心的设计来提高网站 ...</a></li>\r\n<li><a href=\"/article/150.html\" target=\"_blank\" title=\"歪果仁的那些404错误页面设计真的有创意吗？\">歪果仁的那些404错误页面设计真的有创意 ...</a></li>\r\n<li><a href=\"/article/149.html\" target=\"_blank\" title=\"为什么你的客户需要的是响应式网站，而不是APP应用程序\">为什么你的客户需要的是响应式网站，而 ...</a></li>\r\n<li><a href=\"/article/148.html\" target=\"_blank\" title=\"百度启动原创保护计划，如何申请开通百度原创保护？\">百度启动原创保护计划，如何申请开通百 ...</a></li>\r\n\r\n                </ul>\r\n            </div>\r\n            <div class=\"guide\"></div>\r\n            <a class=\"movedown\"></a>\r\n        </div>\r\n    </section>\r\n    \r\n    <section class=\"business\">\r\n      <div class=\"box\">\r\n        <div class=\"caption\">\r\n        	<i></i><span>我们能做什么</span>\r\n            <br class=\"clear\" />\r\n        </div>\r\n        <ul class=\"items list-inline\">\r\n        	<li class=\"pc\">\r\n            	<i></i><strong>高端定制网站</strong>\r\n                <p>企业高端定制网站设计<br />彰显品牌形象</p>\r\n            </li>\r\n            <li class=\"mobi\">\r\n            	<i></i><strong>移动网站建设</strong>\r\n                <p>定制手机网站 / 微网站制作<br />布局移动互联网</p>\r\n            </li>\r\n            <li class=\"sys\">\r\n            	<i></i><strong>业务系统研发</strong>\r\n                <p>基于 B/S 架构的系统研发<br />让业务办公轻松自如</p>\r\n            </li>\r\n            <li class=\"app\">\r\n            	<i></i><strong>APP应用程序</strong>\r\n                <p>基于 iOS / Android 应用开发<br />掌控智能终端时代</p>\r\n            </li>\r\n            <li class=\"host\">\r\n            	<i></i><strong>服务器运维</strong>\r\n                <p>我们不只提供云硬件和网络<br />更加注重技术运维</p>\r\n            </li>\r\n        </ul>\r\n      </div>\r\n    </section>\r\n    \r\n    <section class=\"cases\">\r\n      <div class=\"box\">\r\n    	<div class=\"caption\">\r\n        	<i></i><span>网站设计案例欣赏</span>\r\n            <br class=\"clear\" />\r\n        </div>\r\n        <div class=\"swiper-container items\">\r\n           <div class=\"swiper-wrapper\">\r\n              <div class=\"swiper-slide\">\r\n                  <a href=\"/cases/72.html\" target=\"_blank\">\r\n                  <img src=\"${global_templets_skin}/skin/h5/qzjz/picture/0110028059.jpg\" alt=\"ATA官网建设\" />\r\n                  <p>网站设计<br /><strong>ATA官网建设</strong><br />官方网站 , 上市公司 , 红色</p></a>\r\n                </div>\r\n<div class=\"swiper-slide\">\r\n                  <a href=\"/cases/59.html\" target=\"_blank\">\r\n                  <img src=\"${global_templets_skin}/skin/h5/qzjz/picture/1153507477.jpg\" alt=\"中远国际航空网站设计\" />\r\n                  <p>网站设计<br /><strong>中远国际航空网站设计</strong><br />国有企业 , 蓝色 , 欧美风格 , 上市公司</p></a>\r\n                </div>\r\n<div class=\"swiper-slide\">\r\n                  <a href=\"/cases/62.html\" target=\"_blank\">\r\n                  <img src=\"${global_templets_skin}/skin/h5/qzjz/picture/0244085480.jpg\" alt=\"海尔嫩烤箱官方网站建设\" />\r\n                  <p>网站设计<br /><strong>海尔嫩烤箱官方网站建设</strong><br />上市公司 , 官方网站 , 欧美风格</p></a>\r\n                </div>\r\n\r\n           </div>\r\n        </div>\r\n        <a href=\"/cases/\" title=\"欣赏更多北京网站建设的设计案例\" class=\"more\" target=\"_blank\">更多设计案例</a>\r\n      </div>\r\n    </section>\r\n    \r\n    <section class=\"clients\">\r\n   	  <div class=\"box\">\r\n    	<div class=\"caption\">\r\n        	<i></i><span>他们与千助长期合作</span>\r\n            <br class=\"clear\" />\r\n        </div>\r\n        <ul class=\"items list-inline\">\r\n        	<li class=\"cctv\"><span>CCTV影响力视频网站建设</span></li><li class=\"qinghua\"><span>清华大学国际预科学院网站建设</span></li><li class=\"lenovo\"><span>联想控股成员企业网站建设</span></li><li class=\"cas\"><span>中科院研究所网站设计</span></li><li class=\"apple\"><span>中航苹果官方网站设计</span></li><li class=\"das\"><span>一汽大众汽车门户网站建设</span></li><li class=\"ata\"><span>ATA全美在线官方网站建设</span></li><li class=\"zhongxin\"><span>中信集团控股公司网站建设</span></li><li class=\"haier\"><span>海尔嫩烤箱网站建设</span></li><li class=\"cosco\"><span>中远国际航空货运网站设计</span></li><li class=\"zhongying\"><span>中影集团后期制作网站建设</span></li><li class=\"toread\"><span>探路者冰雪控股网站制作</span></li><li class=\"neusoft\"><span>东软慧聚官方网站制作</span></li><li class=\"zjgl\"><span>中交公路局海外分公司网站建设</span></li><li class=\"report\"><span>中国报道信息门户网站建设</span></li>\r\n        </ul>\r\n      </div>\r\n    </section>\r\n    \r\n    <section class=\"quality\">\r\n      <div class=\"box\">\r\n    	<div class=\"caption\">\r\n        	<i></i><span>不同媒介，同样精彩</span>\r\n            <br class=\"clear\" />\r\n        </div>\r\n        <div class=\"swiper-container items\">\r\n            <div class=\"swiper-wrapper\">\r\n              <div class=\"swiper-slide nth1\">\r\n                <ul class=\"list-inline\">\r\n                  <li class=\"mobi\"><span>响应式手机网站建设</span></li><li class=\"pad\"><span>响应式平板网站建设</span></li><li class=\"pc\"><span>响应式PC网站建设</span></li>\r\n                </ul>\r\n                <p>触及视觉灵魂的设计趋势<br />精心布局的用户体验<br />毫无顾忌地通过任何终端<br />呈现在客户的眼前</p>\r\n              </div>\r\n              <div class=\"swiper-slide nth2\">\r\n              	<ul class=\"list-inline\">\r\n                  <li class=\"ie\"><span>兼容微软IE浏览器的网页设计</span></li><li class=\"chrome\"><span>兼容谷歌Chrome浏览器的网站设计</span></li><li class=\"firefox\"><span>兼容火狐Firefox浏览器的网页设计</span></li><li class=\"safari\"><span>兼容苹果Safari浏览器的网站设计</span></li>\r\n                </ul>\r\n                <p>Html5 + CSS3 响应式布局<br />卓越的浏览器兼容性<br />因为高端，所以出众</p>\r\n              </div>\r\n              <div class=\"swiper-slide nth3\">\r\n              	<ul class=\"list-inline\">\r\n                  <li class=\"windows\"><span>跨windows平台网站制作</span></li><li class=\"ios\"><span>跨ios平台网站制作</span></li><li class=\"andriod\"><span>跨andriod平台网站制作</span></li>\r\n                </ul>\r\n                <p>基于 B/S 架构的网站建设<br />无障碍的跨平台应用<br />无须用户下载安装即可使用<br />云端管理，轻松维护</p>\r\n              </div>\r\n            </div>\r\n        </div>\r\n        <!--\r\n          <a href=\"/tool/responsive/?url=http://www.1000zhu.com\" title=\"北京网站设计响应式网站测试工具\" class=\"lookall\" target=\"_blank\">响应式网站的照妖镜</a>\r\n        -->\r\n      </div>\r\n    </section>\r\n    \r\n    <section class=\"marketing\">\r\n      <div class=\"box\">\r\n        <div class=\"caption\">\r\n        	<i></i><span>整合营销，抢占商机</span>\r\n            <br class=\"clear\" />\r\n        </div>\r\n        <ul class=\"items list-inline\">\r\n        	<li class=\"se\">\r\n            	<i></i><strong>搜索引擎</strong>\r\n                <p>SEO 优化<br />搜索引擎竞价</p>\r\n            </li>\r\n            <li class=\"weixin\">\r\n            	<i></i><strong>微信营销</strong>\r\n                <p>公众账号 / 微网站<br />微盟 ( 微社区 )</p>\r\n            </li>\r\n            <li class=\"weibo\">\r\n            	<i></i><strong>微博营销</strong>\r\n                <p>企业蓝V认证<br />官方微博接入网站</p>\r\n            </li>\r\n            <li class=\"sms\">\r\n            	<i></i><strong>消息推送</strong>\r\n                <p>短信平台接口<br />Email 推送</p>\r\n            </li>\r\n            <li class=\"pay\">\r\n            	<i></i><strong>在线支付</strong>\r\n                <p>支付宝、银联<br />Paypal 接口</p>\r\n            </li>\r\n            <li class=\"bbs\">\r\n            	<i></i><strong>论坛聚人</strong>\r\n                <p>独立开发<br />会员打通</p>\r\n            </li>\r\n        </ul>\r\n      </div>\r\n    </section>\r\n    \r\n    <section class=\"aboutus\">\r\n    	<ul class=\"menu\"><li>思想</li><li>关于</li><li>荣誉</li></ul>\r\n        <div class=\"swiper-container items\">\r\n            <div class=\"swiper-wrapper\">\r\n              <div class=\"swiper-slide nth1\">\r\n                <strong>厚积薄发</strong>\r\n                <p>登上峰顶，不是为了饱览风光，是为了寻找更高的山峰<br />日出东方，告别了昨天的荣耀，将光芒照向更远的地方<br />一路上，我们更在意如何积累和沉淀</p>\r\n                <u>下一秒，让你看，我们到底有多强</u>\r\n              </div>\r\n              <div class=\"swiper-slide nth2\">\r\n              	<strong>千助网建科技（北京）有限公司</strong>\r\n                <p>成立于2008年，坐落于北京中关村科技园区，是中国优秀的互联网服务提供商。自成立以来，专注于高端网站建设、移动互联应用、B/S架构系统研发、云服务器部署和运维，为企业客户的互联网应用提供一站式服务。</p>\r\n                <p>我们始终坚持以客户需求为导向，为追求用户体验设计，提供有针对性的项目解决方案，千助人将不断地超越自我，挑战险峰！</p>\r\n              </div>\r\n              <div class=\"swiper-slide nth3\">\r\n              	<strong>放下荣誉，放眼未来</strong>\r\n                <ul>\r\n                    <li>2016年<u>-</u>荣获国家高新技术企业</li>\r\n                    <li>2013年<u>-</u>已拥有自主知识产权，国家软件著作权达12项之多</li>\r\n                    <li>2011年<u>-</u>注册 \" 千助 \" 商标，打造北京网站建设市场知名品牌</li>\r\n                    <li>2010年<u>-</u>荣获中关村高新技术企业、海淀区创新企业</li>\r\n                    <li>2010年<u>-</u>设计联盟评选为金牌设计企业</li>\r\n                </ul>\r\n              </div>\r\n            </div>\r\n        </div>\r\n        <table class=\"exp\">\r\n        	<tr>\r\n              <td><u>1257</u>位企业客户信赖之选</td>\r\n              <td><u>367</u>上市/集团企业设计作品</td>\r\n              <td><u>2016</u>年获国家高新技术企业</td>\r\n              <td><u>15</u>项国家软件著作权</td>\r\n              <td><u>97%</u>以上的客户续费率</td>\r\n            </tr>\r\n        </table>\r\n    </section>\r\n    \r\n    <section class=\"contact\">\r\n    	<div class=\"box\">\r\n        	<div class=\"above\">\r\n            	<div class=\"wechat\"><img src=\"${global_templets_skin}/skin/h5/qzjz/picture/wechat_code.jpg\" alt=\"扫描关注千助微信公众账号\" /></div>\r\n                <div class=\"left\">\r\n                	<a href=\"tel:4006123011\" title=\"网站制作咨询热线\" class=\"tel\"></a>\r\n                    <p>工作日：4006123011 / 010-80757532<br />非工作日：4006123011 / 18511639815<br />科技园区：北京 · 中关村 · 昌发展<br />办公地址：北京市昌发展万科广场A座6层</p>\r\n                </div>\r\n                <div class=\"right\">\r\n                    官网：www.1000zhu.com<br />Email：Service@1000zhu.com<br />千助网建科技（北京）有限公司<br />京ICP备 08102474 号<br />京公网安备 11010802010984 号\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </section>\r\n\r\n    <section class=\"cooperation\">\r\n        <div class=\"box\">\r\n            <span class=\"title\">友情链接，携手共进</span>\r\n            <ul class=\"list-inline\">\r\n                <li><a href=\"http://www.1000zhu.com\" target=\"_blank\">北京网站建设</a></li>\r\n<li><a href=\"http://www.1000zhu.com\" target=\"_blank\">高端网站建设</a></li>\r\n<li><a href=\"http://www.1000zhu.com\" target=\"_blank\">北京网站设计</a></li>\r\n<li><a href=\"http://www.wanhu.cn\" target=\"_blank\">上海网站建设</a></li>\r\n<li><a href=\"http://www.1000zhu.com\" target=\"_blank\">北京网站建设公司</a></li>\r\n<li><a href=\"http://www.1000zhu.com\" target=\"_blank\">北京网站制作</a></li>\r\n<li><a href=\"http://www.01jianzhan.com\" target=\"_blank\">广州网站建设</a></li>\r\n<li><a href=\"http://www.ynyes.com\" target=\"_blank\">云南网站建设</a></li>\r\n<li><a href=\"https://www.zwcnw.com/\" target=\"_blank\">广州建站</a></li>\r\n<li><a href=\"http://www.1000zhu.com/article/143.html\" target=\"_blank\">上地网站建设</a></li>\r\n<li><a href=\"http://www.sscmwl.com\" target=\"_blank\">网站建设</a></li>\r\n<li><a href=\"http://www.xdnet.cn\" target=\"_blank\">西安网站建设</a></li>\r\n<li><a href=\"http://www.w-e.cc\" target=\"_blank\">烟台网站建设</a></li>\r\n<li><a href=\"http://www.qiangseo.com\" target=\"_blank\">长沙网站推广</a></li>\r\n<li><a href=\"http://www.lkcms.com\" target=\"_blank\">成都网站建设</a></li>\r\n<li><a href=\"http://www.ymars.com\" target=\"_blank\">长沙网站建设</a></li>\r\n<li><a href=\"http://www.zijiren.net\" target=\"_blank\">深圳网站建设</a></li>\r\n<li><a href=\"http://www.ezw.net.cn\" target=\"_blank\">石家庄网站建设</a></li>\r\n<li><a href=\"http://www.bjhyn.net\" target=\"_blank\">北京网站建设</a></li>\r\n\r\n            </ul>\r\n        </div>\r\n        <div class=\"bg\"></div>\r\n    </section>\r\n    \r\n    <div class=\"dock\">\r\n        <ul class=\"icons\">\r\n        	<li class=\"up\"><i></i></li>\r\n            <li class=\"im\">\r\n            	<i></i><p>网站建设咨询<br />在线沟通，请点我<a href=\"http://p.qiao.baidu.com/cps/chat?siteId=7773323&userId=989741\" target=\"_blank\">在线咨询</a></p>\r\n            </li>\r\n            <li class=\"tel\">\r\n            	<i></i><p>我要做网站，<br />小助啊，抓紧给我来个电话！<span class=\"callback\"><input type=\"text\" maxlength=\"12\" /><button>回拨</button></span></p>\r\n            </li>\r\n            <li class=\"wechat\">\r\n            	<i></i><p><img src=\"${global_templets_skin}/skin/h5/qzjz/picture/wechat_code.jpg\" alt=\"扫描关注网站制作微信公众账号\" /></p>\r\n            </li>\r\n            <li class=\"down\"><i></i></li>\r\n        </ul>\r\n        <a class=\"switch\"></a>\r\n    </div>\r\n</body>\r\n</html>\r\n', '/2019/11/15/', '0', '首页', null, null, '0');
INSERT INTO `fly_site_template_page` VALUES ('368933812476063755', '401359718385188864', '371083359947849728', '368208528827232271', '1', 'lmyboke1', 'news', null, '/2019/11/15/', '0', '作品列表页面', null, null, '0');
INSERT INTO `fly_site_template_page` VALUES ('368933812476063756', '378190751797870592', '371083359947849728', '368208528827232271', '1', 'lmyboke1', 'news', null, '/2019/11/15/', '0', '文章详情、作品详情', null, null, '0');
INSERT INTO `fly_site_template_page` VALUES ('368933812476063757', '378190751797870592', '371083359947849728', '368208528827232271', '2', 'lmyboke1', 'news_list', null, '/2019/11/15/', '0', '博客的文章列表页面', null, null, '0');
INSERT INTO `fly_site_template_page` VALUES ('368933812476063758', '378190751797870592', '371083359947849728', '368208528827232271', '2', 'lmyqymb15', 'news_list', null, '/2019/11/15/', '0', '这是首页', null, null, '0');

-- ----------------------------
-- Table structure for `fly_site_template_tags`
-- ----------------------------
DROP TABLE IF EXISTS `fly_site_template_tags`;
CREATE TABLE `fly_site_template_tags` (
  `id` bigint(20) NOT NULL COMMENT '模版的名字，模版编码',
  `site_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '网站id',
  `template_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '模板id',
  `tag_key` varchar(100) NOT NULL COMMENT '针对模板调用的key',
  `tag_value` longtext COMMENT '标签内容',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='模板与用户关联表';

-- ----------------------------
-- Records of fly_site_template_tags
-- ----------------------------
INSERT INTO `fly_site_template_tags` VALUES ('394939677389631500', '378190751797870592', '371083359947849728', 'newstags', 'cccccccc', '2019-11-21 19:13:54');

-- ----------------------------
-- Table structure for `fly_slide`
-- ----------------------------
DROP TABLE IF EXISTS `fly_slide`;
CREATE TABLE `fly_slide` (
  `id` bigint(20) NOT NULL,
  `site_id` bigint(20) DEFAULT NULL COMMENT '轮播图属于哪个站点，对应site.id',
  `url` char(120) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '点击跳转的目标url',
  `isshow` tinyint(2) DEFAULT '1' COMMENT '是否显示，1为显示，0为不显示',
  `sort_order` int(11) DEFAULT '0' COMMENT '排序，数小越靠前',
  `image` char(120) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '轮播图的url，分两种，一种只是文件名，如asd.png  另一种为绝对路径',
  `type` tinyint(2) DEFAULT '1' COMMENT '类型，默认1:内页通用的头部图(有的模版首页也用)；2:只有首页顶部才会使用的图',
  `add_time` int(11) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='网站轮播图，手机、电脑模式网站用到。现主要做CMS类型网站，CMS模式网站这个是用不到的。';

-- ----------------------------
-- Records of fly_slide
-- ----------------------------

-- ----------------------------
-- Table structure for `fly_sms_api`
-- ----------------------------
DROP TABLE IF EXISTS `fly_sms_api`;
CREATE TABLE `fly_sms_api` (
  `id` bigint(20) NOT NULL,
  `api_name` varchar(100) DEFAULT NULL,
  `access_key_id` varchar(255) DEFAULT NULL,
  `access_key_secret` varchar(255) DEFAULT NULL,
  `api_url` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of fly_sms_api
-- ----------------------------
INSERT INTO `fly_sms_api` VALUES ('317765676100235264', '阿里大鱼接口', 'LTAIrDwtR6J1kvdW', 'EMCuJhkLkq7jfPavu1ZdUWd6F8fVkr', 'htt://www.aliyun.com', '2019-04-22 18:09:09', '1');

-- ----------------------------
-- Table structure for `fly_sms_sign`
-- ----------------------------
DROP TABLE IF EXISTS `fly_sms_sign`;
CREATE TABLE `fly_sms_sign` (
  `id` bigint(20) NOT NULL,
  `api_id` bigint(20) NOT NULL,
  `sign_name` varchar(50) NOT NULL,
  `remark` varchar(100) DEFAULT NULL COMMENT '接口描述',
  `create_time` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of fly_sms_sign
-- ----------------------------
INSERT INTO `fly_sms_sign` VALUES ('317766251084787712', '317765676100235264', '悦颜之选', '微信小程序签名', '2019-04-22 18:11:26', '1');

-- ----------------------------
-- Table structure for `fly_sms_template`
-- ----------------------------
DROP TABLE IF EXISTS `fly_sms_template`;
CREATE TABLE `fly_sms_template` (
  `id` bigint(20) NOT NULL,
  `sign_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '所属签名id',
  `template_name` varchar(100) NOT NULL,
  `template_code` varchar(255) NOT NULL,
  `detail` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0' COMMENT '0审核，1显示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of fly_sms_template
-- ----------------------------
INSERT INTO `fly_sms_template` VALUES ('317765676100235265', '317766251084787712', '注册验证码', 'SMS_163605026', '手机注册验证码', '2019-09-02 09:08:17', '1');

-- ----------------------------
-- Table structure for `fly_system_module`
-- ----------------------------
DROP TABLE IF EXISTS `fly_system_module`;
CREATE TABLE `fly_system_module` (
  `id` bigint(20) NOT NULL COMMENT '模型id',
  `module_name` varchar(50) NOT NULL COMMENT '模块名称',
  `module_type` varchar(50) NOT NULL COMMENT '模型类别',
  `remark` varchar(255) DEFAULT NULL COMMENT '模型说明',
  `status` int(1) unsigned DEFAULT '1' COMMENT '使用状态，0停用，1启用，',
  `sort_order` tinyint(3) DEFAULT '50' COMMENT '排序',
  `add_time` datetime DEFAULT NULL COMMENT '模块添加时间',
  `update_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统模块管理';

-- ----------------------------
-- Records of fly_system_module
-- ----------------------------
INSERT INTO `fly_system_module` VALUES ('368208528827232271', '新闻资讯', 'news', '可设置为新闻，资讯，文章栏目使用', '1', '1', '2019-09-08 22:52:39', null, '0');
INSERT INTO `fly_system_module` VALUES ('368208528827232272', '产品模块', 'product', '企业产品展示模块', '1', '2', '2019-09-08 22:53:44', null, '0');

-- ----------------------------
-- Table structure for `fly_template`
-- ----------------------------
DROP TABLE IF EXISTS `fly_template`;
CREATE TABLE `fly_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `template_name` char(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '模版的名字，编码，唯一，限制50个字符以内',
  `template_directory` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '模板存放目录名',
  `user_id` bigint(20) DEFAULT '0' COMMENT '此模版所属的用户，user.id。如果此模版是用户的私有模版，也就是 iscommon=0 时，这里存储导入此模版的用户的id',
  `remark` char(200) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '模版的简介，备注说明，限制300字以内',
  `preview_url` char(200) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '模版预览网址，示例网站网址，绝对路径，',
  `preview_pic` char(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '模版预览图的网址，preview.jpg 图片的网址',
  `template_type` bigint(20) DEFAULT NULL COMMENT '模版所属分类，如广告、科技、生物、医疗等。',
  `company_name` char(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '模版开发者公司名字。如果没有公司，则填写个人姓名',
  `user_name` char(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '模版开发人员的名字，姓名',
  `site_url` char(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '模版开发者官方网站、企业官网。如果是企业，这里是企业官网的网址，格式如： http://www.leimingyun.com  ，如果是个人，则填写个人网站即可',
  `terminal_mobile` tinyint(2) DEFAULT NULL COMMENT '网站模版是否支持手机端, 1支持，0不支持',
  `terminal_pc` tinyint(2) DEFAULT NULL COMMENT '网站模版是否支持PC端, 1支持，0不支持',
  `terminal_ipad` tinyint(2) DEFAULT NULL COMMENT '网站模版是否支持平板电脑, 1支持，0不支持',
  `price` decimal(2,0) DEFAULT '0' COMMENT '模板售价',
  `discount` decimal(2,0) DEFAULT '0' COMMENT '促销折扣',
  `sort_order` int(11) DEFAULT '50' COMMENT '公共模版的排序，数字越小越靠前。',
  `add_time` datetime DEFAULT NULL COMMENT '模版添加时间',
  `update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `status` int(2) DEFAULT '0' COMMENT '审核状态',
  `deleted` int(2) DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`),
  KEY `name` (`template_name`,`user_id`,`template_type`,`company_name`,`user_name`,`terminal_mobile`,`terminal_pc`,`terminal_ipad`,`add_time`,`sort_order`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=371126252897042433 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='模版';

-- ----------------------------
-- Records of fly_template
-- ----------------------------
INSERT INTO `fly_template` VALUES ('366783118428090393', 'default', null, '0', null, 'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=865746219,1622457009&fm=26&gp=0.jpg', null, null, null, null, null, null, null, null, null, '0', null, null, null, '0', '0');
INSERT INTO `fly_template` VALUES ('371083359947849728', 'dsfsdfsdf', 'wxb002', '0', 'dsfsdfdsfd', 'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=865746219,1622457009&fm=26&gp=0.jpg', null, null, 'sdfsdf', null, 'sdfsdf', '1', '1', '1', null, '0', '50', '2019-09-16 21:13:53', null, '0', '0');
INSERT INTO `fly_template` VALUES ('371126252897042432', '修改模板', null, '0', 'dsfsdfdsfd', 'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=865746219,1622457009&fm=26&gp=0.jpg', null, '370389471591673857', 'sdfsdf', null, 'sdfsdf', '1', '1', '1', '60', '0', '50', '2019-09-17 00:04:20', null, '0', '0');

-- ----------------------------
-- Table structure for `fly_template_column`
-- ----------------------------
DROP TABLE IF EXISTS `fly_template_column`;
CREATE TABLE `fly_template_column` (
  `id` bigint(20) NOT NULL COMMENT '分类名称',
  `column_name` varchar(20) DEFAULT NULL,
  `add_time` datetime DEFAULT NULL COMMENT '添加时间',
  `sort_order` int(5) DEFAULT '50' COMMENT '排序',
  `status` int(2) DEFAULT '0' COMMENT '审核状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='模板分类';

-- ----------------------------
-- Records of fly_template_column
-- ----------------------------
INSERT INTO `fly_template_column` VALUES ('370389471591673856', '地产建筑', '2019-09-14 23:17:03', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471591673857', '珠宝服饰', '2019-09-14 23:17:31', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868160', '门户电商', '2019-09-14 23:17:57', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868161', '教育医疗', '2019-09-14 23:18:23', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868162', '金融投资', '2019-09-14 23:18:55', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868163', '政府机构', '2019-09-14 23:19:14', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868164', '装饰装修', '2019-09-14 23:19:34', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868165', '汽车旅游', '2019-09-14 23:19:55', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868166', '木屋构造', '2019-09-14 23:20:45', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868167', '空气净化', '2019-09-14 23:21:07', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868168', '家政服务', '2019-09-14 23:21:28', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868169', '园林绿化', '2019-09-14 23:21:50', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868170', '户外广告', '2019-09-14 23:22:20', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868171', '文化传媒', '2019-09-14 23:22:46', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868172', '律师行业', '2019-09-14 23:23:12', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868173', '会计服务', '2019-09-14 23:23:37', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868174', '土壤工程', '2019-09-14 23:24:02', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868175', '包装设计', '2019-09-14 23:24:36', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868176', '餐饮服务', '2019-09-14 23:25:04', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868177', '科技行业', '2019-09-14 23:25:23', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868178', '招商加盟', '2019-09-14 23:26:09', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868179', '家具装修', '2019-09-14 23:26:28', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868180', '机械工程', '2019-09-14 23:26:49', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868181', '玻璃制造', '2019-09-14 23:27:12', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868182', '艺术品加工', '2019-09-14 23:27:49', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868183', '婚纱摄影', '2019-09-14 23:28:12', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868184', '相册相框', '2019-09-14 23:28:35', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868185', '壁画墙纸', '2019-09-14 23:29:14', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868186', '灯光照明', '2019-09-14 23:29:32', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868188', '门窗系列', '2019-09-14 23:30:44', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868189', '口腔美容', '2019-09-14 23:31:51', '50', '0');
INSERT INTO `fly_template_column` VALUES ('370389471595868190', '书画艺术', '2019-09-14 23:32:06', '50', '0');

-- ----------------------------
-- Table structure for `fly_template_page`
-- ----------------------------
DROP TABLE IF EXISTS `fly_template_page`;
CREATE TABLE `fly_template_page` (
  `id` bigint(20) NOT NULL,
  `template_id` bigint(20) DEFAULT NULL COMMENT '模板主表id',
  `module_id` bigint(20) DEFAULT '0' COMMENT '模型fly_system_module表id；0为首页模板，对应的如新闻模块id，产品模块id',
  `page_type` int(5) DEFAULT '0' COMMENT '模板类型，0网站首页，1频道封面（频道首页）模板，2列表模板，3内容模板，4单独页面模板',
  `template_name` char(20) DEFAULT NULL COMMENT '所属模版的名字',
  `template_catalog` varchar(255) DEFAULT NULL COMMENT '模板目录',
  `template_page` longtext COMMENT '模板内容',
  `remark` char(30) DEFAULT NULL COMMENT '备注，限制30个字以内',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `status` int(1) DEFAULT '0' COMMENT '1正常；0停止',
  `deleted` tinyint(2) DEFAULT '0' COMMENT '是否已经被删除。0正常，1已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='模版页面，CMS模式网站中用到';

-- ----------------------------
-- Records of fly_template_page
-- ----------------------------
INSERT INTO `fly_template_page` VALUES ('507', '366783118428090393', '0', '0', '高端单页面首页', null, '<!DOCTYPE html>\n<html lang=\"zh-cn\">\n<head>\n	<meta charset=\"UTF-8\" />\n	<title>${site_title} - ${global_site_name}</title>\n    <meta name=\"description\" content=\"【国家高新技术企业】千助,北京网站建设领跑者,10年专注高端网站建设,网页设计联盟金牌企业,自主研发13项软件著作权.有针对性的提供北京网站建设报价和方案.网站建设咨询: 4006-123-011\" />\n    <meta name=\"keywords\" content=\"网站建设,北京网站建设,网站制作,北京网站制作,网站设计,北京网站设计,网页设计,北京网页设计,北京网站建设公司,高端网站建设,做网站,建网站,北京高端网站建设公司\" />\n    <meta name=\"viewport\" content=\"width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no\" />\n    <meta name=\"copyright\" content=\"Copyright 1000zhu.com 版权所有\" />\n    <link href=\"${global_templets_skin}/skin/h5/qzjz/css/bootstrap.min.css\" rel=\"stylesheet\" />\n    <link href=\"${global_templets_skin}/skin/h5/qzjz/css/index.min.css\" rel=\"stylesheet\" />\n    <link href=\"${global_templets_skin}/skin/h5/qzjz/css/index.css\" rel=\"stylesheet\" />\n    <!--[if lt IE 9]>\n    <script src=\"${global_templets_skin}/skin/h5/qzjz/js/html5shiv.min.js\"></script>\n    <![endif]-->\n    <script src=\"${global_templets_skin}/skin/h5/qzjz/js/jquery.min.js\"></script>\n    <script src=\"${global_templets_skin}/skin/h5/qzjz/js/index.min.js\"></script>\n</head>\n\n<body>\n    <header>\n        <div class=\"logo\">\n          <a href=\"http://www.1000zhu.com/\"><img src=\"${global_templets_skin}/skin/h5/qzjz/picture/logo.png\" alt=\"北京网站建设_网站制作_千助\" class=\"img-responsive ori\" /></a>\n        </div>\n        <nav class=\"menu\">\n          <ul class=\"list-inline\">\n            <li class=\"active\"><a>首页</a></li>\n            <li><a>业务</a></li>\n            <li><a>案例</a></li>\n            <li><a>客户</a></li>\n            <li><a>品质</a></li>\n            <li><a>增值</a></li>\n            <li><a>关于</a></li>\n            <li><a>联系</a></li>\n          </ul>\n        </nav>\n        <div class=\"hotline\">\n          <a href=\"tel:4006123011\" title=\"网站建设免费咨询热线\"><span>4006-123-011</span></a><u></u>\n        </div>\n        <div class=\"menu-icon\">\n  			<a href=\"tel:4006123011\" title=\"点击直拨高端网站设计热线\"><span class=\"glyphicon glyphicon-earphone\"></span></a>\n            <span class=\"glyphicon glyphicon-th-large\"></span>\n        </div>\n    </header>\n    \n    <div class=\"welcome\"><p><u></u></p></div>\n    \n    <section class=\"video\">\n    	<div class=\"swiper-container\">\n            <div class=\"swiper-wrapper\">\n              <div class=\"swiper-slide nth1\">\n                <div class=\"box\">\n                    <div class=\"r\">\n                        <h1>高端定制网站建设</h1>\n                    </div>\n                </div>\n              </div>\n              <div class=\"swiper-slide nth2\">\n              	<div class=\"box\">\n                    <div class=\"r\">\n                        <span>中标京城地铁网站建设项目</span>\n                    </div>\n                </div>\n              </div>\n              <div class=\"swiper-slide nth3\">\n                  <div class=\"box\">\n                      <div class=\"r\">\n                          <span>祝贺中国开发性金融促进会官方网站建设全新上线</span>\n                      </div>\n                  </div>\n              </div>\n              <div class=\"swiper-slide nth4\">\n                  <div class=\"box\">\n                      <div class=\"r\">\n                          <span>全面中标铭泰集团官方网站设计及旗下子公司的网站制作站群项目</span>\n                      </div>\n                  </div>\n              </div>\n              <div class=\"swiper-slide nth5\">\n                  <div class=\"box\">\n                      <div class=\"r\">\n                          <span>联合开发坏猴子影业官方网站建设项目</span>\n                      </div>\n                  </div>\n              </div>\n              <div class=\"swiper-slide nth6\">\n                  <div class=\"box\">\n                      <div class=\"r\">\n                          <span>成功上线中国扶贫开发协会官方网站</span>\n                      </div>\n                  </div>\n              </div>\n            </div>\n        </div>\n        <div class=\"innerBox\">\n        	<div class=\"news\">\n            	<span>NEWS :</span>\n                <a href=\"/article/\" title=\"更多网站建设相关文章\" class=\"more\" target=\"_blank\">more</a>\n                <ul>\n                  <li><a href=\"/article/152.html\" target=\"_blank\" title=\"给你看八个网页特效，让你的网站建设更加引人入胜！\">给你看八个网页特效，让你的网站建设更 ...</a></li>\n                  <li><a href=\"/article/151.html\" target=\"_blank\" title=\"如何使用以图像为中心的设计来提高网站转化率\">如何使用以图像为中心的设计来提高网站 ...</a></li>\n                  <li><a href=\"/article/150.html\" target=\"_blank\" title=\"歪果仁的那些404错误页面设计真的有创意吗？\">歪果仁的那些404错误页面设计真的有创意 ...</a></li>\n                  <li><a href=\"/article/149.html\" target=\"_blank\" title=\"为什么你的客户需要的是响应式网站，而不是APP应用程序\">为什么你的客户需要的是响应式网站，而 ...</a></li>\n                  <li><a href=\"/article/148.html\" target=\"_blank\" title=\"百度启动原创保护计划，如何申请开通百度原创保护？\">百度启动原创保护计划，如何申请开通百 ...</a></li>\n\n                </ul>\n            </div>\n            <div class=\"guide\"></div>\n            <a class=\"movedown\"></a>\n        </div>\n    </section>\n    \n    <section class=\"business\">\n      <div class=\"box\">\n        <div class=\"caption\">\n        	<i></i><span>我们能做什么</span>\n            <br class=\"clear\" />\n        </div>\n        <ul class=\"items list-inline\">\n        	<li class=\"pc\">\n            	<i></i><strong>高端定制网站</strong>\n                <p>企业高端定制网站设计<br />彰显品牌形象</p>\n            </li>\n            <li class=\"mobi\">\n            	<i></i><strong>移动网站建设</strong>\n                <p>定制手机网站 / 微网站制作<br />布局移动互联网</p>\n            </li>\n            <li class=\"sys\">\n            	<i></i><strong>业务系统研发</strong>\n                <p>基于 B/S 架构的系统研发<br />让业务办公轻松自如</p>\n            </li>\n            <li class=\"app\">\n            	<i></i><strong>APP应用程序</strong>\n                <p>基于 iOS / Android 应用开发<br />掌控智能终端时代</p>\n            </li>\n            <li class=\"host\">\n            	<i></i><strong>服务器运维</strong>\n                <p>我们不只提供云硬件和网络<br />更加注重技术运维</p>\n            </li>\n        </ul>\n      </div>\n    </section>\n    \n    <section class=\"cases\">\n      <div class=\"box\">\n    	<div class=\"caption\">\n        	<i></i><span>网站设计案例欣赏</span>\n            <br class=\"clear\" />\n        </div>\n        <div class=\"swiper-container items\">\n           <div class=\"swiper-wrapper\">\n              <div class=\"swiper-slide\">\n                  <a href=\"/cases/72.html\" target=\"_blank\">\n                  <img src=\"${global_templets_skin}/skin/h5/qzjz/picture/0110028059.jpg\" alt=\"ATA官网建设\" />\n                  <p>网站设计<br /><strong>ATA官网建设</strong><br />官方网站 , 上市公司 , 红色</p></a>\n                </div>\n<div class=\"swiper-slide\">\n                  <a href=\"/cases/59.html\" target=\"_blank\">\n                  <img src=\"${global_templets_skin}/skin/h5/qzjz/picture/1153507477.jpg\" alt=\"中远国际航空网站设计\" />\n                  <p>网站设计<br /><strong>中远国际航空网站设计</strong><br />国有企业 , 蓝色 , 欧美风格 , 上市公司</p></a>\n                </div>\n<div class=\"swiper-slide\">\n                  <a href=\"/cases/62.html\" target=\"_blank\">\n                  <img src=\"${global_templets_skin}/skin/h5/qzjz/picture/0244085480.jpg\" alt=\"海尔嫩烤箱官方网站建设\" />\n                  <p>网站设计<br /><strong>海尔嫩烤箱官方网站建设</strong><br />上市公司 , 官方网站 , 欧美风格</p></a>\n                </div>\n\n           </div>\n        </div>\n        <a href=\"/cases/\" title=\"欣赏更多北京网站建设的设计案例\" class=\"more\" target=\"_blank\">更多设计案例</a>\n      </div>\n    </section>\n    \n    <section class=\"clients\">\n   	  <div class=\"box\">\n    	<div class=\"caption\">\n        	<i></i><span>他们与千助长期合作</span>\n            <br class=\"clear\" />\n        </div>\n        <ul class=\"items list-inline\">\n        	<li class=\"cctv\"><span>CCTV影响力视频网站建设</span></li><li class=\"qinghua\"><span>清华大学国际预科学院网站建设</span></li><li class=\"lenovo\"><span>联想控股成员企业网站建设</span></li><li class=\"cas\"><span>中科院研究所网站设计</span></li><li class=\"apple\"><span>中航苹果官方网站设计</span></li><li class=\"das\"><span>一汽大众汽车门户网站建设</span></li><li class=\"ata\"><span>ATA全美在线官方网站建设</span></li><li class=\"zhongxin\"><span>中信集团控股公司网站建设</span></li><li class=\"haier\"><span>海尔嫩烤箱网站建设</span></li><li class=\"cosco\"><span>中远国际航空货运网站设计</span></li><li class=\"zhongying\"><span>中影集团后期制作网站建设</span></li><li class=\"toread\"><span>探路者冰雪控股网站制作</span></li><li class=\"neusoft\"><span>东软慧聚官方网站制作</span></li><li class=\"zjgl\"><span>中交公路局海外分公司网站建设</span></li><li class=\"report\"><span>中国报道信息门户网站建设</span></li>\n        </ul>\n      </div>\n    </section>\n    \n    <section class=\"quality\">\n      <div class=\"box\">\n    	<div class=\"caption\">\n        	<i></i><span>不同媒介，同样精彩</span>\n            <br class=\"clear\" />\n        </div>\n        <div class=\"swiper-container items\">\n            <div class=\"swiper-wrapper\">\n              <div class=\"swiper-slide nth1\">\n                <ul class=\"list-inline\">\n                  <li class=\"mobi\"><span>响应式手机网站建设</span></li><li class=\"pad\"><span>响应式平板网站建设</span></li><li class=\"pc\"><span>响应式PC网站建设</span></li>\n                </ul>\n                <p>触及视觉灵魂的设计趋势<br />精心布局的用户体验<br />毫无顾忌地通过任何终端<br />呈现在客户的眼前</p>\n              </div>\n              <div class=\"swiper-slide nth2\">\n              	<ul class=\"list-inline\">\n                  <li class=\"ie\"><span>兼容微软IE浏览器的网页设计</span></li><li class=\"chrome\"><span>兼容谷歌Chrome浏览器的网站设计</span></li><li class=\"firefox\"><span>兼容火狐Firefox浏览器的网页设计</span></li><li class=\"safari\"><span>兼容苹果Safari浏览器的网站设计</span></li>\n                </ul>\n                <p>Html5 + CSS3 响应式布局<br />卓越的浏览器兼容性<br />因为高端，所以出众</p>\n              </div>\n              <div class=\"swiper-slide nth3\">\n              	<ul class=\"list-inline\">\n                  <li class=\"windows\"><span>跨windows平台网站制作</span></li><li class=\"ios\"><span>跨ios平台网站制作</span></li><li class=\"andriod\"><span>跨andriod平台网站制作</span></li>\n                </ul>\n                <p>基于 B/S 架构的网站建设<br />无障碍的跨平台应用<br />无须用户下载安装即可使用<br />云端管理，轻松维护</p>\n              </div>\n            </div>\n        </div>\n        <!--\n          <a href=\"/tool/responsive/?url=http://www.1000zhu.com\" title=\"北京网站设计响应式网站测试工具\" class=\"lookall\" target=\"_blank\">响应式网站的照妖镜</a>\n        -->\n      </div>\n    </section>\n    \n    <section class=\"marketing\">\n      <div class=\"box\">\n        <div class=\"caption\">\n        	<i></i><span>整合营销，抢占商机</span>\n            <br class=\"clear\" />\n        </div>\n        <ul class=\"items list-inline\">\n        	<li class=\"se\">\n            	<i></i><strong>搜索引擎</strong>\n                <p>SEO 优化<br />搜索引擎竞价</p>\n            </li>\n            <li class=\"weixin\">\n            	<i></i><strong>微信营销</strong>\n                <p>公众账号 / 微网站<br />微盟 ( 微社区 )</p>\n            </li>\n            <li class=\"weibo\">\n            	<i></i><strong>微博营销</strong>\n                <p>企业蓝V认证<br />官方微博接入网站</p>\n            </li>\n            <li class=\"sms\">\n            	<i></i><strong>消息推送</strong>\n                <p>短信平台接口<br />Email 推送</p>\n            </li>\n            <li class=\"pay\">\n            	<i></i><strong>在线支付</strong>\n                <p>支付宝、银联<br />Paypal 接口</p>\n            </li>\n            <li class=\"bbs\">\n            	<i></i><strong>论坛聚人</strong>\n                <p>独立开发<br />会员打通</p>\n            </li>\n        </ul>\n      </div>\n    </section>\n    \n    <section class=\"aboutus\">\n    	<ul class=\"menu\"><li>思想</li><li>关于</li><li>荣誉</li></ul>\n        <div class=\"swiper-container items\">\n            <div class=\"swiper-wrapper\">\n              <div class=\"swiper-slide nth1\">\n                <strong>厚积薄发</strong>\n                <p>登上峰顶，不是为了饱览风光，是为了寻找更高的山峰<br />日出东方，告别了昨天的荣耀，将光芒照向更远的地方<br />一路上，我们更在意如何积累和沉淀</p>\n                <u>下一秒，让你看，我们到底有多强</u>\n              </div>\n              <div class=\"swiper-slide nth2\">\n              	<strong>千助网建科技（北京）有限公司</strong>\n                <p>成立于2008年，坐落于北京中关村科技园区，是中国优秀的互联网服务提供商。自成立以来，专注于高端网站建设、移动互联应用、B/S架构系统研发、云服务器部署和运维，为企业客户的互联网应用提供一站式服务。</p>\n                <p>我们始终坚持以客户需求为导向，为追求用户体验设计，提供有针对性的项目解决方案，千助人将不断地超越自我，挑战险峰！</p>\n              </div>\n              <div class=\"swiper-slide nth3\">\n              	<strong>放下荣誉，放眼未来</strong>\n                <ul>\n                    <li>2016年<u>-</u>荣获国家高新技术企业</li>\n                    <li>2013年<u>-</u>已拥有自主知识产权，国家软件著作权达12项之多</li>\n                    <li>2011年<u>-</u>注册 \" 千助 \" 商标，打造北京网站建设市场知名品牌</li>\n                    <li>2010年<u>-</u>荣获中关村高新技术企业、海淀区创新企业</li>\n                    <li>2010年<u>-</u>设计联盟评选为金牌设计企业</li>\n                </ul>\n              </div>\n            </div>\n        </div>\n        <table class=\"exp\">\n        	<tr>\n              <td><u>1257</u>位企业客户信赖之选</td>\n              <td><u>367</u>上市/集团企业设计作品</td>\n              <td><u>2016</u>年获国家高新技术企业</td>\n              <td><u>15</u>项国家软件著作权</td>\n              <td><u>97%</u>以上的客户续费率</td>\n            </tr>\n        </table>\n    </section>\n    \n    <section class=\"contact\">\n    	<div class=\"box\">\n        	<div class=\"above\">\n            	<div class=\"wechat\"><img src=\"${global_templets_skin}/skin/h5/qzjz/picture/wechat_code.jpg\" alt=\"扫描关注千助微信公众账号\" /></div>\n                <div class=\"left\">\n                	<a href=\"tel:4006123011\" title=\"网站制作咨询热线\" class=\"tel\"></a>\n                    <p>工作日：4006123011 / 010-80757532<br />非工作日：4006123011 / 18511639815<br />科技园区：北京 · 中关村 · 昌发展<br />办公地址：北京市昌发展万科广场A座6层</p>\n                </div>\n                <div class=\"right\">\n                    官网：www.1000zhu.com<br />Email：Service@1000zhu.com<br />千助网建科技（北京）有限公司<br />京ICP备 08102474 号<br />京公网安备 11010802010984 号\n                </div>\n            </div>\n        </div>\n    </section>\n\n    <section class=\"cooperation\">\n        <div class=\"box\">\n            <span class=\"title\">友情链接，携手共进</span>\n            <ul class=\"list-inline\">\n                <li><a href=\"http://www.1000zhu.com\" target=\"_blank\">北京网站建设</a></li>\n<li><a href=\"http://www.1000zhu.com\" target=\"_blank\">高端网站建设</a></li>\n<li><a href=\"http://www.1000zhu.com\" target=\"_blank\">北京网站设计</a></li>\n<li><a href=\"http://www.wanhu.cn\" target=\"_blank\">上海网站建设</a></li>\n<li><a href=\"http://www.1000zhu.com\" target=\"_blank\">北京网站建设公司</a></li>\n<li><a href=\"http://www.1000zhu.com\" target=\"_blank\">北京网站制作</a></li>\n<li><a href=\"http://www.01jianzhan.com\" target=\"_blank\">广州网站建设</a></li>\n<li><a href=\"http://www.ynyes.com\" target=\"_blank\">云南网站建设</a></li>\n<li><a href=\"https://www.zwcnw.com/\" target=\"_blank\">广州建站</a></li>\n<li><a href=\"http://www.1000zhu.com/article/143.html\" target=\"_blank\">上地网站建设</a></li>\n<li><a href=\"http://www.sscmwl.com\" target=\"_blank\">网站建设</a></li>\n<li><a href=\"http://www.xdnet.cn\" target=\"_blank\">西安网站建设</a></li>\n<li><a href=\"http://www.w-e.cc\" target=\"_blank\">烟台网站建设</a></li>\n<li><a href=\"http://www.qiangseo.com\" target=\"_blank\">长沙网站推广</a></li>\n<li><a href=\"http://www.lkcms.com\" target=\"_blank\">成都网站建设</a></li>\n<li><a href=\"http://www.ymars.com\" target=\"_blank\">长沙网站建设</a></li>\n<li><a href=\"http://www.zijiren.net\" target=\"_blank\">深圳网站建设</a></li>\n<li><a href=\"http://www.ezw.net.cn\" target=\"_blank\">石家庄网站建设</a></li>\n<li><a href=\"http://www.bjhyn.net\" target=\"_blank\">北京网站建设</a></li>\n\n            </ul>\n        </div>\n        <div class=\"bg\"></div>\n    </section>\n    \n    <div class=\"dock\">\n        <ul class=\"icons\">\n        	<li class=\"up\"><i></i></li>\n            <li class=\"im\">\n            	<i></i><p>网站建设咨询<br />在线沟通，请点我<a href=\"http://p.qiao.baidu.com/cps/chat?siteId=7773323&userId=989741\" target=\"_blank\">在线咨询</a></p>\n            </li>\n            <li class=\"tel\">\n            	<i></i><p>我要做网站，<br />小助啊，抓紧给我来个电话！<span class=\"callback\"><input type=\"text\" maxlength=\"12\" /><button>回拨</button></span></p>\n            </li>\n            <li class=\"wechat\">\n            	<i></i><p><img src=\"${global_templets_skin}/skin/h5/qzjz/picture/wechat_code.jpg\" alt=\"扫描关注网站制作微信公众账号\" /></p>\n            </li>\n            <li class=\"down\"><i></i></li>\n        </ul>\n        <a class=\"switch\"></a>\n    </div>\n</body>\n</html>', '首页', null, '2019-09-29 00:25:16', '1', '0');
INSERT INTO `fly_template_page` VALUES ('508', '366783118428090393', '2', '3', 'lmyboke1', null, null, '作品列表页面', null, null, '0', '0');
INSERT INTO `fly_template_page` VALUES ('509', '366783118428090393', '3', '2', 'lmyboke1', null, null, '文章详情、作品详情', null, null, '0', '0');
INSERT INTO `fly_template_page` VALUES ('510', '366783118428090393', '2', '4', 'lmyboke1', null, null, '博客的文章列表页面', null, null, '0', '0');
INSERT INTO `fly_template_page` VALUES ('511', '366783118428090393', '1', '2', 'lmyqymb15', null, null, '这是首页', null, null, '0', '0');
INSERT INTO `fly_template_page` VALUES ('369986913999781888', '366783118428090393', '368208528827232271', '1', 'ertertertert', null, '\n<!DOCTYPE html>\n<html lang=\"zh-cn\">\n<head>\n	<meta charset=\"UTF-8\" />\n	<title>北京网站建设|北京网站制作|北京网站设计|高端网站建设公司 - 千助</title>\n    <meta name=\"description\" content=\"【国家高新技术企业】千助,北京网站建设领跑者,10年专注高端网站建设,网页设计联盟金牌企业,自主研发13项软件著作权.有针对性的提供北京网站建设报价和方案.网站建设咨询: 4006-123-011\" />\n    <meta name=\"keywords\" content=\"网站建设,北京网站建设,网站制作,北京网站制作,网站设计,北京网站设计,网页设计,北京网页设计,北京网站建设公司,高端网站建设,做网站,建网站,北京高端网站建设公司\" />\n    <meta name=\"viewport\" content=\"width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no\" />\n    <meta name=\"copyright\" content=\"Copyright 1000zhu.com 版权所有\" />\n    <link href=\"${global_templets_skin}/skin/h5/qzjz/css/bootstrap.min.css\" rel=\"stylesheet\" />\n    <link href=\"${global_templets_skin}/skin/h5/qzjz/css/index.min.css\" rel=\"stylesheet\" />\n    <link href=\"${global_templets_skin}/skin/h5/qzjz/css/index.css\" rel=\"stylesheet\" />\n    <!--[if lt IE 9]>\n    <script src=\"${global_templets_skin}/skin/h5/qzjz/js/html5shiv.min.js\"></script>\n    <![endif]-->\n    <script src=\"${global_templets_skin}/skin/h5/qzjz/js/jquery.min.js\"></script>\n    <script src=\"${global_templets_skin}/skin/h5/qzjz/js/index.min.js\"></script>\n</head>\n\n<body>\n    <header>\n        <div class=\"logo\">\n          <a href=\"http://www.1000zhu.com/\"><img src=\"${global_templets_skin}/skin/h5/qzjz/picture/logo.png\" alt=\"北京网站建设_网站制作_千助\" class=\"img-responsive ori\" /></a>\n        </div>\n        <nav class=\"menu\">\n          <ul class=\"list-inline\">\n            <li class=\"active\"><a>首页</a></li>\n            <li><a>业务</a></li>\n            <li><a>案例</a></li>\n            <li><a>客户</a></li>\n            <li><a>品质</a></li>\n            <li><a>增值</a></li>\n            <li><a>关于</a></li>\n            <li><a>联系</a></li>\n          </ul>\n        </nav>\n        <div class=\"hotline\">\n          <a href=\"tel:4006123011\" title=\"网站建设免费咨询热线\"><span>4006-123-011</span></a><u></u>\n        </div>\n        <div class=\"menu-icon\">\n  			<a href=\"tel:4006123011\" title=\"点击直拨高端网站设计热线\"><span class=\"glyphicon glyphicon-earphone\"></span></a>\n            <span class=\"glyphicon glyphicon-th-large\"></span>\n        </div>\n    </header>\n    \n    <div class=\"welcome\"><p><u></u></p></div>\n    \n    <section class=\"video\">\n    	<div class=\"swiper-container\">\n            <div class=\"swiper-wrapper\">\n              <div class=\"swiper-slide nth1\">\n                <div class=\"box\">\n                    <div class=\"r\">\n                        <h1>高端定制网站建设</h1>\n                    </div>\n                </div>\n              </div>\n              <div class=\"swiper-slide nth2\">\n              	<div class=\"box\">\n                    <div class=\"r\">\n                        <span>中标京城地铁网站建设项目</span>\n                    </div>\n                </div>\n              </div>\n              <div class=\"swiper-slide nth3\">\n                  <div class=\"box\">\n                      <div class=\"r\">\n                          <span>祝贺中国开发性金融促进会官方网站建设全新上线</span>\n                      </div>\n                  </div>\n              </div>\n              <div class=\"swiper-slide nth4\">\n                  <div class=\"box\">\n                      <div class=\"r\">\n                          <span>全面中标铭泰集团官方网站设计及旗下子公司的网站制作站群项目</span>\n                      </div>\n                  </div>\n              </div>\n              <div class=\"swiper-slide nth5\">\n                  <div class=\"box\">\n                      <div class=\"r\">\n                          <span>联合开发坏猴子影业官方网站建设项目</span>\n                      </div>\n                  </div>\n              </div>\n              <div class=\"swiper-slide nth6\">\n                  <div class=\"box\">\n                      <div class=\"r\">\n                          <span>成功上线中国扶贫开发协会官方网站</span>\n                      </div>\n                  </div>\n              </div>\n            </div>\n        </div>\n        <div class=\"innerBox\">\n        	<div class=\"news\">\n            	<span>NEWS :</span>\n                <a href=\"/article/\" title=\"更多网站建设相关文章\" class=\"more\" target=\"_blank\">more</a>\n                <ul>\n                  <li><a href=\"/article/152.html\" target=\"_blank\" title=\"给你看八个网页特效，让你的网站建设更加引人入胜！\">给你看八个网页特效，让你的网站建设更 ...</a></li>\n<li><a href=\"/article/151.html\" target=\"_blank\" title=\"如何使用以图像为中心的设计来提高网站转化率\">如何使用以图像为中心的设计来提高网站 ...</a></li>\n<li><a href=\"/article/150.html\" target=\"_blank\" title=\"歪果仁的那些404错误页面设计真的有创意吗？\">歪果仁的那些404错误页面设计真的有创意 ...</a></li>\n<li><a href=\"/article/149.html\" target=\"_blank\" title=\"为什么你的客户需要的是响应式网站，而不是APP应用程序\">为什么你的客户需要的是响应式网站，而 ...</a></li>\n<li><a href=\"/article/148.html\" target=\"_blank\" title=\"百度启动原创保护计划，如何申请开通百度原创保护？\">百度启动原创保护计划，如何申请开通百 ...</a></li>\n\n                </ul>\n            </div>\n            <div class=\"guide\"></div>\n            <a class=\"movedown\"></a>\n        </div>\n    </section>\n    \n    <section class=\"business\">\n      <div class=\"box\">\n        <div class=\"caption\">\n        	<i></i><span>我们能做什么</span>\n            <br class=\"clear\" />\n        </div>\n        <ul class=\"items list-inline\">\n        	<li class=\"pc\">\n            	<i></i><strong>高端定制网站</strong>\n                <p>企业高端定制网站设计<br />彰显品牌形象</p>\n            </li>\n            <li class=\"mobi\">\n            	<i></i><strong>移动网站建设</strong>\n                <p>定制手机网站 / 微网站制作<br />布局移动互联网</p>\n            </li>\n            <li class=\"sys\">\n            	<i></i><strong>业务系统研发</strong>\n                <p>基于 B/S 架构的系统研发<br />让业务办公轻松自如</p>\n            </li>\n            <li class=\"app\">\n            	<i></i><strong>APP应用程序</strong>\n                <p>基于 iOS / Android 应用开发<br />掌控智能终端时代</p>\n            </li>\n            <li class=\"host\">\n            	<i></i><strong>服务器运维</strong>\n                <p>我们不只提供云硬件和网络<br />更加注重技术运维</p>\n            </li>\n        </ul>\n      </div>\n    </section>\n    \n    <section class=\"cases\">\n      <div class=\"box\">\n    	<div class=\"caption\">\n        	<i></i><span>网站设计案例欣赏</span>\n            <br class=\"clear\" />\n        </div>\n        <div class=\"swiper-container items\">\n           <div class=\"swiper-wrapper\">\n              <div class=\"swiper-slide\">\n                  <a href=\"/cases/72.html\" target=\"_blank\">\n                  <img src=\"${global_templets_skin}/skin/h5/qzjz/picture/0110028059.jpg\" alt=\"ATA官网建设\" />\n                  <p>网站设计<br /><strong>ATA官网建设</strong><br />官方网站 , 上市公司 , 红色</p></a>\n                </div>\n<div class=\"swiper-slide\">\n                  <a href=\"/cases/59.html\" target=\"_blank\">\n                  <img src=\"${global_templets_skin}/skin/h5/qzjz/picture/1153507477.jpg\" alt=\"中远国际航空网站设计\" />\n                  <p>网站设计<br /><strong>中远国际航空网站设计</strong><br />国有企业 , 蓝色 , 欧美风格 , 上市公司</p></a>\n                </div>\n<div class=\"swiper-slide\">\n                  <a href=\"/cases/62.html\" target=\"_blank\">\n                  <img src=\"${global_templets_skin}/skin/h5/qzjz/picture/0244085480.jpg\" alt=\"海尔嫩烤箱官方网站建设\" />\n                  <p>网站设计<br /><strong>海尔嫩烤箱官方网站建设</strong><br />上市公司 , 官方网站 , 欧美风格</p></a>\n                </div>\n\n           </div>\n        </div>\n        <a href=\"/cases/\" title=\"欣赏更多北京网站建设的设计案例\" class=\"more\" target=\"_blank\">更多设计案例</a>\n      </div>\n    </section>\n    \n    <section class=\"clients\">\n   	  <div class=\"box\">\n    	<div class=\"caption\">\n        	<i></i><span>他们与千助长期合作</span>\n            <br class=\"clear\" />\n        </div>\n        <ul class=\"items list-inline\">\n        	<li class=\"cctv\"><span>CCTV影响力视频网站建设</span></li><li class=\"qinghua\"><span>清华大学国际预科学院网站建设</span></li><li class=\"lenovo\"><span>联想控股成员企业网站建设</span></li><li class=\"cas\"><span>中科院研究所网站设计</span></li><li class=\"apple\"><span>中航苹果官方网站设计</span></li><li class=\"das\"><span>一汽大众汽车门户网站建设</span></li><li class=\"ata\"><span>ATA全美在线官方网站建设</span></li><li class=\"zhongxin\"><span>中信集团控股公司网站建设</span></li><li class=\"haier\"><span>海尔嫩烤箱网站建设</span></li><li class=\"cosco\"><span>中远国际航空货运网站设计</span></li><li class=\"zhongying\"><span>中影集团后期制作网站建设</span></li><li class=\"toread\"><span>探路者冰雪控股网站制作</span></li><li class=\"neusoft\"><span>东软慧聚官方网站制作</span></li><li class=\"zjgl\"><span>中交公路局海外分公司网站建设</span></li><li class=\"report\"><span>中国报道信息门户网站建设</span></li>\n        </ul>\n      </div>\n    </section>\n    \n    <section class=\"quality\">\n      <div class=\"box\">\n    	<div class=\"caption\">\n        	<i></i><span>不同媒介，同样精彩</span>\n            <br class=\"clear\" />\n        </div>\n        <div class=\"swiper-container items\">\n            <div class=\"swiper-wrapper\">\n              <div class=\"swiper-slide nth1\">\n                <ul class=\"list-inline\">\n                  <li class=\"mobi\"><span>响应式手机网站建设</span></li><li class=\"pad\"><span>响应式平板网站建设</span></li><li class=\"pc\"><span>响应式PC网站建设</span></li>\n                </ul>\n                <p>触及视觉灵魂的设计趋势<br />精心布局的用户体验<br />毫无顾忌地通过任何终端<br />呈现在客户的眼前</p>\n              </div>\n              <div class=\"swiper-slide nth2\">\n              	<ul class=\"list-inline\">\n                  <li class=\"ie\"><span>兼容微软IE浏览器的网页设计</span></li><li class=\"chrome\"><span>兼容谷歌Chrome浏览器的网站设计</span></li><li class=\"firefox\"><span>兼容火狐Firefox浏览器的网页设计</span></li><li class=\"safari\"><span>兼容苹果Safari浏览器的网站设计</span></li>\n                </ul>\n                <p>Html5 + CSS3 响应式布局<br />卓越的浏览器兼容性<br />因为高端，所以出众</p>\n              </div>\n              <div class=\"swiper-slide nth3\">\n              	<ul class=\"list-inline\">\n                  <li class=\"windows\"><span>跨windows平台网站制作</span></li><li class=\"ios\"><span>跨ios平台网站制作</span></li><li class=\"andriod\"><span>跨andriod平台网站制作</span></li>\n                </ul>\n                <p>基于 B/S 架构的网站建设<br />无障碍的跨平台应用<br />无须用户下载安装即可使用<br />云端管理，轻松维护</p>\n              </div>\n            </div>\n        </div>\n        <!--\n          <a href=\"/tool/responsive/?url=http://www.1000zhu.com\" title=\"北京网站设计响应式网站测试工具\" class=\"lookall\" target=\"_blank\">响应式网站的照妖镜</a>\n        -->\n      </div>\n    </section>\n    \n    <section class=\"marketing\">\n      <div class=\"box\">\n        <div class=\"caption\">\n        	<i></i><span>整合营销，抢占商机</span>\n            <br class=\"clear\" />\n        </div>\n        <ul class=\"items list-inline\">\n        	<li class=\"se\">\n            	<i></i><strong>搜索引擎</strong>\n                <p>SEO 优化<br />搜索引擎竞价</p>\n            </li>\n            <li class=\"weixin\">\n            	<i></i><strong>微信营销</strong>\n                <p>公众账号 / 微网站<br />微盟 ( 微社区 )</p>\n            </li>\n            <li class=\"weibo\">\n            	<i></i><strong>微博营销</strong>\n                <p>企业蓝V认证<br />官方微博接入网站</p>\n            </li>\n            <li class=\"sms\">\n            	<i></i><strong>消息推送</strong>\n                <p>短信平台接口<br />Email 推送</p>\n            </li>\n            <li class=\"pay\">\n            	<i></i><strong>在线支付</strong>\n                <p>支付宝、银联<br />Paypal 接口</p>\n            </li>\n            <li class=\"bbs\">\n            	<i></i><strong>论坛聚人</strong>\n                <p>独立开发<br />会员打通</p>\n            </li>\n        </ul>\n      </div>\n    </section>\n    \n    <section class=\"aboutus\">\n    	<ul class=\"menu\"><li>思想</li><li>关于</li><li>荣誉</li></ul>\n        <div class=\"swiper-container items\">\n            <div class=\"swiper-wrapper\">\n              <div class=\"swiper-slide nth1\">\n                <strong>厚积薄发</strong>\n                <p>登上峰顶，不是为了饱览风光，是为了寻找更高的山峰<br />日出东方，告别了昨天的荣耀，将光芒照向更远的地方<br />一路上，我们更在意如何积累和沉淀</p>\n                <u>下一秒，让你看，我们到底有多强</u>\n              </div>\n              <div class=\"swiper-slide nth2\">\n              	<strong>千助网建科技（北京）有限公司</strong>\n                <p>成立于2008年，坐落于北京中关村科技园区，是中国优秀的互联网服务提供商。自成立以来，专注于高端网站建设、移动互联应用、B/S架构系统研发、云服务器部署和运维，为企业客户的互联网应用提供一站式服务。</p>\n                <p>我们始终坚持以客户需求为导向，为追求用户体验设计，提供有针对性的项目解决方案，千助人将不断地超越自我，挑战险峰！</p>\n              </div>\n              <div class=\"swiper-slide nth3\">\n              	<strong>放下荣誉，放眼未来</strong>\n                <ul>\n                    <li>2016年<u>-</u>荣获国家高新技术企业</li>\n                    <li>2013年<u>-</u>已拥有自主知识产权，国家软件著作权达12项之多</li>\n                    <li>2011年<u>-</u>注册 \" 千助 \" 商标，打造北京网站建设市场知名品牌</li>\n                    <li>2010年<u>-</u>荣获中关村高新技术企业、海淀区创新企业</li>\n                    <li>2010年<u>-</u>设计联盟评选为金牌设计企业</li>\n                </ul>\n              </div>\n            </div>\n        </div>\n        <table class=\"exp\">\n        	<tr>\n              <td><u>1257</u>位企业客户信赖之选</td>\n              <td><u>367</u>上市/集团企业设计作品</td>\n              <td><u>2016</u>年获国家高新技术企业</td>\n              <td><u>15</u>项国家软件著作权</td>\n              <td><u>97%</u>以上的客户续费率</td>\n            </tr>\n        </table>\n    </section>\n    \n    <section class=\"contact\">\n    	<div class=\"box\">\n        	<div class=\"above\">\n            	<div class=\"wechat\"><img src=\"${global_templets_skin}/skin/h5/qzjz/picture/wechat_code.jpg\" alt=\"扫描关注千助微信公众账号\" /></div>\n                <div class=\"left\">\n                	<a href=\"tel:4006123011\" title=\"网站制作咨询热线\" class=\"tel\"></a>\n                    <p>工作日：4006123011 / 010-80757532<br />非工作日：4006123011 / 18511639815<br />科技园区：北京 · 中关村 · 昌发展<br />办公地址：北京市昌发展万科广场A座6层</p>\n                </div>\n                <div class=\"right\">\n                    官网：www.1000zhu.com<br />Email：Service@1000zhu.com<br />千助网建科技（北京）有限公司<br />京ICP备 08102474 号<br />京公网安备 11010802010984 号\n                </div>\n            </div>\n        </div>\n    </section>\n\n    <section class=\"cooperation\">\n        <div class=\"box\">\n            <span class=\"title\">友情链接，携手共进</span>\n            <ul class=\"list-inline\">\n                <li><a href=\"http://www.1000zhu.com\" target=\"_blank\">北京网站建设</a></li>\n<li><a href=\"http://www.1000zhu.com\" target=\"_blank\">高端网站建设</a></li>\n<li><a href=\"http://www.1000zhu.com\" target=\"_blank\">北京网站设计</a></li>\n<li><a href=\"http://www.wanhu.cn\" target=\"_blank\">上海网站建设</a></li>\n<li><a href=\"http://www.1000zhu.com\" target=\"_blank\">北京网站建设公司</a></li>\n<li><a href=\"http://www.1000zhu.com\" target=\"_blank\">北京网站制作</a></li>\n<li><a href=\"http://www.01jianzhan.com\" target=\"_blank\">广州网站建设</a></li>\n<li><a href=\"http://www.ynyes.com\" target=\"_blank\">云南网站建设</a></li>\n<li><a href=\"https://www.zwcnw.com/\" target=\"_blank\">广州建站</a></li>\n<li><a href=\"http://www.1000zhu.com/article/143.html\" target=\"_blank\">上地网站建设</a></li>\n<li><a href=\"http://www.sscmwl.com\" target=\"_blank\">网站建设</a></li>\n<li><a href=\"http://www.xdnet.cn\" target=\"_blank\">西安网站建设</a></li>\n<li><a href=\"http://www.w-e.cc\" target=\"_blank\">烟台网站建设</a></li>\n<li><a href=\"http://www.qiangseo.com\" target=\"_blank\">长沙网站推广</a></li>\n<li><a href=\"http://www.lkcms.com\" target=\"_blank\">成都网站建设</a></li>\n<li><a href=\"http://www.ymars.com\" target=\"_blank\">长沙网站建设</a></li>\n<li><a href=\"http://www.zijiren.net\" target=\"_blank\">深圳网站建设</a></li>\n<li><a href=\"http://www.ezw.net.cn\" target=\"_blank\">石家庄网站建设</a></li>\n<li><a href=\"http://www.bjhyn.net\" target=\"_blank\">北京网站建设</a></li>\n\n            </ul>\n        </div>\n        <div class=\"bg\"></div>\n    </section>\n    \n    <div class=\"dock\">\n        <ul class=\"icons\">\n        	<li class=\"up\"><i></i></li>\n            <li class=\"im\">\n            	<i></i><p>网站建设咨询<br />在线沟通，请点我<a href=\"http://p.qiao.baidu.com/cps/chat?siteId=7773323&userId=989741\" target=\"_blank\">在线咨询</a></p>\n            </li>\n            <li class=\"tel\">\n            	<i></i><p>我要做网站，<br />小助啊，抓紧给我来个电话！<span class=\"callback\"><input type=\"text\" maxlength=\"12\" /><button>回拨</button></span></p>\n            </li>\n            <li class=\"wechat\">\n            	<i></i><p><img src=\"${global_templets_skin}/skin/h5/qzjz/picture/wechat_code.jpg\" alt=\"扫描关注网站制作微信公众账号\" /></p>\n            </li>\n            <li class=\"down\"><i></i></li>\n        </ul>\n        <a class=\"switch\"></a>\n    </div>\n</body>\n</html>\n\n\n\n\n', null, '2019-09-13 20:37:00', null, '1', '0');
INSERT INTO `fly_template_page` VALUES ('369988339018760192', '366783118428090393', '368208528827232271', '1', 'sdasdasdasd', null, '<!DOCTYPE html>\n<html lang=\"zh-cn\">\n<head>\n	<meta charset=\"UTF-8\" />\n	<title>北京网站建设|北京网站制作|北京网站设计|高端网站建设公司 - 千助</title>\n    <meta name=\"description\" content=\"【国家高新技术企业】千助,北京网站建设领跑者,10年专注高端网站建设,网页设计联盟金牌企业,自主研发13项软件著作权.有针对性的提供北京网站建设报价和方案.网站建设咨询: 4006-123-011\" />\n    <meta name=\"keywords\" content=\"网站建设,北京网站建设,网站制作,北京网站制作,网站设计,北京网站设计,网页设计,北京网页设计,北京网站建设公司,高端网站建设,做网站,建网站,北京高端网站建设公司\" />\n    <meta name=\"viewport\" content=\"width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no\" />\n    <meta name=\"copyright\" content=\"Copyright 1000zhu.com 版权所有\" />\n    <link href=\"${global_templets_skin}/skin/h5/qzjz/css/bootstrap.min.css\" rel=\"stylesheet\" />\n    <link href=\"${global_templets_skin}/skin/h5/qzjz/css/index.min.css\" rel=\"stylesheet\" />\n    <link href=\"${global_templets_skin}/skin/h5/qzjz/css/index.css\" rel=\"stylesheet\" />\n    <!--[if lt IE 9]>\n    <script src=\"${global_templets_skin}/skin/h5/qzjz/js/html5shiv.min.js\"></script>\n    <![endif]-->\n    <script src=\"${global_templets_skin}/skin/h5/qzjz/js/jquery.min.js\"></script>\n    <script src=\"${global_templets_skin}/skin/h5/qzjz/js/index.min.js\"></script>\n</head>\n\n<body>\n    <header>\n        <div class=\"logo\">\n          <a href=\"http://www.1000zhu.com/\"><img src=\"${global_templets_skin}/skin/h5/qzjz/picture/logo.png\" alt=\"北京网站建设_网站制作_千助\" class=\"img-responsive ori\" /></a>\n        </div>\n        <nav class=\"menu\">\n          <ul class=\"list-inline\">\n            <li class=\"active\"><a>首页</a></li>\n            <li><a>业务</a></li>\n            <li><a>案例</a></li>\n            <li><a>客户</a></li>\n            <li><a>品质</a></li>\n            <li><a>增值</a></li>\n            <li><a>关于</a></li>\n            <li><a>联系</a></li>\n          </ul>\n        </nav>\n        <div class=\"hotline\">\n          <a href=\"tel:4006123011\" title=\"网站建设免费咨询热线\"><span>4006-123-011</span></a><u></u>\n        </div>\n        <div class=\"menu-icon\">\n  			<a href=\"tel:4006123011\" title=\"点击直拨高端网站设计热线\"><span class=\"glyphicon glyphicon-earphone\"></span></a>\n            <span class=\"glyphicon glyphicon-th-large\"></span>\n        </div>\n    </header>\n    \n    <div class=\"welcome\"><p><u></u></p></div>\n    \n    <section class=\"video\">\n    	<div class=\"swiper-container\">\n            <div class=\"swiper-wrapper\">\n              <div class=\"swiper-slide nth1\">\n                <div class=\"box\">\n                    <div class=\"r\">\n                        <h1>高端定制网站建设</h1>\n                    </div>\n                </div>\n              </div>\n              <div class=\"swiper-slide nth2\">\n              	<div class=\"box\">\n                    <div class=\"r\">\n                        <span>中标京城地铁网站建设项目</span>\n                    </div>\n                </div>\n              </div>\n              <div class=\"swiper-slide nth3\">\n                  <div class=\"box\">\n                      <div class=\"r\">\n                          <span>祝贺中国开发性金融促进会官方网站建设全新上线</span>\n                      </div>\n                  </div>\n              </div>\n              <div class=\"swiper-slide nth4\">\n                  <div class=\"box\">\n                      <div class=\"r\">\n                          <span>全面中标铭泰集团官方网站设计及旗下子公司的网站制作站群项目</span>\n                      </div>\n                  </div>\n              </div>\n              <div class=\"swiper-slide nth5\">\n                  <div class=\"box\">\n                      <div class=\"r\">\n                          <span>联合开发坏猴子影业官方网站建设项目</span>\n                      </div>\n                  </div>\n              </div>\n              <div class=\"swiper-slide nth6\">\n                  <div class=\"box\">\n                      <div class=\"r\">\n                          <span>成功上线中国扶贫开发协会官方网站</span>\n                      </div>\n                  </div>\n              </div>\n            </div>\n        </div>\n        <div class=\"innerBox\">\n        	<div class=\"news\">\n            	<span>NEWS :</span>\n                <a href=\"/article/\" title=\"更多网站建设相关文章\" class=\"more\" target=\"_blank\">more</a>\n                <ul>\n                  <li><a href=\"/article/152.html\" target=\"_blank\" title=\"给你看八个网页特效，让你的网站建设更加引人入胜！\">给你看八个网页特效，让你的网站建设更 ...</a></li>\n<li><a href=\"/article/151.html\" target=\"_blank\" title=\"如何使用以图像为中心的设计来提高网站转化率\">如何使用以图像为中心的设计来提高网站 ...</a></li>\n<li><a href=\"/article/150.html\" target=\"_blank\" title=\"歪果仁的那些404错误页面设计真的有创意吗？\">歪果仁的那些404错误页面设计真的有创意 ...</a></li>\n<li><a href=\"/article/149.html\" target=\"_blank\" title=\"为什么你的客户需要的是响应式网站，而不是APP应用程序\">为什么你的客户需要的是响应式网站，而 ...</a></li>\n<li><a href=\"/article/148.html\" target=\"_blank\" title=\"百度启动原创保护计划，如何申请开通百度原创保护？\">百度启动原创保护计划，如何申请开通百 ...</a></li>\n\n                </ul>\n            </div>\n            <div class=\"guide\"></div>\n            <a class=\"movedown\"></a>\n        </div>\n    </section>\n    \n    <section class=\"business\">\n      <div class=\"box\">\n        <div class=\"caption\">\n        	<i></i><span>我们能做什么</span>\n            <br class=\"clear\" />\n        </div>\n        <ul class=\"items list-inline\">\n        	<li class=\"pc\">\n            	<i></i><strong>高端定制网站</strong>\n                <p>企业高端定制网站设计<br />彰显品牌形象</p>\n            </li>\n            <li class=\"mobi\">\n            	<i></i><strong>移动网站建设</strong>\n                <p>定制手机网站 / 微网站制作<br />布局移动互联网</p>\n            </li>\n            <li class=\"sys\">\n            	<i></i><strong>业务系统研发</strong>\n                <p>基于 B/S 架构的系统研发<br />让业务办公轻松自如</p>\n            </li>\n            <li class=\"app\">\n            	<i></i><strong>APP应用程序</strong>\n                <p>基于 iOS / Android 应用开发<br />掌控智能终端时代</p>\n            </li>\n            <li class=\"host\">\n            	<i></i><strong>服务器运维</strong>\n                <p>我们不只提供云硬件和网络<br />更加注重技术运维</p>\n            </li>\n        </ul>\n      </div>\n    </section>\n    \n    <section class=\"cases\">\n      <div class=\"box\">\n    	<div class=\"caption\">\n        	<i></i><span>网站设计案例欣赏</span>\n            <br class=\"clear\" />\n        </div>\n        <div class=\"swiper-container items\">\n           <div class=\"swiper-wrapper\">\n              <div class=\"swiper-slide\">\n                  <a href=\"/cases/72.html\" target=\"_blank\">\n                  <img src=\"${global_templets_skin}/skin/h5/qzjz/picture/0110028059.jpg\" alt=\"ATA官网建设\" />\n                  <p>网站设计<br /><strong>ATA官网建设</strong><br />官方网站 , 上市公司 , 红色</p></a>\n                </div>\n<div class=\"swiper-slide\">\n                  <a href=\"/cases/59.html\" target=\"_blank\">\n                  <img src=\"${global_templets_skin}/skin/h5/qzjz/picture/1153507477.jpg\" alt=\"中远国际航空网站设计\" />\n                  <p>网站设计<br /><strong>中远国际航空网站设计</strong><br />国有企业 , 蓝色 , 欧美风格 , 上市公司</p></a>\n                </div>\n<div class=\"swiper-slide\">\n                  <a href=\"/cases/62.html\" target=\"_blank\">\n                  <img src=\"${global_templets_skin}/skin/h5/qzjz/picture/0244085480.jpg\" alt=\"海尔嫩烤箱官方网站建设\" />\n                  <p>网站设计<br /><strong>海尔嫩烤箱官方网站建设</strong><br />上市公司 , 官方网站 , 欧美风格</p></a>\n                </div>\n\n           </div>\n        </div>\n        <a href=\"/cases/\" title=\"欣赏更多北京网站建设的设计案例\" class=\"more\" target=\"_blank\">更多设计案例</a>\n      </div>\n    </section>\n    \n    <section class=\"clients\">\n   	  <div class=\"box\">\n    	<div class=\"caption\">\n        	<i></i><span>他们与千助长期合作</span>\n            <br class=\"clear\" />\n        </div>\n        <ul class=\"items list-inline\">\n        	<li class=\"cctv\"><span>CCTV影响力视频网站建设</span></li><li class=\"qinghua\"><span>清华大学国际预科学院网站建设</span></li><li class=\"lenovo\"><span>联想控股成员企业网站建设</span></li><li class=\"cas\"><span>中科院研究所网站设计</span></li><li class=\"apple\"><span>中航苹果官方网站设计</span></li><li class=\"das\"><span>一汽大众汽车门户网站建设</span></li><li class=\"ata\"><span>ATA全美在线官方网站建设</span></li><li class=\"zhongxin\"><span>中信集团控股公司网站建设</span></li><li class=\"haier\"><span>海尔嫩烤箱网站建设</span></li><li class=\"cosco\"><span>中远国际航空货运网站设计</span></li><li class=\"zhongying\"><span>中影集团后期制作网站建设</span></li><li class=\"toread\"><span>探路者冰雪控股网站制作</span></li><li class=\"neusoft\"><span>东软慧聚官方网站制作</span></li><li class=\"zjgl\"><span>中交公路局海外分公司网站建设</span></li><li class=\"report\"><span>中国报道信息门户网站建设</span></li>\n        </ul>\n      </div>\n    </section>\n    \n    <section class=\"quality\">\n      <div class=\"box\">\n    	<div class=\"caption\">\n        	<i></i><span>不同媒介，同样精彩</span>\n            <br class=\"clear\" />\n        </div>\n        <div class=\"swiper-container items\">\n            <div class=\"swiper-wrapper\">\n              <div class=\"swiper-slide nth1\">\n                <ul class=\"list-inline\">\n                  <li class=\"mobi\"><span>响应式手机网站建设</span></li><li class=\"pad\"><span>响应式平板网站建设</span></li><li class=\"pc\"><span>响应式PC网站建设</span></li>\n                </ul>\n                <p>触及视觉灵魂的设计趋势<br />精心布局的用户体验<br />毫无顾忌地通过任何终端<br />呈现在客户的眼前</p>\n              </div>\n              <div class=\"swiper-slide nth2\">\n              	<ul class=\"list-inline\">\n                  <li class=\"ie\"><span>兼容微软IE浏览器的网页设计</span></li><li class=\"chrome\"><span>兼容谷歌Chrome浏览器的网站设计</span></li><li class=\"firefox\"><span>兼容火狐Firefox浏览器的网页设计</span></li><li class=\"safari\"><span>兼容苹果Safari浏览器的网站设计</span></li>\n                </ul>\n                <p>Html5 + CSS3 响应式布局<br />卓越的浏览器兼容性<br />因为高端，所以出众</p>\n              </div>\n              <div class=\"swiper-slide nth3\">\n              	<ul class=\"list-inline\">\n                  <li class=\"windows\"><span>跨windows平台网站制作</span></li><li class=\"ios\"><span>跨ios平台网站制作</span></li><li class=\"andriod\"><span>跨andriod平台网站制作</span></li>\n                </ul>\n                <p>基于 B/S 架构的网站建设<br />无障碍的跨平台应用<br />无须用户下载安装即可使用<br />云端管理，轻松维护</p>\n              </div>\n            </div>\n        </div>\n        <!--\n          <a href=\"/tool/responsive/?url=http://www.1000zhu.com\" title=\"北京网站设计响应式网站测试工具\" class=\"lookall\" target=\"_blank\">响应式网站的照妖镜</a>\n        -->\n      </div>\n    </section>\n    \n    <section class=\"marketing\">\n      <div class=\"box\">\n        <div class=\"caption\">\n        	<i></i><span>整合营销，抢占商机</span>\n            <br class=\"clear\" />\n        </div>\n        <ul class=\"items list-inline\">\n        	<li class=\"se\">\n            	<i></i><strong>搜索引擎</strong>\n                <p>SEO 优化<br />搜索引擎竞价</p>\n            </li>\n            <li class=\"weixin\">\n            	<i></i><strong>微信营销</strong>\n                <p>公众账号 / 微网站<br />微盟 ( 微社区 )</p>\n            </li>\n            <li class=\"weibo\">\n            	<i></i><strong>微博营销</strong>\n                <p>企业蓝V认证<br />官方微博接入网站</p>\n            </li>\n            <li class=\"sms\">\n            	<i></i><strong>消息推送</strong>\n                <p>短信平台接口<br />Email 推送</p>\n            </li>\n            <li class=\"pay\">\n            	<i></i><strong>在线支付</strong>\n                <p>支付宝、银联<br />Paypal 接口</p>\n            </li>\n            <li class=\"bbs\">\n            	<i></i><strong>论坛聚人</strong>\n                <p>独立开发<br />会员打通</p>\n            </li>\n        </ul>\n      </div>\n    </section>\n    \n    <section class=\"aboutus\">\n    	<ul class=\"menu\"><li>思想</li><li>关于</li><li>荣誉</li></ul>\n        <div class=\"swiper-container items\">\n            <div class=\"swiper-wrapper\">\n              <div class=\"swiper-slide nth1\">\n                <strong>厚积薄发</strong>\n                <p>登上峰顶，不是为了饱览风光，是为了寻找更高的山峰<br />日出东方，告别了昨天的荣耀，将光芒照向更远的地方<br />一路上，我们更在意如何积累和沉淀</p>\n                <u>下一秒，让你看，我们到底有多强</u>\n              </div>\n              <div class=\"swiper-slide nth2\">\n              	<strong>千助网建科技（北京）有限公司</strong>\n                <p>成立于2008年，坐落于北京中关村科技园区，是中国优秀的互联网服务提供商。自成立以来，专注于高端网站建设、移动互联应用、B/S架构系统研发、云服务器部署和运维，为企业客户的互联网应用提供一站式服务。</p>\n                <p>我们始终坚持以客户需求为导向，为追求用户体验设计，提供有针对性的项目解决方案，千助人将不断地超越自我，挑战险峰！</p>\n              </div>\n              <div class=\"swiper-slide nth3\">\n              	<strong>放下荣誉，放眼未来</strong>\n                <ul>\n                    <li>2016年<u>-</u>荣获国家高新技术企业</li>\n                    <li>2013年<u>-</u>已拥有自主知识产权，国家软件著作权达12项之多</li>\n                    <li>2011年<u>-</u>注册 \" 千助 \" 商标，打造北京网站建设市场知名品牌</li>\n                    <li>2010年<u>-</u>荣获中关村高新技术企业、海淀区创新企业</li>\n                    <li>2010年<u>-</u>设计联盟评选为金牌设计企业</li>\n                </ul>\n              </div>\n            </div>\n        </div>\n        <table class=\"exp\">\n        	<tr>\n              <td><u>1257</u>位企业客户信赖之选</td>\n              <td><u>367</u>上市/集团企业设计作品</td>\n              <td><u>2016</u>年获国家高新技术企业</td>\n              <td><u>15</u>项国家软件著作权</td>\n              <td><u>97%</u>以上的客户续费率</td>\n            </tr>\n        </table>\n    </section>\n    \n    <section class=\"contact\">\n    	<div class=\"box\">\n        	<div class=\"above\">\n            	<div class=\"wechat\"><img src=\"${global_templets_skin}/skin/h5/qzjz/picture/wechat_code.jpg\" alt=\"扫描关注千助微信公众账号\" /></div>\n                <div class=\"left\">\n                	<a href=\"tel:4006123011\" title=\"网站制作咨询热线\" class=\"tel\"></a>\n                    <p>工作日：4006123011 / 010-80757532<br />非工作日：4006123011 / 18511639815<br />科技园区：北京 · 中关村 · 昌发展<br />办公地址：北京市昌发展万科广场A座6层</p>\n                </div>\n                <div class=\"right\">\n                    官网：www.1000zhu.com<br />Email：Service@1000zhu.com<br />千助网建科技（北京）有限公司<br />京ICP备 08102474 号<br />京公网安备 11010802010984 号\n                </div>\n            </div>\n        </div>\n    </section>\n\n    <section class=\"cooperation\">\n        <div class=\"box\">\n            <span class=\"title\">友情链接，携手共进</span>\n            <ul class=\"list-inline\">\n                <li><a href=\"http://www.1000zhu.com\" target=\"_blank\">北京网站建设</a></li>\n<li><a href=\"http://www.1000zhu.com\" target=\"_blank\">高端网站建设</a></li>\n<li><a href=\"http://www.1000zhu.com\" target=\"_blank\">北京网站设计</a></li>\n<li><a href=\"http://www.wanhu.cn\" target=\"_blank\">上海网站建设</a></li>\n<li><a href=\"http://www.1000zhu.com\" target=\"_blank\">北京网站建设公司</a></li>\n<li><a href=\"http://www.1000zhu.com\" target=\"_blank\">北京网站制作</a></li>\n<li><a href=\"http://www.01jianzhan.com\" target=\"_blank\">广州网站建设</a></li>\n<li><a href=\"http://www.ynyes.com\" target=\"_blank\">云南网站建设</a></li>\n<li><a href=\"https://www.zwcnw.com/\" target=\"_blank\">广州建站</a></li>\n<li><a href=\"http://www.1000zhu.com/article/143.html\" target=\"_blank\">上地网站建设</a></li>\n<li><a href=\"http://www.sscmwl.com\" target=\"_blank\">网站建设</a></li>\n<li><a href=\"http://www.xdnet.cn\" target=\"_blank\">西安网站建设</a></li>\n<li><a href=\"http://www.w-e.cc\" target=\"_blank\">烟台网站建设</a></li>\n<li><a href=\"http://www.qiangseo.com\" target=\"_blank\">长沙网站推广</a></li>\n<li><a href=\"http://www.lkcms.com\" target=\"_blank\">成都网站建设</a></li>\n<li><a href=\"http://www.ymars.com\" target=\"_blank\">长沙网站建设</a></li>\n<li><a href=\"http://www.zijiren.net\" target=\"_blank\">深圳网站建设</a></li>\n<li><a href=\"http://www.ezw.net.cn\" target=\"_blank\">石家庄网站建设</a></li>\n<li><a href=\"http://www.bjhyn.net\" target=\"_blank\">北京网站建设</a></li>\n\n            </ul>\n        </div>\n        <div class=\"bg\"></div>\n    </section>\n    \n    <div class=\"dock\">\n        <ul class=\"icons\">\n        	<li class=\"up\"><i></i></li>\n            <li class=\"im\">\n            	<i></i><p>网站建设咨询<br />在线沟通，请点我<a href=\"http://p.qiao.baidu.com/cps/chat?siteId=7773323&userId=989741\" target=\"_blank\">在线咨询</a></p>\n            </li>\n            <li class=\"tel\">\n            	<i></i><p>我要做网站，<br />小助啊，抓紧给我来个电话！<span class=\"callback\"><input type=\"text\" maxlength=\"12\" /><button>回拨</button></span></p>\n            </li>\n            <li class=\"wechat\">\n            	<i></i><p><img src=\"${global_templets_skin}/skin/h5/qzjz/picture/wechat_code.jpg\" alt=\"扫描关注网站制作微信公众账号\" /></p>\n            </li>\n            <li class=\"down\"><i></i></li>\n        </ul>\n        <a class=\"switch\"></a>\n    </div>\n</body>\n</html>\n\n\n', '模板说明', '2019-09-13 20:42:40', null, '1', '0');

-- ----------------------------
-- Table structure for `fly_template_site_merge`
-- ----------------------------
DROP TABLE IF EXISTS `fly_template_site_merge`;
CREATE TABLE `fly_template_site_merge` (
  `template_id` bigint(20) NOT NULL COMMENT '模版的名字，模版编码',
  `site_id` bigint(20) NOT NULL,
  PRIMARY KEY (`template_id`),
  UNIQUE KEY `id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='模板与用户关联表';

-- ----------------------------
-- Records of fly_template_site_merge
-- ----------------------------

-- ----------------------------
-- Table structure for `fly_template_sku`
-- ----------------------------
DROP TABLE IF EXISTS `fly_template_sku`;
CREATE TABLE `fly_template_sku` (
  `id` bigint(20) NOT NULL,
  `template_id` bigint(20) DEFAULT NULL,
  `use_time` int(2) unsigned zerofill DEFAULT NULL COMMENT '使用时间，1为一周,2一年,3两年,4三年，5五年',
  `price` decimal(8,2) DEFAULT '0.00' COMMENT '模板销售价格'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of fly_template_sku
-- ----------------------------

-- ----------------------------
-- Table structure for `fly_user`
-- ----------------------------
DROP TABLE IF EXISTS `fly_user`;
CREATE TABLE `fly_user` (
  `id` bigint(20) NOT NULL COMMENT '用户id编号',
  `user_name` char(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '用户名',
  `user_mobile` char(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '手机号,11位',
  `email` char(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '邮箱',
  `password` char(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '加密后的密码',
  `nickname` char(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '姓名、昵称',
  `avatar` char(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '头像',
  `currency` int(11) DEFAULT '0' COMMENT '资金，可以是积分、金币、等等站内虚拟货币',
  `sex` char(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '1男、0女、2未知',
  `freezemoney` decimal(8,2) DEFAULT '0.00' COMMENT '账户冻结余额，金钱,RMB，单位：元',
  `lastip` char(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '最后一次登陆的ip',
  `isfreeze` tinyint(2) DEFAULT '0' COMMENT '是否已冻结，1已冻结（拉入黑名单），0正常',
  `money` decimal(8,2) DEFAULT '0.00' COMMENT '账户可用余额，金钱,RMB，单位：元',
  `idcardauth` tinyint(2) DEFAULT '0' COMMENT '是否已经经过真实身份认证了（身份证、银行卡绑定等）。默认为没有认证。预留字段。1已认证；0未认证',
  `sign` char(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '个人签名',
  `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '自我描述',
  `add_time` datetime NOT NULL COMMENT '注册时间,时间戳',
  `last_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `status` tinyint(3) DEFAULT '0' COMMENT '0 可用, 1 禁用, 2 注销',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`,`user_name`,`user_mobile`) USING BTREE,
  KEY `username` (`user_name`,`email`,`user_mobile`,`isfreeze`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户信息表。系统登陆的用户信息都在此处';

-- ----------------------------
-- Records of fly_user
-- ----------------------------
INSERT INTO `fly_user` VALUES ('392', 'company2', '17000000001', '', '$2a$10$HGv0RBwVXs9BGHI6EMmtcuG64t9Slv9LRL../wPNb3Gi.rrfuzAsi', '代理', 'default.png', '0', '1', '0.00', '127.0.0.1', '0', '0.00', '0', null, null, '2019-08-23 00:55:35', '2019-08-26 08:32:46', '0', '0');
INSERT INTO `fly_user` VALUES ('393', 'company3', '', '', '$2a$10$HGv0RBwVXs9BGHI6EMmtcuG64t9Slv9LRL../wPNb3Gi.rrfuzAsi', 'ceshi', 'default.png', '0', '1', '0.00', '127.0.0.1', '0', '0.00', '0', null, null, '2019-08-23 00:55:37', '2019-08-26 08:32:48', '0', '0');
INSERT INTO `fly_user` VALUES ('366257223594147840', '15110206661', null, null, '$2a$10$63vOQtIYFgSCxv...VRHkuBHGltKPu9DjmILqKB26CeSwa03fpe5O', '衡欠姓', null, '0', null, '0.00', null, '0', '0.00', '0', null, null, '2019-09-03 05:36:33', null, '0', '0');
INSERT INTO `fly_user` VALUES ('366301729152839681', 'company', '17000000002', '', '$2a$10$ODbRELTVhdwnT/zzcbxRD.N6WqqAHk/qMQQdDEak6Zxh1CMUtVR96', '总管理', 'default.png', '0', '1', '0.00', '127.0.0.1', '0', '0.00', '0', null, null, '2019-08-23 00:55:33', '2019-08-26 08:32:42', '0', '0');
INSERT INTO `fly_user` VALUES ('366433284017618944', null, '15110206662', null, '$2a$10$TqIZXB66Lp8JNDvDgLlrb.2IRFqCvberTYxJ8Wp6w8huDsWPNgLNK', '石兔翠', null, '0', null, '0.00', null, '0', '0.00', '0', null, null, '2019-09-03 17:16:09', null, '0', '0');
INSERT INTO `fly_user` VALUES ('366435476288045056', null, '15110206663', null, '$2a$10$/sE3axRxhS3B7smnw0UlieZmPzHbzfXkEWA7ERDxENfWv8U1doOWC', '企业测试', null, '0', null, '0.00', null, '0', '0.00', '0', null, null, '2019-09-04 01:24:51', null, '0', '0');
INSERT INTO `fly_user` VALUES ('366438568849047552', null, '15110206664', null, '$2a$10$iOpTOs8KZAnwyT4EoK5kqujd7jWW7K6dtTVgKxKphabxSEUobwV3C', '季郑城', null, '0', null, '0.00', null, '0', '0.00', '0', null, null, '2019-09-04 01:37:09', null, '0', '0');
INSERT INTO `fly_user` VALUES ('366444570499612672', null, '15110206667', null, '$2a$10$Fou8j40r75H3O65PqFDhoO/zOyXP5m1ae/sVovhQnUGAKIfdXmNNW', '艾哲名', null, '0', null, '0.00', null, '0', '0.00', '0', null, null, '2019-09-04 02:01:00', null, '0', '0');
INSERT INTO `fly_user` VALUES ('366454525470441472', null, '15110206650', null, '$2a$10$l8OxGg7Hbw6H6cObVey6BuzeBxVoFGHgvWU7gV/IJkL4PZEi9zTWC', '法吉书', null, '0', null, '0.00', null, '0', '0.00', '0', null, null, '2019-09-04 14:45:33', null, '0', '0');
INSERT INTO `fly_user` VALUES ('366638723963551744', null, '15110206680', null, '$2a$10$/RPoniUMojP/vNryjb5iPe2pc5IaDcfT/09g4botV09AAzhnOqytC', '匡背摧', null, '0', null, '0.00', null, '0', '0.00', '0', null, null, '2019-09-04 14:52:29', null, '0', '0');
INSERT INTO `fly_user` VALUES ('378556618222075904', null, '15110206660', null, '$2a$10$XRXdxCw54v68ijV85dF05uK2AdCefW2UfJoSlAiPuCrmjmlHkNiUa', '利裹满', null, '0', null, '0.00', null, '0', '0.00', '0', null, null, '2019-10-07 12:09:57', null, '0', '0');

-- ----------------------------
-- Table structure for `fly_user_activation`
-- ----------------------------
DROP TABLE IF EXISTS `fly_user_activation`;
CREATE TABLE `fly_user_activation` (
  `id` bigint(20) unsigned NOT NULL,
  `info_tyoe` int(2) DEFAULT '0' COMMENT '信息类型，0手机，1邮箱',
  `user_id` bigint(20) DEFAULT '0' COMMENT '当前使用验证码的用户Id，可为空',
  `user_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '用户名，手机或者邮箱',
  `code` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '验证码',
  `code_type` tinyint(2) DEFAULT '1' COMMENT '注册码类型：1手机注册验证码,2安全手机设置验证码,3密码重置验证码',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `refer_status` int(2) DEFAULT '0' COMMENT '激活状态，0未激活，1已激活',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户账号验证码激活表';

-- ----------------------------
-- Records of fly_user_activation
-- ----------------------------
INSERT INTO `fly_user_activation` VALUES ('365908144288694272', '0', '0', '15110206660', '201354', '1', '2019-09-02 06:29:25', '0');
INSERT INTO `fly_user_activation` VALUES ('366254263619289088', '0', '0', '15110206660', '883265', '1', '2019-09-03 05:24:47', '0');
INSERT INTO `fly_user_activation` VALUES ('366256898753691648', '0', '0', '15110206660', '998924', '1', '2019-09-03 05:35:15', '0');
INSERT INTO `fly_user_activation` VALUES ('366404957470982144', '0', '0', '15110206660', '820420', '1', '2019-09-03 15:23:35', '0');
INSERT INTO `fly_user_activation` VALUES ('366414384567681024', '0', '0', '15110206660', '309328', '1', '2019-09-03 16:01:03', '0');
INSERT INTO `fly_user_activation` VALUES ('366433122599829504', '0', '0', '15110206660', '369188', '1', '2019-09-03 17:15:30', '0');
INSERT INTO `fly_user_activation` VALUES ('366435298634104832', '0', '0', '15110206660', '422588', '1', '2019-09-04 01:24:09', '0');
INSERT INTO `fly_user_activation` VALUES ('366438466231205888', '0', '0', '15110206660', '192163', '1', '2019-09-04 01:36:44', '0');
INSERT INTO `fly_user_activation` VALUES ('366444468116652032', '0', '0', '15110206660', '251407', '1', '2019-09-04 02:00:35', '0');
INSERT INTO `fly_user_activation` VALUES ('366454436035297280', '0', '0', '15110206660', '670956', '1', '2019-09-04 14:52:12', '1');
INSERT INTO `fly_user_activation` VALUES ('378556483924656128', '0', '0', '15110206660', '643208', '1', '2019-10-07 12:09:25', '1');

-- ----------------------------
-- Table structure for `fly_user_invite`
-- ----------------------------
DROP TABLE IF EXISTS `fly_user_invite`;
CREATE TABLE `fly_user_invite` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `to_user_id` bigint(20) NOT NULL COMMENT '被邀请人id',
  `from_user_id` bigint(20) NOT NULL COMMENT '邀请人ID',
  `status` int(2) DEFAULT '0' COMMENT '状态',
  `create_time` datetime DEFAULT NULL COMMENT '邀请时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=342863254399221761 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of fly_user_invite
-- ----------------------------
INSERT INTO `fly_user_invite` VALUES ('342863254399221760', '342863254038511616', '342833898561994752', '0', '2019-07-08 00:17:16');

-- ----------------------------
-- Table structure for `fly_user_menu`
-- ----------------------------
DROP TABLE IF EXISTS `fly_user_menu`;
CREATE TABLE `fly_user_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父菜单ID',
  `url` varchar(200) DEFAULT '#' COMMENT '请求地址',
  `target` varchar(20) DEFAULT '0' COMMENT '打开方式（0页签 1新窗口）',
  `menu_type` char(1) DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` tinyint(1) DEFAULT '0' COMMENT '菜单状态（1显示 0隐藏）',
  `perms` varchar(100) DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) DEFAULT '#' COMMENT '菜单图标',
  `sort_order` int(4) DEFAULT '0' COMMENT '显示顺序',
  `create_user_id` bigint(20) DEFAULT '0',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) DEFAULT '0',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=402991857371447297 DEFAULT CHARSET=utf8mb4 COMMENT='菜单权限表';

-- ----------------------------
-- Records of fly_user_menu
-- ----------------------------
INSERT INTO `fly_user_menu` VALUES ('396714492291199013', '系统设置', '0', '#', '0', 'M', '1', '', 'layui-icon layui-icon-set', '1', null, '2018-03-16 11:33:00', '400701255061602304', '2019-12-07 14:44:49', '系统管理目录');
INSERT INTO `fly_user_menu` VALUES ('396714492303781888', '财务管理', '0', '#', '0', 'M', '1', '', 'layui-icon layui-icon-website', '2', null, '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00', '系统监控目录');
INSERT INTO `fly_user_menu` VALUES ('396714492303781889', '消息管理', '0', '#', '0', 'M', '1', '', 'layui-icon layui-icon-read', '3', null, '2018-03-16 11:33:00', '0', '2018-03-16 11:33:00', '系统工具目录');
INSERT INTO `fly_user_menu` VALUES ('396714492303781890', '网站管理', '396714492291199013', '/member/site/list.do', '0', 'C', '1', 'user:menu:', 'layui-icon layui-icon-set', '0', null, '2019-08-31 00:29:08', '0', '2019-08-31 00:29:22', '企业用户网站列表');
INSERT INTO `fly_user_menu` VALUES ('396714492303781891', '财务管理', '0', '#', '0', 'M', '1', null, '#', '0', null, '2019-08-31 15:51:24', '400708878162984960', '2019-12-07 15:15:06', '财务管理');
INSERT INTO `fly_user_menu` VALUES ('396714492303781892', '模板开发', '0', '#', '0', 'M', '1', null, 'layui-icon layui-icon-template-1', '0', null, '2019-11-26 14:56:48', '400708906990436352', '2019-12-07 15:15:13', '');
INSERT INTO `fly_user_menu` VALUES ('400703745815478272', '基本信息', '396714492291199013', '/member/company/info.do', '0', 'C', '1', null, 'layui-icon layui-icon-read', '0', '369555184164286491', '2019-12-07 14:54:43', '0', null, '');
INSERT INTO `fly_user_menu` VALUES ('402991857371447296', '友情链接', '0', '#', '0', 'M', '1', null, 'layui-icon layui-icon-website', '0', '369555184164286491', '2019-12-13 22:26:51', '0', null, '');

-- ----------------------------
-- Table structure for `fly_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `fly_user_role`;
CREATE TABLE `fly_user_role` (
  `id` bigint(20) NOT NULL COMMENT '角色ID',
  `role_name` varchar(30) NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) NOT NULL COMMENT '角色权限字符串',
  `status` tinyint(1) DEFAULT NULL COMMENT '角色状态（0停用，1正常 ）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `sort_order` int(4) DEFAULT '0' COMMENT '显示顺序',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色信息表';

-- ----------------------------
-- Records of fly_user_role
-- ----------------------------
INSERT INTO `fly_user_role` VALUES ('366301311295303684', '普通会员', 'member', '1', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-07 15:15:50', '管理员', '1', '0');
INSERT INTO `fly_user_role` VALUES ('366301311295303685', 'VIP会员', 'vip_member ', '1', 'admin', '2018-03-16 11:33:00', 'admin', '2019-08-12 21:07:06', '商代理，可以开通子代理、网站', '2', '0');
INSERT INTO `fly_user_role` VALUES ('366301311295303686', '至尊VIP会员', 'supreme_vip', '1', 'admin', '2019-08-12 21:23:56', '', null, '企业用户组', '3', '0');

-- ----------------------------
-- Table structure for `fly_user_role_menu_merge`
-- ----------------------------
DROP TABLE IF EXISTS `fly_user_role_menu_merge`;
CREATE TABLE `fly_user_role_menu_merge` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of fly_user_role_menu_merge
-- ----------------------------
INSERT INTO `fly_user_role_menu_merge` VALUES ('366301311295303684', '396714492291199013');
INSERT INTO `fly_user_role_menu_merge` VALUES ('366301311295303684', '396714492303781888');
INSERT INTO `fly_user_role_menu_merge` VALUES ('366301311295303684', '396714492303781889');
INSERT INTO `fly_user_role_menu_merge` VALUES ('366301311295303684', '396714492303781890');
INSERT INTO `fly_user_role_menu_merge` VALUES ('366301311295303684', '400703745815478272');
INSERT INTO `fly_user_role_menu_merge` VALUES ('366454525470441472', '396714492303781892');
INSERT INTO `fly_user_role_menu_merge` VALUES ('402991857371447296', '402991857371447296');

-- ----------------------------
-- Table structure for `fly_user_role_merge`
-- ----------------------------
DROP TABLE IF EXISTS `fly_user_role_merge`;
CREATE TABLE `fly_user_role_merge` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户的id，user.id,一个用户可以有多个角色',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色的id，role.id ，一个用户可以有多个角色',
  PRIMARY KEY (`id`),
  KEY `userid` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='用户拥有哪些角色';

-- ----------------------------
-- Records of fly_user_role_merge
-- ----------------------------
INSERT INTO `fly_user_role_merge` VALUES ('366301729152839692', '392', '366301311295303684');
INSERT INTO `fly_user_role_merge` VALUES ('366301729152839693', '366301729152839681', '366301311295303684');
INSERT INTO `fly_user_role_merge` VALUES ('366301729152839694', '393', '366301311295303685');
INSERT INTO `fly_user_role_merge` VALUES ('366415460364713984', '366301311295303684', '366415459915923456');
INSERT INTO `fly_user_role_merge` VALUES ('366433284478992384', '366433284017618944', '366301311295303684');
INSERT INTO `fly_user_role_merge` VALUES ('366435476741029888', '366435476288045056', '366301311295303684');
INSERT INTO `fly_user_role_merge` VALUES ('366438569310420992', '366438568849047552', '366301311295303684');
INSERT INTO `fly_user_role_merge` VALUES ('366444570952597504', '366444570499612672', '366301311295303684');
INSERT INTO `fly_user_role_merge` VALUES ('366454525940203520', '366454525470441472', '366301311295303684');
INSERT INTO `fly_user_role_merge` VALUES ('366638724538171392', '366638723963551744', '366301311295303684');
INSERT INTO `fly_user_role_merge` VALUES ('378556618758946816', '378556618222075904', '366301311295303684');

-- ----------------------------
-- Table structure for `fly_visit_log`
-- ----------------------------
DROP TABLE IF EXISTS `fly_visit_log`;
CREATE TABLE `fly_visit_log` (
  `id` bigint(20) NOT NULL,
  `site_id` int(11) NOT NULL DEFAULT '0' COMMENT '网站id',
  `user_id` int(11) DEFAULT NULL COMMENT '访问用户',
  `current_url` varchar(255) NOT NULL COMMENT '页面URL',
  `source_referrer` varchar(255) DEFAULT NULL COMMENT '来源URL',
  `source_domain` varchar(100) DEFAULT NULL COMMENT '来源域名',
  `visit_ip` varchar(100) DEFAULT NULL COMMENT 'IP地址',
  `visit_cookie` varchar(100) DEFAULT NULL COMMENT 'COOKIE值',
  `visit_user_agent` varchar(450) DEFAULT NULL COMMENT '用户代理',
  `visit_browser` varchar(100) DEFAULT NULL COMMENT '浏览器',
  `visit_os` varchar(100) DEFAULT NULL COMMENT '操作系统',
  `visit_device` varchar(100) DEFAULT NULL COMMENT '设备(COMPUTER,MOBILE,TABLET,等)',
  `visit_country` varchar(100) DEFAULT NULL COMMENT '国家',
  `visit_area` varchar(100) DEFAULT NULL COMMENT '地区',
  `add_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`id`),
  KEY `visitlog_site` (`site_id`) USING BTREE,
  KEY `visitlog_user` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='访问日志表';

-- ----------------------------
-- Records of fly_visit_log
-- ----------------------------
INSERT INTO `fly_visit_log` VALUES ('4626', '1', null, 'http://127.0.0.1:8080/', '', 'DIRECT', '127.0.0.1', '739d2228c0df4cf79eb7169b0af959fe', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'CHROME', 'WINDOWS_10', 'COMPUTER', 'LAN', 'LAN', '2019-09-08 02:50:39');
INSERT INTO `fly_visit_log` VALUES ('4627', '1', null, 'http://127.0.0.1:8080/info/145', 'http://127.0.0.1:8080/', 'DIRECT', '127.0.0.1', '739d2228c0df4cf79eb7169b0af959fe', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'CHROME', 'WINDOWS_10', 'COMPUTER', 'LAN', 'LAN', '2019-09-08 02:51:08');
INSERT INTO `fly_visit_log` VALUES ('4628', '1', '1', 'http://127.0.0.1:8080/info/97', 'http://127.0.0.1:8080/cmscp/core/info/view.do?id=97&queryNodeId=83&queryNodeType=0&queryInfoPermType=&queryStatus=&position=3&', 'DIRECT', '127.0.0.1', '739d2228c0df4cf79eb7169b0af959fe', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'CHROME', 'WINDOWS_10', 'COMPUTER', 'LAN', 'LAN', '2019-09-08 02:56:19');
INSERT INTO `fly_visit_log` VALUES ('4629', '1', '1', 'http://127.0.0.1:8080/info/97', 'http://127.0.0.1:8080/cmscp/core/info/edit.do?id=97&queryNodeId=83&queryNodeType=0&queryInfoPermType=&queryStatus=&position=0&', 'DIRECT', '127.0.0.1', '739d2228c0df4cf79eb7169b0af959fe', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'CHROME', 'WINDOWS_10', 'COMPUTER', 'LAN', 'LAN', '2019-09-08 02:57:02');
INSERT INTO `fly_visit_log` VALUES ('4630', '1', '1', 'http://127.0.0.1:8080/node/52', 'http://127.0.0.1:8080/info/97', 'DIRECT', '127.0.0.1', '739d2228c0df4cf79eb7169b0af959fe', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'CHROME', 'WINDOWS_10', 'COMPUTER', 'LAN', 'LAN', '2019-09-08 02:57:19');
INSERT INTO `fly_visit_log` VALUES ('4631', '1', '1', 'http://127.0.0.1:8080/info/130', 'http://127.0.0.1:8080/node/52', 'DIRECT', '127.0.0.1', '739d2228c0df4cf79eb7169b0af959fe', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'CHROME', 'WINDOWS_10', 'COMPUTER', 'LAN', 'LAN', '2019-09-08 02:57:24');

-- ----------------------------
-- Table structure for `sys_oper_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log` (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) DEFAULT '' COMMENT '方法名称',
  `operator_type` int(1) DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(50) DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) DEFAULT '' COMMENT '请求参数',
  `status` int(1) DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`)
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=utf8mb4 COMMENT='操作日志记录';

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES ('100', '定时任务', '2', 'com.ruoyi.quartz.controller.SysJobController.changeStatus()', '1', 'admin', '研发部门', '/monitor/job/changeStatus', '127.0.0.1', '内网IP', '{\r\n  \"jobId\" : [ \"1\" ],\r\n  \"jobGroup\" : [ \"DEFAULT\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '0', null, '2019-08-09 17:16:37');
INSERT INTO `sys_oper_log` VALUES ('101', '定时任务', '2', 'com.ruoyi.quartz.controller.SysJobController.changeStatus()', '1', 'admin', '研发部门', '/monitor/job/changeStatus', '127.0.0.1', '内网IP', '{\r\n  \"jobId\" : [ \"1\" ],\r\n  \"jobGroup\" : [ \"DEFAULT\" ],\r\n  \"status\" : [ \"1\" ]\r\n}', '0', null, '2019-08-09 17:16:40');
INSERT INTO `sys_oper_log` VALUES ('102', '角色管理', '2', 'com.ruoyi.web.controller.system.SysRoleController.editSave()', '1', 'admin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"2\" ],\r\n  \"roleName\" : [ \"普通角色\" ],\r\n  \"roleKey\" : [ \"common\" ],\r\n  \"roleSort\" : [ \"2\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"普通角色\" ],\r\n  \"menuIds\" : [ \"3,113,114,1056,1057,1058,1059,1060,115\" ]\r\n}', '0', null, '2019-08-09 17:24:00');
INSERT INTO `sys_oper_log` VALUES ('103', '代码生成', '6', 'com.ruoyi.generator.controller.GenController.importTableSave()', '1', 'ry', '测试部门', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\r\n  \"tables\" : [ \"fly_weixin_gzuser\" ]\r\n}', '0', null, '2019-08-09 17:48:58');
INSERT INTO `sys_oper_log` VALUES ('104', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.genCode()', '1', 'ry', '测试部门', '/tool/gen/genCode/fly_weixin_gzuser', '127.0.0.1', '内网IP', '{ }', '0', null, '2019-08-09 17:50:05');
INSERT INTO `sys_oper_log` VALUES ('105', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', '1', 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"1\" ],\r\n  \"tableName\" : [ \"fly_weixin_gzuser\" ],\r\n  \"tableComment\" : [ \"粉丝表\" ],\r\n  \"className\" : [ \"FlyWeixinGzuser\" ],\r\n  \"functionAuthor\" : [ \"ruoyi\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"1\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"序号\" ],\r\n  \"columns[0].javaType\" : [ \"String\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"2\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"openid\" ],\r\n  \"columns[1].javaType\" : [ \"String\" ],\r\n  \"columns[1].javaField\" : [ \"openid\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"3\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"昵称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"nickname\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"EQ\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"4\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"过滤后昵称\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"nicknameTxt\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].isList\" : [ \"1\" ],\r\n  \"columns[3].isQuery\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"5\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"备注名称\" ],\r\n  \"c', '0', null, '2019-08-09 19:04:28');
INSERT INTO `sys_oper_log` VALUES ('106', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', '1', 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"1\" ],\r\n  \"tableName\" : [ \"fly_weixin_gzuser\" ],\r\n  \"tableComment\" : [ \"粉丝表\" ],\r\n  \"className\" : [ \"FlyWeixinGzuser\" ],\r\n  \"functionAuthor\" : [ \"ruoyi\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"1\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"序号\" ],\r\n  \"columns[0].javaType\" : [ \"String\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"2\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"openid\" ],\r\n  \"columns[1].javaType\" : [ \"String\" ],\r\n  \"columns[1].javaField\" : [ \"openid\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"3\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"昵称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"nickname\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"EQ\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"4\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"过滤后昵称\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"nicknameTxt\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].isList\" : [ \"1\" ],\r\n  \"columns[3].isQuery\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"5\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"备注名称\" ],\r\n  \"c', '0', null, '2019-08-09 19:08:31');
INSERT INTO `sys_oper_log` VALUES ('107', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.genCode()', '1', 'admin', '研发部门', '/tool/gen/genCode/fly_weixin_gzuser', '127.0.0.1', '内网IP', '{ }', '0', null, '2019-08-09 19:08:37');
INSERT INTO `sys_oper_log` VALUES ('108', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.addSave()', '1', 'admin', '研发部门', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"0\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"网站管理\" ],\r\n  \"url\" : [ \"\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"4\" ],\r\n  \"icon\" : [ \"fa fa-home\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '0', null, '2019-08-09 19:17:35');
INSERT INTO `sys_oper_log` VALUES ('109', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.editSave()', '1', 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"2005\" ],\r\n  \"parentId\" : [ \"2010\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"粉丝\" ],\r\n  \"url\" : [ \"/weixin/wxuser\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"weixin:wxuser:view\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"#\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '0', null, '2019-08-09 19:17:52');
INSERT INTO `sys_oper_log` VALUES ('110', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.editSave()', '1', 'admin', '研发部门', '/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"1\" ],\r\n  \"deptId\" : [ \"103\" ],\r\n  \"userName\" : [ \"若依\" ],\r\n  \"dept.deptName\" : [ \"研发部门\" ],\r\n  \"phonenumber\" : [ \"15888888888\" ],\r\n  \"email\" : [ \"ry@163.com\" ],\r\n  \"loginName\" : [ \"admin\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"role\" : [ \"1\" ],\r\n  \"remark\" : [ \"管理员\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"1\" ],\r\n  \"postIds\" : [ \"1\" ]\r\n}', '0', null, '2019-08-09 20:07:13');
INSERT INTO `sys_oper_log` VALUES ('111', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.editSave()', '1', 'admin', '研发部门', '/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"1\" ],\r\n  \"deptId\" : [ \"103\" ],\r\n  \"userName\" : [ \"若依\" ],\r\n  \"dept.deptName\" : [ \"研发部门\" ],\r\n  \"phonenumber\" : [ \"15888888888\" ],\r\n  \"email\" : [ \"ry@163.com\" ],\r\n  \"loginName\" : [ \"admin\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"role\" : [ \"1\" ],\r\n  \"remark\" : [ \"管理员\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"1\" ],\r\n  \"postIds\" : [ \"1\" ]\r\n}', '0', null, '2019-08-09 20:07:31');
INSERT INTO `sys_oper_log` VALUES ('112', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.editSave()', '1', 'admin', '研发部门', '/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"1\" ],\r\n  \"deptId\" : [ \"103\" ],\r\n  \"userName\" : [ \"若依x\" ],\r\n  \"dept.deptName\" : [ \"研发部门\" ],\r\n  \"phonenumber\" : [ \"15888888888\" ],\r\n  \"email\" : [ \"ry@163.com\" ],\r\n  \"loginName\" : [ \"admin\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"role\" : [ \"1\" ],\r\n  \"remark\" : [ \"管理员\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"1\" ],\r\n  \"postIds\" : [ \"1\" ]\r\n}', '0', null, '2019-08-09 20:07:42');
INSERT INTO `sys_oper_log` VALUES ('113', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', '1', 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"1\" ],\r\n  \"tableName\" : [ \"fly_weixin_gzuser\" ],\r\n  \"tableComment\" : [ \"粉丝表\" ],\r\n  \"className\" : [ \"WeixinGzuser\" ],\r\n  \"functionAuthor\" : [ \"FlyCms\" ],\r\n  \"remark\" : [ \"微信客户端用户信息表\" ],\r\n  \"columns[0].columnId\" : [ \"1\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"序号\" ],\r\n  \"columns[0].javaType\" : [ \"String\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"2\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"openid\" ],\r\n  \"columns[1].javaType\" : [ \"String\" ],\r\n  \"columns[1].javaField\" : [ \"openid\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"3\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"昵称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"nickname\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"EQ\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"4\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"过滤后昵称\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"nicknameTxt\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].isList\" : [ \"1\" ],\r\n  \"columns[3].isQuery\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"5\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"备注名称\" ', '0', null, '2019-08-09 21:59:55');
INSERT INTO `sys_oper_log` VALUES ('114', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.genCode()', '1', 'admin', '研发部门', '/tool/gen/genCode/fly_weixin_gzuser', '127.0.0.1', '内网IP', '{ }', '0', null, '2019-08-09 22:00:30');
INSERT INTO `sys_oper_log` VALUES ('115', '角色管理', '2', 'com.ruoyi.web.controller.system.SysRoleController.editSave()', '1', 'admin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"1\" ],\r\n  \"roleName\" : [ \"管理员\" ],\r\n  \"roleKey\" : [ \"admin\" ],\r\n  \"roleSort\" : [ \"1\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"管理员\" ],\r\n  \"menuIds\" : [ \"1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,102,1012,1013,1014,1015,103,1016,1017,1018,1019,104,1020,1021,1022,1023,1024,105,1025,1026,1027,1028,1029,106,1030,1031,1032,1033,1034,107,1035,1036,1037,1038,108,500,1039,1040,1041,1042,501,1043,1044,1045,2000,2001,2002,2003,2004,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,1055,111,112,3,113,114,1056,1057,1058,1059,1060,115,2010,2005,2006,2007,2008,2009\" ]\r\n}', '0', null, '2019-08-09 22:10:10');
INSERT INTO `sys_oper_log` VALUES ('116', '在线用户', '7', 'com.ruoyi.web.controller.monitor.SysUserOnlineController.forceLogout()', '1', 'admin', '研发部门', '/monitor/online/forceLogout', '127.0.0.1', '内网IP', '{\r\n  \"sessionId\" : [ \"47d566d1-fa2b-4924-ae3a-15d672f09a1b\" ]\r\n}', '0', null, '2019-08-09 22:10:56');
INSERT INTO `sys_oper_log` VALUES ('117', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.editSave()', '1', 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"2005\" ],\r\n  \"parentId\" : [ \"2010\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"粉丝\" ],\r\n  \"url\" : [ \"/weixin/wxuser\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"weixin:wxuser:view\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"#\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '0', null, '2019-08-09 22:11:45');
INSERT INTO `sys_oper_log` VALUES ('118', '通知公告', '2', 'com.ruoyi.web.controller.system.SysNoticeController.editSave()', '1', 'admin', '研发部门', '/system/notice/edit', '127.0.0.1', '内网IP', '{\r\n  \"noticeId\" : [ \"2\" ],\r\n  \"noticeTitle\" : [ \"维护通知：2018-07-01 若依系统凌晨维护\" ],\r\n  \"noticeType\" : [ \"1\" ],\r\n  \"noticeContent\" : [ \"维护内容\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '0', null, '2019-08-09 22:12:03');
INSERT INTO `sys_oper_log` VALUES ('119', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', '1', 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"1\" ],\r\n  \"tableName\" : [ \"fly_weixin_gzuser\" ],\r\n  \"tableComment\" : [ \"粉丝表\" ],\r\n  \"className\" : [ \"WeixinGzuser\" ],\r\n  \"functionAuthor\" : [ \"FlyCms\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"1\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"序号\" ],\r\n  \"columns[0].javaType\" : [ \"String\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"2\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"openid\" ],\r\n  \"columns[1].javaType\" : [ \"String\" ],\r\n  \"columns[1].javaField\" : [ \"openid\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"3\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"昵称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"nickname\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"LIKE\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"4\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"过滤后昵称\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"nicknameTxt\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"5\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"备注名称\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"columns[4].javaField\" : [ \"bz', '0', null, '2019-08-09 22:47:57');
INSERT INTO `sys_oper_log` VALUES ('120', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', '1', 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"1\" ],\r\n  \"tableName\" : [ \"fly_weixin_gzuser\" ],\r\n  \"tableComment\" : [ \"粉丝表\" ],\r\n  \"className\" : [ \"WeixinGzuser\" ],\r\n  \"functionAuthor\" : [ \"FlyCms\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"1\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"序号\" ],\r\n  \"columns[0].javaType\" : [ \"String\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"2\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"openid\" ],\r\n  \"columns[1].javaType\" : [ \"String\" ],\r\n  \"columns[1].javaField\" : [ \"openid\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"3\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"昵称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"nickname\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"LIKE\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"4\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"过滤后昵称\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"nicknameTxt\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"5\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"备注名称\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"columns[4].javaField\" : [ \"bzname\" ],\r\n  \"columns[4].isInsert\" ', '0', null, '2019-08-09 22:49:21');
INSERT INTO `sys_oper_log` VALUES ('121', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.update()', '1', 'admin', '研发部门', '/system/user/profile/update', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"\" ],\r\n  \"userName\" : [ \"FlyCms\" ],\r\n  \"phonenumber\" : [ \"15888888888\" ],\r\n  \"email\" : [ \"ry@163.com\" ],\r\n  \"sex\" : [ \"1\" ]\r\n}', '0', null, '2019-08-09 22:49:57');
INSERT INTO `sys_oper_log` VALUES ('122', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.genCode()', '1', 'admin', '研发部门', '/tool/gen/genCode/fly_weixin_gzuser', '127.0.0.1', '内网IP', '{ }', '0', null, '2019-08-09 23:01:45');
INSERT INTO `sys_oper_log` VALUES ('123', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', '1', 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"1\" ],\r\n  \"tableName\" : [ \"fly_weixin_gzuser\" ],\r\n  \"tableComment\" : [ \"粉丝表\" ],\r\n  \"className\" : [ \"WeixinGzuser\" ],\r\n  \"functionAuthor\" : [ \"FlyCms\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"1\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"序号\" ],\r\n  \"columns[0].javaType\" : [ \"String\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"2\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"openid\" ],\r\n  \"columns[1].javaType\" : [ \"String\" ],\r\n  \"columns[1].javaField\" : [ \"openid\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"3\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"昵称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"nickname\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"LIKE\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"4\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"过滤后昵称\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"nicknameTxt\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"5\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"备注名称\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"columns[4].javaField\" : [ \"bzname\" ],\r\n  \"columns[4].isInsert\" ', '0', null, '2019-08-09 23:17:33');
INSERT INTO `sys_oper_log` VALUES ('124', '代码生成', '6', 'com.ruoyi.generator.controller.GenController.importTableSave()', '1', 'admin', '研发部门', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\r\n  \"tables\" : [ \"fly_site\" ]\r\n}', '0', null, '2019-08-10 00:19:11');
INSERT INTO `sys_oper_log` VALUES ('125', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', '1', 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"2\" ],\r\n  \"tableName\" : [ \"fly_site\" ],\r\n  \"tableComment\" : [ \"用户站点\" ],\r\n  \"className\" : [ \"FlySite\" ],\r\n  \"functionAuthor\" : [ \"FlyCms\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"24\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"id\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"25\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"用户id\" ],\r\n  \"columns[1].javaType\" : [ \"Long\" ],\r\n  \"columns[1].javaField\" : [ \"userId\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].isRequired\" : [ \"1\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"26\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"公司名称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"companyName\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"LIKE\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"27\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"公司电话\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"companyTel\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"28\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"站点Logo\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"', '0', null, '2019-08-10 00:24:30');
INSERT INTO `sys_oper_log` VALUES ('126', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', '1', 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"2\" ],\r\n  \"tableName\" : [ \"fly_site\" ],\r\n  \"tableComment\" : [ \"用户站点\" ],\r\n  \"className\" : [ \"Site\" ],\r\n  \"functionAuthor\" : [ \"FlyCms\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"24\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"id\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"25\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"用户id\" ],\r\n  \"columns[1].javaType\" : [ \"Long\" ],\r\n  \"columns[1].javaField\" : [ \"userId\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].isRequired\" : [ \"1\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"26\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"公司名称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"companyName\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"LIKE\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"27\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"公司电话\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"companyTel\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"28\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"站点Logo\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"col', '0', null, '2019-08-10 00:29:55');
INSERT INTO `sys_oper_log` VALUES ('127', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', '1', 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"2\" ],\r\n  \"tableName\" : [ \"fly_site\" ],\r\n  \"tableComment\" : [ \"用户站点\" ],\r\n  \"className\" : [ \"Site\" ],\r\n  \"functionAuthor\" : [ \"FlyCms\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"24\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"id\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"25\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"用户id\" ],\r\n  \"columns[1].javaType\" : [ \"Long\" ],\r\n  \"columns[1].javaField\" : [ \"userId\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].isRequired\" : [ \"1\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"26\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"公司名称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"companyName\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"LIKE\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"27\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"公司电话\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"companyTel\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"28\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"站点Logo\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"col', '0', null, '2019-08-10 00:30:31');
INSERT INTO `sys_oper_log` VALUES ('128', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.genCode()', '1', 'admin', '研发部门', '/tool/gen/genCode/fly_site', '127.0.0.1', '内网IP', '{ }', '0', null, '2019-08-10 00:32:00');
INSERT INTO `sys_oper_log` VALUES ('129', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.editSave()', '1', 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"2011\" ],\r\n  \"parentId\" : [ \"2010\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"网站管理\" ],\r\n  \"url\" : [ \"/weixin/site\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"weixin:site:view\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"#\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '0', null, '2019-08-10 00:36:07');
INSERT INTO `sys_oper_log` VALUES ('130', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.editSave()', '1', 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"2005\" ],\r\n  \"parentId\" : [ \"2010\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"粉丝管理\" ],\r\n  \"url\" : [ \"/weixin/wxuser\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"weixin:wxuser:view\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"#\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '0', null, '2019-08-10 00:37:57');
INSERT INTO `sys_oper_log` VALUES ('131', '代码生成', '6', 'com.ruoyi.generator.controller.GenController.importTableSave()', '1', 'admin', '研发部门', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\r\n  \"tables\" : [ \"fly_app_user_merge,fly_app_user\" ]\r\n}', '0', null, '2019-08-10 11:12:31');
INSERT INTO `sys_oper_log` VALUES ('132', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', '1', 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"3\" ],\r\n  \"tableName\" : [ \"fly_app_user\" ],\r\n  \"tableComment\" : [ \"网站访问用户\" ],\r\n  \"className\" : [ \"AppUser\" ],\r\n  \"functionAuthor\" : [ \"FlyCms\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"45\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"id\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"46\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"用户短网址\" ],\r\n  \"columns[1].javaType\" : [ \"String\" ],\r\n  \"columns[1].javaField\" : [ \"shortUrl\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].isRequired\" : [ \"1\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"47\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"昵称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"nickName\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"EQ\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"48\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"性别(1:男;2:女)\" ],\r\n  \"columns[3].javaType\" : [ \"Integer\" ],\r\n  \"columns[3].javaField\" : [ \"gender\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].isList\" : [ \"1\" ],\r\n  \"columns[3].isQuery\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"radio\" ],\r\n  \"columns[3].dictType\" : [ \"sys_user_sex\" ],\r\n  \"columns[4].columnId\" : [ \"49\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"所在城市\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"columns[4].javaF', '0', null, '2019-08-10 11:21:56');
INSERT INTO `sys_oper_log` VALUES ('133', '代码生成', '3', 'com.ruoyi.generator.controller.GenController.remove()', '1', 'admin', '研发部门', '/tool/gen/remove', '127.0.0.1', '内网IP', '{\r\n  \"ids\" : [ \"1\" ]\r\n}', '0', null, '2019-08-10 11:22:06');
INSERT INTO `sys_oper_log` VALUES ('134', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', '1', 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"4\" ],\r\n  \"tableName\" : [ \"fly_app_user_merge\" ],\r\n  \"tableComment\" : [ \"用户与网站关联表\" ],\r\n  \"className\" : [ \"AppUserMerge\" ],\r\n  \"functionAuthor\" : [ \"FlyCms\" ],\r\n  \"remark\" : [ \"用户与网站关联表\" ],\r\n  \"columns[0].columnId\" : [ \"61\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"id\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"62\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"用户id\" ],\r\n  \"columns[1].javaType\" : [ \"Long\" ],\r\n  \"columns[1].javaField\" : [ \"appId\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"63\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"网站ID\" ],\r\n  \"columns[2].javaType\" : [ \"Long\" ],\r\n  \"columns[2].javaField\" : [ \"siteId\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"EQ\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"tplCategory\" : [ \"crud\" ],\r\n  \"packageName\" : [ \"com.ruoyi.weixin\" ],\r\n  \"moduleName\" : [ \"weixin\" ],\r\n  \"businessName\" : [ \"appusermerge\" ],\r\n  \"functionName\" : [ \"用户与网站关联表\" ],\r\n  \"params[treeCode]\" : [ \"\" ],\r\n  \"params[treeParentCode]\" : [ \"\" ],\r\n  \"params[treeName]\" : [ \"\" ]\r\n}', '0', null, '2019-08-10 11:30:58');
INSERT INTO `sys_oper_log` VALUES ('135', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.genCode()', '1', 'admin', '研发部门', '/tool/gen/genCode/fly_app_user', '127.0.0.1', '内网IP', '{ }', '0', null, '2019-08-10 11:31:18');
INSERT INTO `sys_oper_log` VALUES ('136', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.genCode()', '1', 'admin', '研发部门', '/tool/gen/genCode/fly_app_user_merge', '127.0.0.1', '内网IP', '{ }', '0', null, '2019-08-10 11:31:26');
INSERT INTO `sys_oper_log` VALUES ('137', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', '1', 'admin', '研发部门', '/system/menu/remove/2009', '127.0.0.1', '内网IP', '{ }', '0', null, '2019-08-10 11:31:52');
INSERT INTO `sys_oper_log` VALUES ('138', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', '1', 'admin', '研发部门', '/system/menu/remove/2008', '127.0.0.1', '内网IP', '{ }', '0', null, '2019-08-10 11:31:58');
INSERT INTO `sys_oper_log` VALUES ('139', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', '1', 'admin', '研发部门', '/system/menu/remove/2009', '127.0.0.1', '内网IP', '{ }', '0', null, '2019-08-10 11:32:15');
INSERT INTO `sys_oper_log` VALUES ('140', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', '1', 'admin', '研发部门', '/system/menu/remove/2009', '127.0.0.1', '内网IP', '{ }', '0', null, '2019-08-10 11:32:30');
INSERT INTO `sys_oper_log` VALUES ('141', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', '1', 'admin', '研发部门', '/system/menu/remove/2009', '127.0.0.1', '内网IP', '{ }', '0', null, '2019-08-10 11:32:47');
INSERT INTO `sys_oper_log` VALUES ('142', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', '1', 'admin', '研发部门', '/system/menu/remove/2009', '127.0.0.1', '内网IP', '{ }', '0', null, '2019-08-10 11:32:59');
INSERT INTO `sys_oper_log` VALUES ('143', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', '1', 'admin', '研发部门', '/system/menu/remove/2009', '127.0.0.1', '内网IP', '{ }', '0', null, '2019-08-10 11:33:59');
INSERT INTO `sys_oper_log` VALUES ('144', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', '1', 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"2\" ],\r\n  \"tableName\" : [ \"fly_site\" ],\r\n  \"tableComment\" : [ \"用户站点\" ],\r\n  \"className\" : [ \"Site\" ],\r\n  \"functionAuthor\" : [ \"FlyCms\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"24\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"id\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"25\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"用户id\" ],\r\n  \"columns[1].javaType\" : [ \"Long\" ],\r\n  \"columns[1].javaField\" : [ \"userId\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].isRequired\" : [ \"1\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"26\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"公司名称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"companyName\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"LIKE\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"27\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"公司电话\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"companyTel\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"28\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"站点Logo\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"col', '0', null, '2019-08-11 23:52:58');
INSERT INTO `sys_oper_log` VALUES ('145', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', '1', 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"2\" ],\r\n  \"tableName\" : [ \"fly_site\" ],\r\n  \"tableComment\" : [ \"用户站点\" ],\r\n  \"className\" : [ \"Website\" ],\r\n  \"functionAuthor\" : [ \"FlyCms\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"24\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"id\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"25\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"用户id\" ],\r\n  \"columns[1].javaType\" : [ \"Long\" ],\r\n  \"columns[1].javaField\" : [ \"userId\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].isRequired\" : [ \"1\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"26\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"公司名称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"companyName\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"LIKE\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"27\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"公司电话\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"companyTel\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"28\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"站点Logo\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"', '0', null, '2019-08-11 23:53:30');
INSERT INTO `sys_oper_log` VALUES ('146', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.editSave()', '1', 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"2016\" ],\r\n  \"parentId\" : [ \"2010\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"用户管理\" ],\r\n  \"url\" : [ \"/weixin/appuser\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"weixin:appuser:view\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"#\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '0', null, '2019-08-11 23:55:05');
INSERT INTO `sys_oper_log` VALUES ('147', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', '1', 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"3\" ],\r\n  \"tableName\" : [ \"fly_app_user\" ],\r\n  \"tableComment\" : [ \"网站访问用户\" ],\r\n  \"className\" : [ \"AppUser\" ],\r\n  \"functionAuthor\" : [ \"FlyCms\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"45\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"id\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"46\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"用户短网址\" ],\r\n  \"columns[1].javaType\" : [ \"String\" ],\r\n  \"columns[1].javaField\" : [ \"shortUrl\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].isRequired\" : [ \"1\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"47\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"昵称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"nickName\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"EQ\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"48\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"性别(1:男;2:女)\" ],\r\n  \"columns[3].javaType\" : [ \"Integer\" ],\r\n  \"columns[3].javaField\" : [ \"gender\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].isList\" : [ \"1\" ],\r\n  \"columns[3].isQuery\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"radio\" ],\r\n  \"columns[3].dictType\" : [ \"sys_user_sex\" ],\r\n  \"columns[4].columnId\" : [ \"49\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"所在城市\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"columns[4].javaF', '0', null, '2019-08-11 23:56:23');
INSERT INTO `sys_oper_log` VALUES ('148', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', '1', 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"4\" ],\r\n  \"tableName\" : [ \"fly_app_user_merge\" ],\r\n  \"tableComment\" : [ \"用户与网站关联表\" ],\r\n  \"className\" : [ \"AppUserMerge\" ],\r\n  \"functionAuthor\" : [ \"FlyCms\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"61\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"id\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"62\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"用户id\" ],\r\n  \"columns[1].javaType\" : [ \"Long\" ],\r\n  \"columns[1].javaField\" : [ \"appId\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"63\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"网站ID\" ],\r\n  \"columns[2].javaType\" : [ \"Long\" ],\r\n  \"columns[2].javaField\" : [ \"siteId\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"EQ\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"tplCategory\" : [ \"crud\" ],\r\n  \"packageName\" : [ \"com.ruoyi.website\" ],\r\n  \"moduleName\" : [ \"website\" ],\r\n  \"businessName\" : [ \"appusermerge\" ],\r\n  \"functionName\" : [ \"用户与网站关联表\" ],\r\n  \"params[treeCode]\" : [ \"\" ],\r\n  \"params[treeParentCode]\" : [ \"\" ],\r\n  \"params[treeName]\" : [ \"\" ]\r\n}', '0', null, '2019-08-11 23:56:50');
INSERT INTO `sys_oper_log` VALUES ('149', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', '1', 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"2\" ],\r\n  \"tableName\" : [ \"fly_site\" ],\r\n  \"tableComment\" : [ \"用户站点\" ],\r\n  \"className\" : [ \"Website\" ],\r\n  \"functionAuthor\" : [ \"FlyCms\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"24\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"id\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"25\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"用户id\" ],\r\n  \"columns[1].javaType\" : [ \"Long\" ],\r\n  \"columns[1].javaField\" : [ \"userId\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].isRequired\" : [ \"1\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"26\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"公司名称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"companyName\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"LIKE\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"27\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"公司电话\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"companyTel\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"28\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"站点Logo\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"', '0', null, '2019-08-11 23:57:07');
INSERT INTO `sys_oper_log` VALUES ('150', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.genCode()', '1', 'admin', '研发部门', '/tool/gen/genCode/fly_app_user', '127.0.0.1', '内网IP', '{ }', '0', null, '2019-08-11 23:57:15');
INSERT INTO `sys_oper_log` VALUES ('151', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.genCode()', '1', 'admin', '研发部门', '/tool/gen/genCode/fly_app_user', '127.0.0.1', '内网IP', '{ }', '0', null, '2019-08-11 23:57:59');
INSERT INTO `sys_oper_log` VALUES ('152', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.genCode()', '1', 'admin', '研发部门', '/tool/gen/genCode/fly_app_user_merge', '127.0.0.1', '内网IP', '{ }', '0', null, '2019-08-11 23:58:19');
INSERT INTO `sys_oper_log` VALUES ('153', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', '1', 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"2\" ],\r\n  \"tableName\" : [ \"fly_site\" ],\r\n  \"tableComment\" : [ \"用户站点\" ],\r\n  \"className\" : [ \"Website\" ],\r\n  \"functionAuthor\" : [ \"FlyCms\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"24\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"id\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].isList\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"25\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"用户id\" ],\r\n  \"columns[1].javaType\" : [ \"Long\" ],\r\n  \"columns[1].javaField\" : [ \"userId\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].isRequired\" : [ \"1\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"26\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"公司名称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"companyName\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"LIKE\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"27\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"公司电话\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"companyTel\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"28\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"站点Logo\" ],\r\n  \"columns', '0', null, '2019-08-12 00:00:18');
INSERT INTO `sys_oper_log` VALUES ('154', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', '1', 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"3\" ],\r\n  \"tableName\" : [ \"fly_app_user\" ],\r\n  \"tableComment\" : [ \"网站访问用户\" ],\r\n  \"className\" : [ \"AppUser\" ],\r\n  \"functionAuthor\" : [ \"FlyCms\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"45\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"id\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"46\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"用户短网址\" ],\r\n  \"columns[1].javaType\" : [ \"String\" ],\r\n  \"columns[1].javaField\" : [ \"shortUrl\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].isRequired\" : [ \"1\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"47\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"昵称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"nickName\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"EQ\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"48\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"性别\" ],\r\n  \"columns[3].javaType\" : [ \"Integer\" ],\r\n  \"columns[3].javaField\" : [ \"gender\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].isList\" : [ \"1\" ],\r\n  \"columns[3].isQuery\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"radio\" ],\r\n  \"columns[3].dictType\" : [ \"sys_user_sex\" ],\r\n  \"columns[4].columnId\" : [ \"49\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"所在城市\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"columns[4].javaField\" : [', '0', null, '2019-08-12 00:01:33');
INSERT INTO `sys_oper_log` VALUES ('155', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.genCode()', '1', 'admin', '研发部门', '/tool/gen/genCode/fly_app_user', '127.0.0.1', '内网IP', '{ }', '0', null, '2019-08-12 00:01:50');
INSERT INTO `sys_oper_log` VALUES ('156', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.genCode()', '1', 'admin', '研发部门', '/tool/gen/genCode/fly_app_user_merge', '127.0.0.1', '内网IP', '{ }', '0', null, '2019-08-12 00:01:56');
INSERT INTO `sys_oper_log` VALUES ('157', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.genCode()', '1', 'admin', '研发部门', '/tool/gen/genCode/fly_site', '127.0.0.1', '内网IP', '{ }', '0', null, '2019-08-12 00:02:02');
INSERT INTO `sys_oper_log` VALUES ('158', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', '1', 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"2\" ],\r\n  \"tableName\" : [ \"fly_website\" ],\r\n  \"tableComment\" : [ \"用户站点\" ],\r\n  \"className\" : [ \"Website\" ],\r\n  \"functionAuthor\" : [ \"FlyCms\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"24\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"id\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].isList\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"25\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"用户id\" ],\r\n  \"columns[1].javaType\" : [ \"Long\" ],\r\n  \"columns[1].javaField\" : [ \"userId\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].isRequired\" : [ \"1\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"26\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"公司名称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"companyName\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"LIKE\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"27\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"公司电话\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"companyTel\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"28\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"站点Logo\" ],\r\n  \"colu', '0', null, '2019-08-12 00:03:22');
INSERT INTO `sys_oper_log` VALUES ('159', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.genCode()', '1', 'admin', '研发部门', '/tool/gen/genCode/fly_website', '127.0.0.1', '内网IP', '{ }', '0', null, '2019-08-12 00:03:33');
INSERT INTO `sys_oper_log` VALUES ('160', '字典类型', '1', 'com.ruoyi.web.controller.system.SysDictTypeController.addSave()', '1', 'admin', '研发部门', '/system/dict/add', '127.0.0.1', '内网IP', '{\r\n  \"dictName\" : [ \"网站管理\" ],\r\n  \"dictType\" : [ \"fly_website\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '0', null, '2019-08-12 14:36:08');
INSERT INTO `sys_oper_log` VALUES ('161', '字典类型', '3', 'com.ruoyi.web.controller.system.SysDictTypeController.remove()', '1', 'admin', '研发部门', '/system/dict/remove', '127.0.0.1', '内网IP', '{\r\n  \"ids\" : [ \"100\" ]\r\n}', '0', null, '2019-08-12 14:38:58');
INSERT INTO `sys_oper_log` VALUES ('162', '字典类型', '1', 'com.ruoyi.web.controller.system.SysDictTypeController.addSave()', '1', 'admin', '研发部门', '/system/dict/add', '127.0.0.1', '内网IP', '{\r\n  \"dictName\" : [ \"逻辑删除\" ],\r\n  \"dictType\" : [ \"deleted\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"逻辑删除处理\" ]\r\n}', '0', null, '2019-08-12 14:39:27');
INSERT INTO `sys_oper_log` VALUES ('163', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"正常\" ],\r\n  \"dictValue\" : [ \"0\" ],\r\n  \"dictType\" : [ \"deleted\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"1\" ],\r\n  \"listClass\" : [ \"success\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"修改为未删除\" ]\r\n}', '0', null, '2019-08-12 14:41:30');
INSERT INTO `sys_oper_log` VALUES ('164', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \" 已删除\" ],\r\n  \"dictValue\" : [ \"1\" ],\r\n  \"dictType\" : [ \"deleted\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"2\" ],\r\n  \"listClass\" : [ \"danger\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"删除处理\" ]\r\n}', '0', null, '2019-08-12 14:42:00');
INSERT INTO `sys_oper_log` VALUES ('165', '字典类型', '1', 'com.ruoyi.web.controller.system.SysDictTypeController.addSave()', '1', 'admin', '研发部门', '/system/dict/add', '127.0.0.1', '内网IP', '{\r\n  \"dictName\" : [ \"审核状态\" ],\r\n  \"dictType\" : [ \"status\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"审核状态，0未审核，1已审核，2审核未通过\" ]\r\n}', '0', null, '2019-08-12 14:44:06');
INSERT INTO `sys_oper_log` VALUES ('166', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"未审核\" ],\r\n  \"dictValue\" : [ \"0\" ],\r\n  \"dictType\" : [ \"status\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"1\" ],\r\n  \"listClass\" : [ \"danger\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"未删除\" ]\r\n}', '0', null, '2019-08-12 14:44:54');
INSERT INTO `sys_oper_log` VALUES ('167', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"已审核\" ],\r\n  \"dictValue\" : [ \"1\" ],\r\n  \"dictType\" : [ \"status\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"2\" ],\r\n  \"listClass\" : [ \"success\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"已审核状态\" ]\r\n}', '0', null, '2019-08-12 14:45:43');
INSERT INTO `sys_oper_log` VALUES ('168', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', '1', 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"2\" ],\r\n  \"tableName\" : [ \"fly_website\" ],\r\n  \"tableComment\" : [ \"用户站点\" ],\r\n  \"className\" : [ \"Website\" ],\r\n  \"functionAuthor\" : [ \"FlyCms\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"24\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"id\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].isList\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"25\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"用户id\" ],\r\n  \"columns[1].javaType\" : [ \"Long\" ],\r\n  \"columns[1].javaField\" : [ \"userId\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].isRequired\" : [ \"1\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"26\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"公司名称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"companyName\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"LIKE\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"27\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"公司电话\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"companyTel\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"28\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"站点Logo\" ],\r\n  \"colu', '0', null, '2019-08-12 14:46:16');
INSERT INTO `sys_oper_log` VALUES ('169', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"未通过\" ],\r\n  \"dictValue\" : [ \"2\" ],\r\n  \"dictType\" : [ \"status\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"3\" ],\r\n  \"listClass\" : [ \"warning\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"审核未通过\" ]\r\n}', '0', null, '2019-08-12 18:11:01');
INSERT INTO `sys_oper_log` VALUES ('170', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', '1', 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"2\" ],\r\n  \"tableName\" : [ \"fly_website\" ],\r\n  \"tableComment\" : [ \"用户站点\" ],\r\n  \"className\" : [ \"Website\" ],\r\n  \"functionAuthor\" : [ \"FlyCms\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"24\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"id\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].isList\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"25\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"用户id\" ],\r\n  \"columns[1].javaType\" : [ \"Long\" ],\r\n  \"columns[1].javaField\" : [ \"userId\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].isRequired\" : [ \"1\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"26\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"公司名称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"companyName\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"LIKE\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"27\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"公司电话\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"companyTel\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"28\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"站点Logo\" ],\r\n  \"colu', '0', null, '2019-08-12 18:11:58');
INSERT INTO `sys_oper_log` VALUES ('171', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.genCode()', '1', 'admin', '研发部门', '/tool/gen/genCode/fly_website', '127.0.0.1', '内网IP', '{ }', '0', null, '2019-08-12 18:12:31');
INSERT INTO `sys_oper_log` VALUES ('172', '用户站点', '2', 'com.ruoyi.web.controller.website.WebsiteController.editSave()', '1', 'admin', '研发部门', '/website/website/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"272843906771857408\" ],\r\n  \"userId\" : [ \"0\" ],\r\n  \"companyName\" : [ \"\" ],\r\n  \"companyTel\" : [ \"\" ],\r\n  \"siteLogo\" : [ \"timg11564851091689.jpg\" ],\r\n  \"siteName\" : [ \"捷微官网\" ],\r\n  \"sitePcTemplate\" : [ \"\" ],\r\n  \"siteMTemplate\" : [ \"wxhome\" ],\r\n  \"siteAppTemplate\" : [ \"\" ],\r\n  \"recordInformation\" : [ \"<p style=\\\"color: rgb(0, 0, 0); white-space: normal;\\\"><span lucida=\\\"\\\" courier=\\\"\\\" font-size:=\\\"\\\" white-space:=\\\"\\\" background-color:=\\\"\\\" style=\\\"color: rgb(34, 34, 34);\\\">中国·北京·朝阳区科荟前街1号院奥林佳泰大厦9层929</span></p><p style=\\\"color: rgb(0, 0, 0); white-space: normal;\\\"><span lucida=\\\"\\\" courier=\\\"\\\" font-size:=\\\"\\\" white-space:=\\\"\\\" background-color:=\\\"\\\" style=\\\"color: rgb(34, 34, 34);\\\">公司：北京囯炬信息技术有限公司</span></p><p style=\\\"color: rgb(0, 0, 0); white-space: normal;\\\"><span lucida=\\\"\\\" courier=\\\"\\\" font-size:=\\\"\\\" white-space:=\\\"\\\" background-color:=\\\"\\\" style=\\\"color: rgb(34, 34, 34);\\\">邮编：100190</span></p><p style=\\\"color: rgb(0, 0, 0); white-space: normal;\\\"><span lucida=\\\"\\\" courier=\\\"\\\" font-size:=\\\"\\\" white-space:=\\\"\\\" background-color:=\\\"\\\" style=\\\"color: rgb(34, 34, 34);\\\">电话：010-64808099</span></p><p style=\\\"color: rgb(0, 0, 0); white-space: normal;\\\"><span lucida=\\\"\\\" courier=\\\"\\\" font-size:=\\\"\\\" white-space:=\\\"\\\" background-color:=\\\"\\\" style=\\\"color: rgb(34, 34, 34);\\\">邮箱：jeecg@sina.com</span></p><p><br/></p>\" ],\r\n  \"longitude\" : [ \"116.381319\" ],\r\n  \"latitude\" : [ \"40.015457\" ],\r\n  \"hdurl\" : [ \"http://www.h5huodong.com/cms/index.do?mainId=ff808081661e74bb01661f12de5e03d6\" ],\r\n  \"shortUrl\" : [ \"https://w.url.cn/s/AoUtL1H\" ],\r\n  \"shareFriendTitle\" : [ \"\" ],\r\n  \"shareFriendDesc\" : [ \"\" ],\r\n  \"shareFriendCircle\" : [ \"\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"deleted\" : [ \"0\" ]\r\n}', '0', null, '2019-08-12 18:25:06');
INSERT INTO `sys_oper_log` VALUES ('173', '角色管理', '2', 'com.ruoyi.web.controller.system.SysRoleController.editSave()', '1', 'admin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"2\" ],\r\n  \"roleName\" : [ \"普通角色\" ],\r\n  \"roleKey\" : [ \"common\" ],\r\n  \"roleSort\" : [ \"2\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"普通角色\" ],\r\n  \"menuIds\" : [ \"1,100,1000,107,1035,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020\" ]\r\n}', '0', null, '2019-08-12 20:47:22');
INSERT INTO `sys_oper_log` VALUES ('174', '角色管理', '2', 'com.ruoyi.web.controller.system.SysRoleController.authDataScopeSave()', '1', 'admin', '研发部门', '/system/role/authDataScope', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"2\" ],\r\n  \"roleName\" : [ \"普通角色\" ],\r\n  \"roleKey\" : [ \"common\" ],\r\n  \"dataScope\" : [ \"2\" ],\r\n  \"deptIds\" : [ \"100,101,105,106,107\" ]\r\n}', '0', null, '2019-08-12 21:07:06');
INSERT INTO `sys_oper_log` VALUES ('175', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.editSave()', '1', 'admin', '研发部门', '/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"2\" ],\r\n  \"deptId\" : [ \"105\" ],\r\n  \"userName\" : [ \"若依\" ],\r\n  \"dept.deptName\" : [ \"测试部门\" ],\r\n  \"phonenumber\" : [ \"15666666666\" ],\r\n  \"email\" : [ \"ry@qq.com\" ],\r\n  \"loginName\" : [ \"ry\" ],\r\n  \"sex\" : [ \"1\" ],\r\n  \"role\" : [ \"1\", \"2\" ],\r\n  \"remark\" : [ \"测试员\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"1,2\" ],\r\n  \"postIds\" : [ \"2\" ]\r\n}', '0', null, '2019-08-12 21:08:51');
INSERT INTO `sys_oper_log` VALUES ('176', '角色管理', '1', 'com.ruoyi.web.controller.system.SysRoleController.addSave()', '1', 'admin', '研发部门', '/system/role/add', '127.0.0.1', '内网IP', '{\r\n  \"roleName\" : [ \"企业用户\" ],\r\n  \"roleKey\" : [ \"company\" ],\r\n  \"roleSort\" : [ \"3\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"企业用户组\" ],\r\n  \"menuIds\" : [ \"1,103,1016,1017,1018,1019,104,1020,1021,1022,1023,1024,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020\" ]\r\n}', '0', null, '2019-08-12 21:23:56');
INSERT INTO `sys_oper_log` VALUES ('177', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.editSave()', '1', 'admin', '研发部门', '/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"2\" ],\r\n  \"deptId\" : [ \"105\" ],\r\n  \"userName\" : [ \"若依\" ],\r\n  \"dept.deptName\" : [ \"测试部门\" ],\r\n  \"phonenumber\" : [ \"15666666666\" ],\r\n  \"email\" : [ \"ry@qq.com\" ],\r\n  \"loginName\" : [ \"ry\" ],\r\n  \"sex\" : [ \"1\" ],\r\n  \"role\" : [ \"2\", \"100\" ],\r\n  \"remark\" : [ \"测试员\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"2,100\" ],\r\n  \"postIds\" : [ \"2,3\" ]\r\n}', '0', null, '2019-08-12 21:24:25');
INSERT INTO `sys_oper_log` VALUES ('178', '用户站点', '1', 'com.ruoyi.web.controller.website.WebsiteController.addSave()', '1', 'admin', '研发部门', '/website/website/add', '127.0.0.1', '内网IP', '{\r\n  \"companyName\" : [ \"sdasdasdsa\" ],\r\n  \"companyTel\" : [ \"234234432\" ],\r\n  \"siteLogo\" : [ \"4345345\" ],\r\n  \"siteName\" : [ \"345345345\" ],\r\n  \"sitePcTemplate\" : [ \"345345\" ],\r\n  \"siteMTemplate\" : [ \"345345\" ],\r\n  \"siteAppTemplate\" : [ \"345345\" ],\r\n  \"recordInformation\" : [ \"345345\" ],\r\n  \"longitude\" : [ \"34534\" ],\r\n  \"latitude\" : [ \"34534\" ],\r\n  \"hdurl\" : [ \"4534\" ],\r\n  \"shortUrl\" : [ \"345345\" ],\r\n  \"shareFriendTitle\" : [ \"34534\" ],\r\n  \"shareFriendDesc\" : [ \"35345\" ],\r\n  \"shareFriendCircle\" : [ \"345345\" ],\r\n  \"createDate\" : [ \"2019-07-30\" ],\r\n  \"status\" : [ \"1\" ]\r\n}', '0', null, '2019-08-13 00:34:37');

-- ----------------------------
-- Table structure for `weixin_company`
-- ----------------------------
DROP TABLE IF EXISTS `weixin_company`;
CREATE TABLE `weixin_company` (
  `id` varchar(32) NOT NULL,
  `jwid` varchar(64) NOT NULL COMMENT '公众号',
  `name` varchar(64) NOT NULL COMMENT '名称',
  `application_type` varchar(10) DEFAULT NULL COMMENT '应用类型',
  `qrcodeimg` varchar(255) DEFAULT NULL COMMENT '微信二维码图片',
  `user_id` bigint(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `weixin_number` varchar(64) DEFAULT NULL COMMENT '微信号',
  `weixin_appid` varchar(50) DEFAULT NULL COMMENT '微信AppId',
  `weixin_appsecret` varchar(255) DEFAULT NULL COMMENT '微信AppSecret',
  `account_type` varchar(255) DEFAULT NULL COMMENT '公众号类型',
  `auth_status` varchar(255) DEFAULT NULL COMMENT '是否认证',
  `access_token` varchar(2000) DEFAULT NULL COMMENT 'Access_Token',
  `token_gettime` datetime DEFAULT NULL COMMENT 'token获取的时间',
  `apiticket` varchar(255) DEFAULT NULL COMMENT 'api凭证',
  `apiticket_gettime` datetime DEFAULT NULL COMMENT 'apiticket获取时间',
  `jsapiticket` varchar(255) DEFAULT NULL COMMENT 'jsapiticket',
  `jsapiticket_gettime` datetime DEFAULT NULL COMMENT 'jsapiticket获取时间',
  `auth_type` varchar(10) DEFAULT NULL COMMENT '类型：1手动授权，2扫码授权',
  `business_info` varchar(5000) DEFAULT NULL COMMENT '功能的开通状况：1代表已开通',
  `func_info` varchar(5000) DEFAULT NULL COMMENT '公众号授权给开发者的权限集',
  `nick_name` varchar(200) DEFAULT NULL COMMENT '授权方昵称',
  `headimgurl` varchar(1000) DEFAULT NULL COMMENT '授权方头像',
  `authorization_info` varchar(5000) DEFAULT NULL COMMENT '授权信息',
  `authorizer_refresh_token` varchar(500) DEFAULT NULL COMMENT '刷新token',
  `token` varchar(32) DEFAULT NULL COMMENT '令牌',
  `authorization_status` varchar(10) DEFAULT NULL COMMENT '授权状态（1正常，2已取消）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_jwid` (`jwid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统公众号表';

-- ----------------------------
-- Records of weixin_company
-- ----------------------------
INSERT INTO `weixin_company` VALUES ('ff80808151f1628c0151f16470cb0006', 'gh_f268aa85d1c7', '捷微活动管家', '', '6669fede4e5047229b2f57c020eee13f.jpg', null, null, 'guojusoft', 'wxc60a4d9cbac8092d', '?', '1', '1', 'oQwQRFwLMBfm_y2JUaEPKUhEgjvDOs74LAAv_kYyGwWGZixMqHyGSSiCFAQCC', '2019-07-12 18:30:01', 'IpK_1T69hDhZkLQTlwsAXyjkZaTEYbX_kE6IkQ8b5WYOjIwZ4krIfp2iANwfePSHRZ2dY0ccPrEg62U_P3vcHg', '2019-07-12 18:30:01', 'kgt8ON7yVITDhtdwci0qeeC2RWfOAQ1Sc_HMdjrzSlrhTCHlT3OE-6tp0NFL378JxIAuJhAjnQ2yDS_yeAiwQw', '2019-07-12 18:30:01', '', null, null, null, null, null, null, 'jeewx', '1');

-- ----------------------------
-- Table structure for `weixin_user`
-- ----------------------------
DROP TABLE IF EXISTS `weixin_user`;
CREATE TABLE `weixin_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id 算法自生成',
  `short_url` varchar(20) NOT NULL,
  `nick_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '昵称',
  `gender` tinyint(1) DEFAULT NULL COMMENT '性别(1:男;2:女)',
  `city` varchar(255) DEFAULT NULL COMMENT '所在城市',
  `province` varchar(255) DEFAULT NULL COMMENT '所在省份',
  `country` varchar(255) DEFAULT NULL COMMENT '所在国家',
  `avatar_url` varchar(255) DEFAULT NULL COMMENT '微信头像',
  `open_id` varchar(255) DEFAULT NULL COMMENT 'open_id',
  `union_id` varchar(255) DEFAULT NULL COMMENT 'union_id',
  `phone` varchar(255) DEFAULT NULL COMMENT '绑定手机号',
  `tagid_list` varchar(255) DEFAULT NULL COMMENT '用户被打上的标签ID列表',
  `platform` int(1) DEFAULT NULL COMMENT '用户平台(微信：1；百度：2；支付宝：3)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `status` int(1) DEFAULT NULL COMMENT '用户状态（0：无效用户;1：有效用户）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=342863254038511617 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of weixin_user
-- ----------------------------
INSERT INTO `weixin_user` VALUES ('342833898561994752', 'MrYN7b', '飞舞九天', '1', 'Changping', 'Beijing', 'China', 'https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83eq68R7EyAJQwmM6iaVNardaXO9Vs2XTBnibQiaib9zR8Q7ic0YEBTVorGuEMKINtF7hQBVxYz1rHreibRqw/132', 'oJlfI5RXk8bB4A3GrU3cCrS8tUMw', 'oBODzjtEoKonsskDc0BPQ6Y8krCI', null, null, '1', '2019-06-30 22:20:37', null, '0');
INSERT INTO `weixin_user` VALUES ('342863254038511616', '2qINZn', '浅唱『微笑』', '1', '', 'Beijing', 'China', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLAwLIfvLKQZqwVhzNHD8rgiaIb3SgiaEswanT1BqbicvpZiba1Z1ErCpQIaD1fAIbZlhYZy2dOyRMFDA/132', 'oJlfI5Z6wsj5CQjRRQDPebka2Z0g', 'oBODzjl1CTHPv2C9E4ZpRoC90bCk', null, null, '1', '2019-07-01 00:17:15', null, '0');
