 package com.flycms.modules.news.controller;


 import com.flycms.common.controller.BaseController;
 import com.flycms.common.exception.ApiAssert;
 import com.flycms.common.utils.result.Result;
 import com.flycms.common.validator.Sort;
 import com.flycms.modules.company.entity.Company;
 import com.flycms.modules.news.entity.News;
 import com.flycms.modules.news.entity.NewsDO;
 import com.flycms.modules.shiro.ShiroUtils;
 import com.flycms.modules.site.entity.SiteColumn;
 import net.sf.json.JSONArray;
 import org.apache.commons.lang.math.NumberUtils;
 import org.apache.shiro.authz.annotation.RequiresPermissions;
 import org.springframework.stereotype.Controller;
 import org.springframework.ui.Model;
 import org.springframework.util.StringUtils;
 import org.springframework.web.bind.annotation.*;

 import java.util.Collections;
 import java.util.List;

 /**
  * Biz-Boot, All rights reserved
  * 版权：企业之家网 -- 企业建站管理系统<br/>
  * 开发公司：97560.com<br/>
  *
 * 图文、新闻
 * @author 孙开飞
 */
@Controller
@RequestMapping("/admin/news")
public class NewsAdminController extends BaseController {
	 // ///////////////////////////////
	 // /////       增加       ////////
	 // ///////////////////////////////

	 // ///////////////////////////////
	 // /////        刪除      ////////
	 // ///////////////////////////////

	 // ///////////////////////////////
	 // /////        修改      ////////
	 // ///////////////////////////////



	 // ///////////////////////////////
	 // /////        查詢      ////////
	 // ///////////////////////////////

     //管理员角色列表页面
     @RequiresPermissions("nwes:list:view")
     @GetMapping("/news_list{url.suffix}")
     public String newsList()
     {
         return"system/admin/news/list_news";
     }

     //管理员角色列表json翻页数据
     @RequiresPermissions("nwes:list:view")
     @GetMapping("/news_listData{url.suffix}")
     @ResponseBody
     public Object newslistData(News news,
                                     @RequestParam(value = "page",defaultValue = "1") Integer page,
                                     @RequestParam(value = "limit",defaultValue = "10") Integer limit,
                                     @Sort(accepts = {"add_time", "id"}) @RequestParam(defaultValue = "id") String sort,
                                     @RequestParam(defaultValue = "desc") String order)
     {
         return newsService.selectAdminNewsListLayPager(news, page, limit, sort, order);
     }
}