package com.flycms.modules.visit.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 14:21 2019/11/18
 */
@Setter
@Getter
public class Visit implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Long siteId;
    private Long userId;
    private String currentUrl;
    private String sourceReferrer;
    private String sourceDomain;
    private String visitIp;
    private String visitCookie;
    private String visitUserAgent;
    private String visitBrowser;
    private String visitOs;
    private String visitDevice;
    private String visitCountry;
    private String visitArea;
    private Date addTime;             //添加时间
}
