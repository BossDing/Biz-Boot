package com.flycms.modules.help.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:04 2019/8/17
 */
@Setter
@Getter
public class Help implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Long catId;
    private Long siteId;
    private String title;
    private String content;
    private Integer sortOrder;
    private Date createTime;
    private Integer deleted;
}
