package com.flycms.modules.help.service.impl;

import com.flycms.common.pager.Pager;
import com.flycms.common.utils.mark.SnowFlakeUtils;
import com.flycms.common.utils.result.Result;
import com.flycms.modules.help.dao.HelpDao;
import com.flycms.modules.help.entity.Help;
import com.flycms.modules.help.service.HelpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @Description: 帮助服务
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:53 2019/8/17
 */
@Service
public class HelpServiceImpl implements HelpService {
    @Autowired
    private HelpDao helpDao;
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    @Override
    @Transactional
    public Object addHelp(Help help){
        if(this.checkHelpByTitle(help.getSiteId(),help.getTitle(),null)){
            return Result.failure("该帮助标题已存在");
        }
        help.setId(SnowFlakeUtils.nextId());
        help.setCreateTime(new Date());
        return helpDao.save(help);
    }
    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 按id删除帮助信息
     *
     * @param id
     * @return
     */
    public int deleteById(Long id){
        return helpDao.deleteById(id);
    }
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////

    /**
     * 更新帮助信息
     *
     * @param help
     * @return
     */
    public Object updateHelp(Help help){
        if(this.checkHelpByTitle(help.getSiteId(),help.getTitle(),help.getId())){
            return Result.failure("该帮助标题已存在");
        }
        return helpDao.update(help);
    }


    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    /**
     * 按网站id查询标题是否存在
     *
     * @param siteId
     *         网站id
     * @param title
     *         帮助信息标题
     * @param id
     *         需要排除id
     * @return
     */
    public boolean checkHelpByTitle(Long siteId, String title, Long id){
        int totalCount = helpDao.checkHelpByTitle(siteId,title,id);
        return totalCount > 0 ? true : false;
    }

    /**
     * 按id查询友情链接
     *
     * @param id
     * @return
     */
    public Help findById(Long id){
        return helpDao.findById(id);
    }

    /**
     * 查询网站所属帮助信息
     *
     * @param siteid
     *         所输网站id
     * @param title
     *         帮助内容标题
     * @param page
     *         当前页数
     * @param pageSize
     *         每页显示数量
     * @param sort
     *         指定排序字段
     * @param order
     *         排序方式
     * @return
     */
    public Object queryHelpPager(Long siteid, String title, Integer page, Integer pageSize, String sort, String order) {
        StringBuffer whereStr = new StringBuffer(" 1 = 1");
        if (!StringUtils.isEmpty(siteid) && siteid > 0l) {
            whereStr.append(" and siteid = #{entity.siteid}");
        }
        if (!StringUtils.isEmpty(title)) {
            whereStr.append(" and title like concat('%',#{entity.title},'%')");
        }
        whereStr.append(" and deleted = 1");

        Pager<Help> pager = new Pager(page, pageSize);
        //排序设置
        if (!StringUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        //查询条件
        Help help = new Help();
        help.setSiteId(siteid);
        help.setTitle(title);
        pager.setEntity(help);
        pager.setWhereStr(whereStr.toString());
        pager.setList(helpDao.queryList(pager));
        pager.setTotal(helpDao.queryTotal(pager));
        return pager;
    }
}
