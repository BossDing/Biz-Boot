package com.flycms.modules.links.service.impl;

import com.flycms.common.pager.Pager;
import com.flycms.common.utils.mark.SnowFlakeUtils;
import com.flycms.common.utils.result.LayResult;
import com.flycms.common.utils.result.Result;
import com.flycms.modules.company.entity.Company;
import com.flycms.modules.company.service.CompanyService;
import com.flycms.modules.links.dao.LinksDao;
import com.flycms.modules.links.entity.Links;
import com.flycms.modules.links.service.LinksService;
import com.flycms.modules.shiro.ShiroUtils;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:53 2019/8/17
 */
@Service
public class LinksServiceImpl implements LinksService {
    @Autowired
    private LinksDao linksDao;

    @Autowired
    private CompanyService companyService;

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    /**
     * 添加友情链接
     *
     * @param links
     * @return
     */
    @Override
    @Transactional
    public Object addLink(Links links){
        if(this.checkLinksByLinkName(links.getSiteId(),links.getLinkName(),null)){
            return Result.failure("该节点下菜单名称已存在");
        }
        links.setId(SnowFlakeUtils.nextId());
        links.setCreateTime(new Date());
        return linksDao.save(links);
    }
    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 按id删除友情链接
     *
     * @param id
     * @return
     */
    public int deleteById(Long id){
        return linksDao.deleteById(id);
    }

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////

    /**
     * 更新网站友情链接
     *
     * @param links
     * @return
     */
    @Override
    @Transactional
    public Object updateLink(Links links){
        if(this.checkLinksByLinkName(links.getSiteId(),links.getLinkName(),links.getId())){
            return Result.failure("该节点下菜单名称已存在");
        }
        return linksDao.update(links);
    }


    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 按网站id查询该网站下是否已有同名网站
     *
     * @param siteId
     *         网站id
     * @param linkName
     *         友情链接网站名称
     * @param id
     *         需要排除id
     * @return
     */
    public boolean checkLinksByLinkName(Long siteId, String linkName, Long id) {
        int totalCount = linksDao.checkLinksByLinkName(siteId,linkName,id);
        return totalCount > 0 ? true : false;
    }

    /**
     * 按id查询友情链接
     *
     * @param id
     * @return
     */
    public Links findById(Long id){
        return linksDao.findById(id);
    }

    /**
     * 网站友情链接翻页列表
     *
     * @param links
     *         根据分类名查询
     * @param page
     * @param pageSize
     * @param sort
     * @param order
     * @return
     */
    public Object selectLinksLayListPager(Links links, Integer page, Integer pageSize, String sort, String order) {
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        MapperFacade mapper = mapperFactory.getMapperFacade();
        StringBuffer whereStr = new StringBuffer(" 1 = 1");
        if (!StringUtils.isEmpty(links.getSiteId()) && links.getSiteId() > 0l) {
            whereStr.append(" and site_id = #{entity.siteId}");
        }
        if (!StringUtils.isEmpty(links.getLinkName())) {
            whereStr.append(" and link_name like concat('%',#{entity.linkName},'%')");
        }
        whereStr.append(" and deleted = 1");

        Pager<Links> pager = new Pager(page, pageSize);
        //排序设置
        if (!StringUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        //查询条件
        Links entity = new Links();
        entity.setSiteId(links.getSiteId());
        entity.setLinkName(links.getLinkName());
        pager.setEntity(entity);
        pager.setWhereStr(whereStr.toString());
        return LayResult.success(0, "true", linksDao.queryTotal(pager), linksDao.queryList(pager));
    }

    /**
     * 查询网站所属友情链接信息
     *
     * @param siteId
     *         所输网站id
     * @param linkType
     *         链接类型
     * @param page
     *         当前页数
     * @param pageSize
     *         每页显示数量
     * @param sort
     *         指定排序字段
     * @param order
     *         排序方式
     * @return
     */
    public Pager<Links> queryLinksPager(Long siteId, int linkType, Integer page, Integer pageSize, String sort, String order) {
        StringBuffer whereStr = new StringBuffer(" 1 = 1");
        if (!StringUtils.isEmpty(siteId) && siteId > 0l) {
            whereStr.append(" and site_id = #{entity.siteId}");
        }
        if (!StringUtils.isEmpty(linkType)) {
            whereStr.append(" and link_type like concat('%',#{entity.linkType},'%')");
        }
        whereStr.append(" and deleted = 1");

        Pager<Links> pager = new Pager(page, pageSize);
        //排序设置
        if (!StringUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        //查询条件
        Links links = new Links();
        links.setSiteId(siteId);
        links.setLinkType(linkType);
        pager.setEntity(links);
        pager.setWhereStr(whereStr.toString());
        pager.setList(linksDao.queryList(pager));
        pager.setTotal(linksDao.queryTotal(pager));
        return pager;
    }
}
