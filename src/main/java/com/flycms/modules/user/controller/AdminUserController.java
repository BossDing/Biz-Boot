package com.flycms.modules.user.controller;

import com.flycms.common.controller.BaseController;
import com.flycms.common.utils.result.Result;
import com.flycms.common.validator.Sort;
import com.flycms.modules.shiro.ShiroUtils;
import com.flycms.modules.user.entity.*;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 用户管理后台视图
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:52 2019/8/22
 */
@Controller
@RequestMapping("/admin/user")
public class AdminUserController extends BaseController {

    @GetMapping("/add_admin{url.suffix}")
    public String addUser(Model model)
    {
        List<AdminRole> roles= adminRoleService.selectRoleAll();
        model.addAttribute("roles",roles);
        return "system/admin/user/add_admin";
    }

    @PostMapping("/add_admin{url.suffix}")
    @ResponseBody
    public Object addUser(Admin admin){
        if(admin.getRoleId()==null || admin.getRoleId().length==0){
            return Result.failure("管理员角色未选择");
        }
        if (StringUtils.isBlank(admin.getPassword())) {
            return Result.failure("密码不能为空");
        }
        return adminService.addAdmin(admin);
    }

    //管理员信息编辑页面
    @GetMapping("/admin_edit{url.suffix}")
    public String editAdmin(@RequestParam(value = "id", required = false) String id,Model model)
    {
        if (!NumberUtils.isNumber(id)) {
            return "redirect:/404.do";
        }
        Admin user=adminService.findById(Long.parseLong(id));
        if(user==null){
            return "redirect:/404.do";
        }
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        MapperFacade mapper = mapperFactory.getMapperFacade();
        AdminVo admin = mapper.map(user, AdminVo.class);
        model.addAttribute("admin",admin);
        return "system/admin/user/edit_admin";
    }


    @GetMapping("/admin_info{url.suffix}")
    public String adminInfo(@RequestParam(value = "name", required = false) String name,Model model)
    {
        Admin user= adminService.findById(ShiroUtils.getLoginUser().getId());
        List<AdminRole> roles= adminRoleService.selectRoleAll();
        String roleIds=adminRoleService.selectUserRolesByUserId(ShiroUtils.getLoginUser().getId());
        model.addAttribute("roleIds",roleIds);
        model.addAttribute("user",user);
        model.addAttribute("roles",roles);
        return "system/admin/user/info";
    }

    @GetMapping("/password{url.suffix}")
    public String password()
    {
        return "system/admin/user/password";
    }

    @PostMapping("/password{url.suffix}")
    @ResponseBody
    public Object password(@RequestParam(value = "oldPassword", required = false)String oldPassword,
                           @RequestParam(value = "password", required = false)String password,
                           @RequestParam(value = "repassword", required = false)String repassword) {
        if (StringUtils.isBlank(oldPassword)) {
            return Result.failure("原来密码不能为空");
        } else if (oldPassword.length() < 6 && oldPassword.length() >= 32) {
            return Result.failure("密码最少6个字符，最多32个字符");
        }
        if (StringUtils.isBlank(password)) {
            return Result.failure("新密码不能为空");
        } else if (password.length() < 6 && password.length() >= 32) {
            return Result.failure("密码最少6个字符，最多32个字符");
        }
        if (!repassword.equals(password)) {
            return Result.failure("两次密码必须一样");
        }
        return adminService.updatePassword(oldPassword,password);
    }

    @GetMapping("/admin_list{url.suffix}")
    public String adminList()
    {
        return "system/admin/user/list_admin";
    }

    @GetMapping("/admin_listData{url.suffix}")
    @ResponseBody
    public Object adminList(@RequestParam(value = "name", required = false)String name,
                           @RequestParam(value = "page",defaultValue = "1") Integer page,
                           @RequestParam(value = "limit",defaultValue = "10") Integer limit,
                           @Sort(accepts = {"add_time", "id"}) @RequestParam(defaultValue = "add_time") String sort,
                           @RequestParam(defaultValue = "desc") String order) {

        return adminService.selectUserListPager(name, page, limit, sort, order);
    }


    @GetMapping("/user_list{url.suffix}")
    public String userList()
    {
        return "system/admin/user/list_user";
    }

    @GetMapping("/user_listData{url.suffix}")
    @ResponseBody
    public Object userList(@RequestParam(value = "name", required = false)String name,
                           @RequestParam(value = "page",defaultValue = "1") Integer page,
                           @RequestParam(value = "limit",defaultValue = "10") Integer limit,
                           @Sort(accepts = {"add_time", "id"}) @RequestParam(defaultValue = "add_time") String sort,
                           @RequestParam(defaultValue = "desc") String order) {

        return userService.selectUserListPager(name, page, limit, sort, order);
    }

    @GetMapping("/user_info{url.suffix}")
    public String userInfo(@RequestParam(value = "name", required = false)String name,Model model)
    {
        User user=userService.findById(ShiroUtils.getLoginUser().getId());
        List<UserRole> roles= userRoleService.selectRoleAll();
        String roleIds=userRoleService.selectUserRolesByUserId(ShiroUtils.getLoginUser().getId());
        model.addAttribute("roleIds",roleIds);
        model.addAttribute("user",user);
        model.addAttribute("roles",roles);
        return "system/member/user/info";
    }

}