package com.flycms.modules.user.controller;

import com.flycms.common.controller.BaseController;
import com.flycms.common.exception.PageAssert;
import com.flycms.common.utils.result.Result;
import com.flycms.common.validator.Sort;
import com.flycms.modules.user.entity.AdminRole;
import com.flycms.modules.user.entity.AdminRoleDO;
import com.flycms.modules.user.entity.UserRole;
import com.flycms.modules.user.entity.UserRoleDO;
import com.flycms.modules.user.service.AdminRoleService;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 管理员权限组管理操作
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 1:26 2019/8/24
 */
@Controller
@RequestMapping("/admin/role")
public class AdminRoleController extends BaseController {
    @Autowired
    private AdminRoleService adminRoleService;

    @GetMapping("/add_admin_role{url.suffix}")
    public String addAdminRole(Model model) {
        return "system/admin/popup/add_admin_role";
    }

    @PostMapping("/add_admin_role{url.suffix}")
    @ResponseBody
    public Object addAdminRole(AdminRoleDO adminRoleDO){
        if(StringUtils.isEmpty(adminRoleDO.getRoleName())){
            return Result.failure("该角色名称已存在");
        }
        if(StringUtils.isEmpty(adminRoleDO.getRoleKey())){
            return Result.failure("该角色key已存在");
        }
        AdminRole adminRole=new AdminRole();
        adminRole.setRoleName(adminRoleDO.getRoleName());
        adminRole.setRoleKey(adminRoleDO.getRoleKey());
        adminRole.setRemark(adminRoleDO.getRemark());
        adminRole.setStatus(adminRoleDO.getStatus());
        adminRole.setSortOrder(adminRoleDO.getSortOrder());
        return adminRoleService.addAdminRole(adminRole);
    }

    //批量删除角色
    @PostMapping("/del_admin_role{url.suffix}")
    @ResponseBody
    public Object delAdminRole(String ids){
        if(StringUtils.isEmpty(ids)){
            return Result.failure("角色id为空或者不存在");
        }
        return adminRoleService.deleteRoleByIds(ids);
    }

    //更新角色权限页面
    @RequestMapping("/update_admin_role${url.suffix}")
    public String updateAdminRole(@RequestParam(value = "id", required = false) String id,Model model){
        if (!NumberUtils.isNumber(id)) {
            return "redirect:/404.do";
        }
        AdminRole role=adminRoleService.findById(Long.parseLong(id));
        model.addAttribute("role",role);
        return "system/admin/popup/update_admin_role";
    }

    //修改管理员角色
    @PostMapping("/update_admin_role${url.suffix}")
    @ResponseBody
    public Object updateAdminRole(AdminRoleDO adminRoleD){
        if(StringUtils.isEmpty(adminRoleD.getRoleName())){
            return Result.failure("该角色名称已存在");
        }
        if(StringUtils.isEmpty(adminRoleD.getRoleKey())){
            return Result.failure("该角色key已存在");
        }
        AdminRole adminRole=new AdminRole();
        adminRole.setId(adminRoleD.getId());
        adminRole.setRoleName(adminRoleD.getRoleName());
        adminRole.setRoleKey(adminRoleD.getRoleKey());
        adminRole.setRemark(adminRoleD.getRemark());
        adminRole.setStatus(adminRoleD.getStatus());
        adminRole.setSortOrder(adminRoleD.getSortOrder());
        return adminRoleService.updateAdminRole(adminRole);
    }

    //管理员角色列表页面
    @RequiresPermissions("system:menu:view")
    @GetMapping("/admin_role_list{url.suffix}")
    public String adminRoleList()
    {
        return"system/admin/user/admin_role";
    }

    //管理员角色列表json翻页数据
    @RequiresPermissions("system:menu:list")
    @GetMapping("/admin_role_listData{url.suffix}")
    @ResponseBody
    public Object adminRolelistData(AdminRole adminRole,
                           @RequestParam(value = "page",defaultValue = "1") Integer page,
                           @RequestParam(value = "limit",defaultValue = "10") Integer limit,
                           @Sort(accepts = {"add_time", "id","sort_order"}) @RequestParam(defaultValue = "sort_order") String sort,
                           @RequestParam(defaultValue = "desc") String order)
    {
        return adminRoleService.selectRoleListPager(adminRole, page, limit, sort, order);
    }


    @GetMapping("/add_user_role{url.suffix}")
    public String addUserRole(Model model) {
        return "system/admin/popup/add_user_role";
    }

    @PostMapping("/add_user_role{url.suffix}")
    @ResponseBody
    public Object addUserRole(UserRoleDO userRoleDO){
        if(StringUtils.isEmpty(userRoleDO.getRoleName())){
            return Result.failure("该角色名称已存在");
        }
        if(StringUtils.isEmpty(userRoleDO.getRoleKey())){
            return Result.failure("该角色key已存在");
        }
        UserRole userRole=new UserRole();
        userRole.setRoleName(userRoleDO.getRoleName());
        userRole.setRoleKey(userRoleDO.getRoleKey());
        userRole.setRemark(userRoleDO.getRemark());
        userRole.setStatus(userRoleDO.getStatus());
        userRole.setSortOrder(userRoleDO.getSortOrder());
        return userRoleService.addUserRole(userRole);
    }

    //批量删除角色
    @PostMapping("/del_user_role{url.suffix}")
    @ResponseBody
    public Object delUserRole(String ids){
        if(StringUtils.isEmpty(ids)){
            return Result.failure("角色id为空或者不存在");
        }
        return userRoleService.deleteRoleByIds(ids);
    }

    //更新角色权限页面
    @RequestMapping("/update_user_role${url.suffix}")
    public String updateUserRole(@RequestParam(value = "id", required = false) String id,Model model){
        if (!NumberUtils.isNumber(id)) {
            return "redirect:/404.do";
        }
        UserRole role=userRoleService.findById(Long.parseLong(id));
        model.addAttribute("role",role);
        return "system/admin/popup/update_user_role";
    }

    @PostMapping("/update_user_role${url.suffix}")
    @ResponseBody
    public Object updateUserRole(UserRoleDO userRoleDO){
        if(StringUtils.isEmpty(userRoleDO.getRoleName())){
            return Result.failure("该角色名称已存在");
        }
        if(StringUtils.isEmpty(userRoleDO.getRoleKey())){
            return Result.failure("该角色key已存在");
        }
        UserRole userRole=new UserRole();
        userRole.setId(userRoleDO.getId());
        userRole.setRoleName(userRoleDO.getRoleName());
        userRole.setRoleKey(userRoleDO.getRoleKey());
        userRole.setRemark(userRoleDO.getRemark());
        userRole.setStatus(userRoleDO.getStatus());
        userRole.setSortOrder(userRoleDO.getSortOrder());
        return userRoleService.updateUserRole(userRole);
    }

    //用户权限列表
    @RequiresPermissions("system:menu:view")
    @GetMapping("/user_role_list{url.suffix}")
    public String userRoleList()
    {
        return"system/admin/user/user_role";
    }

    //用户json翻页数据
    @RequiresPermissions("system:menu:list")
    @GetMapping("/user_role_listData{url.suffix}")
    @ResponseBody
    public Object userRolelistData(UserRole role,
                           @RequestParam(value = "page",defaultValue = "1") Integer page,
                           @RequestParam(value = "limit",defaultValue = "10") Integer limit,
                           @Sort(accepts = {"add_time", "id", "sort_order"}) @RequestParam(defaultValue = "sort_order") String sort,
                           @RequestParam(defaultValue = "desc") String order)
    {
        return userRoleService.selectRoleListPager(role, page, limit, sort, order);
    }



}
