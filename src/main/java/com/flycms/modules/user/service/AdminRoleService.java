package com.flycms.modules.user.service;


import com.flycms.common.utils.result.Result;
import com.flycms.modules.user.entity.AdminRole;
import com.flycms.modules.user.entity.AdminRoleMenu;
import com.flycms.modules.user.entity.AdminRoleMerge;

import java.util.List;
import java.util.Set;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 角色业务层
 * 
 * @author 孙开飞
 */
public interface AdminRoleService {

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    /**
     * 新增保存角色信息
     *
     * @param adminRole 角色信息
     * @return 结果
     */
    public int addAdminRole(AdminRole adminRole);

    /**
     * 批量选择授权用户角色
     *
     * @param roleId 角色ID
     * @param userIds 需要删除的用户数据ID
     * @return 结果
     */
    public int insertAuthUsers(Long roleId, String userIds);

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 取消授权用户角色
     *
     * @param adminRoleMerge 用户和角色关联信息
     * @return 结果
     */
    public int deleteAuthUser(AdminRoleMerge adminRoleMerge);

    /**
     * 批量取消授权用户角色
     *
     * @param roleId 角色ID
     * @param userIds 需要删除的用户数据ID
     * @return 结果
     */
    public int deleteAuthUsers(Long roleId, String userIds);

    /**
     * 通过角色ID删除角色
     *
     * @param roleId 角色ID
     * @return 结果
     */
    public boolean deleteRoleById(Long roleId);

    /**
     * 批量删除角色用户信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     * @throws Exception 异常
     */
    public Result deleteRoleByIds(String ids);

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    /**
     * 修改保存角色信息
     *
     * @param adminRole 角色信息
     * @return 结果
     */
    public Object updateAdminRole(AdminRole adminRole);

    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 校验角色名称是否唯一
     *
     * @param roleName 角色信息
     * @param id 需要排除的角色id
     * @return 结果
     */
    public boolean checkRoleNameUnique(String roleName,Long id);

    /**
     * 校验角色权限是否唯一
     *
     * @param roleKey 角色信息
     * @param id 需要排除的角色id
     * @return 结果
     */
    public boolean checkRoleKeyUnique(String roleKey,Long id);

    /**
     * 通过角色ID查询角色
     *
     * @param id 角色ID
     * @return 角色对象信息
     */
    public AdminRole findById(Long id);

    /**
     * 根据条件分页查询角色数据
     * 
     * @param role 角色信息
     * @return 角色数据集合信息
     */
    public Object selectRoleListPager(AdminRole role, Integer page, Integer limit, String sort, String order) ;

    /**
     * 根据用户ID查询角色
     *
     * @param userId 用户ID
     * @return 角色id列表
     */
    public String selectUserRolesByUserId(Long userId);

    /**
     * 根据用户ID查询角色
     * 
     * @param userId 用户ID
     * @return 权限列表
     */
    public Set<String> selectRoleKeys(Long userId);

    /**
     * 根据用户ID查询角色
     * 
     * @param userId 用户ID
     * @return 角色列表
     */
    //public List<Role> selectRolesByUserId(Long userId);

    /**
     * 查询所有角色
     * 
     * @return 角色列表
     */
    public List<AdminRole> selectRoleAll();

    /**
     * 通过角色ID查询角色使用数量
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    public int countUserRoleByRoleId(Long roleId);

    /**
     * 角色状态修改
     * 
     * @param adminRole 角色信息
     * @return 结果
     */
    public int changeStatus(AdminRole adminRole);

}
