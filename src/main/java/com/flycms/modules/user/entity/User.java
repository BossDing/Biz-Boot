package com.flycms.modules.user.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class User implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;		        //用户id
	private String userName;	    //用户名
	private String email;		    //邮箱
	private String password;	    //加密后的密码
	private String avatar;		//头像,图片地址，如 /avatar/29.jpg
	private String nickname;	    //昵称
    private String sex;		    //性别，三个值：男、女、未知
	private String lastip;		//最后一次登陆的ip
	private String userMobile;		    //手机号
	private Integer currency;	    //资金，可以是积分、金币、等等站内虚拟货币。具体货币名字在system表，name=CURRENCY_NAME 通过后台－系统设置进行配置
	private float money;		    //账户可用余额，金钱,RMB，单位：元
	private float freezemoney;	//账户冻结余额，金钱,RMB，单位：元
	private Short isfreeze;	    //是否已冻结，1已冻结（拉入黑名单），0正常
	private Short idcardauth; 	//是否已经经过真实身份认证了（身份证、银行卡绑定等）。默认为没有认证。预留字段
	private String sign;		    //用户签名,限制50个汉字或100个英文字符
    private String account;       //用户自我描述，限制255个字以内
	private Date createTime;	        //注册时间,时间戳
	private Date lastTime;	    //最后登录时间,时间戳
	private Integer status;       // 0 可用, 1 禁用, 2 注销
	private Integer deleted;      // 0 可用, 1 删除

}