package com.flycms.modules.user.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class UserInvite implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;

    private Long toUserId;

    private Long fromUserId;

    private Integer status;

    private Date createTime;
}