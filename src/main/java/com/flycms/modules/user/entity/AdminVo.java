package com.flycms.modules.user.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 前台编辑信息等展示实体类
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:52 2019/8/30
 */

@Setter
@Getter
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class AdminVo implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;		//用户id
	private String userName;	//用户名
	private String email;		//邮箱
	private String avatar;		//头像,图片地址，如 /avatar/29.jpg
	private String nickname;	//昵称
	private String phone;		//手机号
	private String sign;		//用户签名,限制50个汉字或100个英文字符
	private String sex;		//性别，三个值：男、女、未知
	private String account;  //用户自我描述，限制255个字以内
	private Date createTime;	//注册时间,时间戳
	private Integer status;   // 0 可用, 1 禁用, 2 注销
}