package com.flycms.modules.user.entity;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 用户和角色关联 fly_admin_role_merge
 * 
 * @author 孙开飞
 */
@Setter
@Getter
public class AdminRoleMerge implements Serializable {
    private static final long serialVersionUID = 1L;

    private  Long id;
    /** 用户ID */
    private Long userId;
    
    /** 角色ID */
    private Long roleId;

}
