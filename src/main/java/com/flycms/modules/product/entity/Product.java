package com.flycms.modules.product.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 13:30 2019/8/14
 */
@Getter
@Setter
public class Product implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Long siteId;
    private Long columnId;
    private String title;
    private String titlepic;
    private String content;
    private String keywords;
    private String description;
    private Date createTime;
    private Date updateTime;
    private Integer status;
    private Integer deleted;
}
