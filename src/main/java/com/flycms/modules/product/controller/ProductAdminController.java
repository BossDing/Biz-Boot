package com.flycms.modules.product.controller;

import com.flycms.modules.product.service.ProductService;
import com.flycms.common.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 17:04 2019/8/14
 */
@Controller
@RequestMapping("/admin/product/")
public class ProductAdminController extends BaseController {
    @Autowired
    private ProductService productService;

    /**
     * 产品管理列表
     */
    @RequestMapping("/list{url.suffix}")
    public String productlist(HttpServletRequest request , Model model){
        return theme.getAdminTemplate("product/list");
    }
}
