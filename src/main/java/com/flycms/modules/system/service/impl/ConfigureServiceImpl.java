package com.flycms.modules.system.service.impl;

import com.flycms.common.pager.Pager;
import com.flycms.common.utils.mark.SnowFlakeUtils;
import com.flycms.common.utils.result.LayResult;
import com.flycms.common.utils.result.Result;
import com.flycms.common.utils.text.Convert;
import com.flycms.modules.system.dao.ConfigureDao;
import com.flycms.modules.system.entity.Configure;
import com.flycms.modules.system.service.ConfigureService;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 *  网站系统基础 服务层
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 14:14 2018/7/8
 */
@Service
public class ConfigureServiceImpl implements ConfigureService {
    @Autowired
    private ConfigureDao systemDao;

    @Autowired
    private RedisTemplate redisTemplate;

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    /**
     * 添加系统配置信息
     *
     * @param system
     * @return
     */
    @Transactional
    public Result addSystem(Configure system){
        if(this.checkSystemByKeycode(system.getKeyCode(),null)){
            return Result.failure("该系统配置已存在！");
        }
        system.setId(SnowFlakeUtils.nextId());
        system.setCreateTime(new Date());
        systemDao.save(system);
        return Result.success();
    }
    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    /**
     * 批量删除配置信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public Result deleteByIds(String ids){
        Long[] roleIds = Convert.toLongArray(ids);
        int totalCount=systemDao.deleteByIds(roleIds);
        if(totalCount == 0){
            Result.failure("删除失败");
        }
        return Result.success();
    }
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////

    /**
     * 按id修改系统配置信息
     *
     * @param system 系统配置信息
     * @return 结果
     */
    public Object updateSystem(Configure system)
    {
        if(StringUtils.isEmpty(system.getId())){
            return Result.failure("系统配置id不能为空");
        }
        if(this.checkSystemByKeycode(system.getKeyCode(),system.getId())){
            return Result.failure("该系统配置Key已存在");
        }
        // 修改角色信息
        systemDao.updateSystem(system);
        return Result.success();
    }

    /**
     * 按keyCode修改系统配置信息
     *
     * @param keyCode
     * @param keyValue
     * @return 结果
     */
    public Object updateKeyValue(String keyCode, String keyValue)
    {
        if(StringUtils.isEmpty(keyCode)){
            return Result.failure("系统配置Key不能为空");
        }
        // 修改角色信息
        systemDao.updateKeyValue(keyCode, keyValue);
        return Result.success();
    }
    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 查询系统变量keyCode是否有重复
     *
     * @param keyCode
     * @param id 需要排除id
     * @return
     */
    public boolean checkSystemByKeycode(String keyCode, Long id) {
        int totalCount = systemDao.checkSystemByKeycode(keyCode,id);
        return totalCount > 0 ? true : false;
    }

    /**
     * 按id查询配置信息
     *
     * @param id
     * @return
     */
    @Cacheable(value="system",key="'system_'+#id")
    public Configure findById(Long id){
        Configure system=systemDao.findById(id);
        return system;
    }

    /**
     * 按系统变量key查询对应的value
     *
     * @param keyCode
     * @return
     */
/*    @Cacheable(value="system",key="'key_'+#keyCode")
    public String findByKeyCode(String keyCode){
        String key="system::key_"+keyCode;
        ValueOperations<String, String> operations = redisTemplate.opsForValue();
        boolean hasKey = redisTemplate.hasKey(key);
        if (hasKey) {
            java.lang.System.out.println(key+"================="+ operations.get(key));
            return operations.get(key);
        }
        System system=systemDao.findByKeyCode(keyCode);
        // 写入缓存
        operations.set(key, system.getKeyValue(), -1, TimeUnit.HOURS);
        if(system != null){
            return system.getKeyValue();
        }
        return null;
    }*/

    @Cacheable(value="system",key="'key_'+#keyCode")
    public String findByKeyCode(String keyCode){
        Configure system=systemDao.findByKeyCode(keyCode);
        if(system != null){
            return system.getKeyValue();
        }
        return null;
    }

    public Object selectSystemListLayPager(Configure system, Integer page, Integer pageSize, String sort, String order) {
        //javabean 映射工具
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        MapperFacade mapper = mapperFactory.getMapperFacade();

        StringBuffer whereStr = new StringBuffer(" 1 = 1");
        if (!StringUtils.isEmpty(system.getKeyCode())) {
            whereStr.append(" and key_code = #{entity.keyCode}");
        }
        if (!StringUtils.isEmpty(system.getCreateTime())) {
            whereStr.append(" AND date_format(add_time,'%y%m%d') &lt;= date_format(#{entity.addTime},'%y%m%d')");
        }
        Pager<Configure> pager = new Pager(page, pageSize);
        //排序设置
        if (!StringUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        //查询条件
        Configure entity = new Configure();
        entity.setKeyCode(system.getKeyCode());
        entity.setCreateTime(system.getCreateTime());
        pager.setEntity(entity);
        pager.setWhereStr(whereStr.toString());
        return LayResult.success(0, "true", systemDao.queryTotal(pager), systemDao.queryList(pager));
    }
}

