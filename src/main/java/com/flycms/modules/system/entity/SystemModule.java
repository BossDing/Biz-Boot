package com.flycms.modules.system.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:18 2019/8/17
 */
@Setter
@Getter
public class SystemModule implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String moduleName;
    private String moduleType;
    private String remark;
    private String status;
    private String sortOrder;
    private Date addTime;	//模块添加时间
    private Date updateTime;	//最后修改时间
    private Integer deleted;
}
