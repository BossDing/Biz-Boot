package com.flycms.modules.system.controller;

import com.flycms.common.controller.BaseController;
import com.flycms.modules.shiro.ShiroUtils;
import com.flycms.modules.user.entity.UserMenu;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:50 2019/8/19
 */
@Controller
public class IndexMemberController extends BaseController {
    //企业会员首页
    @GetMapping("/member/index{url.suffix}")
    public String siteList(Model model)
    {
        // 根据用户id取出菜单
        List<UserMenu> menus = userMenuService.selectMenusByUser(ShiroUtils.getLoginUser().getId());
        model.addAttribute("menus", menus);
        model.addAttribute("user", getUser());
        return "system/member/index";
    }

    /**
     * 登陆成功之后进入的页面，欢迎页面
     */
    @RequestMapping("/member/welcome${url.suffix}")
    public String welcome(HttpServletRequest request, Model model){
        return "system/member/welcome";
    }
}
