package com.flycms.modules.notify.service.impl;

import com.flycms.common.pager.Pager;
import com.flycms.common.utils.mark.SnowFlakeUtils;
import com.flycms.common.utils.result.LayResult;
import com.flycms.common.utils.result.Result;
import com.flycms.modules.notify.dao.MessageDao;
import com.flycms.modules.notify.entity.Message;
import com.flycms.modules.notify.entity.MessageVO;
import com.flycms.modules.notify.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: 站内短信发布
 * @email 79678111@qq.com
 * @Date: 16:28 2019/11/18
 */
@Service
public class MessageServiceImpl implements MessageService {
    @Autowired
    private MessageDao messageDao;

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    @Transactional
    public Object addMessage(Message nessage) {
        nessage.setId(SnowFlakeUtils.nextId());
        nessage.setAddTime(new Date());
        messageDao.save(nessage);
        return Result.success();
    }

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////


    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    /**
     * 站内信息分页
     *
     * @param message
     * @param page
     * @param pageSize
     * @param sort
     * @param order
     * @return
     */
    public Object selectMessageListPager(Message message, Integer page, Integer pageSize, String sort, String order) {
        StringBuffer whereStr = new StringBuffer(" 1 = 1");
        if (!StringUtils.isEmpty(message.getTitle())) {
            whereStr.append(" and title like concat('%',#{entity.title},'%')");
        }
        if (!StringUtils.isEmpty(message.getSenderId())) {
            whereStr.append(" and sender_id = #{entity.senderId}");
        }
        if (!StringUtils.isEmpty(message.getRecipientId())) {
            whereStr.append(" and recipient_id = #{entity.recipientId}");
        }
        whereStr.append(" and deleted = 0");
        Pager<Message> pager = new Pager(page, pageSize);
        //排序设置
        if (!StringUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        //查询条件
        Message entity = new Message();
        entity.setTitle(message.getTitle());
        entity.setSenderId(message.getSenderId());
        entity.setRecipientId(message.getRecipientId());
        pager.setEntity(entity);
        pager.setWhereStr(whereStr.toString());
        List<Message> sitelsit = messageDao.queryList(pager);
        List<MessageVO> volsit = new ArrayList<MessageVO>();
        sitelsit.forEach(bean -> {
            MessageVO vo=new MessageVO();
            vo.setId(bean.getId().toString());
            vo.setTitle(bean.getTitle());
            vo.setAddTime(bean.getAddTime());
            volsit.add(vo);
        });
        return LayResult.success(0, "true", messageDao.queryTotal(pager), volsit);
    }
}
