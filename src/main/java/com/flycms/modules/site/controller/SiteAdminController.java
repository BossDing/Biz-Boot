package com.flycms.modules.site.controller;


import com.flycms.common.controller.BaseController;
import com.flycms.common.utils.result.Result;
import com.flycms.common.validator.Sort;
import com.flycms.modules.site.entity.Site;
import com.flycms.modules.site.service.SiteService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 公共的
 * @author 孙开飞
 */
@Controller
@RequestMapping("/admin/site")
public class SiteAdminController extends BaseController {


    @GetMapping("/list{url.suffix}")
    public String siteList()
    {
        return "system/admin/site/list";
    }

    @GetMapping("/listData{url.suffix}")
    @ResponseBody
    public Object siteList(Site site,
                           @RequestParam(value = "page",defaultValue = "1") Integer page,
                           @RequestParam(value = "pageSize",defaultValue = "10") Integer pageSize,
                           @Sort(accepts = {"add_time", "id"}) @RequestParam(defaultValue = "add_time") String sort,
                           @RequestParam(defaultValue = "desc") String order) {
        if(SecurityUtils.getSubject().hasAllRoles(Arrays.asList("admin"))){
            return siteService.selectSiteListPager(site, page, pageSize, sort, order);
        }else if(SecurityUtils.getSubject().hasAllRoles(Arrays.asList("agency"))){
            return "system/agency/welcome";
        }else if(SecurityUtils.getSubject().hasAllRoles(Arrays.asList("company"))){
            return siteService.selectCompanySiteListPager(site, page, pageSize, sort, order);
        }
        return Result.failure("请求错误！");
    }
}