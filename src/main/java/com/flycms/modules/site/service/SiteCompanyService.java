package com.flycms.modules.site.service;

import com.flycms.modules.site.entity.SiteCompany;

import java.util.List;
import java.util.Set;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 网站和用户相关联操作 服务层
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 2:08 2019/8/27
 */
public interface SiteCompanyService {

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    public Object addSiteCompany(SiteCompany siteCompany);


    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 按网站id删除网站与企业关联信息
     *
     * @param siteId
     *         网站id
     * @return
     */
    public int deleteSiteCompanyById(Long[] siteId);
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    public boolean checkSiteCompany(Long siteId);

    /**
     * 企业id查询当前拥有的网站数量
     *
     * @param companyId
     * @return
     */
    public Set<Long> querySiteIdList(Long companyId);
}
