package com.flycms.modules.site.service;

import com.flycms.common.utils.result.Result;
import com.flycms.modules.site.entity.Site;
import com.flycms.modules.site.entity.UserSiteVO;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;


/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 站点相关
 * @author 孙开飞
 */
public interface SiteService {
    /**
     * 添加模块信息
     *
     * @param site
     * @return
     */
    public Object addSite(Site site);

    /**
     * 用户添加网站信息
     *
     * @param site
     * @return
     */
    public Object addUserSite(Site site);

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 批量删除角色信息
     *
     * @param ids 需要删除的数据ID
     * @throws Exception
     */
    public Result deleteSiteByIds(String ids);
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    /**
     * 后台更新默认网站信息
     *
     * @param site
     * @return
     */
    public Object updateAdminSiteById(Site site);

    /**
     * 修改网站基本信息
     *
     * @param site
     * @return
     */
    public Object updateSiteById(Site site);

    /**
     * 更新网站开关状态
     *
     * @param status
     * @param id
     * @return
     */
    public Object updateSiteByStatus(Boolean status,Long id);

    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 查询短域名是否被占用
     *
     * @param domain
     * @return
     */
    public boolean checkSiteByDomain(String domain,Long siteId);

    /**
     * 昵称检查是否已保护
     *
     * @param domain
     * @return
     */
    public boolean holdSiteByDomain(String domain);
    /**
     * 按id查询网站信息
     *
     * @param id
     * @return
     */
    public Site findById(Long id);

    /**
     * 按网站Domain查询网站信息
     *
     * @param domain
     * @return
     */
    public Site findByDomain(String domain);

    /**
     * 短域名查询网站id
     *
     * @param domain
     * @return
     */
    public Long findSiteByDomain(String domain);

    public String findByKeyCode(String keyCode);
    /**
     * 按网站id和用户id查询网站信息
     *
     * @param siteId
     *         网站id
     * @param companyId
     *         用户id
     * @return
     */
    public Site findSiteByCompanyId(Long siteId,Long companyId);

    /**
     * 管理员查看所有网站翻页列表
     *
     * @param site
     *         根据分类名查询
     * @param page
     * @param pageSize
     * @param sort
     * @param order
     * @return
     */
    public Object selectSiteListPager(Site site, Integer page, Integer pageSize, String sort, String order);

    /**
     * 企业用户所有网站翻页列表
     *
     * @param site
     *         根据分类名查询
     * @param page
     * @param pageSize
     * @param sort
     * @param order
     * @return
     */
    public Object selectCompanySiteListPager(Site site, Integer page, Integer pageSize, String sort, String order);

    /**
     * 查询用户拥有的所有网站数量
     *
     * @return
     */
    public int selectCompanySiteTotal();
    /**
     * 查询用户所有网站列表
     *
     * @return
     */
    public List<UserSiteVO> selectCompanySiteList();
}
