package com.flycms.modules.site.dao;

import com.flycms.common.dao.BaseDao;
import com.flycms.common.pager.Pager;
import com.flycms.modules.site.entity.Content;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:08 2019/6/9
 */
@Repository
public interface ContentDao extends BaseDao<Content> {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 按用户id查询所有的所有管理的网站
     *
     * @param pager
     * @return
     */
    public int queryContentTotal(Pager pager);

    /**
     * 按用户id查询所有的所有管理的网站
     *
     * @param pager
     * @return
     */
    public List<Content> queryContentList(Pager pager);
}
