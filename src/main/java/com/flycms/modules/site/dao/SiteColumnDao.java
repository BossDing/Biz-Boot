package com.flycms.modules.site.dao;

import com.flycms.common.dao.BaseDao;
import com.flycms.modules.site.entity.SiteColumn;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:08 2019/6/9
 */
@Repository
public interface SiteColumnDao extends BaseDao<SiteColumn> {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    /**
     * 查询网站id和相同父级下是否有重复分类名称
     *
     * @param siteId
     *         网站id
     * @param parentId
     *         父级id
     * @param columnTitle
     *         分类名称
     * @return
     */
    public int checkSiteColumnByTitle(@Param("id") Long id,@Param("siteId") Long siteId, @Param("parentId") Long parentId,@Param("columnTitle") String columnTitle);

    /**
     * 按分类id加网站id查询分类信息
     *
     * @param id
     *         分类id
     * @param siteId
     *         网站id
     * @return
     */
    public SiteColumn findByIdAndSiteId(@Param("id") Long id,@Param("siteId") Long siteId);

    /**
     * 按网站id查询所有分类信息
     *
     * @param siteId
     * @return
     */
    public List<SiteColumn> findBySiteId(@Param("siteId") Long siteId);
}
