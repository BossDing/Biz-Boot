package com.flycms.modules.site.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 网站栏目
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:18 2019/8/17
 */
@Setter
@Getter
public class SiteColumn implements Serializable {
	private static final long serialVersionUID = 1L;
	// Fields
	private Long id;
	private Long parentId;
	private Long siteId;
	//对应模型表fly_system_module里的id，每个id对应一个模型
	private Long channel;
	private String columnTitle;
	private String columnCode;
	private String columnUrl;
	private Integer newWindow;
	private String columnIcon;	//本栏目的图片、图标，可在模版中使用{siteColumn.icon}进行调用此图以显示
	private Integer columnHidden;	//栏目间的排序
	private Integer columnType;      //栏目属性，1频道封面（频道首页）模板，2列表模板，3内容模板，4单独页面模板
	private Long tempindex;
	private Long templist;
	private Long tempcontent;
	private Long tempalone;
	private String title;
	private String keywords;
	private String description;
	private String content;
	private Integer sortOrder;
	private Date createTime;
	private Date updateTime;
	private Integer status;
	private Integer deleted;
}