package com.flycms.modules.site.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 用户后台查询拥有的网站实体类
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 15:23 2019/8/23
 */
@Setter
@Getter
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class UserSiteVO implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;                    //网站id
    private String siteName;           //网站名称
    private String domain;             //网站二级域名
    private String bindDomain;	     //用户自己绑定的域名
    private Date createTime;              //添加网站时间
    private Date expireTime;          //网站运营到期时间
    private Boolean status;      	     //站点开关，1正常；2冻结
    private Integer verify;        //管理员审核状态：0未审核，1已审核，2审核未通过，3已过期
}
