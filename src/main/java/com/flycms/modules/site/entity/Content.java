package com.flycms.modules.site.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 23:55 2019/10/17
 */
@Setter
@Getter
public class Content implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private int mold;    //内容类型
    private Long siteId;
    private Long columnId;
    private String title;
    private String moduleType;
    private Date createTime;             //添加网站时间
}
