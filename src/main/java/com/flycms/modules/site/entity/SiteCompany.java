package com.flycms.modules.site.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * site entitiy
 * @author 孙开飞
 */
@Setter
@Getter
public class SiteCompany implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Long companyId;
    private Long siteId;
}
