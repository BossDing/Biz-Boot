package com.flycms.modules.company.service.impl;

import com.flycms.common.pager.Pager;
import com.flycms.common.utils.mark.SnowFlakeUtils;
import com.flycms.common.utils.result.Result;
import com.flycms.modules.company.dao.CompanyDao;
import com.flycms.modules.company.entity.Company;
import com.flycms.modules.company.entity.CompanyUser;
import com.flycms.modules.company.service.CompanyService;
import com.flycms.modules.shiro.ShiroUtils;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:53 2019/8/17
 */
@Service
public class CompanyServiceImpl implements CompanyService {
    @Autowired
    private CompanyDao companyDao;

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    /**
     * 用户添加企业信息或者编辑企业信息
     * 当系统使用登录用户查询是否已关联企业基本信息，没关联的就将体检的企业基本信息做添加处理
     * 如果关联了就做修改处理
     *
     * @param company
     * @return
     */
    @Transactional
    public Object addAndModifyCompany(Company company) {
        Company checkCompany=this.findCompanyByUser(ShiroUtils.getLoginUser().getId());
        //只要未检测当前用户关联过企业信息，那么提交的信息做增加处理
        if(checkCompany==null){
            if(this.checkCompanyByFullName(company.getFullName(),null)){
                return Result.failure("该公司已存在，如果不是贵公司人员拥有请联系管理员");
            }
            if(this.checkCompanyByShortName(company.getShortName(),null)){
                return Result.failure("贵公司简称已被占用，请选择其他名称");
            }
            company.setId(SnowFlakeUtils.nextId());
            company.setCreateTime(new Date());
            int total = companyDao.save(company);
            if(total > 0){
                //关联企业与用户信息
                CompanyUser companyUser=new CompanyUser();
                companyUser.setId(SnowFlakeUtils.nextId());
                companyUser.setCompanyId(company.getId());
                companyUser.setUserId(ShiroUtils.getLoginUser().getId());
                companyUser.setUserType(1);//设为当前网站管理员
                companyDao.saveCompanyUser(companyUser);
                return Result.success("公司基本信息添加成功");
            }else{
                return Result.failure("公司基本信息添加失败");
            }
        }else{
            if(StringUtils.isEmpty(company.getId())){
                return Result.failure("公司id不能为空");
            }
            //理论上需要编辑的企业信息id和根据系统查询到的企业ID应该是一致的
            if(!checkCompany.getId().equals(company.getId())){
                return Result.failure("请勿非法提交参数");
            }
            if(this.checkCompanyByFullName(company.getFullName(),company.getId())){
                return Result.failure("该公司已存在，如果不是贵公司人员拥有请联系管理员");
            }
            if(this.checkCompanyByShortName(company.getShortName(),company.getId())){
                return Result.failure("贵公司简称已被占用，请选择其他名称");
            }
            company.setUpdateTime(new Date());
            int total = companyDao.update(company);
            if(total > 0){
                return Result.success("公司基本信息修改成功");
            }else{
                return Result.failure("公司基本信息修改失败");
            }
        }
    }

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 按id删除企业信息
     *
     * @param id
     * @return
     */
    public int deleteById(Long id){
        return companyDao.deleteById(id);
    }

    /**
     * 按企业id删除企业与用户关联信息
     *
     * @param companyId
     * @return
     */
    public int deleteCompanyUserByCompanyId(Long companyId){
        return companyDao.deleteCompanyUserByCompanyId(companyId);
    }

    /**
     * 按用户id删除企业与用户关联信息
     *
     * @param userId
     * @return
     */
    public int deleteCompanyUserByUserId(Long userId){
        return companyDao.deleteCompanyUserByUserId(userId);
    }
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 查询企业全称是否已存在
     *
     * @param fullName
     *         公司简称
     * @param id
     *         需要排除的id
     * @return
     */
    public boolean checkCompanyByFullName(String fullName,Long id) {
        int totalCount = companyDao.checkCompanyByFullName(fullName,id);
        return totalCount > 0 ? true : false;
    }

    /**
     * 查询企业简称是否已存在
     *
     * @param shortName
     *         公司简称
     * @param id
     *         需要排除的id
     * @return
     */
    public boolean checkCompanyByShortName(String shortName,Long id) {
        int totalCount = companyDao.checkCompanyByShortName(shortName,id);
        return totalCount > 0 ? true : false;
    }

    /**
     * 查询当前用户和企业是否已关联
     *
     * @param companyId
     * @param userId
     * @return
     */
    public boolean checkCompanyByUser(Long companyId,Long userId) {
        int totalCount = companyDao.checkCompanyByUser(companyId,userId);
        return totalCount > 0 ? true : false;
    }

    /**
     * 按用户id查询企业信息
     *
     * @param companyId
     * @return
     */
    public Company findById(Long companyId){
        return companyDao.findById(companyId);
    }

    /**
     * 按用户id查询企业信息
     *
     * @param userId
     * @return
     */
    public Company findCompanyByUser(Long userId){
        return companyDao.findCompanyByUser(userId);
    }

    /**
     * 查询网站所属帮助信息
     *
     * @param industry
     *         行业id
     * @param fullName
     *         公司全称
     * @param page
     *         当前页数
     * @param pageSize
     *         每页显示数量
     * @param sort
     *         指定排序字段
     * @param order
     *         排序方式
     * @return
     */
    public Object queryCompanyPager(Long industry, String fullName, Integer page, Integer pageSize, String sort, String order) {
        StringBuffer whereStr = new StringBuffer(" 1 = 1");
        if (!StringUtils.isEmpty(industry) && industry > 0l) {
            whereStr.append(" and industry = #{entity.industry}");
        }
        if (!StringUtils.isEmpty(fullName)) {
            whereStr.append(" and full_name like concat('%',#{entity.fullName},'%')");
        }
        whereStr.append(" and deleted = 1");

        Pager<Company> pager = new Pager(page, pageSize);
        //排序设置
        if (!StringUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        //查询条件
        Company company = new Company();
        company.setIndustry(industry);
        company.setFullName(fullName);
        pager.setEntity(company);
        pager.setWhereStr(whereStr.toString());
        pager.setList(companyDao.queryList(pager));
        pager.setTotal(companyDao.queryTotal(pager));
        return pager;
    }
}
