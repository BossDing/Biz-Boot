package com.flycms.modules.company.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: 企业与用户关联实体类
 * @email 79678111@qq.com
 * @Date: 15:41 2019/11/5
 */
@Setter
@Getter
public class CompanyUser implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Long userId;
    private Long companyId;
    private Integer userType;
}
