package com.flycms.modules.template.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: 网站关联模板实体类
 * @email 79678111@qq.com
 * @Date: 20:34 2019/9/30
 */
@Setter
@Getter
public class SiteTemplateMerge implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;			    //编号
    private Long siteId;		    //所属网站的id
    private Long templateId;		//所属模板id
    private String templateName;  //模板名称
    private String templateCatalog;  //购买模板时模板存放路径
    private Date addTime;         //添加时间
    private Date expireTime;     //到期时间
}
