package com.flycms.modules.template.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 模版。
 * 只要是网站中使用过的模版，都会进入此处进行记录。无论是云端模版、用户自己上传的模版、还是私有模版
 * @author 孙开飞
 */
@Setter
@Getter
public class Template implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;			//自动编号
	private String templateName;			//模版的名字，编码，唯一，限制50个字符以内
	private String templateDirectory;  //模板存放目录名，如：
	private Long userId;		//此模版所属的用户，user.id。如果此模版是用户的私有模版，也就是 iscommon=0 时，这里存储导入此模版的用户的id
	private String remark;		//模版的简介，备注说明，限制200字以内
	private String previewUrl;	//模版预览网址，示例网站网址，绝对路径，
	private Long templateType;			//模版所属分类，如广告、科技、生物、医疗等
	private String companyName;	//模版开发者公司名字。如果没有公司，则填写个人姓名。限制50字符以内
	private String userName;	//模版开发人员的名字，姓名，限制10个字符以内
	private String siteUrl;		//模版开发者官方网站、企业官网。如果是企业，这里是企业官网的网址，格式如： http://www.leimingyun.com  ，如果是个人，则填写个人网站即可
	private Short terminalMobile;	//网站模版是否支持手机端, 1支持，0不支持
	private Short terminalPc;		//网站模版是否支持PC端, 1支持，0不支持
	private Short terminalIpad;		//网站模版是否支持平板电脑, 1支持，0不支持
	private String previewPic;	//模版预览图的网址，preview.jpg 图片的网址
	private BigDecimal price;		    //模板售价
	private BigDecimal discount;      //销售折扣
	private int sortOrder;			//公共模版的排序，数字越小越靠前。
	private Date addTime;		//模版添加时间
	private Date updateTime;		//模版添加时间
	private int status;
}