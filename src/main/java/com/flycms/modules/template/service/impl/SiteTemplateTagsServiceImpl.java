package com.flycms.modules.template.service.impl;

import com.flycms.common.pager.Pager;
import com.flycms.common.utils.RedisUtils;
import com.flycms.common.utils.mark.SnowFlakeUtils;
import com.flycms.common.utils.result.Result;
import com.flycms.modules.template.dao.SiteTemplateTagsDao;
import com.flycms.modules.template.entity.SiteTemplateTags;
import com.flycms.modules.template.service.SiteTemplateTagsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: 用户自定义标签服务
 * @email 79678111@qq.com
 * @Date: 14:35 2019/11/21
 */
@Service
@EnableCaching
public class SiteTemplateTagsServiceImpl implements SiteTemplateTagsService {
    private static final Logger log = LoggerFactory.getLogger(SiteTemplateTagsServiceImpl.class);

    @Autowired
    private SiteTemplateTagsDao siteTemplasteTagsDao;

    @Autowired
    RedisUtils redisUtils;	//注入工具类
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    /**
     * 添加用户自定义模板标签
     *
     * @param siteTemplateTags
     * @return
     */
    @Transactional
    public Result addSiteTemplateTags(SiteTemplateTags siteTemplateTags){
        if(this.checTemplateTagsByKeycode(siteTemplateTags.getSiteId(),siteTemplateTags.getTemplateId(),siteTemplateTags.getTagKey(),null)){
            return Result.failure("该模板标签已存在！");
        }
        siteTemplateTags.setId(SnowFlakeUtils.nextId());
        siteTemplateTags.setAddTime(new Date());
        siteTemplasteTagsDao.save(siteTemplateTags);
        return Result.success();
    }

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    /**
     * 按id删除用户自定义标签
     *
     * @param id
     * @return
     */
    public int deleteById(Long id){
        return siteTemplasteTagsDao.deleteById(id);
    }
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 查询网站id和类别查询模板标签名称是否重复
     *
     * @param siteId
     *         网站id
     * @param templateId
     *         模板id
     * @param tagKey
     *         用户自定义模板名称
     * @param id
     *         需要排除id
     * @return
     */
    public boolean checTemplateTagsByKeycode(Long siteId,Long templateId,String tagKey,Long id) {
        int totalCount = siteTemplasteTagsDao.checkTemplateTags(siteId,templateId,tagKey,id);
        return totalCount > 0 ? true : false;
    }

    /**
     * 按网站当前网站id和当前使用模板id查询相关所有自定义标签
     *
     * @param siteId
     * @param templateId
     * @return
     */
    @Cacheable(value="templateTags",key = "#siteId + '_' + #templateId")
    public List<SiteTemplateTags> selectSiteTemplateTagsList(Long siteId,Long templateId) {
        StringBuffer whereStr = new StringBuffer(" 1 = 1");
        if (!StringUtils.isEmpty(siteId)) {
            whereStr.append(" and site_id  = #{entity.siteId}");
        }
        if (!StringUtils.isEmpty(templateId)) {
            whereStr.append(" and template_id = #{entity.templateId}");
        }
        Pager<SiteTemplateTags> pager = new Pager();
        //排序设置
        pager.addOrderProperty("id", true,true);
        //使用limit进行查询翻页
        pager.addLimitProperty(false);
        //查询条件
        SiteTemplateTags entity = new SiteTemplateTags();
        entity.setSiteId(siteId);
        entity.setTemplateId(templateId);
        pager.setEntity(entity);
        pager.setWhereStr(whereStr.toString());
        return siteTemplasteTagsDao.queryList(pager);
    }
}
