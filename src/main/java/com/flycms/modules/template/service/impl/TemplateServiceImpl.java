package com.flycms.modules.template.service.impl;

import com.flycms.common.pager.Pager;
import com.flycms.common.utils.mark.SnowFlakeUtils;
import com.flycms.common.utils.result.Result;
import com.flycms.modules.shiro.ShiroUtils;
import com.flycms.modules.template.dao.TemplateDao;
import com.flycms.modules.template.dao.TemplateSkuDao;
import com.flycms.modules.template.entity.Template;
import com.flycms.modules.template.entity.TemplateSku;
import com.flycms.modules.template.entity.TemplateVO;
import com.flycms.modules.template.service.TemplateService;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 模板工具类 服务层
 * 
 * @author 孙开飞
 * 
 */
@Service
public class TemplateServiceImpl implements TemplateService {
	private static final Logger log = LoggerFactory.getLogger(TemplateServiceImpl.class);

	@Autowired
	private TemplateDao templateDao;

	@Autowired
	private TemplateSkuDao templateSkuDao;

	// ///////////////////////////////
	// /////       增加       ////////
	// ///////////////////////////////
	/**
	 * 开发人员添加模板信息
	 *
	 * @param template
	 * @return
	 */
	@Transactional
	public Object addDeveloperTemplate(Template template){
		if(this.checkTemplateByName(template.getTemplateName(),null)){
			return Result.failure("该同类型模板页面名称已存在！");
		}
		template.setId(SnowFlakeUtils.nextId());
		template.setUserId(ShiroUtils.getLoginUser().getId());
		template.setAddTime(new Date());
		templateDao.save(template);
		return Result.success();
	}
	// ///////////////////////////////
	// /////        刪除      ////////
	// ///////////////////////////////

	/**
	 * 按id删除模板主表信息
	 *
	 * @param id
	 *         模板id
	 * @return
	 */
	public int deleteById(Long id){
		return templateDao.deleteById(id);
	}

	// ///////////////////////////////
	// /////        修改      ////////
	// ///////////////////////////////

	/**
	 * 修改模板主表信息
	 *
	 * @param template
	 * @return
	 */
	@Transactional
	public Object updateDeveloperTemplatePage(Template template){
		if(this.checkTemplateByName(template.getTemplateName(),template.getId())){
			return Result.failure("该模板名称已存在！");
		}
		template.setUpdateTime(new Date());
		template.setStatus(1);//默认可使用状态
		templateDao.update(template);
		return Result.success();
	}

	// ///////////////////////////////
	// /////        查詢      ////////
	// ///////////////////////////////
	/**
	 * 排除当前模板id后查询模板名是否存在,如果id设置为null则查全部的模板名
	 *
	 * @param templateName
	 *         模板名称
	 * @param id
	 *         需要排除的id,可设置为null
	 * @return
	 */
	public boolean checkTemplateByName(String templateName,Long id) {
		int totalCount = templateDao.checkTemplateByName(templateName,id);
		return totalCount > 0 ? true : false;
	}

	/**
	 * 得到当前请求需要渲染的模板相对路径
	 * 
	 * @param template
	 * @return
	 */
	public String getTemplatePath(String template) {
		return template;
	}
	
	/**
	 * 模板物理地址是否存在
	 * 
	 * @param theme
	 * @return
	 */
	public Boolean isExist(String theme) {
		String themePath = "views/templates/"+ theme + ".html";
		//logger.info("模板当前路径：" + themePath);
		File file = new File(themePath);
		if (file.exists()) {
			return true;
		} else {
			log.error("模板不存在：" + file.getAbsolutePath());
			return false;
		}
	}
	
	/**
	 * 得到通用模板
	 * 
	 * @param template
	 * @return
	 * @throws Exception
	 */
	public String getPcTemplate(String tpName,String template) {
		String themePath = "webTheme/"+tpName+"/"+template;
		if (this.isExist(themePath)) {
			return this.getTemplatePath(themePath);
		}
		log.error("模板文件不存在！！");
		return this.getTemplatePath("FileNotFound");
	}

	public String getAdminTemplate(String template) {
		String themePath = template;
		if (this.isExist(themePath)) {
			return this.getTemplatePath(themePath);
		}
		log.error("模板文件不存在！！");
		return this.getTemplatePath("FileNotFound");
	}

	/**
	 * 错误提示页面
	 *
	 * @param model
	 * @param message
	 * @return
	 */
	public String getException(Model model,String message) {
		model.addAttribute("message",message);
		return this.getTemplatePath("exception");
	}

	/**
	 * 按id查询模板信息
	 *
	 * @param id
	 * @return
	 */
    @Cacheable(key="'template_'+#id",value="template")
	public Template findById(Long id){
		return templateDao.findById(id);
	}

	/**
	 * 按模板id查询SKU信息
	 *
	 * @param templateId
	 * @return
	 */
	public List<TemplateSku> selectTemplateSku(Long templateId) {
		StringBuffer whereStr = new StringBuffer(" 1 = 1");
		whereStr.append(" and template_id = #{entity.templateId}");
		Pager<Template> pager = new Pager();
		//排序设置
		pager.addOrderProperty("use_time", true,true);
		//使用limit进行查询翻页
		pager.addLimitProperty(false);
		//查询条件
		TemplateSku entity = new TemplateSku();
		entity.setTemplateId(templateId);//当前用户id
		pager.setEntity(entity);
		pager.setWhereStr(whereStr.toString());
		return templateSkuDao.queryList(pager);
	}

	/**
	 * 查询用户所有发布的模板列表
	 *
	 * @return
	 */
	public List<TemplateVO> selectTemplateList() {
		//javabean 映射工具
		MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
		MapperFacade mapper = mapperFactory.getMapperFacade();

		StringBuffer whereStr = new StringBuffer(" 1 = 1");
		whereStr.append(" and deleted = 0");
		whereStr.append(" and user_id = #{entity.userId}");
		Pager<Template> pager = new Pager();
		//排序设置
		pager.addOrderProperty(null, false,false);
		//使用limit进行查询翻页
		pager.addLimitProperty(false);
		//查询条件
		Template entity = new Template();
		entity.setUserId(ShiroUtils.getLoginUser().getId());//当前用户id
		pager.setEntity(entity);
		pager.setWhereStr(whereStr.toString());
		List<Template> sitelsit = templateDao.queryList(pager);
		return mapper.mapAsList(sitelsit,TemplateVO.class);
	}

	/**
	 * 后台模板列表翻页
	 *
	 * @param template
	 * @param page
	 * @param pageSize
	 * @param sort
	 * @param order
	 * @return
	 */
	public Pager<Template> selectTemplatePager(Template template, Integer page, Integer pageSize, String sort, String order) {
		StringBuffer whereStr = new StringBuffer(" 1 = 1");
		if (!StringUtils.isEmpty(template.getTemplateName())) {
			whereStr.append(" and template_name like concat('%',#{entity.templateName},'%')");
		}
		whereStr.append(" and deleted = 0");
		if (!StringUtils.isEmpty(template.getUserId())) {
			whereStr.append(" and user_id = #{entity.userId}");
		}
		if (!StringUtils.isEmpty(template.getTemplateType())) {
			whereStr.append(" and template_type = #{entity.templateType}");
		}
		if (!StringUtils.isEmpty(template.getStatus())) {
			whereStr.append(" and status = #{entity.status}");
		}
		Pager<Template> pager = new Pager(page, pageSize);
		//排序设置
		if (!StringUtils.isEmpty(sort)) {
			Boolean rank = "desc".equals(order) ? true : false;
			pager.addOrderProperty(sort, rank,true);
		}
		//使用limit进行查询翻页
		pager.addLimitProperty(true);
		//查询条件
		Template entity = new Template();
		entity.setTemplateName(template.getTemplateName());
		entity.setUserId(template.getUserId());
		entity.setTemplateType(template.getTemplateType());
		entity.setStatus(template.getStatus());
		pager.setEntity(entity);
		pager.setWhereStr(whereStr.toString());
		pager.setList(templateDao.queryList(pager));
		pager.setTotal(templateDao.queryTotal(pager));
		return pager;
	}
}
