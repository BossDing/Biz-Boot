package com.flycms.modules.template.dao;

import com.flycms.common.dao.BaseDao;
import com.flycms.modules.template.entity.TemplatePage;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:08 2019/6/9
 */
@Repository
public interface TemplatePageDao extends BaseDao<TemplatePage> {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    /**
     * 查询添加模板名称是否存在
     *
     * @param templateId
     *         模板主表id
     * @param moduleId
     *         模型fly_system_module表id；对应的如新闻模块id，产品模块id
     * @param pageType
     *         模板类型，0网站首页，1频道封面（频道首页）模板，2列表模板，3内容模板，4单独页面模板
     * @param templateName
     *         模板名称
     * @return
     */
    public int checkTemplatePageByName(@Param("templateId") Long templateId,
                                       @Param("moduleId") Long moduleId,
                                       @Param("pageType") Integer pageType,
                                       @Param("templateName") String templateName);

}
