package com.flycms.common.exception;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 自定义异常
 *
 * @author 孙开飞
 */
public class ApiException extends RuntimeException {

  private static final long serialVersionUID = 1L;
  private int code;
  private String message;

  public ApiException(String message) {
    this.code = -1;
    this.message = message;
  }

  public ApiException(int code, String message) {
    this.code = code;
    this.message = message;
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  @Override
  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
