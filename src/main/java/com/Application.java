package com;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 运行入口
 * @author 孙开飞
 *
 */
@EnableCaching
@SpringBootApplication(scanBasePackages = {"com.flycms"})
@MapperScan(basePackages = {"com.flycms.modules.*.dao","com.flycms.dao"})
@ServletComponentScan
@EnableScheduling
@EnableTransactionManagement
@EnableAspectJAutoProxy(proxyTargetClass=true, exposeProxy=true)
public class Application {
    public static void main(String[] args) {
    	SpringApplication.run(Application.class, args);
    }
}
